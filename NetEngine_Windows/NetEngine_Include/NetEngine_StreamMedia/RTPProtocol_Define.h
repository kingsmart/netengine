#pragma once
/********************************************************************
//	Created:	2019/8/27   15:06
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_StreamMedia\StreamMedia_RtpProtocol\RTPProtocol_Define.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_StreamMedia\StreamMedia_RtpProtocol
//	File Base:	RTPProtocol_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	RTP协议处理模块头文件
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                       导出的枚举型
//////////////////////////////////////////////////////////////////////////
//负载RTP包数据类型
typedef enum en_StreamMedia_RTPProtocol_PayloadType
{
	ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOAD_TYPE_H264 = 96                   //负载类型为H264
}ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOADTYPE;
//NALU类型
typedef enum en_RTPProtocol_NaluType
{
	ENUM_RTPPROTOCOL_NALU_TYPE_RESERVED = 0,                              //保留
	ENUM_RTPPROTOCOL_NALU_TYPE_SINGLE_NALU = 1,                           //单个NAL单元包
	ENUM_RTPPROTOCOL_NALU_TYPE_AREA_A = 2,                                //片分区A
	ENUM_RTPPROTOCOL_NALU_TYPE_AREA_B = 3,                                //片分区B
	ENUM_RTPPROTOCOL_NALU_TYPE_AREA_C = 4,                                //片分区C
	ENUM_RTPPROTOCOL_NALU_TYPE_IDR = 5,                                   //IDR图像中的片
	ENUM_RTPPROTOCOL_NALU_TYPE_SEI = 6,                                   //补充增强信息单元（SEI）
	ENUM_RTPPROTOCOL_NALU_TYPE_SPS = 7,                                   //SPS
	ENUM_RTPPROTOCOL_NALU_TYPE_PPS = 8,                                   //PPS
	ENUM_RTPPROTOCOL_NALU_TYPE_DELIMI = 9,                                //分解符
	ENUM_RTPPROTOCOL_NALU_TYPE_SEQSTOP = 10,                              //序列结束
	ENUM_RTPPROTOCOL_NALU_TYPE_STREAMSTOP = 11,                           //码流结束
	ENUM_RTPPROTOCOL_NALU_TYPE_FILL = 12,                                 //填充
	//聚合包
	ENUM_RTPPROTOCOL_NALU_TYPE_STAP_A = 24,                               //单时间聚合包类型A
	ENUM_RTPPROTOCOL_NALU_TYPE_STAP_B = 25,                               //单时间聚合包类型B 
	ENUM_RTPPROTOCOL_NALU_TYPE_MTAP_16 = 26,                              //多时间聚合包类型(MTAP)16位位移
	ENUM_RTPPROTOCOL_NALU_TYPE_MTAP_24 = 27,                              //多时间聚合包类型(MTAP)24位位移
	//分片单元
	ENUM_RTPPROTOCOL_NALU_TYPE_FU_A = 28,                                 //分片类型A-无DON
	ENUM_RTPPROTOCOL_NALU_TYPE_FU_B = 29                                  //分片类型B
}ENUM_RTPPROTOCOL_NALUTYPE;
//////////////////////////////////////////////////////////////////////////
//                       导出的数据结构
//////////////////////////////////////////////////////////////////////////
//RTP包
typedef struct tag_StreamMedia_RTPProtocol_Packet
{
	int nMsgLen;                                                          //包大小
	TCHAR tszMsgBuffer[2048];                                             //包缓冲区
}STREAMMEDIA_RTPPROTOCOL_PACKET;
//RTP协议
typedef struct tag_StreamMedia_RTPProtocol_Hdr
{
	ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOADTYPE enPayload;                   //RTP载荷媒体数据类型
	ENUM_RTPPROTOCOL_NALUTYPE enNALType;                                  //NAL载荷类型
	unsigned int unTimeStamp;                                             //媒体采样时间戳
	unsigned int unNALNri;                                                //包级别
}STREAMMEDIA_RTPPROTOCOL_HDR;
//RTP头扩展
typedef struct tag_RTPProtocol_RTPExtern
{
	WORD wRTPExProfile;                                                   //用户定义
	WORD wRTPExCount;                                                     //扩展个数
}RTPPROTOCOL_RTPEXTERN;
//////////////////////////////////////////////////////////////////////////
//                       导出的函数
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD RTPProtocol_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                      打包函数导出                                    */
/************************************************************************/
/********************************************************************
函数名称：RTPProtocol_Packet_Init
函数功能：初始化RTP打包器
 参数.一：pxhSsrc
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出创建的RTP唯一标识符
 参数.二：enPayLoad
  In/Out：In
  类型：枚举型
  可空：Y
  意思：输入初始化的RTP包负载类型
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RTPProtocol_Packet_Init(XNETHANDLE *pxhSsrc, ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOADTYPE enPayLoad = ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOAD_TYPE_H264);
/********************************************************************
函数名称：RTPProtocol_Packet_Destory
函数功能：销毁一个RTP打包器
 参数.一：xhSsrc
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要销毁的句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RTPProtocol_Packet_Destory(XNETHANDLE xhSsrc);
/********************************************************************
函数名称：RTPProtocol_Packet_Send
函数功能：投递(发送)一段数据给RTP组包器
 参数.一：xhSsrc
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要操作的句柄
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要投递的缓冲区
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要投递缓冲区的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：目前仅支持H264编码格式
*********************************************************************/
extern "C" BOOL RTPProtocol_Packet_Send(XNETHANDLE xhSsrc, LPCTSTR lpszMsgBuffer, int nMsgLen);
/********************************************************************
函数名称：RTPProtocol_Packet_Recv
函数功能：获取一个RTP包
 参数.一：xhSsrc
  In/Out：In
  类型：句柄
  可空：N
  意思：要操作的句柄
 参数.二：pStl_RTPPacket
  In/Out：Out
  类型：STL容器指针
  可空：N
  意思：当前NAL单元组装的RTP包
 参数.三：wProfile
  In/Out：In
  类型：无符号短整数型
  可空：Y
  意思：输入扩展头自定义标识符
 参数.四：pStl_ListExtern
  In/Out：In
  类型：STL容器指针
  可空：Y
  意思：输入扩展数据
返回值
  类型：逻辑型
  意思：是否成功
备注：如果被分片,pStl_RTPPacket会有多个元素,你需要从头到尾方式轮训取出发送
*********************************************************************/
extern "C" BOOL RTPProtocol_Packet_Recv(XNETHANDLE xhSsrc, list<STREAMMEDIA_RTPPROTOCOL_PACKET> *pStl_RTPPacket, WORD wProfile = 0, list<uint32_t> *pStl_ListExtern = NULL);
/********************************************************************
函数名称：RTPProtocol_Packet_GetSReport
函数功能：获取一个发送者报告包
 参数.一：xhSsrc
  In/Out：In
  类型：句柄
  可空：N
  意思：要操作的句柄
 参数.二：lpszCName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送者所属机器名称
 参数.三：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的RTCP发送者数据
 参数.四：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出打好包的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：你应该每隔5秒获取一次并且发送这个报告包
*********************************************************************/
extern "C" BOOL RTPProtocol_Packet_GetSReport(XNETHANDLE xhSsrc, LPCTSTR lpszCName, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/************************************************************************/
/*                      打包函数导出                                    */
/************************************************************************/
/********************************************************************
函数名称：RTPProtocol_Parse_Init
函数功能：初始化RTP包解析器
 参数.一：pxhSsrc
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出创建的RTP唯一标识符
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RTPProtocol_Parse_Init(XNETHANDLE *pxhToken);
/********************************************************************
函数名称：RTPProtocol_Parse_Destory
函数功能：销毁一个RTP解析器
 参数.一：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要销毁的句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RTPProtocol_Parse_Destory(XNETHANDLE xhToken);
/********************************************************************
函数名称：RTPProtocol_Parse_Send
函数功能：投递(发送)一段数据给RTP解析器
 参数.一：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要操作的句柄
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要投递的缓冲区
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要投递缓冲区的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：投递的缓冲区应该是一段UDP接受到的完成包,暂不支持沾包处理
*********************************************************************/
extern "C" BOOL RTPProtocol_Parse_Send(XNETHANDLE xhToken, LPCTSTR lpszMsgBuffer, int nMsgLen);
/********************************************************************
函数名称：RTPProtocol_Parse_Recv
函数功能：获取一个完成的RTP包数据
 参数.一：xhSsrc
  In/Out：In
  类型：句柄
  可空：N
  意思：要操作的句柄
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出获取到的数据
 参数.三：pInt_MsgLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小,输出需要的大小
 参数.四：pSt_RTPHdr
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：输出RTP协议头信息
 参数.五：pSt_RTPExt
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：输出RTP扩展头信息
 参数.六：pStl_ListExtern
  In/Out：Out
  类型：STL容器指针
  可空：Y
  意思：输出扩展头的扩展信息
 参数.七：bPacket
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：为真表示不关心分片,每次输出的都是一个完成的包
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RTPProtocol_Parse_Recv(XNETHANDLE xhToken, TCHAR *ptszMsgBuffer, int *pInt_MsgLen, STREAMMEDIA_RTPPROTOCOL_HDR *pSt_RTPHdr, RTPPROTOCOL_RTPEXTERN *pSt_RTPExt = NULL, list<uint32_t> *pStl_ListExtern = NULL, BOOL bPacket = FALSE);
/********************************************************************
函数名称：RTPProtocol_Parse_GetCount
函数功能：获取RTP接受统计
 参数.一：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要操作的句柄
 参数.二：pInt_PktCount
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出接受到的包个数
 参数.三：pInt_ByteCount
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出接受到的字节
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RTPProtocol_Parse_GetCount(XNETHANDLE xhToken, __int64 *pInt_PktCount, __int64 *pInt_ByteCount);
/************************************************************************/
/*                      帮助函数导出                                    */
/************************************************************************/
extern "C" BOOL RTPProtocol_Help_GetRTPTime(unsigned int nTimeBase, unsigned int *pInt_TimeStamp, ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOADTYPE enPayload = ENUM_STREAMMEDIA_RTPPROTOCOL_PAYLOAD_TYPE_H264);