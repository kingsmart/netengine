#pragma once
/********************************************************************
//	Created:	2012/9/8  12:07
//	File Name: 	G:\U_DISK_Path\NetSocketEngine\NetEngien_AvCoder\NetEngine_AudioCoder\NetEngine_AudioCoder\AudioCoder_Define.h
//	File Path:	G:\U_DISK_Path\NetSocketEngine\NetEngien_AvCoder\NetEngine_AudioCoder\NetEngine_AudioCoder
//	File Base:	AudioCoder_Define
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	音频解码器导出定义文件
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                      音频编码方式
//////////////////////////////////////////////////////////////////////////
typedef enum en_AVCodec_AudioType
{
	ENUM_AVCODEC_AUDIO_TYPE_MP2 = 0x15000,
	ENUM_AVCODEC_AUDIO_TYPE_MP3,
	ENUM_AVCODEC_AUDIO_TYPE_G723 = 0x15034,
	ENUM_AVCODEC_AUDIO_TYPE_G726 = 0x1100B
}ENUM_AVCODEC_AUDIOTYPE;
//////////////////////////////////////////////////////////////////////////
//                      导出的数据结构
//////////////////////////////////////////////////////////////////////////
typedef struct
{
	int nCodecType;                                                       //编解码类型
	CHAR tszCodecName[64];                                                //编解码名称
}AVCODEC_AUDIO_CODECLIST, *LPAVCODEC_AUDIO_CODECLIST;
typedef struct
{
	ENUM_AVCOLLECT_AUDIOSAMPLEFORMAT enSampleFmt;                         //采样格式
	BOOL bKeyFrame;                                                       //是否是关键帧
	int nFrameTimes;                                                      //第几帧
	int nSampleRate;                                                      //采样率
	int nChannle;                                                         //通道个数
	int nNBSample;                                                        //采样个数
	int64_t nChannleLayout;                                               //通道层
}AVCODEC_AUDIO_INFO, *LPAVCODEC_AUDIO_INFO;
typedef struct
{
	CHAR tszArgsName[MAX_PATH];                                           //要附加的音频过滤器名称，比如volume（设置音量）或tempo（播放速度）
	CHAR tszArgsValue[MAX_PATH];                                          //名称的值，比如 volume音量大小.0.1-1
	int nSampleFmt;                                                       //采样格式
	int nSampleRate;                                                      //采样率
	int nNBSample;                                                        //样本数
	int nChannleLayout;                                                   //通道层
	int nIndex;                                                           //索引，从0开始传入，不可重复,混合器才有用
}AVCODEC_AUDIO_FILTER, *LPAVCODEC_AUDIO_FILTER;
//////////////////////////////////////////////////////////////////////////
//                      回调函数
//////////////////////////////////////////////////////////////////////////
typedef void(CALLBACK *CALLBACK_NETENGINE_AVCODER_AUDIO_STREAM_DECODEC)(XNETHANDLE xhNet, uint8_t *pszBuffer, int nLen, AVCODEC_AUDIO_INFO *pSt_AudioInfo,LPVOID lParam);
//////////////////////////////////////////////////////////////////////////
//                      导出函数
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD AudioCodec_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*               音频编解码实时流导出函数                               */
/************************************************************************/
/********************************************************************
函数名称：AudioCodec_Stream_Init
函数功能：初始化流编解码器
 参数.一：pxhNet
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出初始化成功的音频编码句柄
 参数.二：nAvCoder
  In/Out：In
  类型：枚举型
  可空：N
  意思：要编码成的音频格式
 参数.三：nBitRate
  In/Out：In
  类型：整数型
  可空：N
  意思：要编码的音频数据的码率
 参数.四：nSampleFmt
  In/Out：In
  类型：枚举型
  可空：N
  意思：要编码成的音频采样格式
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Stream_EnInit(XNETHANDLE *pxhNet, ENUM_AVCODEC_AUDIOTYPE nAvCoder = ENUM_AVCODEC_AUDIO_TYPE_MP2, int nBitRate = 64000, ENUM_AVCOLLECT_AUDIOSAMPLEFORMAT nSampleFmt = ENUM_AVCOLLECT_AUDIO_SAMPLE_FMT_S16);
/********************************************************************
函数名称：AudioCodec_Stream_EnCodec
函数功能：编码音频
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入编码器句柄
 参数.二：ptszPCMBuffer
  In/Out：In
  类型：无符号整数型指针
  可空：N
  意思：要编码的音频数据
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要编码的数据缓冲区长度
 参数.四：ptszBuffer
  In/Out：Out
  类型：字节指针
  可空：N
  意思：导出编码好的数据
 参数.五：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出编码好的数据长度
返回值
  类型：逻辑型
  意思：是否编码成功
备注：此函数未做安全检查ptszBuffer需要足够大的缓冲区,如果返回真并且pInt_Len返回0
	  那么表示你提供的缓冲区不足够编码,需要继续!
*********************************************************************/
extern "C" BOOL AudioCodec_Stream_EnCodec(XNETHANDLE xhNet, uint8_t *ptszPCMBuffer, int nLen, uint8_t *ptszBuffer, int *pInt_Len);
/********************************************************************
函数名称：AudioCodec_Stream_DeInit
函数功能：初始化解码器
 参数.一：pxhNet
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出初始化成功的解码器句柄
 参数.二：nAvCodec
  In/Out：In
  类型：枚举型
  可空：N
  意思：要使用哪个解码器
 参数.三：fpCall_StreamFrame
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：每次解码一个数据会通过回调返回
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数参数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Stream_DeInit(XNETHANDLE *pxhNet, ENUM_AVCODEC_AUDIOTYPE nAvCodec, CALLBACK_NETENGINE_AVCODER_AUDIO_STREAM_DECODEC fpCall_StreamFrame, LPVOID lParam = NULL);
/********************************************************************
函数名称：AudioCodec_Stream_DeCodec
函数功能：解码音频数据
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：解码器句柄
 参数.二：pszSourceBuffer
  In/Out：In
  类型：无符号整数型指针
  可空：N
  意思：要解码的数据缓冲区
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要解码的数据缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：解码成功的数据通过回调函数返回
*********************************************************************/
extern "C" BOOL AudioCodec_Stream_DeCodec(XNETHANDLE xhNet, uint8_t *pszSourceBuffer, int nLen);
/********************************************************************
函数名称：AudioCodec_Stream_Destroy
函数功能：销毁一个音频编解码器
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：编解码器句柄
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Stream_Destroy(XNETHANDLE xhNet);
/************************************************************************/
/*               音频帮助函数导出                                       */
/************************************************************************/
/********************************************************************
函数名称：AudioCodec_Help_GetList
函数功能：获得当前版本支持的音频编解码器
 参数.一：pStl_ListEncoder
  In/Out：Out
  类型：LIST容器指针
  可空：N
  意思：导出获取到的编码器
 参数.二：pStl_ListDecoder
  In/Out：Out
  类型：LIST容器指针
  可空：N
  意思：导出获取到的支持的解码器
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_GetList(list<AVCODEC_AUDIO_CODECLIST> *pStl_ListEncoder, list<AVCODEC_AUDIO_CODECLIST> *pStl_ListDecoder);
/********************************************************************
函数名称：AudioCodec_Help_ResamplerInit
函数功能：音频重采样初始化
 参数.一：pxhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化的编码器句柄
 参数.二：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出重采样后每个BUFF区需要的编码大小
 参数.三：nSrcRate
  In/Out：In
  类型：整数型
  可空：N
  意思：输入原始音频数据码率
 参数.四：nDstRate
  In/Out：In
  类型：整数型
  可空：N
  意思：输入目标音频数据码率
 参数.五：enSrcSampleFmt
  In/Out：In
  类型：枚举型
  可空：N
  意思：输入原始采样格式
 参数.六：enDstSampleFmt
  In/Out：In
  类型：枚举型
  可空：N
  意思：输入目标采样格式
 参数.七：nSrcSNBample
  In/Out：In
  类型：整数型
  可空：N
  意思：输入原始数据每秒样本数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_ResamplerInit(XNETHANDLE *pxhNet, int *pInt_Len, int nSrcRate = 64000, int nDstRate = 64000, ENUM_AVCOLLECT_AUDIOSAMPLEFORMAT enSrcSampleFmt = ENUM_AVCOLLECT_AUDIO_SAMPLE_FMT_S16, ENUM_AVCOLLECT_AUDIOSAMPLEFORMAT enDstSampleFmt = ENUM_AVCOLLECT_AUDIO_SAMPLE_FMT_S16P, __int64 nSrcSNBample = 1152);
/********************************************************************
函数名称：AudioCodec_Help_ResamplerCvt
函数功能：重采样一段数据
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入编码器句柄
 参数.二：ptszSrcBuffer
  In/Out：In
  类型：无符号整数型指针
  可空：N
  意思：输入要重采样的数据
 参数.三：nSrcLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入数据大小.通过初始化得到输入大小
 参数.四：ptszDstBuffer
  In/Out：Out
  类型：无符号整数型指针
  可空：N
  意思：输出重采样后的数据
 参数.五：pInt_DstLen
  In/Out：Out
  类型：整数型
  可空：N
  意思：输出采样数据大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_ResamplerCvt(XNETHANDLE xhNet, uint8_t *ptszSrcBuffer, int nSrcLen, uint8_t *ptszPCMBuffer, int *pInt_DstLen);
/********************************************************************
函数名称：AudioCodec_Help_ResamplerDestroy
函数功能：销毁音频重采样器
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入编码器句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_ResamplerDestroy(XNETHANDLE xhNet);
/********************************************************************
函数名称：AudioCodec_Help_FilterInit
函数功能：初始化过滤器
 参数.一：pxhNet
  In/Out：Out
  类型：网络句柄
  可空：N
  意思：导出初始化成功的音频过滤器
 参数.二：lpszFilterStr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：转换后的目标过滤器,可以和原始的一样
 参数.三：pStl_ListFile
  In/Out：In
  类型：LIST容器指针
  可空：N
  意思：输入要混合的音频文件和参数
返回值
  类型：逻辑型
  意思：是否成功
备注：参数二为目标拼音格式,为字符串,参数通过:分割,如下所示
	 sample_rates=48000:sample_fmts=s16p:channel_layouts=stereo
*********************************************************************/
extern "C" BOOL AudioCodec_Help_FilterInit(XNETHANDLE *pxhNet, LPCTSTR lpszFilterStr, AVCODEC_AUDIO_FILTER *pSt_AudioFilter);
/********************************************************************
函数名称：AudioCodec_Help_FilterCvt
函数功能：进行一帧的转换
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入要操作的过滤器句柄
 参数.二：ptszSrcBuffer
  In/Out：In
  类型：无符号字符指针
  可空：N
  意思：要转换的数据缓冲区
 参数.三：nSrcLen
  In/Out：In
  类型：整数型
  可空：N
  意思：转换数据缓冲区大小
 参数.四：ptszDstBuffer
  In/Out：Out
  类型：无符号字符指针
  可空：N
  意思：转换后的数据缓冲区
 参数.五：pInt_DstLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_FilterCvt(XNETHANDLE xhNet, uint8_t *ptszSrcBuffer, int nSrcLen, uint8_t *ptszDstBuffer, int *pInt_DstLen);
/********************************************************************
函数名称：VideoCodec_Help_FilterDestroy
函数功能：销毁一个过滤器资源
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入要操作的句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_FilterDestroy(XNETHANDLE xhNet);
/********************************************************************
函数名称：AudioCodec_Help_MixInit
函数功能：初始化混合器
 参数.一：pxhNet
  In/Out：Out
  类型：网络句柄
  可空：N
  意思：导出初始化成功的音频过滤器
 参数.二：lpszFilterStr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要为视频添加的过滤器字符串
 参数.三：pStl_ListFile
  In/Out：In
  类型：LIST容器指针
  可空：N
  意思：输入要混合的音频文件和参数
返回值
  类型：逻辑型
  意思：是否成功
备注：混合器能实现一个或者多个过滤器和音频文件附加混合，参数的格式如下。表示采样率，格式，和通道
	 sample_rates=48000:sample_fmts=s16p:channel_layouts=stereo。具体可以参考FFMPEG
*********************************************************************/
extern "C" BOOL AudioCodec_Help_MixInit(XNETHANDLE *pxhNet, LPCTSTR lpszFilterStr, list<AVCODEC_AUDIO_FILTER> *pStl_ListFile);
/********************************************************************
函数名称：AudioCodec_Help_MixCvt
函数功能：进行一帧的混合
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入要操作的过滤器句柄
 参数.二：nIndex
  In/Out：In
  类型：整数型
  可空：N
  意思：要操作的索引混合文件索引
 参数.三：ptszSrcBuffer
  In/Out：In
  类型：无符号字符指针
  可空：N
  意思：输入要混合的原始数据
 参数.四：nSrcLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入混合数据大小
 参数.五：ptszDstBuffer
  In/Out：Out
  类型：无符号字符指针
  可空：N
  意思：导出混合后的音频数据缓冲区
 参数.六：pInt_DstLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小,输出混合后缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_MixCvt(XNETHANDLE xhNet, int nIndex, uint8_t *ptszSrcBuffer, int nSrcLen, uint8_t *ptszDstBuffer, int *pInt_DstLen);
/********************************************************************
函数名称：AudioCodec_Help_MixDestroy
函数功能：销毁一个混合器资源
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入要操作的句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AudioCodec_Help_MixDestroy(XNETHANDLE xhNet);