#pragma once
/********************************************************************
//	Created:	2018/8/31   10:18
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_AvCoder\NetEngine_VideoCoder\VideoCoder_Error.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_AvCoder\NetEngine_VideoCoder
//	File Base:	VideoCoder_Error
//	File Ext:	h
//  Project:    NetEngine(����ͨ������)
//	Author:		qyt
//	Purpose:	��Ƶ�����������������
//	History:    
*********************************************************************/
/************************************************************************/
/*                       ʵʱ����������                               */
/************************************************************************/
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENINIT_PARAMENT 0x40D0000     //��ʼ��ʧ��,��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENINIT_FINDCODER 0x40D0001    //���ұ�����ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENINIT_MALLOCCODEC 0x40D0002  //�����ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENINIT_OPEN 0x40D0003         //�򿪱�����ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENINIT_MALLOCFRAME 0x40D0004  //����֡�ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENINIT_MALLOCPACKET 0x40D0006 //����PAKCET�ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENCODEC_PARAMENT 0x40D0010    //��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENCODEC_NOTFOUND 0x40D0011    //û���ҵ����
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENCODEC_VIDEO 0x40D0011       //������Ƶʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENCODEC_NOFINISH 0x40D0012    //û�����
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_ENCODEC_EAGAIN 0x40D0013      //���������㹻����,�����ѹ������
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_PARAMENT 0x40D0020     //��������,��ʼ��������ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_FINDCODER 0x40D0021    //û���ҵ�ָ��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_PARERS 0x40D0022       //����������ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_MALLOCCODEC 0x40D0023  //����������ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_OPEN 0x40D0024         //�򿪽�����ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_MALLOCFRAME 0x40D0025  //����֡�ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DEINIT_MALLOCPACKET 0x40D0026 //���뱨�ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DECODEC_PARAMENT 0x40D0020    //����ʧ��,��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DECODEC_NOTFOUND 0x40D0021    //û���ҵ��������ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DECODEC_PARSE 0x40D0021       //����ʧ��,���ܲ���һ����������Ƶ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_DECODEC_MALLOC 0x40D0022      //����������ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_GETINFO_PARAMENT 0x40D0030    //��ȡ��Ϣʧ��,��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_GETINFO_NOTFOUND 0x40D0031    //û���ҵ����
#define ERROR_NETENGINE_AVCODEC_VIDEO_STREAM_GETSPS_NOTFOUND 0x40D0040     //û���ҵ�ָ�����
/************************************************************************/
/*                       ������������                                   */
/************************************************************************/
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_GETLIST_PARAMENT 0x40D1000     //��������,��ȡʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVTINIT_PARAMENT 0x40D1010     //û���ҵ����
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVTINIT_SRCMALLOC 0x40D1011    //�����ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVTINIT_DSTMALLOC 0x40D1012    //�����ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVTINIT_GETCTX 0x40D1013       //��ȡת��������ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVT_NOTFOUND 0x40D1020         //û���ҵ�
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVT_FAILED 0x40D1021           //ת��ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_CVT_LEN 0x40D1022              //������̫С��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERINIT_PARAMENT 0x40D1030  //��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERINIT_MALLOC 0x40D1031    //�����ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERINIT_SRCCREATE 0x40D1031 //����ԭʼ������ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERINIT_DSTCREATE 0x40D1032 //����Ŀ�������ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERINIT_PARSE 0x40D1033     //����ʧ��,�޷�����
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERINIT_CHECK 0x40D1034     //����������,�޷�����
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERCVT_PARAMENT 0x40D1040   //��������
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERCVT_NOTFOUND 0x40D1041   //û���ҵ�ָ�����
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERCVT_ADDFRAME 0x40D1042   //���ӹ�������֡ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERCVT_GETFRAME 0x40D1043   //�����ӵĹ�����֡�л�ȡ��֡ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERCVT_MALLOC 0x40D1044     //�����ڴ�ʧ��
#define ERROR_NETENGINE_AVCODEC_VIDEO_HELP_FILTERCVT_LEN 0x40D1045        //�ṩ�Ļ�����̫С