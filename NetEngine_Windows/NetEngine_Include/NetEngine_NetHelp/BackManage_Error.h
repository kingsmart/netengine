#pragma once
/********************************************************************
//	Created:	2017/5/26   10:26
//	Filename: 	G:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_NetHelp\NetHelp_BackManage\BackManage_Error.h
//	File Path:	G:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_NetHelp\NetHelp_BackManage
//	File Base:	BackManage_Error
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	后台管理服务导出错误
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                        导出的后台网络服务错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_INIT_PARAMENT 0xAB00F001   //参数错误
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_INIT_HTTPTHREAD 0xAB00F002 //创建HTTP线程失败
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_INIT_TCPTHREAD 0xAB00F003  //创建TCP客户端线程失败
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_PARSE_PARAMENT 0xAB00F010  //解析失败,参数错误
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_PARSE_JSON 0xAB00F011      //解析失败,JSON错误
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_PARSE_PROTOCOL 0xAB00F012  //解析失败,协议错误
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_PARSE_NOTSUPPORT 0xAB00F013//这个协议不被支持
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_DESTORY_HTTPTHREAD 0xAB00F020
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_SERVICE_DESTORY_TCPTHREAD 0xAB00F021   
//////////////////////////////////////////////////////////////////////////
//                        导出的后台服务信息获取错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_GETINFO_HARDWARE_PARAMENT 0xAB00F101
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_GETINFO_SOFTWARE_PARAMENT 0xAB00F110
#define ERROR_NETENGINE_NETHELP_BACKMANAGE_GETINFO_SOFTWARE_GETNAME 0xAB00F111