#pragma once
/********************************************************************
//	Created:	2018/3/28   11:00
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_NetHelp\NetHelp_APIHttp\APIHttp_Error.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_NetHelp\NetHelp_APIHttp
//	File Base:	APIHttp_Error
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:	    qyt
//	Purpose:	HTTP服务导出错误码
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                        导出的简单HTTP服务错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETCLIENT_PARAMENT 0xA0B0000     //参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETCLIENT_EMPTY 0xA0B0001        //没有客户端可以获取包
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETCLIENT_NOTFOUND 0xA0B0002     //没有可以处理的包
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETCLIENT_SIZE 0xA0B0003         //提供的缓冲区大小不够
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETCLIENT_NOTDATA 0xA0B0004      //此客户端没有数据
#define ERROR_NETENGINE_NETHELP_HTTP_API_PARSE_PARAMENT 0xA0B0010         //解析参数失败
#define ERROR_NETENGINE_NETHELP_HTTP_API_PARSE_NOHTTPHDR 0xA0B0011        //没有HTTP头，或者HTTP不标准，无法继续
#define ERROR_NETENGINE_NETHELP_HTTP_API_PARSE_NOTMETHOD 0xA0B0012        //没有方法名
#define ERROR_NETENGINE_NETHELP_HTTP_API_PARSE_NOTURL 0xA0B0013           //没有URI地址
#define ERROR_NETENGINE_NETHELP_HTTP_API_PARSE_NOTVER 0xA0B0014           //没有HTTP版本号
#define ERROR_NETENGINE_NETHELP_HTTP_API_SENDMSG_PARAMENT 0xA0B0020       //参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_API_INSERT_PARAMENT 0xA0B0030        //参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_API_INSERT_NOTFOUND 0xA0B0031        //没有找到HTTP协议头分割符
#define ERROR_NETENGINE_NETHELP_HTTP_API_INSERT_MALLOCHDR 0xA0B0032       //申请HTTP头字段内存失败
#define ERROR_NETENGINE_NETHELP_HTTP_API_INSERT_MALLOCBODY 0xA0B0033      //申请HTTP数据内存失败
#define ERROR_NETENGINE_NETHELP_HTTP_API_INSERT_MALLOCMSG 0xA0B0034       //申请消息内存失败
#define ERROR_NETENGINE_NETHELP_HTTP_API_CLOSE_PARAMENT 0xA0B0040         //关闭客户端失败，参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_API_CLOSE_NOTFOUND 0xA0B0041         //关闭客户端失败，没有找到
#define ERROR_NETENGINE_NETHELP_HTTP_API_SENDMSG_LEN 0xA0B0050            //发送失败，提供的缓冲区大小不够
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETRANDOM_PARAMENT 0xA0B0070     //参数错误，获取失败
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETRANDOM_EMPTY 0xA0B0071        //没有任何可用队列
#define ERROR_NETENGINE_NETHELP_HTTP_API_GETRANDOM_NOPKT 0xA0B0072        //没有可用的包
//////////////////////////////////////////////////////////////////////////
//                        导出的HTTP帮助处理函数错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETFILED_PARAMENT 0xA0B1000  //获取字段内容失败，参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETFILED_NOTFOUND 0xA0B1001  //没有找到指定的字段
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETURLAPI_PARAMENT 0xA0B1010 //获取失败，参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETURLAPI_NOTTYPE 0xA0B1011  //没有找到API类型
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETURLAPI_NOTNAME 0xA0B1012  //没有找到API名称
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETURLAPI_NOTVER 0xA0B1013   //没有找到API版本
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_REGPROCESS_PARAMENT 0xA0B1020//注册处理程序失败，参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_REGPROCESS_EXIST 0xA0B1021   //注册处理程序失败，这个处理程序名称已经存在
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_CANCPROCESS_PARAMENT 0xA0B1030 //注销处理程序失败，参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_PARAMENT 0xA0B1040 //参数错误，处理失败
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_NOTEXIST 0xA0B1041 //不存在的处理程序
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_EXEC 0xA0B1042     //执行失败，可能不是一个标准的注册程序
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_HTTPCODE 0xA0B1043 //获取HTTP状态码失败
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_MINITYPE 0xA0B1044 //获取返回类型失败
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_LEN 0xA0B1045      //获取长度失败
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_HDPROCESS_MSG 0xA0B1046      //获取返回内容失败
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_EXECGI_PARAMENT 0xA0B1050    //执行CGI程序失败,参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_EXECGI_NOTDATA 0xA0B1051     //CGI程序执行完毕,但是没有任何数据返回
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_EXECGI_TITLE 0xA0B1052       //解析CGI标题失败,可能不是标准的CGI程序
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETAUTH_PARAMENT 0xA0B1060   //参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETAUTH_BUFFER 0xA0B1061     //缓冲区错误,不是标准的密码区
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETAUTH_USER 0xA0B1062       //没有取到用户名
#define ERROR_NETENGINE_NETHELP_HTTP_HELPAPI_GETAUTH_NOTSUPPORT 0xA0B1063 //不支持的验证类型
//////////////////////////////////////////////////////////////////////////
//                        导出的HTTP配置文件管理器错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_INITCODE_NOTFOUND 0xA0B2000   //参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_INITCODE_PARSE 0xA0B2001      //解析错误
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_GETCODE_PARAMENT 0xA0B2010    //参数错误
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_GETCODE_NOTFOUND 0xA0B2011    //没有找到
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_INITMIME_PARAMENT 0xA0B2020   //参数错误，初始化失败
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_INITMIME_READFILE 0xA0B2021   //读取配置文件错误
#define ERROR_NETENGINE_NETHELP_HTTP_CONFIG_GETMIME_PARAMENT 0xA0B2030    //参数错误