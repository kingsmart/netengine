#pragma once
/********************************************************************
//	Created:	2012/11/23  16:24
//	File Name: 	G:\U_DISK_Path\NetSocketEngine\NetEngine_Lib\NetEngine_Algorithm\Algorithm_Define.h
//	File Path:	G:\U_DISK_Path\NetSocketEngine\NetEngine_Lib\NetEngine_Algorithm
//	File Base:	Algorithm_Define
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	算法库导出模块函数
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                     导出的函数
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD Algorithm_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                     字符串算法导出函数                               */
/************************************************************************/
/********************************************************************
函数名称：Algorithm_String_XFastMatch
函数功能：血与荣誉快速匹配算法
 参数.一：lpszSourceStr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：被匹配的原始字符串
 参数.二：lpszFindStr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要匹配的字符串
 参数.三：pInt_Pos
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出匹配后的字符串位置，123456 m 45 返回的就是6
 参数.四：nSourceLen
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入原始字符串长度，可以为0。0原始字符串不能是二进制。
返回值
  类型：逻辑型
  意思：是否匹配成功
备注：支持字符串和二进制快速匹配
*********************************************************************/
extern "C" BOOL Algorithm_String_XFastMatch(LPCTSTR lpszSourceStr, LPCTSTR lpszFindStr, int *pInt_Pos, int nSourceLen = 0);
/************************************************************************/
/*                     查找与排序导出函数                               */
/************************************************************************/
/********************************************************************
函数名称：Algorithm_FSort_DoubleSort
函数功能：冒泡排序算法
 参数.一：pInt_ArrayNumber
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入：要排序的数组,输出：排序好的数组
 参数.二：nCount
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要排序的个数
返回值
  类型：逻辑型
  意思：是否排序成功
备注：
*********************************************************************/
extern "C" BOOL Algorithm_FSort_DoubleSort(int *pInt_ArrayNumber, int nCount);
/********************************************************************
函数名称：Algorithm_FSort_QSort
函数功能：快速排序算法
 参数.一：lPBase
  In/Out：In/Out
  类型：五类型指针
  可空：N
  意思：输入：要排序的结构,输出：排序好的
 参数.二：nNumber
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要排序的个数
 参数.三：nSize
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要排序的结构大小
返回值
  类型：逻辑型
  意思：是否排序成功
备注：
*********************************************************************/
extern "C" BOOL Algorithm_FSort_QSort(LPVOID lPBase, int nNumber, int nSize);
/********************************************************************
函数名称：Algorithm_FSort_InsertSort
函数功能：插入排序
 参数.一：pInt_ArrayNumber
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入：要排序的数组,输出：排序好的数组
 参数.二：nCount
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要排序的个数
返回值
  类型：逻辑型
  意思：是否排序成功
备注：
*********************************************************************/
extern "C" BOOL Algorithm_FSort_InsertSort(int *pInt_Array, int nValue);
/************************************************************************/
/*                     数学类算法导出函数                               */
/************************************************************************/
/********************************************************************
函数名称：Algorithm_Math_Swap
函数功能：两个数字交换,支持多种类型
 参数.一：lPSource
  In/Out：In
  类型：无类型指针
  可空：N
  意思：要交换的值
 参数.二：lPDest
  In/Out：In
  类型：无类型指针
  可空：N
  意思：要交换的值
 参数.三：lpszType
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要交换的值的类型
返回值
  类型：逻辑型
  意思：是否成功
备注：支持的类型有:int long uint32_t __int64 uint64_t float double
*********************************************************************/
extern "C" BOOL Algorithm_Math_Swap(LPVOID lPSource, LPVOID lPDest, LPCTSTR lpszType = _T("int"));
/********************************************************************
函数名称：Algorithm_Math_GetValue
函数功能：获取一个数字的符号位从低到高位上的数字
 参数.一：nValue
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要获取的数字
 参数.二：pInt_Bits
  In/Out：Out
  类型：整数型
  可空：N
  意思：输出获取到的内容
返回值
  类型：逻辑型
  意思：是否成功
备注：支持正负数
*********************************************************************/
extern "C" BOOL Algorithm_Math_GetValue(__int64 nValue, __int64 *pInt_Bits);
/********************************************************************
函数名称：Algorithm_Math_GetBit
函数功能：获取二进制数值的某一位
 参数.一：lParam
  In/Out：In
  类型：无符号指针
  可空：N
  意思：输入要获取的值
 参数.二：nBits
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要获取哪一位的值
 参数.三：lPBits
  In/Out：Out
  类型：无符号指针
  可空：N
  意思：输出获取到的值(如果此位为0,那么返回0,如果为1,返回这个位的十进制数)
 参数.四：nSize
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入输出缓冲区大小.sizeof(*)
返回值
  类型：逻辑型
  意思：是否成功
备注：145 = 10010001,从右到左数0 1 2 3
*********************************************************************/
extern "C" BOOL Algorithm_Math_GetBit(LPVOID lParam, int nBits, LPVOID lPBits, int nSize = 4);
/********************************************************************
函数名称：Algorithm_Math_SetBit
函数功能：设置二进制数值的某一位
 参数.一：lParam
  In/Out：In/Out
  类型：无符号指针
  可空：N
  意思：输入要设置的值,输出设置完成后的值
 参数.二：nBits
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要设置哪一位的值
 参数.三：nSet
  In/Out：In
  类型：整数型
  可空：Y
  意思：这个位要设置为1,还是0,只能是这两个值
 参数.四：nSize
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入输出缓冲区大小.sizeof(*)
返回值
  类型：逻辑型
  意思：是否成功
备注：145 = 10010001,从右到左数0 1 2 3
*********************************************************************/
extern "C" BOOL Algorithm_Math_SetBit(LPVOID lParam, int nBits, int nSet = 1, int nSize = 4);