#pragma once
#include <string>
/********************************************************************
//	Created:	2017/6/10   11:37
//	Filename: 	G:\NetEngine_Windows\NetEngine_SourceCode\XyRyNet_CommHdr.h
//	File Path:	G:\NetEngine_Windows\NetEngine_SourceCode
//	File Base:	XyRyNet_CommHdr
//	File Ext:	h
//      Project:        NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	血与荣誉公用头文件
//	History:    
*********************************************************************/
///////////////////////////////////////////////////////////////////////////
//                          导出宏定义
///////////////////////////////////////////////////////////////////////////
#ifdef XYRY_NETENGINE_API_CALL
#define XYRY_NETENGINE_APICALL __stdcall
#else
#define XYRY_NETENGINE_APICALL
#endif
///////////////////////////////////////////////////////////////////////////
//                          导出的类型定义
///////////////////////////////////////////////////////////////////////////
typedef void* XHANDLE;
typedef unsigned long long XNETHANDLE;
typedef unsigned long long *PXNETHANDLE;
///////////////////////////////////////////////////////////////////////////
//                          导出环境类型定义
///////////////////////////////////////////////////////////////////////////
#ifdef _UNICODE
typedef std::wstring tstring;
#else
typedef std::string tstring;
#endif
///////////////////////////////////////////////////////////////////////
//                  自定义操作
///////////////////////////////////////////////////////////////////////
#define MAKEVERSION(a,b) ((WORD)(((BYTE) (a)) | ((WORD)((BYTE) (b))) << 8))
