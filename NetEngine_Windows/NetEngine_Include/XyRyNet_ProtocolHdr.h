#pragma once
/********************************************************************
//	Created:	2017/03/17  14:12
//	File Name: 	NetEngine_Linux/NetEngine_SourceCode/XyRyNet_ProtocolHdr.h
//	File Path:	NetEngine_Linux/NetEngine_SourceCode
//	File Base:	XyRyNet_ProtocolHdr.h
//	File Ext:	h
//      Project:      NetEngine(网络通信引擎)
//	Author:	qyt
//	Purpose:     公用协议头文件
//	History:
*********************************************************************/
///////////////////////////////////////////////////////////////////////////
//                          导出的数据定义
///////////////////////////////////////////////////////////////////////////
/*************************************************************************
                         协议头
*************************************************************************/
#define NETENGIEN_COMMUNICATION_PACKET_PROTOCOL_HEADER 0x11               //固定填充头部
#define NETENGIEN_COMMUNICATION_PACKET_PROTOCOL_TAIL 0xFF                 //固定填充尾部
//P2XP节点类型
#define NETENGINE_P2XP_PEER_TYPE_NORMAL 0xA1 << 0x01                      //普通节点
#define NETENGINE_P2XP_PEER_TYPE_SUPER 0xA1 << 0x02                       //超级节点
#define NETENGINE_P2XP_PEER_TYPE_ROUTING 0xA1 << 0x03                     //路由节点
#define NETENGINE_P2XP_PEER_TYPE_SERVERS 0xA1 << 0x04                     //服务器节点，可以有多个。用于服务器无法使用的时候
#define NETENGINE_P2XP_PEER_TYPE_ENGINE 0xA1 << 0x05                      //引擎节点，仅此一个
#define NETENGINE_P2XP_PEER_TYPE_DATATRAN 0xA1 << 0x06                    //数据传输节点
/*********************************************************************
                         操作类型定义
*********************************************************************/
typedef enum en_NetEngine_XComm_Protocol
{
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_NORMAL = 0,                   //普通协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_XLOG = 1,                     //网络日志协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_AUTH = 2,                     //网络验证服务协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_SMS = 3,                      //流媒体协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_STORAGE = 4,                  //存储服务协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_BACKSERVICE = 5,              //后台服务协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_P2XP = 6,                     //P2XP协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_XMQ = 7,                      //消息队列服务
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_XDDS = 8,                     //消息分发服务
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_UDX = 9,                      //UDX协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_HEARTBEAT = 0xA,              //心跳协议
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_CHUNKED = 0xB,                //CHUNKED包模式，使用此模式协议头的unPacketSize字段Post将无效
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_LEAVE = 0xF,                  //离开包，投递了这个包后后续包都将被抛弃
	ENUM_ENGINE_COMMUNICATION_PROTOCOL_TYPE_USER = 100                    //用户包,用户自定义包开始为101开始100以内为协议内部保留            
}ENUM_NETENGINE_XCOMM_PROTOCOL;
/**************************************************************************
                          操作码定义,不能超过0xFFFF大小
**************************************************************************/
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_ISOK 0x0001                 //通用成功
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_ISFAILED 0x0002             //通用失败
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_LEAVE 0x000A                //离开包
//日志协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_XLOG_PUSH 0x1001            //日志推送的消息
//网络验证服务子协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQDEL 0x2001          //删除用户
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPDEL 0x2002
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQREGISTER 0x2003     //注册
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPREGISTER 0x2004
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQLOGIN 0x2005        //登陆
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPLOGIN 0x2006
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQPAY 0x2007          //充值
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPPAY 0x2008
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQGETPASS 0x2009      //找回密码
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPGETPASS 0x200A
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQGETTIME 0x200B      //获取剩余时间
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPGETTIME 0x200C
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REQDATAS 0x200D        //数据
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_REPDATAS 0x200E
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_AUTH_TIMEDOUT 0x2FFF        //通知客户端时间到期
//流媒体协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_SMS_RTSPREQPUSH 0x3001      //请求推送
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_SMS_RTSPREPPUSH 0x3002      //回复请求,如果TOKEN值存在,回复会改变协议头的TOKEN字段,你需要自己判断
//存储服务协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_TRANSMISSION 0x4000 //文件传输
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQUPDATE 0x4001    //上传文件请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPUPDATE 0x4002    //上传文件请求确认
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQQUERY 0x4003     //查询文件请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPQUERY 0x4004     //查询文件请求确认
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQDELETE 0x4005    //删除文件请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPDELETE 0x4006    //删除文件请求确认
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQDOWN 0x4007      //下载文件请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPDOWN 0x4008      //下载文件确认

#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQDIRCREATE 0x4010 //请求目录创建
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPDIRCREATE 0x4011 //确认目录创建
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQDIRQUERY 0x4012  //请求目录查询协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPDIRQUERY 0x4013  //确认目录查询
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REQDIRDELETE 0x4014 //请求目录删除协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_STORAGE_REPDIRDELETE 0x4015 //确认目录删除
//后台服务协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_DOWNFILE 0x5001          //下载并且运行一个程序
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_DELETEFILE 0x5002        //删除指定文件
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_DELETEDIR 0x5003         //删除指定目录
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_UPFILE 0x5004            //上传一个文件到指定FTP
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_REQGETLIST 0x5005        //请求获取文件夹中的文件列表
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_REPGETLIST 0x5A05        //回复列表
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_EXEC 0x5006              //执行指定程序
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_POPMESSAGE 0x5007        //弹出指定消息
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_STOPPROCESS 0x5008       //结束指定进程
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_SHUTDOWN 0x5009          //远程关闭计算机
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_ECMD 0x500A              //执行命令
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_BS_NOTHINGTODO 0x500F       //没有需要执行的任务
//P2XP协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REQLANLIST 0x6001      //同步列表协议,同步本地局域网IP中的客户端
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REPLANLIST 0x6002      //同步列表回复确认协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REQWLANLIST 0x6003     //同步列表协议,同步同一外网IP中局域网
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REPWLANLIST 0x6004     //同步万维网IP确认协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REQUSERQUERY 0x6005    //查询用户是否存在
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REPUSERQUERY 0x6006    //确认用户查询协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REQCONNECT 0x6007      //请求用户连接到网络服务
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_P2XP_REPCONNECT 0x6008      //确认连接协议
//消息队列协议 Code字段
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_MQ_REQPOST 0x7001           //投递包请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_MQ_REPPOST 0x7002           //投递包回复
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_MQ_REQGET 0x7003            //获取包请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_MQ_REPGET 0x7004            //获取包回复
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_MQ_REQDEL 0x7005            //删除包请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_MQ_REPDEL 0x7006            //删除包回复
//消息分发服务
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_CREATEDOMAIN 0x8001     //域ID被创建
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_ACKDOMAIN 0x8002        //域ID被删除
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_DELETEDOMAIN 0x8003     //域ID被删除
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_DELETETOPIC 0x8004      //主题被删除
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_TOPICREQCREATE 0x8005   //主题请求是否存在
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_TOPICACKCREATE 0x8006   //回复
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_DDS_PUBLISHCREATE 0x8007    //一个发布者被创建
//UDX通信协议
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_TRANSMISSION 0x9000            //数据通信协议
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_RETREQ 0x9001                  //请求重传
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_RETREP 0x9002                  //重传错误
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_LOGINREQ 0x9003                //用户登录请求   
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_LOGINREP 0x9004                //用户登录回复 
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_NOTIFYWINDOW 0x9101            //滑动窗口大小通知
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_NOTIFYCLOSE 0x9102             //通道关闭通知
#define ENUM_ENGINE_COMMUNICATION_PROTOCOL_CODE_UDX_NOTIFYACK 0x9103               //序列确认通知     
//心跳协议
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_HB_SYN 0xA001               //客户端同步请求
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_HB_ACK 0xA002               //服务器心跳回复请求
//CHUNK包模式  
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_CHUNKED_START 0xB001        //开始传输CHUNK包
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_CHUNKED_PROCESS 0xB002      //进行中的CHUNK包
#define NETENGINE_COMMUNICATION_PROTOCOL_OPERATOR_CODE_CHUNKED_END 0xB003          //结束CHUNK包
///////////////////////////////////////////////////////////////////////////
//                          导出的枚举
///////////////////////////////////////////////////////////////////////////
//客户端类型
typedef enum en_ProtocolClient_Type
{
	ENUM_PROTOCOL_FOR_SERVICE_TYPE_USER = 10,
	ENUM_PROTOCOL_FOR_SERVICE_TYPE_SERVICE,
	ENUM_PROTOCOL_FOR_SERVICE_TYPE_PROXY
}ENUM_PROTOCOLCLIENT_TYPE, *LPENUM_PROTOCOLCLIENT_TYPE;
//客户端设备类型
typedef enum en_ProtocolDevice_Type
{
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_PC = 20,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_SURFACE = 21,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_ANDROID = 22,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_IPAD = 23,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_IOS = 24,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_WEB = 25,
	ENUM_PROTOCOL_FOR_DEVICE_TYPE_EMBEDDED = 26,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_OTHER = 99,
    ENUM_PROTOCOL_FOR_DEVICE_TYPE_ALL = 100
}ENUM_PROTOCOLDEVICE_TYPE,*LPENUM_PROTOCOLDEVICE_TYPE;
//节点传输类型
typedef enum en_P2XPPeer_TransmissionType
{
    ENUM_NETENGINE_P2XPPEER_TRANSMISSIONTYPE_STRAIGHT = 0,                //直链
    ENUM_NETENGINE_P2XPPEER_TRANSMISSIONTYPE_PROXY = 1,                   //服务代理
    ENUM_NETENGINE_P2XPPEER_TRANSMISSIONTYPE_HOLES = 2                    //打洞
}ENUM_P2XPPEER_TRANSMISSIONTYPE;
//负载类型
typedef enum en_NetEngine_ProtocolHdr_Payload_Type
{
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_UNKNOW = 0,                   //未定义,或者没有后续
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_STRING = 1,                   //文本或者字符串
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_BIN = 2,                      //二进制
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_JSON = 3,                     //JSON
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_BSON = 4,                     //BSON
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_XML = 5,                      //XML
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_SYSTEM = 6,                   //系统保留
	ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE_USER = 10                     //用户使用
}ENUM_NETENGINE_PROTOCOLHDR_PAYLOAD_TYPE;
//加密类型
typedef enum en_NetEngine_ProtocolHdr_Crypto_Type
{
	ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE_UNKNOW = 0,                    //没有加密
	ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE_AES = 1,                       //AES
	ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE_DES = 2,                       //DES
	ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE_RSA = 3,                       //RSA
	ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE_SYSTEM = 4,                    //系统保留
	ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE_USER = 5                       //用户使用
}ENUM_NETENGINE_PROTOCOLHDR_CRYPTO_TYPE;
///////////////////////////////////////////////////////////////////////////
//                          导出的数据结构
///////////////////////////////////////////////////////////////////////////
#pragma pack(push)
#pragma pack(1)
//////////////////////////////////////////////////////////////////////////协议头
typedef struct tag_NetEngine_ProtocolHdr
{
    WORD wHeader;                                                         //协议头头部 固定的赋值
    XNETHANDLE xhToken;                                                   //唯一标识符
    UINT unOperatorType;                                                  //操作类型
    UINT unOperatorCode;                                                  //操作码
    UINT unPacketSize;                                                    //数据包大小，后续包的大小，不是长度，而是内存大小
	BYTE byVersion;                                                       //协议版本
	BYTE byIsReply;                                                       //是否需要回复包 0 否，1是
	WORD wReserve;                                                        //自定义数据位或者保留
	WORD wPacketSerial;                                                   //包序列号
    WORD wTail;                                                           //协议头尾部 固定的赋值
}NETENGINE_PROTOCOLHDR,*LPNETENGINE_PROTOCOLHDR;
//扩展协议
typedef struct tag_NetEngine_ProtocolHdrEx
{
	WORD wHeader : 8;                                                     //协议头头部 固定的赋值
	WORD wVersion : 4;                                                    //协议头版本号标志
	WORD wPayload : 4;                                                    //后续数据包负载类型
	XNETHANDLE xhToken;                                                   //唯一标识符
	UINT unOperatorType : 16;                                             //操作类型
	UINT unOperatorCode : 16;                                             //操作码
	UINT unPacketCrypt : 4;                                               //加密标志,0没有加密,其他值表示加密，加密类型自定义
	UINT unPacketCount : 10;                                              //0不分包,> 0 分包个数
	UINT unPacketSerial : 8;                                              //包序列号,只有分包的时候这个值才有效，其他时候请填充0
	UINT unPacketSize : 10;                                               //数据包大小，后续包的大小，不包括协议头和协议尾
	WORD wReserve : 8;                                                    //自定义数据位或者保留
	WORD wIsReply : 8;                                                    //是否需要回复包 0 否，1是
}NETENGINE_PROTOCOLHDREX, *LPNETENGINE_PROTOCOLHDREX;
typedef struct tag_NetEngine_ProtocolTailEx
{
	WORD wCheckSum : 8;                                                   //数据校验码,数据区校验
	WORD wTail : 8;                                                       //协议头尾部 固定的赋值
}NETENGINE_PROTOCOLTAILEX, *LPNETENGINE_PROTOCOLTAILEX;
//////////////////////////////////////////////////////////////////////////心跳包
typedef struct tag_NetNegine_Protocol_HeartBeat
{
    CHAR tszMachineAddr[32];                                             //机器IP地址
    int nMachineCode;                                                     //服务编号
    __int64 nTimer;                                                       //心跳时间 time(NULL)
    struct
    {
        int nCpuUsage;                                                    //CPU占用率
        int nMemoryUsage;                                                 //内存占用率
        int nNetUsage;                                                    //网络占用率
        int nDiskUsage;                                                   //硬盘占用率
		int nGraphUsage;                                                  //显卡占用率
    }st_HBComputerInfo;
}NETENGINE_PROTOCOLHEARTBEAT,*LPNETENGINE_PROTOCOLHEARTBEAT;
//////////////////////////////////////////////////////////////////////////文件传输头文件,可用于P2XP文件传输和组包管理模块文件传输
typedef struct tag_NetEngine_ProtocolFile
{
	CHAR tszFilePath[MAX_PATH];                                           //文件路径
    CHAR tszFileName[MAX_PATH];                                           //文件名称,文件传输的时候才需要,其他时候不需要
    CHAR tszFileMD5[64];                                                  //文件MD5
	CHAR tszFileTime[64];                                                 //文件创建时间，如果这个参数不填,那么服务器将会设置为接受到的文件时间
    __int64 nFileSize;                                                    //文件大小
}NETENGINE_PROTOCOLFILE,*LPNETENGINE_PROTOCOLFILE;
typedef struct tag_NetEngine_ProtocolFileEx
{
	NETENGINE_PROTOCOLFILE st_ProtocolFile;
	CHAR tszUserName[64];                                                 //文件所属用户
}NETENGINE_PROTOCOLFILEEX, *LPNETENGINE_PROTOCOLFILEEX;
//////////////////////////////////////////////////////////////////////////P2XP扩展协议
//连接信息
typedef struct tag_P2XP_Client_Information
{
	CHAR tszUserName[64];                                                 //用户信息
    CHAR tszPrivateAddr[32];                                              //私有本地地址，内网地址
    CHAR tszPublicAddr[32];                                               //外网地址
    CHAR tszConnectAddr[32];                                              //链接地址
    CHAR tszUserLocation[32];                                             //位置信息
    CHAR tszUserArea[20];                                                 //用户ISP
	__int64 dwConnectType;                                                //连接类型
    WORD dwPeerType;                                                      //节点类型
}P2XP_CLIENT_INFOMATION, *LPP2XP_CLIENT_INFOMATION;
//P2XP通道连接命令
typedef struct tag_P2XP_Client_IONet
{
    CHAR tszSourceUser[32];                                               //请求连接的用户
    CHAR tszDestUser[32];                                                 //要连接的用户
    CHAR tszConnectAddr[32];                                              //连接的IP地址
    int nDestPort;                                                        //要连接的端口
    BOOL bIsTcp;                                                          //连接类型TCP,否则为UDP
}P2XP_CLIENT_IONET, *LPP2XP_CLIENT_IONET;
typedef struct tag_NetEngine_P2XPProtocol
{
    __int64 dwPacketTime;                                                 //构建包时间
    UINT usTTL;                                                           //TTL
}NETENGINE_P2XPPROTOCOL, *LPNETENGINE_P2XPPROTOCOL;
//////////////////////////////////////////////////////////////////////////
//消息队列服务协议
typedef struct tag_NetEngine_ProtocolXmq
{
	TCHAR tszMQKey[256];                                                  //此消息的KEY
	int nIPType;                                                          //哪种类型用户可以获取0 不关心 1 TCP 2 UDP				  
	int nKeepTime;                                                        //保存时间，单位秒，如果为0，获取一次后被抛弃。-1 永久存在，tszMQKey不能为空，如果有多个永久存在的包wPacketSerial必须有值
}NETENGINE_PROTOCOL_XMQ, *LPNETENGINE_PROTOCOL_XMQ;
//数据分发服务子协议
typedef struct tag_NetEngine_XDDS_Protocol
{
    TCHAR tszTopic[MAX_PATH];                                             //主题
    TCHAR tszDDSAddr[64];                                                 //分发地址
    BOOL bCreater;                                                        //是否是创建者
    BOOL bTcp;                                                            //是否启用TCP，默认UDP
    int nPort;                                                            //端口
}NETENGINE_PROTOCOL_XDDS,*LPNETENGINE_PROTOCOL_XDDS;
//////////////////////////////////////////////////////////////////////////
//网络注册协议
typedef struct tag_NetEngine_Protocol_UserReg
{
	TCHAR tszUserName[64];                                                //用户名
	TCHAR tszPassword[64];                                                //密码
	TCHAR tszEMailAddr[64];                                               //电子邮件地址
	TCHAR tszLoginTime[64];                                               //登录时间
	TCHAR tszRegTime[64];                                                 //注册时间
	__int64 nQQNumber;                                                    //QQ号
	__int64 nPhoneNumber;                                                 //电话号码
	__int64 nIDNumber;                                                    //身份证号
	int nUserLeave;                                                       //用户等级
	BOOL bIsOnline;                                                       //是否在线
}NETENGINE_PROTOCOL_USERREG, *LPNETENGINE_PROTOCOL_USERREG;
//网络验证协议               
typedef struct tag_NetEngine_Protocol_Auth
{
	CHAR tszUserName[64];                                             //用户名
	CHAR tszPassword[64];                                             //密码
	ENUM_PROTOCOLCLIENT_TYPE enClientType;                            //用户类型
	ENUM_PROTOCOLDEVICE_TYPE enDeviceType;                            //设备类型
}NETENGINE_PROTOCOL_USERAUTH, *LPNETENGINE_PROTOCOL_USERAUTH;
//网络日志协议
typedef struct tag_NetEngine_XLog_Protocol
{
	CHAR tszFuncName[64];                                             //函数名称
	CHAR tszLogTimer[64];                                             //日志时间
	int nLogLine;                                                     //代码行数
	int nLogLeave;                                                    //日志级别
	int nLogLen;                                                      //打印的日志长度
}NETENGINE_XLOG_PROTOCOL, *LPNETENGINE_XLOG_PROTOCOL;
#pragma pack(pop)
