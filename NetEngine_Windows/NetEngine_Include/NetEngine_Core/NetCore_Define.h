#pragma once
/********************************************************************
//	Created:	2012/1/3  15:04
//	File Name: 	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngineCore\NetEngineCore_Define.h
//	File Path:	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngineCore
//	File Base:	NetEngineCore_Define
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	网络服务器开发组件-导出定义
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                      导出定义
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                      网络核心帮助函数定义                            */
/************************************************************************/
//网络连接类型定义
#define NETCORE_SOCKETHELP_CONNECTTYPE_CONNECTOKOFNOTCONNECTED 0x11110001 //已经连接但是可能并没有网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_LANTOINTERNET 0x11110002           //通过局域网连接到网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_MODEMTOINTERNET 0x11110003         //通过MODEM连接到网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_NOTCONNECTED 0x11110004            //没有连接到网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_PROXYTOINTERNET 0x11110005         //通过代理连接到网络
//网络协议版本号定义
#define NETCORE_SOCKETHELP_NETSTATE_VERSION_IPV4 0x11110010               //IPV4版本
#define NETCORE_SOCKETHELP_NETSTATE_VERSION_IPV6 0x11110011               //IPV6版本
#define NETCORE_SOCKETHELP_NETSTATE_VERSION_UNKNOW 0x1111001F             //无法识别的协议版本
//网络端口占用者信息
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_TCP 0x11110020               //TCP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_UDP 0x11110021               //UDP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_RAW 0x11110022               //RAW
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_UNIX 0x11110023              //UNIX
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_ICMP 0x11110024              //ICMP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_IP 0x11110025                //IP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_UNKNOW 0x1111002F            //无法识别的协议类
//网络状态信息
#define NETCORE_SOCKETHELP_NETSTATE_NET_CLOSED 1                          //网络关闭
#define NETCORE_SOCKETHELP_NETSTATE_NET_LISTEN 2                          //监听
#define NETCORE_SOCKETHELP_NETSTATE_NET_SYNSENT 3                         //再发送连接请求后等待匹配的连接请求
#define NETCORE_SOCKETHELP_NETSTATE_NET_SYNRECEIVED 4                     //再收到和发送一个连接请求后等待对方对连接请求的确认
#define NETCORE_SOCKETHELP_NETSTATE_NET_ESTABLISHED 5                     //代表一个打开的连接
#define NETCORE_SOCKETHELP_NETSTATE_NET_FINWAIT1 6                        //等待远程TCP连接中断请求，或先前的连接中断请求的确认
#define NETCORE_SOCKETHELP_NETSTATE_NET_FINWAIT2 7                        //从远程TCP等待连接中断请求
#define NETCORE_SOCKETHELP_NETSTATE_NET_CLOSEWAIT 8                       //等待从本地用户发来的连接中断请求
#define NETCORE_SOCKETHELP_NETSTATE_NET_CLOSING 9                         //等待远程TCP对连接中断的确认
#define NETCORE_SOCKETHELP_NETSTATE_NET_LASTACK 10                        //等待原来的发向远程TCP的连接中断请求的确认
#define NETCORE_SOCKETHELP_NETSTATE_NET_TIMEWAIT 11                       //等待足够的时间以确保远程TCP接收到连接中断请求的确认
/************************************************************************/
/*          串口操作导出定义                                              */
/************************************************************************/
#define NETENGINE_NETCORE_SERIALPORT_PARITYNONE 0x000000A1                //没有校验位
#define NETENGINE_NETCORE_SERIALPORT_PARITYODD 0x000000A2                 //设置为奇效验
#define NETENGINE_NETCORE_SERIALPORT_PARITYEVEN 0x000000A3                //偶校验
/************************************************************************/
/*                      帮助函数LOW网络信息数据结构成员导出值           */
/************************************************************************/
//网路接口类型 dwType 返回值
#define NETCORE_NETHELP_CARDINFO_TYPE_OTHER 1                             //未知的网络接口类型
#define NETCORE_NETHELP_CARDINFO_TYPE_CSMACD 6                            //一个以太网接口类型
#define NETCORE_NETHELP_CARDINFO_TYPE_TOKENRING 9                         //一个令牌环网网络
#define NETCORE_NETHELP_CARDINFO_TYPE_PPP 23                              //一个PPP网络接口
#define NETCORE_NETHELP_CARDINFO_TYPE_LOOPBACK 24                         //回环接口类型：127.0.0.1
#define NETCORE_NETHELP_CARDINFO_TYPE_ATM 37                              //一个ATM网路接口
#define NETCORE_NETHELP_CARDINFO_TYPE_IEEE80211 71                        //一个IEEE802.11无线局域网络接口
#define NETCORE_NETHELP_CARDINFO_TYPE_TUNNEL 131                          //一个被封装的隧道网络接口类型
#define NETCORE_NETHELP_CARDINFO_TYPE_1394 144                            //一个IEEE1394高性能网络串口
//网络操作状态 dwOperStatus 返回状态
#define NETCORE_NETHELP_CARDINFO_STATUS_NON_OPERATIONAL 0                 //网络适配器被禁止的状态
#define NETCORE_NETHELP_CARDINFO_STATUS_UNREACHABLE 1                     //没有连接的状态
#define NETCORE_NETHELP_CARDINFO_STATUS_DISCONNECTED 2                    //电缆未连接的状态
#define NETCORE_NETHELP_CARDINFO_STATUS_CONNECTING 3                      //广域网适配器连接中的状态
#define NETCORE_NETHELP_CARDINFO_STATUS_CONNECTED 4                       //广域网适配器连接上远程对等点的状态
#define NETCORE_NETHELP_CARDINFO_STATUS_OPERATIONAL 5                     //局域网适配器默认的连接状态
/************************************************************************/
/*                      网络程序状态设置                                */
/************************************************************************/
#define NETCORE_NETHELP_PROCESSNET_STATE_CLOSE 12                         //关闭网络连接
/************************************************************************/
/*                      NETBIOS数据报操作导出定义                       */
/************************************************************************/
#define NETCORE_NETBIOS_DGRAM_ADDNAME 0x00000001                          //添加一个名称
#define NETCORE_NETBIOS_DGRAM_ADDGROUP 0x00000010                         //添加一组名称
#define NETCORE_NETBIOS_DGRAM_ALLLANA 0x00000100                          //注册所有LANA编号
/************************************************************************/
/*                      剪贴板数据类型导出定义                          */
/************************************************************************/
#define NETCORE_PIPCOMMUNICATION_CLIPBROAD_CF_ANSITEXT 1                  //文本内容
#define NETCORE_PIPCOMMUNICATION_CLIPBROAD_CF_UNICODETEXT 13              //文本内容 UNICODE
#define NETCORE_PIPCOMMUNICATION_CLIPBROAD_CF_BITMAP 2                    //图片内容
/************************************************************************/
/*                      异步消息选择模型事件                            */
/************************************************************************/
#define NETCORE_SERVER_TCP_WSAASYNCSELECT_EVENT_ACCEPT 0xABCD0001         //这是一个接受到连接的事件
#define NETCORE_SERVER_TCP_WSAASYNCSELECT_EVENT_DISCONNECT 0xABCD0002     //客户端关闭了连接的事件
#define NETCORE_SERVER_TCP_WSAASYNCSELECT_EVENT_READ 0xABCD0003           //接受到数据，有可读事件
#define NETCORE_SERVER_TCP_WSAASYNCSELECT_EVENT_CONNECT 0xABCD0004        //有一个连接事件发生，触发这个事件，数据结构其他数据无效
#define NETCORE_SERVER_TCP_WSAASYNCSELECT_EVENT_CONNECTFAILED 0xABCD0005  //有一个失败的连接发生，触发这个事件，数据结构其他数据无效
/************************************************************************/
/*                      选择模型-主动获取事件类型                       */
/************************************************************************/
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_ALL 0xABCD0010                 //所有事件，输入作用，不支持输出
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_LOGIN 0xABCD0011               //用户登录事件，用户连接到服务器，此事件无数据
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_LEAVE 0xABCD0012               //用户离开事件，用户断开连接，此事件无数据
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_RECV 0xABCD0013                //数据到达事件，用户发送数据给服务器
/************************************************************************/
/*                        枚举类型                                       */
/************************************************************************/
//负载状态
typedef enum en_NetCore_SockOpt_HBLoad
{
	ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_UNKNOW = 0,                          //未知状态
	ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_IDLE = 1,                            //负载空闲
	ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_IDEAL,                               //理想负载
	ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_NORMAL,                              //正常负载
	ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_BUSY,                                //忙碌的负载
	ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_DANGER                               //危险负载，资源一直在90%以上
}ENUM_NETCORE_SOCKOPT_HBLOAD, *LPENUM_NETCORE_SOCKOPT_HBLOAD;
//////////////////////////////////////////////////////////////////////////
//                      数据结构导出定义
//////////////////////////////////////////////////////////////////////////
//网络信息状态查询
typedef struct tag_NetCore_NetHelp_NetState
{
	CHAR tszAppName[128];                                                 //应用程序名称
	CHAR tszUserName[32];                                                 //应用程序所属用户
	DWORD dwProVersion;                                                   //协议版本
	DWORD dwProtocol;                                                     //协议类型
	DWORD dwNetState;                                                     //网络状态，UDP无效
	int nPid;                                                             //应用程序PID
}NETCORE_NETHELP_NETSTATE,*LPNETCORE_NETHELP_NETSTATE;
typedef struct tag_NetCore_NetHelp_NetCard
{
	CHAR tszIFName[128];                                                 //网卡名称
	CHAR tszIPAddr[32];                                                  //网卡IP地址
	CHAR tszBroadAddr[32];                                               //网卡的广播地址
	CHAR tszMaskAddr[32];                                                //网卡的子网地址
}NETCORE_NETHELP_NETCARD, *LPNETCORE_NETHELP_NETCARD;
/************************************************************************/
/*                      网络帮助函数导出定义                            */
/************************************************************************/
//IP统计数据导出的结构体，该结构存储在特定计算机上运行的有关IP协议的信息
typedef struct tag_NetCore_Protocol_IpStatics         
{ 
	DWORD dwForwarding;                                                   //指定IP转发是否有效
	DWORD dwDefaultTTL;                                                   //为从你的电脑出发的数据报指定默认的初始TTL
	DWORD dwInReceives;                                                   //指定接收到的数据报的数量
	DWORD dwInHdrErrors;                                                  //指定接收到的数据报中，协议头出错的数据报的个数
	DWORD dwInAddrErrors;                                                 //指定接收到的数据报中，地址出错的数据报的个数
	DWORD dwForwDatagrams;                                                //指定转发的数据报的数量
	DWORD dwInUnknownProtos;                                              //指定接收数据的数据报中，协议未知的数据报的数量 
	DWORD dwInDiscards;                                                   //指定接收到的数据报中，丢弃的数据报的数量
	DWORD dwInDelivers;                                                   //指定接收到的数据报中，已经传送的数据报的数量
	DWORD dwOutRequests;                                                  //指定外出的数据报中，IP正在请求传输的数据报的数量
	DWORD dwRoutingDiscards;                                              //指定外出的数据报中，丢弃的数据报数量
	DWORD dwOutDiscards;                                                  //指定传输的数据报中，丢弃的数据报的数量
	DWORD dwOutNoRoutes;                                                  //指定没有路由目的地（计算机中没有其他目的地址的路由）的数据报的数量
	DWORD dwReasmTimeout;                                                 //指定一个分段数据报到来的最大时间
	DWORD dwReasmReqds;                                                   //指定需要组合的数据报的数量
	DWORD dwReasmOks;                                                     //指定成功重新组合的数据报的数量
	DWORD dwReasmFails;                                                   //指定不能重新组合的数据报的数量
	DWORD dwFragOks;                                                      //指定成功分段的数据报的数量
	DWORD dwFragFails;                                                    //指定不能分段的数据报的数量
	DWORD dwFragCreates;                                                  //指定已经分段的数据报数量
	DWORD dwNumIf;                                                        //指定接口的数量
	DWORD dwNumAddr;                                                      //指定此电脑关联的IP地址的数量
	DWORD dwNumRoutes;                                                    //指定IP路由表中的可用路由的数量
}NETCORE_PROTOCOL_IPSTATICS,*LPNETCORE_PROTOCOL_IPSTATICS;
//该结构体检索本地计算机的TCP统计信息。
typedef struct tag_NetCore_Protocol_TcpStatics
{
	DWORD dwRtoAlgorithm;                                                 //指定使用哪个超时重发算法，可能的取值有MIB_TCP_RTO_CONSTANT，MIB_TCP_RTO_RSRE，MIB_TCP_RTO_VANJ等
	DWORD dwRtoMin;                                                       //指定超时重发算法的最小值（以毫秒为单位，下同）        
	DWORD dwRtoMax;                                                       //指定超时重发算法的最大值
	DWORD dwMaxConn;                                                      //指定最大的链接数量，如果为-1，则是系统允许的最大值 
	DWORD dwActiveOpens;                                                  //指定机器向服务器初始化了多少个连接
	DWORD dwPassiveOpens;                                                 //指定机器监听到来多少个客户端连接
	DWORD dwAttemptFails;                                                 //指定多少个连接视图失败了
	DWORD dwEstabResets;                                                  //指定已经被重置的链接数量 
	DWORD dwCurrEstab;                                                    //指定当前连接的数量
	DWORD dwInSegs;                                                       //指定接收到的段的数量
	DWORD dwOutSegs;                                                      //指定发送的段的数量
	DWORD dwRetransSegs;                                                  //指定重发的段的数量
	DWORD dwInErrs;                                                       //指定接收错误的数量
	DWORD dwOutRsts;                                                      //指定重设标志位后，又传输了多少分段数据报
	DWORD dwNumConns;                                                     //指定连接的总数
}NETCORE_PROTOCOL_TCPSTATICS,*LPNETCORE_PROTOCOL_TCPSTATICS;
//获取UDP统计信息数据
typedef struct tag_NetCore_Protocol_UdpStatics
{  
	DWORD dwInDatagrams;                                                  //指定接收到的数据报的数量
	DWORD dwNoPorts;                                                      //指定接收到的数据报中，因为端口号无效而被丢弃的数量
	DWORD dwInErrors;                                                     //指定接收到的错误数据报的数量
	DWORD dwOutDatagrams;                                                 //指定以传输的数据报的数量
	DWORD dwNumAddrs;                                                     //指定UDP监听表中入口的数量
}NETCORE_PROTOCOL_UDPSTATICS,*LPNETCORE_PROTOCOL_UDPSTATICS;
//TCP进程连接列表
typedef struct tag_NetCore_Net_ProcessTable 
{
	DWORD dwProcessID;                                                    //进程ID
	CHAR tszLoaclAddr[16];                                               //本地IP地址
	CHAR tszRemoteAddr[16];                                              //远程IP地址
	int nLocalPort;                                                       //本地端口
	int nRemotePort;                                                      //远程端口
	DWORD dwStatus;                                                       //进程网络状态

	BOOL bFlagNet;                                                        //网络连接类型 TRUE:TCP FALSE:UDP
}NETCORE_NET_PROCESSTABLE,*LPNETCORE_NET_PROCESSTABLE;
//ICMP协议统计数据信息
typedef struct tag_NetCore_Protocol_IcmpInOutStatics
{  
	DWORD dwMsgs;                                                         //指定发送的或者接受到的消息数量
	DWORD dwErrors;                                                       //指定发送的或者接受到的消息中，错误消息的数量
	DWORD dwDestUnreachs;                                                 //指定发送的或者接受到的“目的不可达”消息的数量
	DWORD dwTimeExcds;                                                    //指定发送的或者接受到的TTL超时消息数量
	DWORD dwParmProbs;                                                    //指定发送的或者接受到的消息中，有参数问题的消息的数量
	DWORD dwSrcQuenchs;                                                   //指定发送的或者接受到的源结束的消息数量
	DWORD dwRedirects;                                                    //指定发送的或者接受到的重定向消息的数量
	DWORD dwEchos;                                                        //指定发送的或者接受到的ICMP回显请求的数量
	DWORD dwEchoReps;                                                     //指定发送的或者接受到的IMCP回显应答的数量
	DWORD dwTimestamps;                                                   //指定发送的或者接受到的时间戳请求的数量
	DWORD dwTimestampReps;                                                //指定发送的或者接受到的时间戳应答的数量
	DWORD dwAddrMasks;                                                    //指定发送的或者接受到的地址掩码的数量
	DWORD dwAddrMaskReps;                                                 //指定发送的或者接受到的地址掩码应答的数量
}PROTOCOL_INOUT_ICMPSTATICS,*LPPROTOCOL_INOUT_ICMPSTATICS;      
//ICMP统计数据相信信息
typedef struct tag_NetCore_Protocol_IcmpStatics
{
	//有两个相同的结构体，一个是用来获取发送，一个用来获取接受，使用方法相同
	PROTOCOL_INOUT_ICMPSTATICS st_InIcmpStatics;
	PROTOCOL_INOUT_ICMPSTATICS st_OutIcmpStatics;
}NETCORE_PROTOCOL_ICMPSTATICS,*LPNETCORE_PROTOCOL_ICMPSTATICS;
/************************************************************************/
/*                      套接字操作导出结构体                               */
/************************************************************************/
//心跳机器负载
typedef struct
{
	ENUM_NETCORE_SOCKOPT_HBLOAD en_CPULoad;
	ENUM_NETCORE_SOCKOPT_HBLOAD en_MEMLoad;
	ENUM_NETCORE_SOCKOPT_HBLOAD en_NETLoad;
	ENUM_NETCORE_SOCKOPT_HBLOAD en_DISKLoad;
	ENUM_NETCORE_SOCKOPT_HBLOAD en_GRAPHLoad;
}NETCORE_SOCKOPT_HBLOAD, *LPNETCORE_SOCKOPT_HBLOAD;
typedef struct
{
	int nCPURate;
	int nMemRate;
	int nNetRate;
	int nDiskRate;
	int nGraphRate;
}NETCORE_SOCKOPT_LOADRATE, *LPNETCORE_SOCKOPT_LOADRATE;
/************************************************************************/
/*                   NetBios数据报接受格式                              */
/************************************************************************/
//数据报接受数据结构，注意此结构体如果你接受的是广播或者多数据 你需要创建足够多的结构内存
typedef struct tag_NetCore_NetBios_DGRam_Recv          
{
	UCHAR uszLana;                                                        //接受的编号
	CHAR tszSender[64];                                                  //发送者 
	CHAR *ptszBuffer;                                                    //接受到的数据，需要你自己管理缓冲区
}NETBIOS_DGRAM_RECV,*LPNETBIOS_DGRAM_RECV;
/************************************************************************/
/*                      SPI协议枚举导出数据结构                         */
/************************************************************************/
//通过枚举SPI得到的SPI协议信息
typedef struct tag_NetCore_SocketSpi_Protocols
{
	DWORD dwServiceFlags1;
	DWORD dwServiceFlags2;
	DWORD dwServiceFlags3;
	DWORD dwServiceFlags4;
	DWORD dwProviderFlags;
	GUID ProviderId;
	DWORD dwCatalogEntryId;
	typedef struct 
	{
		int ChainLen;							 
		DWORD ChainEntries[7];     
	} WSAPROTOCOLCHAIN;
	int iVersion;
	int iAddressFamily;
	int iMaxSockAddr;
	int iMinSockAddr;
	int iSocketType;
	int iProtocol;
	int iProtocolMaxOffset;
	int iNetworkByteOrder;
	int iSecurityScheme;
	DWORD dwMessageSize;
	DWORD dwProviderReserved;
	WCHAR  szProtocol[256];
}NETCORE_SOCKETSPI_PROTOCOLS,*LPNETCORE_SOCKETSPI_PROTOCOLS;
/************************************************************************/
/*                      本地网络信息结构                                */
/************************************************************************/
//网络参数信息
typedef struct tag_NetCore_SocketHelp_NetParam
{
	CHAR tszHostName[64];                                                  //本地电脑的主机名称
	CHAR tszDomainNmae[64];                                                //本地电脑注册的域名
	list<tstring> stl_ListDns;                                           //NDS服务器列表
}NETCORE_SOCKHELP_NETPARAM, *LPNETCORE_SOCKHELP_NETPARAM;
/************************************************************************/
/*                      异步消息选择模型事件处理消息结构体              */
/************************************************************************/
typedef struct tag_NetCore_AsycnSelect_Handle 
{
	DWORD dwEvent;                                                        //所属事件

	CHAR tszAddr[16];                                                    //客户IP地址
	int nPort;                                                            //客户端口号码
	//ACCEPT和CLOSE事件下面两个数据无用
	LPCSTR lpszRecvBuffer;                                               //接受到的数据
	int nRecvLen;                                                         //接受到的数据长度
}NETCORE_ASYNCSELECT_HANDLE, *LPNETCORE_ASYNCSELECT_HANDLE;
/************************************************************************/
/*                      原始套接字编程数据结构参数                          */
/************************************************************************/
typedef struct tag_NetCore_RawSocket_NetParam
{
	TCHAR tszSrcMac[8];                                                   //原始MAC地址(ARP协议需要)
	TCHAR tszDstMac[8];                                                   //目的MAC地址
	CHAR tszSrcAddr[64];                                                  //原始地址
	CHAR tszDstAddr[64];                                                  //目标地址
	int nSrcPort;                                                         //原端口
	int nDstPort;                                                         //目标端口
	USHORT usLen;                                                         //负载数据包大小，如果没有填入0
	//负载协议
	struct
	{
		int nWinSize;                                                     //滑动窗口大小
		int nSequeue;                                                     //当前序列号
		int nAck;                                                         //确认号
		CHAR cFlags;                                                      //当前标记
	}st_TCPHdr;
	struct
	{
		int nSequeue;
	}st_ICMPHdr;
}NETCORE_RAWSOCKET_NETPARAM, *LPNETCORE_RAWSOCKET_NETPARAM;
/************************************************************************/
/*                      UDX协议数据结构参数                             */
/************************************************************************/
//UDX配置信息
typedef struct tag_NetCore_UDXConfig 
{ 
	BOOL bEnableLogin;                                                    //是否启用登录离开模式
	BOOL bEnableReorder;                                                  //是否启用乱序重组
	BOOL bEnableRryTime;                                                  //是否启用重传超时
	BOOL bEnableLost;                                                     //是否允许最小丢包,如果不允许,丢包后将强制断开
	BOOL bEnableMtap;                                                     //是否启用聚合包发送数据,启用后将允许低延迟发送,不启用将无延迟发送
	int nWindowSize;                                                      //是否启用滑动窗口,0为不启用,大于0表示启用字节大小
}NETCORE_UDXCONFIG, *LPNETCORE_UDXCONFIG;
//////////////////////////////////////////////////////////////////////////
//                      回调函数
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                      串口通信数据接受回调函数                        */
/************************************************************************/
//接受串口的数据，第一个参数是数据来自哪个串口，第二个参数是数据，第三个是接受到的长度，第四个是自定义参数
typedef void(CALLBACK *CALLBACK_NETCORE_SERIALPORT_RECV)(LPCSTR,LPVOID,int,LPVOID); 
/************************************************************************/
/*                      事件选择模型服务器数据回调函数                  */
/************************************************************************/
//连接事件回调函数，为真为连接成功，为假为连接失败。第二个参数表示连接的IP和端口
typedef void(CALLBACK *CALLBACK_NETCORE_EVENTSELECT_SRV_CONNECT)(BOOL,LPCSTR,LPVOID);  
//接受到事件。第一个参数为接受到的用户IP地址，第二个参数为接受到的信息，第三个参数为接受到的数据大小
typedef void(CALLBACK *CALLBACK_NETCORE_EVENTSELECT_SRV_RECV)(LPCSTR, LPCSTR,int,LPVOID);
//用户离开事件，参数意思为离开的用户地址
typedef void(CALLBACK *CALLBACK_NETCORE_EVENTSELECT_SRV_LEAVE)(LPCSTR,LPVOID);         
/************************************************************************/
/*          Select TCP 服务器回调函数                                     */
/************************************************************************/
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_SERVER_SELECT_LOGIN)(LPCSTR,LPVOID);  //有客户进入，返回字符串客户地址和端口
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_SERVER_SELECT_RECV)(LPCSTR, LPCSTR,int,LPVOID);     //接受到数据，返回字符串的客户地址和接受到的数据
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_SERVER_SELECT_LEAVE)(LPCSTR,LPVOID);  //客户离开，返回客户离开的地址和端口号码
//轻量级服务器回调函数，参数：套接字句柄,客户端地址 客户端消息 消息长度 自定义数据
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_SERVER_SELECT_SCONNECT)(SOCKET m_Socket, LPCSTR lpszClientAddr,LPCSTR lpszMsgBuffer,int nMsgLen,LPVOID lParam);
/************************************************************************/
/*                      OVERLAPPED重叠IO数据回调函数                    */
/************************************************************************/
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_OVERLAPPED_LOGIN)(LPCSTR lpszAddr,LPVOID lParam);
//接受数据回调函数，参数意思：客户端地址 接受到的消息 消息的长度 自定义参数
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_OVERLAPPED_RECV)(LPCSTR lpszAddr, LPCSTR lpszRecvMsg,int nMsgLen,LPVOID lParam);
typedef void(CALLBACK* CALLBACK_NETCORE_TCP_OVERLAPPED_LEAVE)(LPCSTR lpszAddr,LPVOID lParam);
/************************************************************************/
/*          SELECT UDP 服务器回调函数                                      */
/************************************************************************/
typedef void(CALLBACK *CALLBACK_NETCORE_UDP_SERVER_SELEC_NETEVENT)(XNETHANDLE xhNet, LPCSTR lpszRecvAddr, LPCSTR lpszMsgBuffer, int nLen, LPVOID lParam);
/************************************************************************/
/*                      IOCP-TCP服务器数据回调函数                      */
/************************************************************************/
//如果返回FALSE,则表示过滤此IP的连接,不允许连接;否则就是此链接已经成功建立,下面的SOCKET参数如果是UDP将不起作用
typedef BOOL(CALLBACK *CALLBACK_NETCORE_TCP_IOCP_LOGIN)(LPCSTR lpszClientAddr, SOCKET hSocket, LPVOID lParam);
//数据收取
typedef void(CALLBACK *CALLBACK_NETCORE_TCP_IOCP_RECV)(LPCSTR lpszClientAddr, SOCKET hSocket, LPCSTR lpszRecvMsg, int nMsgLen, LPVOID lParam);
//有连接断开时IOCP调用此函数通知前台
typedef void(CALLBACK *CALLBACK_NETCORE_TCP_IOCP_LEAVE)(LPCSTR lpszClientAddr, SOCKET hSocket, LPVOID lParam);
/************************************************************************/
/*         套接字选项管理回调函数                                          */
/************************************************************************/
/*                        心跳管理                                       */
//心跳离开事件,根据模式返回不同参数
typedef void(CALLBACK* CALLBACK_NETCORE_SOCKOPT_HEARTBEAT_EVENT)(LPCTSTR lpszClientAddr, SOCKET hSocket,int nStatus,LPVOID lParam);
/************************************************************************/
/*                      管道数据回调函数                                */
/************************************************************************/
//命名管道：接受到的数据消息，参数： 哪个命名管道返回的数据消息，读取的数据缓冲区，缓冲区数据长度 
typedef void(CALLBACK* CALLBACK_NETCORE_PIPECOMMUNICATIONS_NAMED_READ)(LPCSTR,LPCSTR,int,LPVOID);
//邮槽：接受数据消息，参数意思：哪个邮槽的数据，数据的缓冲区，数据的长度 
typedef void(CALLBACK* CALLBACK_NETCORE_PIPECOMMUNICATIONS_MAILSLOT_READ)(LPCSTR,LPCSTR,DWORD,LPVOID);
/************************************************************************/
/*                      红外线数据查询回调函数                          */
/************************************************************************/
//打印找到的设备。参数意思：第几个设备，是否查询（为真为用户查询），设备ID，设备名称，对方平台
typedef void (CALLBACK* CALLBACK_NETCORE_WIRELESS_INFRARED_PRINTDEV)(int,BOOL,LPCSTR,LPCSTR,LPCSTR,LPVOID); 
/************************************************************************/
/*                      高速缓存事件触发回调函数                        */
/************************************************************************/
//告知用户缓存写到文件中的大小
typedef void(CALLBACK* CALLBACK_NETCORE_CACHEFILE_FLUSHEVENT)(LPCSTR lpszFileName,int nMemLength,LPVOID lParam);
/************************************************************************/
/*                      UDX回调函数                                     */
/************************************************************************/
//客户进入
typedef void(CALLBACK* CALLBACK_NETCORE_UDX_LOGIN)(LPCSTR lpszClientAddr, LPVOID lParam);
//客户离开
typedef void(CALLBACK* CALLBACK_NETCORE_UDX_LEAVE)(LPCSTR lpszClientAddr, LPVOID lParam);
//////////////////////////////////////////////////////////////////////////
//                      导出函数声明
//////////////////////////////////////////////////////////////////////////
//获取错误码
extern "C" DWORD NetCore_GetLastError(int *pInt_ErrorCode = NULL);        
/************************************************************************/
/*                      TCP导出函数声明                                 */
/************************************************************************/
/************************************************************************/
/*                   消息选择模型服务器导出函数                         */
/************************************************************************/
/********************************************************************
函数名称：NetCore_TCPWSAAsyncSelect_Init
函数功能：初始化消息选择模型
 参数.一：hWnd
  In/Out：In
  类型：句柄
  可空：N
  意思：要与哪个窗口句柄绑定
 参数.二：uMsg
  In/Out：In
  类型：无符号整数型
  可空：N
  意思：设置消息
返回值
  类型：逻辑型
  意思：是否成功初始化 只会返回真
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPWSAAsyncSelect_Init(HWND hWnd,UINT uMsg);
/********************************************************************
函数名称：NetCore_TCPWSAAsyncSelect_Close
函数功能：关闭服务器，回收内存
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPWSAAsyncSelect_Close();
/********************************************************************
函数名称：NetCore_TCPWSAAsyncSelect_Listen
函数功能：建立监听池
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要监听的端口
返回值
  类型：逻辑型
  意思：是否建立成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPWSAAsyncSelect_Listen(int nPort);
/********************************************************************
函数名称：NetCore_TCPWSAAsyncSelect_Send
函数功能：发送数据
 参数.一：lpszSendBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送数据的缓冲区
 参数.二：nBufLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的缓冲区长度
 参数.三：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：发送给哪个客户，如果为空表示广播数据 表达方式 IP:PORT
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPWSAAsyncSelect_Send(LPCSTR lpszSendBuffer,int nBufLen,LPCSTR lpszAddr = NULL);
/********************************************************************
函数名称：NetCore_WSAAsyncSelect_Connect
函数功能：新建一个连接到连接池中
 参数.一：lpszDestAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要连接到的IP地址
 参数.二：nDestPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要连接到的端口
 参数.三：nLocalPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：本地端口地址，默认不指定
返回值
  类型：逻辑型
  意思：是否成功建立连接
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPWSAAsyncSelectp_Connect(LPCSTR lpszDestAddr,int nDestPort,int nLocalPort = 0);
/********************************************************************
函数名称：NetCore_TCPWSAAsyncSelect_HandleMessage
函数功能：处理网络消息函数
 参数.一：wParam
  In/Out：In
  类型：消息高位
  可空：N
  意思：这个一般是指套接字句柄
 参数.二：lParam
  In/Out：In
  类型：消息地位
  可空：N
  意思：这个一般是指错误码和具体消息类型
 参数.三：pSt_AsyncSelect_Handle
  In/Out：Out
  类型：结构体指针
  可空：N
  意思：导出获取到的内容
返回值
  类型：逻辑型
  意思：是否成功处理消息
备注：你需要自己管理这个处理线程，所以你最好在线程中运行此函数
*********************************************************************/
extern "C" BOOL NetCore_TCPWSAAsyncSelect_HandleMessage(WPARAM wParam,LPARAM lParam,LPNETCORE_ASYNCSELECT_HANDLE pSt_AsyncSelect_Handle);
/************************************************************************/
/*                   事件选择模型服务器导出函数                         */
/************************************************************************/
/********************************************************************
函数名称：NetCore_TCPEventSelect_Init
函数功能：事件选择模型初始化启动服务器
 参数.一：u_nPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：服务器监听端口号
返回值
  类型：逻辑型
  意思：是否启动成功
备注：此模型目前只做消息转发用
*********************************************************************/
extern "C" BOOL NetCore_TCPEventSelect_Init(int u_nPort = 14567);
/********************************************************************
函数名称：NetCore_TCPEventSelect_SetCallBack
函数功能：事件选择模型服务器数据回调
 参数.一：fpCall_Connect
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：客户进入发生的信息
 参数.二：fpCall_Recv
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：服务器收到数据发生的信息
 参数.三：fpCall_Leave
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：客户端离开发生的回调
 参数.四：lPConnnect
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：客户进入回调函数参数
 参数.五：lPRecv
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：服务器收到数据发生的回调函数参数
 参数.六：lPLeave
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：客户端离开回调函数参数
返回值
  类型：逻辑型
  意思：是否设置成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPEventSelect_SetCallBack(CALLBACK_NETCORE_EVENTSELECT_SRV_CONNECT fpCall_Connect,CALLBACK_NETCORE_EVENTSELECT_SRV_RECV fpCall_Recv,CALLBACK_NETCORE_EVENTSELECT_SRV_LEAVE fpCall_Leave,LPVOID lPConnnect = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/********************************************************************
函数名称：NetCore_EventSelect_Tcp_Stop
函数功能：停止事件选择模型服务器
返回值
  类型：逻辑型
  意思：是否成功停止
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPEventSelectp_Stop();
/************************************************************************/
/*                   选择模型服务器导出函数                             */
/************************************************************************/
/************************************************************************
函数名称：NetCore_TCPSelect_Start
函数功能：初始化这个选择模型并且启动这个服务
  参数一：nPort
   In/Out：In
   可空：N
   意思：要绑定的端口
  参数二：nTimeOut
   In/Out：In
   可空：Y
   意思：超时时间设置。默认0.1秒
  参数三：bKeepAlive
   In/Out：In
   可空：Y
   意思：超时时间设置，默认不设置
返回值
  类型：BOOL
  意思：是否启动成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_Start(int nPort,int nTimeOut = 100,BOOL bKeepAlive = FALSE);
/********************************************************************
函数名称：NetCore_TCPSelect_Send
函数功能：异步IO发送数据给客户端
 参数.一：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的客户端，如果为NULL，那么表示发送给所有客户，如果不为NULL，那么发送给指定用户
 参数.二：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：发送缓冲区长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_Send(LPCSTR lpszAddr, LPCSTR lpszBuffer,int nLen);
/************************************************************************
函数名称：NetCore_TCPSelect_SetCallBack
函数功能：用户注册函数，如果你不关心这些事件，可以不用管！
  参数一：fpCall_Login
   In/Out：In
   类型：回调函数
   可空：N
   意思：用户进入事件
  参数二：fpCallS_Recv
   In/Out：In
   类型：回调函数
   可空：N
   意思：接受到数据事件
  参数三：fpCall_Leave
   In/Out：In
   类型：回调函数
   可空：N
   意思：用户离开事件
  参数四：lPRecv
   In/Out：In
   类型：无类型指针
   可空：N
   意思：接受数据自定义参数
  参数五：lPLogin
   In/Out：In
   类型：无类型指针
   可空：N
   意思：用户登录回调函数自定义参数
  参数六：lPLeave
   In/Out：In
   类型：无类型指针
   可空：N
   意思：用户离开自定义参数
返回值
  类型：无
  意思：
备注：在服务器启动的时候，你应该主动调用这个函数来处理4个事件，如果你不想通过回调的方式获取这些事件
      也就是说不想通过异步方式，那么你可以调用读取IO事件的函数来主动获取下面的事件
************************************************************************/
extern "C"  void NetCore_TCPSelect_SetCallBack(CALLBACK_NETCORE_TCP_SERVER_SELECT_LOGIN fpCall_Login,CALLBACK_NETCORE_TCP_SERVER_SELECT_RECV fpCall_Recv,CALLBACK_NETCORE_TCP_SERVER_SELECT_LEAVE fpCall_Leave,LPVOID lPConnect = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/************************************************************************
函数名称：NetCore_TCPSelect_Stop
函数功能：停止选择模型服务器
返回值
  类型：逻辑型
  意思：是否成功停止
备注：只会返回真
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_Stop();
/************************************************************************
函数名称：NetCore_TCPSelect_GetClientCount
函数功能：获取客户端链接数量
返回值
  类型：整数型
  意思：返回获取到的客户数量
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_GetClientCount();
/************************************************************************
函数名称：NetCore_TCPSelect_GetFlow
函数功能：获取服务器发送和接受到的流量，单位字节
  参数一：pdwFlow_Up
   In/Out：Out
   类型：指向无符号长整型的指针
   可空：N
   意思：上传的流量
  参数二：pdwFlow_Down
   In/Out：Out
   类型：指向无符号长整型的指针
   可空：N
   意思：下载的流量
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_GetFlow(DWORD *pdwFlow_Up,DWORD *pdwFlow_Down);
/********************************************************************
函数名称：NetCore_TCPSelect_RemoveClient
函数功能：移除一个指定的客户
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要移除的客户IP地址+端口 ip:port
返回值
  类型：逻辑型
  意思：是否成功移除
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_RemoveClient(LPCSTR lpszClientAddr);
/********************************************************************
函数名称：NetCore_TCPSelect_ReadIOEvent
函数功能：读取IO事件，主动模式
 参数.一：ptszAddr
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：导出发生事件的客户地址,输入：只有在接受数据事件才有用，表示查找这个客户地址的第一个数据
 参数.二：ptszBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出事件消息内容，只有RECV事件有效
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型
  可空：N
  意思：导出消息长度
 参数.四：pdwEvent
  In/Out：In/Out
  类型：双字
  可空：N
  意思：输入 要获取的事件类型，可以是ALL，ALL表示所有类型，那么将弹出事件驱动表中第一个事件，输出，如果是ALL，那么输出这个事件是什么事件
        当然，你也可以输入指定的事件类型，那么这个参数导出将没有作用
返回值
  类型：逻辑型
  意思：是否成功获取
备注：如果设置了回调，那么此函数将失效，就算你使用主动方式获取IO网络事件，网络内部也不会阻塞。请放心使用
      但是你也要及时处理，因为内部数据过多会造成服务器内存占用过大
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_ReadIOEvent(CHAR *ptszAddr,CHAR *ptszBuffer,int *pInt_MsgLen,DWORD *pdwEvent);
/********************************************************************
函数名称：NetCore_TCPSelect_ConvertToNetAddr
函数功能：把指定IP地址转换为网络地址
 参数.一：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要转换的IP地址
 参数.二：pdw_Addr
  In/Out：Out
  类型：双字指针
  可空：N
  意思：导出转换成功后的网络地址
返回值
  类型：逻辑型
  意思：是否转换成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_ConvertToNetAddr(LPCSTR lpszAddr,DWORD *pdw_Addr);
/********************************************************************
函数名称：NetCore_TCPSelect_GetAddrInfo
函数功能：获取服务器地址信息
 参数.一：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出绑定的IP地址
 参数.二：pInt_Port
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出绑定的端口号码
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_GetAddrInfo(CHAR *ptszClientAddr, int *pInt_Port);
//--------扩展SELECT TCP服务器函数-支持多个服务器，参数意思和上面的一样，多个了句柄参数而已
extern "C" BOOL NetCore_TCPSelect_StartEx(XNETHANDLE *pxNetId,int nPort,int nTimeOut = 100,BOOL bKeepAlive = FALSE);
extern "C" BOOL NetCore_TCPSelect_SendEx(XNETHANDLE xNetId,LPCSTR lpszAddr, LPCSTR lpszBuffer,int nLen);
extern "C" BOOL NetCore_TCPSelect_SendAllEx(XNETHANDLE xNetId, LPCTSTR lpszBuffer, int nLen);
extern "C" BOOL NetCore_TCPSelect_GetFlowEx(XNETHANDLE xNetId,DWORD *pdwFlow_Up,DWORD *pdwFlow_Down);
extern "C" BOOL NetCore_TCPSelect_RemoveClientEx(XNETHANDLE xNetId,LPCSTR lpszClientAddr);
extern "C" BOOL NetCore_TCPSelect_ReadIOEventEx(XNETHANDLE xNetId,CHAR *ptszAddr,CHAR *ptszBuffer,int *pInt_MsgLen,DWORD *pdwEvent);
extern "C" BOOL NetCore_TCPSelect_StopEx(XNETHANDLE xNetId,BOOL bIsClearFlow = TRUE);
extern "C" int NetCore_TCPSelect_GetClientCountEx(XNETHANDLE xNetId);
extern "C" void NetCore_TCPSelect_SetCallBackEx(XNETHANDLE xNetId,CALLBACK_NETCORE_TCP_SERVER_SELECT_LOGIN fpCall_Login,CALLBACK_NETCORE_TCP_SERVER_SELECT_RECV fpCall_Recv,CALLBACK_NETCORE_TCP_SERVER_SELECT_LEAVE fpCall_Leave,LPVOID lPLogin = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
extern "C" BOOL NetCore_TCPSelect_ConvertToNetAddrEx(XNETHANDLE xNetId,LPCSTR lpszAddr,DWORD *pdw_Addr);
extern "C" BOOL NetCore_TCPSelect_GetAddrInfoEx(XNETHANDLE xNetId, CHAR *ptszClientAddr, int *pInt_Port);
/************************************************************************/
/*          TCP轻量级服务器函数导出定义                                 */
/************************************************************************/
/************************************************************************
函数名称：NetCore_TCPSelect_ShortStart
函数功能：启动一个轻量级服务器
  参数一：nPort
   In/Out：In
   类型：整数型
   可空：N
   意思：要绑定的端口号
  参数二：lpCall_TcpSConnect
   In/Out：In
   类型：回调函数
   可空：N
   意思：导出用户发送过来的数据
  参数三：lParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：自定义数据
  参数四：bNotify
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否仅仅通知数据到达,如果是,那么将又你自己recv数据
返回值
  类型：逻辑型
  意思：是否启动成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortStart(int nPort,CALLBACK_NETCORE_TCP_SERVER_SELECT_SCONNECT lpCall_TcpSConnect,LPVOID lParam = NULL, BOOL bNotify = FALSE);
/************************************************************************
函数名称：NetCore_TCPSelect_ShortSend
函数功能：发送一段数据
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要给哪个客户端发送数据
  参数二：lpszMsgBuffer
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要发送的数据缓冲区
  参数三：nMsgLen
   In/Out：In
   类型：整数型
   可空：N
   意思：要发送数据的大小
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortSend(LPCSTR lpszClientAddr,LPCSTR lpszMsgBuffer,int nMsgLen);
/************************************************************************
函数名称：NetCore_TCPSelect_ShortClose
函数功能：关闭客户端
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要关闭的客户端
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortClose(LPCTSTR lpszClientAddr);
/************************************************************************
函数名称：NetCore_TCPSelect_ShortDestroy
函数功能：销毁短连接服务器
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortDestroy();
/********************************************************************
函数名称：NetCore_TCPSelect_ShortGetSocket
函数功能：获得监听句柄
返回值
  类型：套接字句柄
  意思：返回套接字监听句柄
备注：
*********************************************************************/
extern "C" SOCKET NetCore_TCPSelect_ShortGetSocket();
/************************************************************************/
/*                   重叠IO服务器导出函数                               */
/************************************************************************/
/********************************************************************
函数名称：NetCore_TCPOverLapped_SetCallBack
函数功能：设置重叠IO回调
 参数.一：fpCall_OLLogin
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：用户连接回调
 参数.二：fpCall_OLRecv
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：数据到达回调
 参数.三：fpCall_OLLeave
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：用户离开回调
 参数.四：lPLogin
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：连接回调自定义参数
 参数.五：lPRecv
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：数据到达回调自定义参数
 参数.六：lPLeave
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：用户离开回调自定义参数
返回值
  类型：逻辑型
  意思：是否设置成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPOverLapped_SetCallBack(CALLBACK_NETCORE_TCP_OVERLAPPED_LOGIN fpCall_OLLogin,CALLBACK_NETCORE_TCP_OVERLAPPED_RECV fpCall_OLRecv,CALLBACK_NETCORE_TCP_OVERLAPPED_LEAVE fpCall_OLLeave,LPVOID lPLogin = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/********************************************************************
函数名称：NetCore_TCPOverLapped_Start
函数功能：启动重叠IO模型的服务器
 参数.一：u_sPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要监听的端口
 参数.二：nClientCount
  In/Out：In
  类型：整数型
  可空：Y
  意思：最大客户端数量，默认为10000，超过后将被直接关闭连接
返回值
  类型：逻辑型
  意思：是否启动成功
备注：超过的连接客户端将激发离开事件，并且数据位 Max Connect
*********************************************************************/
extern "C" BOOL NetCore_TCPOverLapped_Start(int u_sPort,int nClientCount = 10000);
/********************************************************************
函数名称：NetCore_TCPOverLapped_Stop
函数功能：关闭重叠IO服务器
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPOverLapped_Stop();
/********************************************************************
函数名称：NetCore_TCPOverLapped_Send
函数功能：发送数据到指定客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的客户端地址
 参数.二：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：pInt_MsgLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：发送的数据长度，TCP是流式套接字，如果你发现发送的数据长度不是你指定的大小，你可以自己移动指针，我们这里不给你分包处理
 参数.四：bIsWait
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否等待完成，由于是重叠IO非阻塞，如果为真（默认），那么将会阻塞这个函数，并且直到这个发送成功完成，否则请为假，系统内部会自动判断，这样不会阻塞
返回值
  类型：逻辑型
  意思：是否投递成功
备注：如果返回值ERROR_NETCORE_TCP_OVERLAPPED_SEND_HASERROR 说明错误严重，你需要自己处理
*********************************************************************/
extern "C" BOOL NetCore_TCPOverLapped_Send(LPCSTR lpszClientAddr, LPCSTR lpszSendMsg,int *pInt_MsgLen,BOOL bIsWait = TRUE);
/************************************************************************/
/*                   TCP服务器IOCP模型导出函数                          */
/************************************************************************/
/********************************************************************
函数名称：NetCore_TCPIocp_Start
函数功能：启动一个IOCP服务器
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：要绑定的IOCP服务器端口
 参数.二：nMaxClient
  In/Out：In
  类型：整数型
  可空：Y
  意思：最大允许的客户端数量
 参数.三：nThreads
  In/Out：In
  类型：整数型
  可空：Y
  意思：设置网络事件线程池启动数量,0将根据CPU个数启动
 参数.四：bKeepAlive
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否开启TCP心跳功能,默认不开启
返回值
  类型：逻辑型
  意思：是否启动成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_StartEx(XNETHANDLE *pxhNet, int nPort = 5001, int nMaxClient = 10000, int nThreads = 0, BOOL bKeepAlive = FALSE);
/********************************************************************
函数名称：NetCore_TCPIocp_Destroy
函数功能：销毁IOCP服务器
 参数.一：bIsClearFlow
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否清理上传下载的数据,默认清理
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_DestroyEx(XNETHANDLE xhNet, BOOL bIsClearFlow = TRUE);
/********************************************************************
函数名称：NetCore_TCPIocp_SendMsg
函数功能：发送数据
 参数.一：lpszSendAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要给哪个客户端发送
 参数.二：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的数据长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_SendEx(XNETHANDLE xhNet, LPCTSTR lpszSendAddr, LPCTSTR lpszSendMsg, int nMsgLen);
/********************************************************************
函数名称：NetCore_TCPIocp_SendAll
函数功能：发送数据给所有客户端
 参数.一：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.二：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的数据长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_SendAllEx(XNETHANDLE xhNet, LPCTSTR lpszSendMsg, int nMsgLen);
/************************************************************************
函数名称：NetCore_TCPIocp_GetAllEx
函数功能：获取所有客户端列表
  参数.一：pStl_ListClient
   In/Out：Out
   类型：LIST容器指针
   可空：N
   意思：导出获取到的客户端地址列表
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPIocp_GetAllEx(XNETHANDLE xhNet, list<tstring> *pStl_ListClient);
/********************************************************************
函数名称：NetCore_TCPIocp_CloseForClient
函数功能：关闭一个连接到服务器的客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要关闭哪个客户端
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_CloseForClientEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：NetCore_TCPIocp_GetFlow
函数功能：获取IOCP服务发送和接受的流量信息
 参数.一：pullFlow_Up
  In/Out：Out
  类型：四字指针
  可空：N
  意思：总共发送了多少字节
 参数.二：pullFlow_Down
  In/Out：Out
  类型：四字指针
  可空：N
  意思：总共接受了多少字节
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_GetFlowEx(XNETHANDLE xhNet, DWORD64 *pullFlow_Up, DWORD64 *pullFlow_Down);
/************************************************************************
函数名称：NetCore_TCPIocp_SetStatus
函数功能：设置客户端状态
 参数.一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：客户端地址
 参数.二：bIsBreak
   In/Out：In
   类型；逻辑型
   可空：N
   意思：为真设置客户端跳过，不处理发送和接受，为假不跳过
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPIocp_SetStatusEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, BOOL bIsBreak);
/********************************************************************
函数名称：NetCore_TCPIocp_RegisterCallBack
函数功能：注册IOCP回调函数
 参数.一：fpCallePoll_Login
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：注册用户连接回调函数
 参数.二：fpCallePoll_Recv
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：注册用户数据到达回调函数
 参数.三：fpCallePoll_Leave
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：注册用户离开回调函数
 参数.四：lPLogin
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：登录回调的自定义参数
 参数.五：lPRecv
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：数据回调的自定义参数
 参数.六：lPLeave
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：离开回调的自定义参数
返回值
  类型：逻辑型
  意思：是否注册成功
备注：不注册无法使用IOCP
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_RegisterCallBackEx(XNETHANDLE xhNet, CALLBACK_NETCORE_TCP_IOCP_LOGIN fpCallePoll_Login, CALLBACK_NETCORE_TCP_IOCP_RECV fpCallePoll_Recv, CALLBACK_NETCORE_TCP_IOCP_LEAVE fpCallePoll_Leave, LPVOID lPLogin = NULL, LPVOID lPRecv = NULL, LPVOID lPLeave = NULL);
/********************************************************************
函数名称：NetCore_TCPIocp_GetAddrForSocket
函数功能：通过SOCKET句柄获取IP地址
 参数.一：hSocket
  In/Out：In
  类型：套接字
  可空：N
  意思：要获取地址的SOCKET句柄
 参数.二：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：获取到的客户端IP地址
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_GetAddrForSocketEx(XNETHANDLE xhNet, SOCKET hSocket, CHAR *ptszClientAddr);
/********************************************************************
函数名称：NetCore_TCPIocp_GetSocketForAddr
函数功能：通过IP地址获取SOCKET
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取的客户端IP地址
 参数.一：hSocket
  In/Out：Out
  类型：套接字
  可空：N
  意思：获取到的SOCKET句柄
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPIocp_GetSocketForAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_TCPIocp_GetRecvTime
函数功能：获取一个客户端距离上次接受数据相差多少秒
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：用户地址
 参数.二：pInt_Timer
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出获取的相差时间，如果是0,表示还没有接受过数据
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPIocp_GetRecvTimeEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, int *pInt_Timer);
/************************************************************************/
/*                      UDP导出函数声明                                 */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UDPIocp_Start
函数功能：启动一个IOCP服务器
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：要绑定的IOCP服务器端口
 参数.二：nThreads
  In/Out：In
  类型：整数型
  可空：Y
  意思：要设置网络事件处理线程数量,0为使用CPU个数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPIocp_StartEx(XNETHANDLE *pxhNet, int nPort = 5000, int nThreads = 0);
/********************************************************************
函数名称：NetCore_UDPIocp_Destroy
函数功能：销毁IOCP服务器
 参数.一：bIsClearFlow
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否清理上传下载的数据,默认清理
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPIocp_DestroyEx(XNETHANDLE xhNet, BOOL bIsClearFlow = TRUE);
/********************************************************************
函数名称：NetCore_UDPIocp_SendMsg
函数功能：发送数据
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要给哪个客户端发送
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：pInt_Len
  In/Out：In
  类型：整数型指针
  可空：N
  意思：输入发送的数据缓冲区长度，输出真实发送数据长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPIocp_SendMsgEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, LPCSTR lpszMsgBuffer, int *pInt_Len);
/********************************************************************
函数名称：NetCore_UDPIocp_GetFlow
函数功能：获取IOCP服务发送和接受的流量信息
 参数.一：pullFlow_Up
  In/Out：Out
  类型：四字指针
  可空：N
  意思：总共发送了多少字节
 参数.二：pullFlow_Down
  In/Out：Out
  类型：四字指针
  可空：N
  意思：总共接受了多少字节
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPIocp_GetFlowEx(XNETHANDLE xhNet, DWORD64 *pullFlow_Up, DWORD64 *pullFlow_Down);
/********************************************************************
函数名称：NetCore_UDPIocp_RegisterCallBack
函数功能：注册IOCP回调函数
 参数.一：fpCallePoll_Recv
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：注册用户数据到达回调函数
 参数.二：lPRecv
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：数据回调的自定义参数
返回值
  类型：逻辑型
  意思：是否注册成功
备注：不注册无法使用IOCP
*********************************************************************/
extern "C" BOOL NetCore_UDPIocp_RegisterCallBackEx(XNETHANDLE xhNet, CALLBACK_NETCORE_TCP_IOCP_RECV fpCallePoll_Recv, LPVOID lPRecv = NULL);
/************************************************************************/
/*                             UDPSelect                                */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UDPSelect_Init
函数功能：初始化一个UDP服务或者客户端
 参数.一：pxhNet
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出初始化成功的UDP服务句柄
 参数.二：nBindPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：是否需要提供一个服务，默认不需要
 参数.三：bIsCall
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：设置启动模式，直接回调还是主动接受
 参数.四：fpCall_UDPEvent
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：输入数据接受回调函数地址
 参数.五：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功
备注：模式设置必须要回调函数有值才有作用
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_Init(XNETHANDLE *pxhNet, int nBindPort = 0, BOOL bIsCall = FALSE, CALLBACK_NETCORE_UDP_SERVER_SELEC_NETEVENT fpCall_UDPEvent = NULL, LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_UDPSelect_SendTo
函数功能：发送一条UDP数据给指定的服务器
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要发送的缓冲区数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型
  可空：N
  意思：输入要发送数据大小，输出真实发送数据大小
 参数.四：lpszSendAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要发送到的UDP服务地址
 参数.五：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要发送数据到的UDP服务器端口
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己判断实际发送大小是否等于需要发送大小
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_SendTo(XNETHANDLE xhNet, LPCTSTR lpszMsgBuffer, int *pInt_Len, LPCTSTR lpszSendAddr, int nPort);
/********************************************************************
函数名称：NetCore_UDPSelect_Recv
函数功能：接受一条UDP数据
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
 参数.四：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出接受数据的UDP发送过来的地址和端口
 参数.三：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出接受到的数据缓冲区
 参数.四：pInt_Len
  In/Out：In/Out
  类型：整数型
  可空：N
  意思：输入要接受数据大小，输出真实接受数据大小
 参数.五：bSelect
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否启用SELECT判断,默认不使用
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_Recv(XNETHANDLE xhNet, CHAR *ptszClientAddr, CHAR *ptszMsgBuffer, int *pInt_Len, BOOL bSelect = FALSE);
/********************************************************************
函数名称：NetCore_UDPSelect_Stop
函数功能：停止一个UDP服务
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_Stop(XNETHANDLE xhNet);
/********************************************************************
函数名称：NetCore_UDPSelect_Stop
函数功能：停止一个UDP服务
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
 参数.二：bIsCall
  In/Out：In
  类型：逻辑型
  可空：N
  意思：输入要设置的模式，真为被动回调，假为主动接受
返回值
  类型：逻辑型
  意思：是否成功
备注：这个函数的作用必须是初始化设置了回调函数地址才有作用
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_SetMode(XNETHANDLE xhNet, BOOL bIsCall = TRUE);
/************************************************************************/
/*          原始套机字接口定义                                             */
/************************************************************************/
/************************************************************************
函数名称：NetCore_ICMPRaw_Init
函数功能：初始化一个ICMP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_ICMPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_ICMPRaw_Get
函数功能：设置ICMP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入ICMP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（IP+ICMP）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_ICMPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket, TCHAR *ptszMsgBuffer, int *pInt_Len);
/************************************************************************
函数名称：NetCore_TCPRaw_Init
函数功能：初始化一个TCP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_TCPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_TCPRaw_Get
函数功能：设置TCP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入TCP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（IP+TCP）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：导出的数据只是协议头，不包含附加数据，你需要自己附加数据后发送
************************************************************************/
extern "C" BOOL NetCore_TCPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket, TCHAR *ptszMsgBuffer, int *pInt_Len);
/************************************************************************
函数名称：NetCore_UDPRaw_Init
函数功能：初始化一个UDP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_UDPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_UDPRaw_Get
函数功能：设置UDP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入UDP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（IP+UDP）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_UDPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket, TCHAR *ptszMsgBuffer, int *pInt_Len);
/************************************************************************
函数名称：NetCore_ARPRaw_Init
函数功能：初始化一个ARP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_ARPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_ARPRaw_Get
函数功能：设置ARP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入ARP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（以太网头+ARP协议）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_ARPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket, TCHAR *ptszMsgBuffer, int *pInt_Len);
//////////////////////////////////////////////////////////////////////////
//                          串口通信导出函数
//////////////////////////////////////////////////////////////////////////
/********************************************************************
函数名称：NetCore_SerialPort_OpenDev
函数功能：初始化异步串口操作模块
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要初始化哪个串口号
 参数.二：dwBaudRate
  In/Out：In
  类型：双字
  可空：Y
  意思：波特率，默认2400
 参数.三：byParity
  In/Out：In
  类型：无符号字符
  可空：Y
  意思：串口事件，默认无事件
 参数.四：byStopBits
  In/Out：In
  类型：无符号字符
  可空：Y
  意思：停止位，默认1个停止位
 参数.五：byByteSize
  In/Out：In
  类型：无符号字符
  可空：Y
  意思：初始化大小，数据大小，默认8
 参数.六：fpCall_SerialPortRecv
  In/Out：Out
  类型：回调函数
  可空：Y
  意思：回调函数地址，可空，如果空将通过主动方式接受数据
 参数.七：lParam
  In/Out：In
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_OpenDev(LPCSTR lpszComPort,DWORD dwBaudRate = CBR_2400,BYTE byParity = NETENGINE_NETCORE_SERIALPORT_PARITYNONE,BYTE byStopBits = 1,BYTE byByteSize = 8,CALLBACK_NETCORE_SERIALPORT_RECV fpCall_SerialPortRecv = NULL,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_SerialPort_SendData
函数功能：异步发送串口数据
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据字符串指针
 参数.二：byDates
  In/Out：In
  类型：字节指针
  可空：N
  意思：要发送的数据
 参数.三：nWriteLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：如果错误你需要获取错误码来知道为什么错误
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_SendData(LPCSTR lpszComPort,BYTE *byDates,int nWriteLen);
/********************************************************************
函数名称：NetCore_SerialPort_RecvData
函数功能：异步读取数据
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个串口的数据
 参数.二：lpVoidData
  In/Out：Out
  类型：无类型指针
  可空：N
  意思：导出读取到的数据
 参数.三：pdwReadLen
  In/Out：In/Out
  类型：双字指针
  可空：N
  意思：输入要读取多大的数据，输出读取到的实际数据大小
返回值
  类型：逻辑型
  意思：是否读取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_RecvData(LPCSTR lpszComPort,BYTE *byDates,int *pInt_Len);
/********************************************************************
函数名称：NetCore_SerialPort_CloseDev
函数功能：关闭指定串口
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要关闭的串口号，如果为NULL，关闭所有此模块保存的串口表
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：不在使用串口的时候可以关闭它。不然会造成下次使用出现问题
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_CloseDev(LPCSTR lpszComPort = NULL); 
/************************************************************************
函数名称：NetCore_SerialPort_IsOpenDev
函数功能：是否打开了指定的串口
  参数一：lpszComPort
   In/Out：In
   类型：常量字符指针 
   可空：N
   意思：要判断的串口号
返回值
  类型：逻辑型
  意思：为真为打开，为假为没有打开
备注：  
************************************************************************/
extern "C" BOOL NetCore_SerialPort_IsOpenDev(LPCSTR lpszComPort);
/************************************************************************/
/*                      套接字帮助函数导出                              */
/************************************************************************/
/********************************************************************
函数名称：NetCore_SocketHelp_IsPortOccupation
函数功能：端口是否被使用
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要检查的端口号码
 参数.二：nProto
  In/Out：In
  类型：整数型
  可空：Y
  意思：协议类型，要查找端口的所属协议
返回值
  类型：逻辑型
  意思：是否被使用，假为没有
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_IsPortOccupation(int nPort, int nProto);
/********************************************************************
函数名称：NetCore_SocketHelp_GetPortState
函数功能：获取本地端口状态
 参数.一：uPort
  In/Out：In
  类型：双字
  可空：N
  意思：要检查的端口号
 参数.二：pSt_NetState
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出占用信息
返回值
  类型：逻辑型
  意思：返回假后你需要判断错误码，ERROR_NETCORE_NETHELP_NETSTATE_NOTFOUND 这个表示没有被占用 否则返回真 占用
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetPortState(DWORD uPort, NETCORE_NETHELP_NETSTATE *pSt_NetState);
/********************************************************************
函数名称：NetCore_SocketHelp_GetNetConnectType
函数功能：获取网络连接类型
 参数.一：pdw_Type
  In/Out：Out
  类型：双字
  可空：N
  意思：导出网络连接方式
返回值
  类型：逻辑型
  意思：时候正确获取到连接类型
备注：返回真后通过参数获取
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetNetConnectType(DWORD *pdw_Type);
/********************************************************************
函数名称：NetCore_SocketHelp_GetProtocolStatics
函数功能：获取网络协议状态信息
 参数.一：pSt_IpStatics
  In/Out：Out
  类型：结构体指针
  可空：Y
  意思：IP协议统计信息
 参数.二：pSt_TcpStatics
  In/Out：Out
  类型：结构体指针
  可空：Y
  意思：导出TCP协议状态统计信息
 参数.三：pSt_UdpStatics
  In/Out：Out
  类型：结构体指针
  可空：Y
  意思：导出UDP协议状态统计信息
 参数.四：pSt_IcmpStatics
  In/Out：Out
  类型：结构体指针
  可空：Y
  意思：导出ICMP协议状态统计信息
 参数.五：dwProtocolVer
  In/Out：In
  类型：双字
  可空：Y
  意思：要获取的IP版本，默认是IPV4
返回值
  类型：逻辑型
  意思：是否获取到指定协议状态信息
备注：前面的协议结构参数允许多个或者单个为空，那么将不取这些信息 
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetProtocolStatics(LPNETCORE_PROTOCOL_IPSTATICS pSt_IpStatics,LPNETCORE_PROTOCOL_TCPSTATICS pSt_TcpStatics,LPNETCORE_PROTOCOL_UDPSTATICS pSt_UdpStatics,LPNETCORE_PROTOCOL_ICMPSTATICS pSt_IcmpStatics,DWORD dwProtocolVer = NETCORE_SOCKETHELP_NETSTATE_VERSION_IPV4);
/********************************************************************
函数名称：NetCore_SocketHelp_ProcessNet
函数功能：获取进程TCP网络连接列表
 参数.一：pSt_NetCore_ProcessTable
  In/Out：Out
  类型：结构体指针
  可空：N
  意思：输出网络进程列表TCP连接
 参数.二：pInt_TcpCount
  In/Out：Out
  类型：整数型
  可空：N
  意思：导出建立TCP连接的进程个数
 参数.三：pInt_UdpCount
  In/Out：Out
  类型：整数型
  可空：N
  意思：导出建立UDP连接的进程个数
返回值
  类型：逻辑型
  意思：是否成功获取到
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_ProcessNet(LPNETCORE_NET_PROCESSTABLE pSt_NetCore_ProcessTable,int *pInt_TcpCount,int *pInt_UdpCount);
/********************************************************************
函数名称：NetCore_SocketHelp_SetProcessNetState
函数功能：设置网络程序连接状态
 参数.一：pSt_NetCore_ProcessTable
  In/Out：In
  类型：结构体
  可空：N
  意思：通过获取进程连接得到的进程描述表
 参数.二：dwNetState
  In/Out：In
  类型：双字
  可空：N
  意思：要设置的进程网络状态
返回值
  类型：逻辑型
  意思：是否成功设置
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_SetProcessNetState(NETCORE_NET_PROCESSTABLE st_NetCore_ProcessTable,DWORD dwNetState);
/********************************************************************
函数名称：NetCore_SocketHelp_GetNetParam
函数功能：获取网络接口信息
 参数.一：pSt_NetParam
  In/Out：Out
  类型：结构体指针
  可空：N
  意思：导出的网络信息，参考结构体定义
返回值
  类型：逻辑型
  意思：是否成功获取
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetNetParam(NETCORE_SOCKHELP_NETPARAM *pSt_NetParam);
/********************************************************************
函数名称：NetCore_SocketHelp_GetHostName
函数功能：获取主机地址名称
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取的主机名称
 参数.二：bIsPutList
  In/Out：In
  类型：逻辑型
  可空：N
  意思：是否获取所有地址
 参数.三：pStl_ListIPAddr
  In/Out：Out
  类型：LIST容器指针
  可空：N
  意思：输出获取到的地址列表
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetHostName(LPCSTR lpszHostName, BOOL bIsPutList, list<string> *pStl_ListIPAddr);
/********************************************************************
函数名称：NetCore_SocketHelp_DomainToAddr
函数功能：域名转IP地址
 参数.一：lpszDomain
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入一个标准的域名地址
 参数.二：ptszIPAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出转为到的IP地址
返回值
  类型：逻辑型
  意思：是否转换成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_DomainToAddr(LPCTSTR lpszDomain, CHAR *ptszIPAddr);                             
/********************************************************************
函数名称：NetCore_SocketHelp_GetNetCardList
函数功能：获取网卡列表信息
 参数.一：pStl_ListIFInfo
  In/Out：Out
  类型：STL列表容器指针
  可空：Y
  意思：输出获取到的网卡信息列表
 参数.二：lpszIPAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：判断这个IP地址是否存在于当前网卡（是否为本地IP地址），真表示存在
返回值
  类型：逻辑型
  意思：是否获取成功
备注：两个参数不能同时使用，也不能同时不使用
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetNetCardList(list<NETCORE_NETHELP_NETCARD> *pSt_ListIFInfo = NULL, LPCTSTR lpszIPAddr = NULL);
/************************************************************************/
/*                      网络套接字高级操作导出函数                      */
/************************************************************************/
/************************************************************************/
/*                      组播通信函数导出                                 */
/********************************************************************
函数名称：NetCore_GroupCast_SDCreate
函数功能：创建一个发送者组播
 参数.一：phSocket
  In/Out：Out
  类型：网络套接字
  可空：N
  意思：导出创建好的组播发送套接字
 参数.二：lpszSendAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送到的组播地址，搜索获得用户可使用的组播地址范围
 参数.三：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送到的端口
 参数.四：nTTL
  In/Out：In
  类型：整数型
  可空：Y
  意思：要设置组播跳转的TTL值，可以为空，不设置，采用默认
 参数.五：bLoop
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否允许本地回环数据，默认允许，windows下这个选项无效
返回值
  类型：逻辑型
  意思：是否创建成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_SDCreate(SOCKET *phSocket,LPCTSTR lpszSendAddr,int nPort,int nTTL = 0, BOOL bLoop = TRUE);
/********************************************************************
函数名称：NetCore_GroupCast_RVCreate
函数功能：创建一个接受组播服务
 参数.一：phSocket
  In/Out：Out
  类型：网络套接字
  可空：N
  意思：导出创建好的组播套接字
 参数.二：lpszRecvAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要接受的组播地址，必须和发送创建的一样的地址
 参数.三：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要接受到的端口，和发送者一样的端口
 参数.四：bLoop
  In/Out：In
  类型：整数型
  可空：N
  意思：是否接受回环数据，如果为真，那么本机发送出来的数据能够接受得到，否则将不会接受，LINUX下这个参数不起作用
返回值
  类型：逻辑型
  意思：是否创建成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_RVCreate(SOCKET *phSocket,LPCTSTR lpszRecvAddr,int nPort,BOOL bLoop = TRUE);
/********************************************************************
函数名称：NetCore_GroupCast_SDend
函数功能：发送者发送消息
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：操作哪个组播套接字
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送的消息
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：发送消息的长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_SDend(SOCKET hSocket,LPCTSTR lpszMsgBuffer,int nLen);
/********************************************************************
函数名称：NetCore_GroupCast_RVecv
函数功能：接收者接受组播消息
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：操作哪个组播套接字
 参数.二：ptszMsgBuffer
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：要接受的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出接受到的数据缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功接受
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_RVecv(SOCKET hSocket,CHAR *ptszMsgBuffer,int *pInt_Len);
/********************************************************************
函数名称：NetCore_GroupCast_Close
函数功能：关闭一个组播服务
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：要关闭哪个组播
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_Close(SOCKET hSocket);
/*                      广播通信函数导出定义                               */
/********************************************************************
函数名称：NetCore_BroadCast_SendInit
函数功能：初始化发送端
 参数.一：phSocket
  In/Out：Out
  类型：套接字指针
  可空：N
  意思：导出的套接字
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送端口
 参数.三：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果有多张网卡，那么需要指定哪张网卡IP地址发送广播包
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_SendInit(SOCKET *phSocket,int nPort,LPCTSTR lpszAddr = NULL);
/********************************************************************
函数名称：NetCore_BroadCast_Send
函数功能：发送广播消息
 参数.一：hSocket
  In/Out：In
  类型：套接字
  可空：N
  意思：要给哪个套接字发送数据
 参数.二：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的内容
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：UDP发送方式，如果超过MTU，会被分片，建议大数据包使用组包起模块配合使用.
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_Send(SOCKET hSocket,LPCTSTR lpszSendMsg,int nLen);
/********************************************************************
函数名称：NetCore_BroadCast_RecvInit
函数功能：初始化接受数据
 参数.一：phSocket
  In/Out：Out
  类型：套接字指针
  可空：N
  意思：导出的套接字
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要接受数据的端口
返回值
  类型：逻辑型
  意思：是否成功初始化
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_RecvInit(SOCKET *phSocket,int nPort);
/********************************************************************
函数名称：NetCore_BroadCast_Recv
函数功能：接受广播数据
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：要接受哪个套接字的数据
 参数.二：ptszBuffer
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：输入足够大的缓冲区，输出接受到的数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入要接受的缓冲区大小，输出接受到的缓冲区大小
 参数.四：ptszAddr
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：输出这条数据发送者的IP地址信息
返回值
  类型：逻辑型
  意思：是否成功接受到数据
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_Recv(SOCKET hSocket,CHAR *ptszBuffer,int *pInt_Len, CHAR *ptszAddr = NULL);
/********************************************************************
函数名称：NetCore_BroadCast_Close
函数功能：关闭一个指定的广播服务
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：要关闭的套接字句柄
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_Close(SOCKET hSocket);
/*                      心跳管理功能导出函数                        */
/********************************************************************
函数名称：SocketOpt_HeartBeat_Init
函数功能：初始化心跳服务
 参数.一：nTimeOut
  In/Out：In
  类型：套接字指针
  可空：Y
  意思：每隔多少秒检测一次心跳
 参数.二：nTimeNumber
  In/Out：In
  类型：整数型
  可空：Y
  意思：超过多少次没有心跳认为断线
 参数.三：fpCall_HeartBeatEvent
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：心跳超时后的回调函数
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数的参数
 参数.五：bIsAddr
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否启用客户端地址模式,只允许同时使用一个模式
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：如果回调函数没有设置，你需要通过获取超时函数来得到超时的用户
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_InitEx(XNETHANDLE *pxhNet, int nTimeOut = 5, int nTimeNumber = 3, CALLBACK_NETCORE_SOCKOPT_HEARTBEAT_EVENT fpCall_HeartBeatEvent = NULL, LPVOID lParam = NULL, BOOL bIsAddr = TRUE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_Destory
函数功能：销毁心跳管理
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_DestoryEx(XNETHANDLE xhNet);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ForceOutAddr
函数功能：强制让一个客户端心跳超时
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要超时的客户端地址ID
 参数.二：nStatus
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：强制退出用户信息状态
 参数.三：bIgnore
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否忽略这个地址是否在心跳管理器中，默认不忽略
返回值
  类型：逻辑型
  意思：是否成功
备注：在某些极端情况下，开发人员可能需要这样的功能
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ForceOutAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, int nStatus = 0, BOOL bIgnore = FALSE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ForceOutSkt
函数功能：强制让一个客户端心跳超时
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要超时的客户端句柄
 参数.二：nStatus
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：强制退出用户信息状态
 参数.三：bIgnore
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否忽略这个地址是否在心跳管理器中，默认不忽略
返回值
  类型：逻辑型
  意思：是否成功
备注：在某些极端情况下，开发人员可能需要这样的功能
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ForceOutSktEx(XNETHANDLE xhNet, SOCKET hSocket, int nStatus = 0, BOOL bIgnore = FALSE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_InsertAddr
函数功能：插入一个客户端到心跳管理器
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要激活的客户端，唯一标识符，可以是任意字符串
返回值
  类型：逻辑型
  意思：是否激活成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_InsertAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：SocketOpt_HeartBeat_InsertSocket
函数功能：插入一个客户端到心跳管理器
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要激活的客户端，唯一标识符
返回值
  类型：逻辑型
  意思：是否激活成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_InsertSocketEx(XNETHANDLE xhNet, SOCKET hSocket);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ActiveAddr
函数功能：激活一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要激活的客户端地址，唯一标识符，可以是任意字符串
 参数.二：pSt_ProtocolHeartBeat
  In/Out：In
  类型：数据结构指针
  可空：Y
  意思：心跳协议,可以为NULL
返回值
  类型：逻辑型
  意思：是否激活成功
备注：每收到一次心跳消息，都需要调用此函数进行激活，超过设定时间将被认为断线
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ActiveAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, NETENGINE_PROTOCOLHEARTBEAT *pSt_ProtocolHeartBeat = NULL);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ActiveSocket
函数功能：激活一个客户端
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要激活的客户端，唯一标识符
 参数.二：pSt_ProtocolHeartBeat
  In/Out：In
  类型：数据结构指针
  可空：Y
  意思：心跳协议
返回值
  类型：逻辑型
  意思：是否激活成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ActiveSocketEx(XNETHANDLE xhNet, SOCKET hSocket, NETENGINE_PROTOCOLHEARTBEAT *pSt_ProtocolHeartBeat = NULL);
/********************************************************************
函数名称：SocketOpt_HeartBeat_DeleteAddr
函数功能：从心跳管理中删除一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的唯一标识符
返回值
  类型：逻辑型
  意思：是否删除成功
备注：心跳被动离开不需要调用此函数，内部会自动删除。只有你主动删除才需要调用
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_DeleteAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：SocketOpt_HeartBeat_DeleteSocket
函数功能：从心跳管理中删除一个客户端
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要删除的唯一标识符
返回值
  类型：逻辑型
  意思：是否删除成功
备注：心跳被动离开不需要调用此函数，内部会自动删除。只有你主动删除才需要调用
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_DeleteSocketEx(XNETHANDLE xhNet, SOCKET hSocket);
/********************************************************************
函数名称：SocketOpt_HeartBeat_GetTimeOut
函数功能：从心跳管理中获取一个超时的客户端标识符
 参数.一：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：获取到的唯一标识符
 参数.二：phSocket
  In/Out：Out
  类型：套接字句柄
  可空：Y
  意思：获取到的唯一标识符
返回值
  类型：逻辑型
  意思：是否获取成功
备注：如果设置了回调函数，这个函数将不起作用，此方式你需要手动调用函数删除超时客户端
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_GetTimeOutEx(XNETHANDLE xhNet, CHAR *ptszClientAddr = NULL, SOCKET *phSocket = NULL);
/********************************************************************
函数名称：SocketOpt_HeartBeat_SetLoadAttr
函数功能：设置负载属性
 参数.一：nTimeCal
  In/Out：In
  类型：整数型
  可空：N
  意思：要设置每次计算负载属性的时间间隔（秒）
 参数.二：bOPen
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否开启还是关闭
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_SetLoadAttrEx(XNETHANDLE xhNet, int nTimeCal, BOOL bOPen = FALSE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_GetLoadAttr
函数功能：获取负载属性
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：可以为NULL，表示通过参数二套接字获取客户端负载属性
 参数.二：hSocket
  In/Out：In
  类型：套接字句柄
  可空：Y
  意思：可以为0,表示通过参数一客户端地址获取负载属性
 参数.三：pSt_HBLoad
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出客户端负载属性
 参数.四：pSt_LoadRate
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：导出客户端负载率
返回值
  类型：逻辑型
  意思：是否成功
备注：参数1和参数2不能同时为NULL
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_GetLoadAttrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, SOCKET hSocket, NETCORE_SOCKOPT_HBLOAD *pSt_HBLoad, NETCORE_SOCKOPT_LOADRATE *pSt_LoadRate = NULL);
/************************************************************************/
/*                      NetBios函数定义导出                             */
/************************************************************************/
/********************************************************************
函数名称：NetCore_NetBios_Stream_Start
函数功能：启动NETBIOS服务器
 参数.一：lpszName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要启动的名称
返回值
  类型：逻辑型
  意思：是否启动成功
备注：初始化NetBIOS接口，分配一些资源，及后在每个使用异步侦听事件的LANA。等待事件被触发，然后处理客户端连接。
*********************************************************************/
extern "C" BOOL NetCore_NetBios_Stream_Start(LPCSTR lpszName);
/********************************************************************
函数名称：NetCore_NetBios_Stream_Stop
函数功能：停止面向流的NETBIOS服务
 参数.一：lpszName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要停止的服务
返回值
  类型：逻辑型
  意思：是否成功停止
备注：
*********************************************************************/
extern "C" BOOL NetCore_NetBios_Stream_Stop(LPCSTR lpszName);
/********************************************************************
函数名称：NetCore_NetBios_DGram_Start
函数功能：启动数据报NETBIOS服务
返回值
  类型：逻辑型
  意思：是否成功启动
备注：
*********************************************************************/
extern "C" BOOL NetCore_NetBios_DGram_Start();
/********************************************************************
函数名称：NetCore_NetBios_DGram_SetMode
函数功能：设置数据报流模式
 参数.一：lpszLocalName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：本地NETBIOS名称
 参数.二：dwType
  In/Out：In
  类型：无符号长整数型
  可空：N
  意思：类型
 参数.三：dwLana
  In/Out：In
  类型：无符号长整数型
  可空：N
  意思：LANA编号
 参数.四：dwLenDatagrams
  In/Out：In
  类型：无符号长整数型
  可空：N
  意思：数据报长度
返回值
  类型：逻辑型
  意思：是否设置成功
备注：此函数在你启动后需要设置或者需要改变其参数值的时候需要设置
*********************************************************************/
extern "C" BOOL NetCore_NetBios_DGram_SetMode(LPCSTR lpszLocalName,DWORD dwType,DWORD dwLana,DWORD dwLenDatagrams);
/********************************************************************
函数名称：NetCore_NetBios_DGram_Send
函数功能：点对点发送数据报
 参数.一：szRecipientName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：接受者的NETBIOS名称
 参数.二：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：nBufLen
  In/Out：In
  类型：整数型
  可空：N
  意思：发送的数据长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_NetBios_DGram_Send(LPCSTR szRecipientName,LPCSTR lpszBuffer,int nBufLen);
/********************************************************************
函数名称：NetCore_NetBios_DGram_BroadcastSend
函数功能：发送广播消息
 参数.一：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.二：nBufLen
  In/Out：In
  类型：整数型
  可空：N
  意思：数据的长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_NetBios_DGram_BroadcastSend(LPCSTR lpszBuffer,int nBufLen);
/********************************************************************
函数名称：NetCore_NetBios_DGram_RecvEx
函数功能：接受数据报数据
 参数.一：pSt_NetCore_NetBios_DGramRecv
  In/Out：Out
  类型：结构体指针
  可空：N
  意思：返回接受到的数据的数据结构
 参数.二：bBroadCast
  In/Out：In
  类型：逻辑型
  可空：N
  意思：是接受广播数据还是点对点数据
返回值
  类型：逻辑型
  意思：是否成功接受到数据
备注：
*********************************************************************/
extern "C" BOOL NetCore_NetBios_DGram_RecvEx(NETBIOS_DGRAM_RECV *pSt_NetCore_NetBios_DGramRecv,BOOL bBroadCast = FALSE);
/************************************************************************/
/*                     SPI导出函数定义                                  */
/************************************************************************/
/********************************************************************
函数名称：NetCore_SocketSpi_Install
函数功能：安装LSP模块
 参数.一：lpszPathName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：LSP模块路径地址
 参数.二：lpszProtoName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要被安装的LSP协议名称,这个参数作为后续操作参数
返回值
  类型：逻辑型
  意思：是否成功安装
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketSpi_Install(LPCTSTR lpszPathName, LPCTSTR lpszProtoName);
/********************************************************************
函数名称：NetCore_SocketSpi_Delete
函数功能：删除一个关联的SPI协议层
 参数.一：lpszProtoName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要移除的指定协议层名称
返回值
  类型：逻辑型
  意思：是否成功移除
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketSpi_Delete(LPCTSTR lpszProtoName);
/********************************************************************
函数名称：NetCore_SocketSpi_WSCGetProtocols
函数功能：枚举协议 它能够枚举各种协议，包括分层协议，基础协议和协议链。
 参数.一：pSt_SocketSpiProtocols
  In/Out：Out
  类型：结构体指针
  可空：N
  意思：需要足够大的缓冲区来存放导出的协议链信息
 参数.二：pnProtocols
  In/Out： In/Out
  类型：整数型指针
  可空：Y
  意思：如果不为NULL,这个函数只会导出拥有的协议链个数
返回值
  类型：逻辑型
  意思：是否获取成功
备注：一般的，请你先获取有多少个协议链，在传递第二次调用此函数获取有协议链信息
*********************************************************************/
extern "C" BOOL NetCore_SocketSpi_WSCGetProtocols(LPNETCORE_SOCKETSPI_PROTOCOLS pSt_SocketSpiProtocols,int *pnProtocols = NULL);
//////////////////////////////////////////////////////////////////////////
//                  进程通信导出函数定义
//////////////////////////////////////////////////////////////////////////
/************************************************************************
函数名称：NetCore_PipCommunications_ReadCmdReturn
函数功能：读取CMD命令管道返回的内容
参数.一：lpszCmd
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要执行CMD命令
 参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出的内容
 参数.三：nCountLine
  In/Out：In
  类型：整数型
  可空：Y
  意思：要读取几行,0表示全部读取
 参数.四：nReadLen
  In/Out：In
  类型：整数型
  可空：Y
  意思：从第几行开始读取
 参数.五：pInt_Len
  类型：整数型指针
  可空：Y
  意思：输出读取到的返回内容大小，可以为NULL，不获取
返回值
  类型：逻辑性
  意思：是否执行成功
备注：
*************************************************************************/
extern "C" BOOL NetCore_PipCommunications_ReadCmdReturn(LPCSTR lpszCmd,CHAR *ptszOutBuffer,int nCountLine = 0, int nReadLen = 0, int *pInt_Len = NULL);
/************************************************************************/
/*                  剪贴板                                              */
/************************************************************************/
/********************************************************************
函数名称：NetCore_PipCommunications_ClipBoard_Set
函数功能：设置剪贴板中的内容
 参数.一：lpszSetBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要传输到缓冲区的内容
 参数.二：dwWriteType
  In/Out：In
  类型：双字
  可空：Y
  意思：内容的格式，为空默认为UNICODE字符串
返回值
  类型：逻辑型
  意思：是否成功设置
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_ClipBoard_Set(LPCSTR lpszSetBuffer,DWORD dwWriteType = NETCORE_PIPCOMMUNICATION_CLIPBROAD_CF_UNICODETEXT);
/********************************************************************
函数名称：NetCore_PipCommunications_ClipBoard_GetFormat
函数功能：获取剪贴板中的内容格式
 参数.一：pOutDwType
  In/Out：Out
  类型：双字
  可空：Y
  意思：导出获取到的内容格式，通过定义表查询
返回值
  类型：逻辑型
  意思：是否成功获取到内容格式
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_ClipBoard_GetFormat(DWORD *pOutDwType = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_ClipBoard_Get
函数功能：获取剪贴板中的内容
 参数.一：ptszOutBuffer
  In/Out：In
  类型：字符指针
  可空：N
  意思：导出到的获取到的内容指针，如果是文字，直接可以使用，如果是图片，需要转换格式
 参数.二：dwOpenType
  In/Out：In
  类型：双字
  可空：Y
  意思：要获取的内容格式
返回值
  类型：逻辑型
  意思：是否成功获取得到指定内容的格式
备注：通过NetCore_PipCommunications_ClipBoard_GetFormat 首先获取下剪贴板中第一个内容格式是什么
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_ClipBoard_Get(CHAR *ptszOutBuffer,DWORD dwOpenType = NETCORE_PIPCOMMUNICATION_CLIPBROAD_CF_UNICODETEXT);
/************************************************************************/
/*                    匿名管道导出定义                                  */
/************************************************************************/
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Create
函数功能：创建匿名管道
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的匿名管道进程名称
 参数.二：lpszParament
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要启动进程的参数，可以为NULL
 参数.三：pInt_ProcessId
  In/Out：Out
  类型：双字指针
  可空：Y
  意思：要启动进程的参数，可以为NULL
 参数.四：pInt_hProcess
  In/Out：Out
  类型：句柄
  可空：Y
  意思：要启动进程的参数，可以为NULL
返回值
  类型：逻辑型
  意思：是否创建成功
备注：支持子进程通信
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Create(LPCSTR lpszProcessName,LPCSTR lpszParament = NULL,DWORD *pInt_ProcessId = NULL,HANDLE pInt_hProcess = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Close
函数功能：关闭指定的匿名管道
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要关闭的匿名管道名称
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Close(LPCSTR lpszProcessName);
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Read
函数功能：读取匿名管道中的数据
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个匿名管道
 参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：读取到的数据缓冲区
 参数.三：pDwLen
  In/Out：Out
  类型：双字指针
  可空：N
  意思：读取到的数据长度
返回值
  类型：逻辑型
  意思：是否有数据成功读取得到
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Read(LPCSTR lpszProcessName,CHAR *ptszOutBuffer,DWORD *pDwLen);
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Read
函数功能：读取匿名管道中的数据
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个匿名管道
 参数.二：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写入的数据缓冲区
 参数.三：pDwLen
  In/Out：In/Out
  类型：双字指针
  可空：N
  意思：要写入的数据长度，导出实际写入的数据长度
返回值
  类型：逻辑型
  意思：数据是否成功写入匿名管道
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Write(LPCSTR lpszProcessName,LPCSTR lpszBuffer,DWORD *pDwLen);
/************************************************************************/
/*                    命名管道导出定义                                  */
/************************************************************************/
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Create
函数功能：创建命名管道
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的命名管道名称
 参数.二：bIsCallBack
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否通过回调方式来获取数据，默认否
返回值
  类型：逻辑型
  意思：是否创建成功
备注：创建例子：，支持多进程局域网通信 _T("\\\\.\\pipe\\MyNamedPipeOne")
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Create(LPCSTR lpszHostName,BOOL bIsCallBack = FALSE);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_SetCallBack
函数功能：设置回调函数
 参数.一：fpCall_Read
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：回调函数地址，导出回调数据
 参数.二：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数参数
返回值
  类型：逻辑型
  意思：是否设置成功
备注：如果你不打算使用回调函数，可以不调用此函数
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_SetCallBack(CALLBACK_NETCORE_PIPECOMMUNICATIONS_NAMED_READ fpCall_Read,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Close
函数功能：关闭指定的命名管道或者关闭全部命名管道
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果为空，将全部关闭，否则，请指定一个命名管道名称
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：如果你直接结束程序，也可以不调用，系统会自动销毁所有资源
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Close(LPCSTR lpszHostName = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Read
函数功能：读取指定命名管道中的数据
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个命名管道中的数据
 参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：读取到的数据缓冲区
 参数.三：pdw_OutLen
  In/Out：In/Out
  类型：双字
  可空：N
  意思：输入：要读取的大小，输出，实际读取到的大小
返回值
  类型：逻辑型
  意思：是否成功读取到数据
备注：如果你已经设置了指定的命名管道为回调接受数据，那么你不能调用此函数
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Read(LPCSTR lpszHostName,CHAR *ptszOutBuffer,DWORD *pdw_OutLen);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Write
函数功能：写入数据到指定的命名管道
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写入到哪个命名管道中
 参数.二：ptszOutBuffer
  In/Out：In
  类型：字符指针
  可空：N
  意思：写入到的数据缓冲区
 参数.三：dwMsgLen
  In/Out：In
  类型：双字
  可空：N
  意思：要写入的大小
返回值
  类型：逻辑型
  意思：是否成功写入数据 
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Write(LPCSTR lpszHostName,LPCSTR lpszInBuffer,DWORD dwMsgLen);
/************************************************************************/
/*                    邮槽通信导出定义                                  */
/************************************************************************/
/********************************************************************
函数名称：NetCore_PipCommunications_MailSlot_Create
函数功能：创建一个邮槽
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的邮槽名称
 参数.二：bIsCallBack
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否使用回调方式接受数据，默认不使用
返回值
  类型：逻辑型
  意思：是否创建成功
备注：格式 "\\\\.\\mailslot\\MyMailSlot"  最后一个字符串可以自命名 
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MailSlot_Create(LPCSTR lpszHostName,BOOL bIsCallBack = FALSE);
/********************************************************************
函数名称：NetCore_PipCommunications_MailSlot_SetCallBack
函数功能：设置邮槽回调接受数据函数
 参数.一：fpCall_MailSlotRead
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：要设置的回调函数地址
返回值
  类型：逻辑型
  意思：是否设置成功
备注：如果你创建邮槽没有使用到回调方式接受数据，可以不调用此函数
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MailSlot_SetCallBack(CALLBACK_NETCORE_PIPECOMMUNICATIONS_MAILSLOT_READ fpCall_MailSlotRead);
/********************************************************************
函数名称：NetCore_PipCommunications_MailSlot_Write
函数功能：写入内容到邮槽
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个邮槽的内容
 参数.二：lpszWriteBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出获取到的数据
 参数.三：nMsgLen
  In/Out：In
  类型：双字
  可空：Y
  意思：输入要写入的缓冲区大小，可以为空或者0，如果缓冲区大小太复杂非连续字符，请输入大小
返回值
  类型：逻辑型
  意思：是否成功写入
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MailSlot_Write(LPCSTR lpszHostName,LPCSTR lpszWriteBuffer,int nMsgLen = 0);
/********************************************************************
函数名称：NetCore_PipCommunications_MailSlot_Read
函数功能：读取邮槽里面的内容
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个邮槽的内容
 参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出获取到的数据
 参数.三：pdw_OutLen
  In/Out：In/Out
  类型：双字
  可空：N
  意思：输入：要读取的缓冲区准备好的大小，输出：实际缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功读取
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MailSlot_Read(LPCSTR lpszHostName,CHAR *ptszOutBuffer,DWORD *pdw_OutLen);
/********************************************************************
函数名称：NetCore_PipCommunications_MailSlot_Close
函数功能：关闭指定的邮槽
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要关闭的邮槽名称，为空将关闭所有邮槽
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MailSlot_Close(LPCSTR lpszHostName = NULL);
/************************************************************************/
/*                    内存映射导出定义                                  */
/************************************************************************/
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryMap_Create
函数功能：创建一个内存映射
 参数.一：lpszMapName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的映射文件名称
 参数.二：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果为空，表示直接创建映射文件，否则请输入你想要映射的文件绝对路径
返回值
  类型：逻辑型
  意思：是否创建成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryMap_Create(LPCSTR lpszMapName,LPCSTR lpszFileName = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryMap_Write
函数功能：写入数据到内存映射中
 参数.一：lpszMapName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写入到哪个内存映射中
 参数.二：lpVoid_Buffer
  In/Out：In
  类型：无类型指针
  可空：N
  意思：要写入的数据
 参数.三：dwMsgLen
  In/Out：In
  类型：双字
  可空：N
  意思：写入数据的长度
返回值
  类型：逻辑型
  意思：是否成功写入数据到内存映射中
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryMap_Write(LPCSTR lpszMapName,LPVOID lpVoid_Buffer,DWORD dwMsgLen);
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryMap_Read
函数功能：从内存映射中读取数据
 参数.一：lpszMapName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取的内存映射名称
 参数.二：lpVoid_Buffer
  In/Out：Out
  类型：无类型指针
  可空：N
  意思：读取到的数据
返回值
  类型：逻辑型
  意思：是否读取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryMap_Read(LPCSTR lpszMapName,LPVOID lpVoid_Buffer);
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryMap_Close
函数功能：关闭一个指定的内存映射
 参数.一：lpszMapName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要关闭的内存映射名称
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryMap_Close(LPCSTR lpszMapName);
//////////////////////////////////////////////////////////////////////////
//                  无线通信技术函数导出定义
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                  蓝牙通信导出函数定义                                */
/************************************************************************/
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_Scan
函数功能：搜索蓝牙设备
  参数一：BthCount
   In/Out：In
   类型：整数型
   可空：N
   意思：找到的设备数量。没有为0
返回值
  类型：逻辑型
  意思：是否搜索执行成功
备注：
************************************************************************/
extern "C" BOOL NetCore_Wireless_BlueTooth_Scan(int &BthCount);
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_ShowSheet
函数功能：显示序号为nIndex的蓝牙设备的属性
  参数一：nIndex
   In/Out：In
   类型：整数型
   可空：N
   意思：索引
  参数二：hWnd
   In/Out：In
   类型：句柄
   可空：Y
   意思：父窗口的句柄，为空表示默认激活窗口为父窗口
返回值
  类型：逻辑型
  意思：是否显示成功
备注：
************************************************************************/
extern "C" BOOL NetCore_Wireless_BlueTooth_ShowSheet(UINT nIndex,HWND hWnd = NULL);
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_RequestAuthenticateDevice
函数功能：请求与远程蓝牙设备配对
  参数一：nIndex
   In/Out：In
   类型：整数型
   可空：N
   意思：索引
  参数二：hWnd
   In/Out：In
   类型：句柄
   可空：Y
   意思：请求的窗口句柄，如果为空默认取桌面
  参数三：pResult
   In/Out：Out
   类型：双字型
   可空：Y
   意思：输出匹配的返回值
返回值
  类型：逻辑型
  意思：是否匹配成功
备注：
************************************************************************/
extern "C" BOOL NetCore_Wireless_BlueTooth_RequestAuthenticateDevice(UINT nIndex,HWND hWnd = NULL,DWORD *pResult = NULL);
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_FormatBthAddress
函数功能：格式化蓝牙地址到字符串
  参数一：pBthAddress
   In/Out：In/Out
   类型：字符指针
   可空：N
   意思：要格式化的蓝牙地址，输出格式化后的蓝牙地址
返回值
  类型：逻辑型
  意思：是否格式化成功
备注：
************************************************************************/
extern "C" BOOL NetCore_Wireless_BlueTooth_FormatBthAddress(CHAR *pBthAddress);
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_EnumerateLocalRadios
函数功能：枚举本地蓝牙设备
  参数一：BthCount
   In/Out：Out
   类型：整数型
   可空：N
   意思：返回本地蓝牙设备数量
返回值
  类型：逻辑型
  意思：是否搜索执行成功
备注：这个函数是搜索本机的蓝牙设备
************************************************************************/
extern "C" BOOL NetCore_Wireless_BlueTooth_EnumerateLocalRadios(UINT &BthCount);
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_RemoveAllBthDevInfo
函数功能：移除所有蓝牙设备信息
返回值
  类型：无
  意思：
备注：
************************************************************************/
extern "C" void NetCore_Wireless_BlueTooth_RemoveAllBthDevInfo();
/************************************************************************
函数名称：NetCore_Wireless_BlueTooth_RemoveAllLocalRadio
函数功能：移除所有本地无线设备
返回值
  类型：无
  意思：
备注：
************************************************************************/
extern "C" void NetCore_Wireless_BlueTooth_RemoveAllLocalRadio();
/************************************************************************/
/*                  红外线通信导出函数定义                              */
/************************************************************************/
/********************************************************************
函数名称：NetCore_Wireless_InfraredIRDA_EnumDev
函数功能：答应搜索到的红外线设备信息
 参数.一：fpInfrared_DevPrint
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：打印信息
返回值
  类型：逻辑型
  意思：是否成功搜索到设备
备注：
*********************************************************************/
extern "C" BOOL NetCore_Wireless_InfraredIRDA_EnumDev(CALLBACK_NETCORE_WIRELESS_INFRARED_PRINTDEV fpInfrared_DevPrint);
/************************************************************************/
/*                  高速文件缓存服务                                    */
/************************************************************************/
/********************************************************************
函数名称：NetCore_FileCache_Create
函数功能：创建文件缓存
 参数.一：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：文件缓存路径
 参数.二：nSize
  In/Out：In
  类型：整数型
  可空：N
  意思：文件缓存大小
 参数.三：nFlushTime
  In/Out：In
  类型：整数型
  可空：Y
  意思：自动刷新时间，刷新到文件中去，如果为0,默认，将不启用
 参数.四：fpCall_CacheFileTimer
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：如果刷新时间不启用，此参数没有作用，如果启用了刷新时间，那么此参数必须设置
 参数.五：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否创建成功
备注：XNETHANDLE xhCache 是导出的句柄，以后要操作这个创建的文件缓存都需要用到这个句柄，后面的参数说明都省略了这个句柄参数
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Create(PXNETHANDLE pxhCache,LPCSTR lpszFileName,int nSize,int nFlushTime = 0,CALLBACK_NETCORE_CACHEFILE_FLUSHEVENT fpCall_CacheFileTimer = NULL,LPVOID lParam = NULL); 
/********************************************************************
函数名称：NetCore_FileCache_Write
函数功能：写入文件缓存
 参数.一：lpszWriteBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写的内容
 参数.二：nWriteLen
  In/Out：In
  类型：整数型
  可空：N
  意思：写入数据大小
返回值
  类型：逻辑型
  意思：是否写入成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Write(XNETHANDLE xhCache,LPCSTR lpszWriteBuffer,int nWriteLen);
/********************************************************************
函数名称：NetCore_FileCache_Read
函数功能：读取文件缓存
 参数.一：ptszBuffer
  In/Out：In
  类型：字符指针
  可空：N
  意思：读到的内存缓冲区
 参数.二：pInt_Len
  In/Out：In
  类型：整数型指针
  可空：N
  意思：输入读的内存缓冲区大小，输出读到的内存缓冲区大小
返回值
  类型：逻辑型
  意思：是否读取成功
备注：直接从内存读的，所以速度非常快！直接读取完毕一次性
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Read(XNETHANDLE xhCache,CHAR *ptszBuffer,int *pInt_Len);
/********************************************************************
函数名称：NetCore_FileCache_Flush
函数功能：主动刷新内存到文件
返回值
  类型：逻辑型
  意思：是否刷新成功
备注：同步刷新。
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Flush(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_FileCache_ReSize
函数功能：重置大小，每次刷新后清理后，需要调用这个函数还原到以前的大小
返回值
  类型：逻辑型
  意思：还原成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_ReSize(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_FileCache_Clear
函数功能：清理高速缓存中的数据
返回值
  类型：逻辑型
  意思：是否清理成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Clear(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_FileCache_Close
函数功能：把内存中的数据写到文件并且关闭文件
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Close(XNETHANDLE xhCache);
/************************************************************************/
/*                  数据缓存导出函数                                      */
/************************************************************************/
//拥有扩展函数的第一个参数都是句柄
/********************************************************************
函数名称：NetCore_DataCache_Init
函数功能：初始化数据回溯缓冲池
 参数.一：nBufferCount
  In/Out：In
  类型：整数型
  可空：N
  意思：支持可以回溯的队列个数
 参数.二：bClear
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：如果满了后，是否清理最开始的包，默认清理，否则将无法继续写入回溯
 参数.三：nMemSize
  In/Out：In
  类型：整数型
  可空：Y
  意思：要创建的回溯队列中每个缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：使用了内存预加载技术。
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_Init(XNETHANDLE *pxhCache,int nBufferCount,BOOL bClear = TRUE,int nMemSize = 1024000,BOOL bFile = FALSE);
/********************************************************************
函数名称：NetCore_DataCache_Destory
函数功能：销毁回溯缓冲池
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_Destory(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_DataCache_PushSend
函数功能：压入一个发送的数据到回溯队列中
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的唯一标识符
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的缓冲区（内部有自己的内存空间）
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要压入的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：压入的数据后，这个回溯系统会自动管理内存和维护队列
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_PushSend(XNETHANDLE xhCache,LPCTSTR lpszClientId,LPCTSTR lpszBuffer, int nLen);
/********************************************************************
函数名称：NetCore_DataCache_PushRecv
函数功能：压入一个接受的数据到回溯队列中
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的唯一标识符
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的缓冲区（内部有自己的内存空间）
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要压入的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：压入的数据后，这个回溯系统会自动管理内存和维护队列
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_PushRecv(XNETHANDLE xhCache,LPCTSTR lpszClientId,LPCTSTR lpszBuffer, int nLen);
/********************************************************************
函数名称：NetCore_DataCache_GetSend
函数功能：从一个发送回溯缓冲队列中获得一个指定的数据包
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取数据的队列唯一标识符
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出获得的缓冲区大小
 参数.四：nPktSerial
  In/Out：In
  类型：整数型
  可空：Y
  意思：要获得可用队列中的第几个回溯包，默认最近的第一个
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_GetSend(XNETHANDLE xhCache,LPCTSTR lpszClientId,CHAR *ptszBuffer,int *pInt_Len,int nPktSerial = 0);
/********************************************************************
函数名称：NetCore_DataCache_GetRecv
函数功能：从一个接受回溯缓冲队列中获得一个指定的数据包
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取数据的队列唯一标识符
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出获得的缓冲区大小
 参数.四：nPktSerial
  In/Out：In
  类型：整数型
  可空：Y
  意思：要获得可用队列中的第几个回溯包，默认最近的第一个
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_GetRecv(XNETHANDLE xhCache,LPCTSTR lpszClientId,CHAR *ptszBuffer,int *pInt_Len,int nPktSerial = 0);
/********************************************************************
函数名称：NetCore_DataCache_Close
函数功能：关闭一个指定的回溯队列并且清理相关资源
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的回溯队列唯一标识符
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_Close(XNETHANDLE xhCache,LPCTSTR lpszClientId);
/************************************************************************/
/*                  UDX导出函数                                         */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UDXSocket_Init
函数功能：初始化UDX
 参数.一：pSt_UDXConfig
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：设置UDX的服务器选项
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要绑定的端口号
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_InitEx(XNETHANDLE *pxhNet, NETCORE_UDXCONFIG *pSt_UDXConfig, int nPort);
/********************************************************************
函数名称：NetCore_UDXSocket_CBSet
函数功能：设置回调函数
 参数.一：fpCall_UDXLogin
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：用户连接处理回调函数
 参数.二：fpCall_UDXLeave
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：用户离开回调处理函数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_CBSetEx(XNETHANDLE xhNet, CALLBACK_NETCORE_UDX_LOGIN fpCall_UDXLogin, CALLBACK_NETCORE_UDX_LEAVE fpCall_UDXLeave, LPVOID lPLogin = NULL, LPVOID lPLeave = NULL);
/********************************************************************
函数名称：NetCore_UDXSocket_Close
函数功能：关闭一个指定客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要操作的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_CloseEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：NetCore_UDXSocket_Send
函数功能：发送数据函数
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要操作的客户端
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要发送的数据大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_SendEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, LPCTSTR lpszMsgBuffer, int nMsgLen);
/********************************************************************
函数名称：NetCore_UDXSocket_Recv
函数功能：获取数据
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要操作的客户端
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出获取到的数据
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出数据大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_RecvEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：NetCore_UDXSocket_Destroy
函数功能：销毁UDX资源
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_DestroyEx(XNETHANDLE xhNet);