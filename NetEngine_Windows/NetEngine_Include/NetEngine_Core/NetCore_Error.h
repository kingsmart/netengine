#pragma once
/********************************************************************
//	Created:	2012/1/3  15:03
//	File Name: 	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngineCore\NetEngineCore_Error.h
//	File Path:	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngineCore
//	File Base:	NetEngineCore_Error
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	服务器组件开发库-错误导出定义
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                         高速缓存导出错误
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                          文件缓存池错误                                */
/************************************************************************/
#define ERROR_NETCORE_CACHE_FILE_CREATE_PARAMENT 0x10A0000               //参数错误，无法继续
#define ERROR_NETCORE_CACHE_FILE_CREATE_CREATED 0x10A0001                //已经创建过了，除非你关闭，不然无法继续！
#define ERROR_NETCORE_CACHE_FILE_CREATE_ISFAILED 0x10A0002               //创建文件失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_SEEK 0x10A0003                   //移动文件指针失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_WRITE 0x10A0004                  //占用文件大小失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_MAP 0x10A0005                    //映射文件到内存失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_THREAD 0x10A0006                 //创建线程失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_CALLBACK 0x10A0007               //设置回调函数失败
#define ERROR_NETCORE_CACHE_FILE_WRITE_PARAMENT 0x10A0000                //参数错误，请仔细检查
#define ERROR_NETCORE_CACHE_FILE_WRITE_NOTCREATE 0x10A0011               //写入失败，没有创建
#define ERROR_NETCORE_CACHE_FILE_READ_PARAMENT 0x10A0000                 //参数错误，无法绩溪
#define ERROR_NETCORE_CACHE_FILE_READ_SIZESMALL 0x10A0010                //你输入的缓冲区太小了，无法容纳
#define ERROR_NETCORE_CACHE_FILE_READ_NOTCREATE 0x10A0011                //没有创建。
#define ERROR_NETCORE_CACHE_FILE_FLUSH_NOTCREATE 0x10A0021               //没有创建。
#define ERROR_NETCORE_CACHE_FILE_FLUSH_ISFAILED 0x10A0022                //刷新失败，内部错误
#define ERROR_NETCORE_CACHE_FILE_FLUSH_CHANGESIZE 0x10A0023              //刷新失败，改变文件大小失败
#define ERROR_NETCORE_CACHE_FILE_CLOSE_NOTCREATE 0x10A0040               //没有创建，无法关闭
#define ERROR_NETCORE_CACHE_FILE_CLOSE_UNMAP 0x10A0041                   //关闭内存失败
#define ERROR_NETCORE_CACHE_FILE_CLOSE_FILE 0x10A0042                    //关闭文件失败
#define ERROR_NETCORE_CACHE_FILE_CLOSE_THREAD 0x10A0043                  //关闭线程失败
#define ERROR_NETCORE_CACHE_FILE_RESIZE_NOTCREATE 0x10A0050              //重置失败，没有创建缓存文件
#define ERROR_NETCORE_CACHE_FILE_RESIZE_RESIZE 0x10A0051                 //重置文件大小失败，系统错误
/************************************************************************/
/*                          文件缓存池扩展错误                             */
/************************************************************************/
#define ERROR_NETCORE_CAHCE_EX_FILE_CREATE_MALLOC 0x10A0101              //申请内存失败
#define ERROR_NETCORE_CAHCE_EX_FILE_WRITE_FIND 0x10A0110                 //写失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_READ_FIND 0x10A0120                  //读失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_FLUSH_FIND 0x10A0130                 //刷新失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_CLEAR_FIND 0x10A0140                 //清理失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_CLOSE_FIND 0x10A0150                 //关闭失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_RESIZE_FIND 0x10A0160                //没有找到，重置失败
/************************************************************************/
/*                          数据缓存池错误                                */
/************************************************************************/
#define ERROR_NETCORE_CACHE_DATA_PUSHSEND_PARAMENT 0x10A0201             //参数错误
#define ERROR_NETCORE_CACHE_DATA_PUSHSEND_RESIZE 0x10A0202               //改变文件大小失败
#define ERROR_NETCORE_CACHE_DATA_PUSHRECV_PARAMENT 0x10A0210             //参数错误
#define ERROR_NETCORE_CACHE_DATA_PUSHRECV_RESIZE 0x10A0211               //改变文件大小失败
#define ERROR_NETCORE_CACHE_DATA_GETSEND_PARAMENT 0x10A0220              //参数错误
#define ERROR_NETCORE_CACHE_DATA_GETSEND_NOTFOUND 0x10A0221              //获取失败，没有找到唯一标识符
#define ERROR_NETCORE_CACHE_DATA_GETSEND_NOTENABLE 0x10A0222             //没有找到可用的数据回溯包
#define ERROR_NETCORE_CACHE_DATA_GETSEND_LEN 0x10A0223                   //长度错误，不够
#define ERROR_NETCORE_CACHE_DATA_GETRECV_PARAMENT 0x10A0230              //参数错误
#define ERROR_NETCORE_CACHE_DATA_GETRECV_NOTFOUND 0x10A0231              //获取失败，没有找到唯一标识符
#define ERROR_NETCORE_CACHE_DATA_GETRECV_NOTENABLE 0x10A0232             //没有找到可用的数据回溯包
#define ERROR_NETCORE_CACHE_DATA_GETRECV_LEN 0x10A0233                   //长度错误，不够
#define ERROR_NETCORE_CACHE_DATA_CLOSE_PARAMENT 0x10A0240                //参数错误
#define ERROR_NETCORE_CACHE_DATA_CREATE_SNDFILE 0x10A0250                //创建发送缓存回溯文件失败
#define ERROR_NETCORE_CACHE_DATA_CREATE_RCVFILE 0x10A0251                //创建接受缓存回溯文件失败
/************************************************************************/
/*                          数据缓存池扩展错误                          */
/************************************************************************/
#define ERROR_NETCORE_CAHCE_EX_DATA_NOTFOUND 0x10A0100                   //没有找到指定句柄
#define ERROR_NETCORE_CAHCE_EX_DATA_CREATE_MALLOC 0x10A0110              //申请内存失`败
//////////////////////////////////////////////////////////////////////////
//                     进程通信机制错误导出表
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                     CMD命令返回获取错误                              */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_CMD_OPEN 0x10A1101                //打开匿名管道失败
/************************************************************************/
/*                     剪贴板操作错误                                   */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_CLIPBOARD_OPEN 0x10A1110          //打开剪贴板失败，可能没有权限
#define ERROR_NETCORE_PIPCOMMUNICATION_CLIPBOARD_MALLOC 0x10A1111        //申请空间失败，无法继续
#define ERROR_NETCORE_PIPCOMMUNICATION_CLIPBOARD_FORMAT 0x10A1112        //格式错误，可能不支持
#define ERROR_NETCORE_PIPCOMMUNICATION_CLIPBOARD_GETDATA 0x10A1113       //获取数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_CLIPBOARD_NOTDATAS 0x10A1114      //剪贴板中没有任何内容
/************************************************************************/
/*                     匿名管道                                         */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_READHANDLE 0x10A1201    //读取句柄创建失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_WRITEHANDLE 0x10A1202   //写入句柄创建失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_ERRORHANDLE 0x10A1203   //错误句柄创建失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_CREATEPIPE 0x10A1204    //创建匿名管道失败 
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_CREATEPROCESS 0x10A1205 //创建子进程失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_FIND_NOTEXIST 0x10A1206 //不存在，无法继续
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_READ 0x10A1207          //读取数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_WRITE 0x10A1208         //写入数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_NOTEXIST 0x10A1209      //不存在，不需要删除
/************************************************************************/
/*                     命名管道                                         */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_ISEXIST 0x10A1301           //命名管道已经存在，无法创建
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATEFILE 0x10A1302        //创建文件失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATEPIPE 0x10A1303        //创建命名管道失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATEEVENT 0x10A1304       //创建事件失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATETHREAD 0x10A1305      //创建线程失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_NOTFOUND 0x10A1306          //没有找到指定的命名管道
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_TERMINATETHREAD 0x10A1307   //结束线程失败 
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CALLPARAMENT 0x10A1308      //设置回调函数的时候参数为空
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_NOTSUPPEDREAD 0x10A1309     //不支持此读取方法，因为已经设置为回调方式
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_READ 0x10A130A              //读取数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_WRITE 0x10A130B             //写入数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_WRITELEN 0x10A130C          //写入数据的实际大小与传输的大小不一致，失败
/************************************************************************/
/*                     邮槽通信错误导出                                 */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_CREATEREAD 0x10A1401     //创建读取邮槽句柄失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_CREATEWRITE 0x10A1402    //创建写入邮槽句柄失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_WRITE 0x10A1403          //写入数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_WRITELEN 0x10A1404       //写入长度不正确
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_READ 0x10A1405           //读取数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_NOTFOUND 0x10A1406       //没有找到指定的邮槽
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_NOTSUPPETREAD 0x10A1407  //不支持此读取方法，这个邮槽并定义为回调接受数据 
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_CREATETHREAD 0x10A1408   //创建线程失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_ISEXIST 0x10A1409        //此邮槽已经存在 
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_NOTEXIST 0x10A140A       //邮槽不存在
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_TERMINATETHREAD 0x10A140B//结束线程失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MAILSLOT_CALLPARAMENT 0x10A140C   //邮槽回调参数为NULL
/************************************************************************/
/*                     内存映射错误表                                   */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_CREATEFILE 0x10A1501    //创建文件句柄失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_CREATEMAP 0x10A1502     //创建内存映射失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_GETFILESIZE 0x10A1503   //获取文件大小失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_MAPVIEW 0x10A1504       //映射视图失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_NOTFOUND 0x10A1505      //没有找到指定的内存映射文件
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_NOTSUPPET 0x10A1506     //不支持此调用方法
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_UNMAP 0x10A1507         //卸载映射视图失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_OPENMAP 0x10A1508       //打开内存映射文件失败
//////////////////////////////////////////////////////////////////////////
//                         原始套接字错误表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_RAWSOCKET_IPHDR_PARAMENT 0x10A2000                 //参数错误
#define ERROR_NETCORE_RAWSOCKET_IPHDR_NOTSUPPORT 0x10A2001               //不支持的协议类型
#define ERROR_NETCORE_RAWSOCKET_TCPHDR_PARAMENT 0x10A2010                //参数错误
#define ERROR_NETCORE_RAWSOCKET_UDPHDR_PARAMENT 0x10A2020
#define ERROR_NETCORE_RAWSOCKET_ICMPHDR_PARAMENT 0x10A2030
//TCP
#define ERROR_NETCORE_RAWSOCKET_TCP_INIT_PARAMENT 0x10A2100              //参数错误
#define ERROR_NETCORE_RAWSOCKET_TCP_INIT_SOCKET 0x10A2101                //获取套接字失败
#define ERROR_NETCORE_RAWSOCKET_TCP_INIT_SET 0x10A2102                   //设置套机字失败
#define ERROR_NETCORE_RAWSOCKET_TCP_SET_PARAMENT 0x10A2110               //参数错误
//UDP
#define ERROR_NETCORE_RAWSOCKET_UDP_INIT_PARAMENT 0x10A2200              //参数错误
#define ERROR_NETCORE_RAWSOCKET_UDP_INIT_SOCKET 0x10A2201                //初始化套接字失败
#define ERROR_NETCORE_RAWSOCKET_UDP_INIT_SET 0x10A2202                   //设置套接字属性失败
#define ERROR_NETCORE_RAWSOCKET_UDP_SET_PARAMENT 0x10A2210               //参数错误
//ICMP
#define ERROR_NETCORE_RAWSOCKET_ICMP_INIT_PARAMENT 0x10A2300             //参数错误
#define ERROR_NETCORE_RAWSOCKET_ICMP_INIT_SOCKET 0x10A2301               //初始化套接字失败
#define ERROR_NETCORE_RAWSOCKET_ICMP_INIT_SET 0x10A2302                  //设置套接字属性失败
#define ERROR_NETCORE_RAWSOCKET_ICMP_SET_PARAMENT 0x10A2310              //参数错误
//ARP
#define ERROR_NETCORE_RAWSOCKET_ARP_INIT_PARAMENT 0x10A2400              //参数错误
#define ERROR_NETCORE_RAWSOCKET_ARP_INIT_SOCKET 0x10A2401                //设置套接字失败
#define ERROR_NETCORE_RAWSOCKET_ARP_SET_PARAMENT 0x10A2410               //设置属性失败，参数错误
//////////////////////////////////////////////////////////////////////////
//                     串口通信操作导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_COM_INIT_OPENCOM 0x10A3001                         //打开串口错误
#define ERROR_NETCORE_COM_INIT_GETCOMSTATE 0x10A3002                     //获取串口状态错误
#define ERROR_NETCORE_COM_INIT_SETCOMSTATE 0x10A3003                     //设置串口状态错误
#define ERROR_NETCORE_COM_INIT_ISOPEND 0x10A3004                         //打开串口失败，这个串口已经被打开
#define ERROR_NETCORE_COM_INIT_STARTMASK 0x10A3005                       //启动事件失败
#define ERROR_NETCORE_COM_INIT_WAITEVENT 0x10A3006                       //等待信号事件失败
#define ERROR_NETCORE_COM_INIT_NONT 0x10A3007                            //不是NT操作系统
#define ERROR_NETCORE_COM_INIT_READUNKNOW 0x10A3008                      //读取数据发生未知错误
#define ERROR_NETCORE_COM_INIT_READBUFFSMALL 0x10A3009                   //要读取的缓冲区数据太小了
#define ERROR_NETCORE_COM_SEND_INITFALED 0x10A3010                       //没有初始化或者初始化错误
#define ERROR_NETCORE_COM_SEND_WRITELENGTH 0x10A3011                     //写入长度不合适
#define ERROR_NETCORE_COM_SEND_CREATEOL 0x10A3012                        //创建一个OVERLAPPED事件失败
#define ERROR_NETCORE_COM_SEND_WRITELENINCONSISTENT 0x10A3013            //写入长度不一致
#define ERROR_NETCORE_COM_SEND_NOTFOUND 0x10A3014                        //发送失败，没有找到串口
#define ERROR_NETCORE_COM_SEND_NOPENDING 0x10A3015                       //发送失败，非处理发送错误
#define ERROR_NETCORE_COM_SEND_GETOLRESULT 0x10A3016                     //发送失败，获取发送处理结果失败 
#define ERROR_NETCORE_COM_SEND_CLEARERROR 0x10A3017                      //清理错误缓冲区失败
#define ERROR_NETCORE_COM_SEND_CLEARSEND 0x10A3018                       //清理发送缓冲区失败
#define ERROR_NETCORE_COM_CLOSE_NOTFOUND 0x10A3020                       //关闭串口失败，没有找到指定的串口
#define ERROR_NETCORE_COM_CLOSE_READTHREAD 0x10A3021                     //关闭线程失败
#define ERROR_NETCORE_COM_READ_NOTFOUND 0x10A3030                        //接受数据失败，没有找到指定串口
#define ERROR_NETCORE_COM_READ_HANDLEISFAILED 0x10A3031                  //接受数据失败，串口句柄异常
#define ERROR_NETCORE_COM_READ_CREATEOL 0x10A3032                        //创建接受事件失败
#define ERROR_NETCORE_COM_READ_NOPENDING 0x10A3033                       //不是处理中，错误异常
#define ERROR_NETCORE_COM_READ_GETOLRESULT 0x10A3034                     //获取重叠IO返回值失败
#define ERROR_NETCORE_COM_READ_NOTSUPPORT 0x10A3035                      //不支持此方式读取数据，此串口被设置为回调 
#define ERROR_NETCORE_COM_READ_CLEARERROR 0x10A3036                      //读取数据清理错误失败
#define ERROR_NETCORE_COM_READ_CLEARRECV 0x10A3037                       //清理接受缓冲区失败
//////////////////////////////////////////////////////////////////////////
//                         网络核心套接字错误表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_PARAMENT 0x10A4001            //参数错误
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_ISFAILED 0x10A4002            //创建失败，内部错误
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_SIGEMPTY 0x10A4003            //清空信号失败
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_SIGACTION 0x10A4004           //设置信号失败
#define ERROR_NETCORE_SOCKET_SOCKET_BIND_LISTEN 0x10A4010                //监听端口失败
#define ERROR_NETCORE_SOCKET_SOCKET_BIND_BIND 0x10A4011                  //绑定端口失败
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_ISFULL 0x10A4021                //发送缓冲区已满，需要等待
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_ISFAILED 0x10A4022              //发送失败，内部错误
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_LEN 0x10A4023                   //发送的数据长度与实际的长度不合适
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_ISCLOSED 0x10A4025              //发送的套接字被对方强制关闭
#define ERROR_NETCORE_SOCKET_SOCKET_RECV_RETRY 0x10A4031                 //接受失败，需要重试
#define ERROR_NETCORE_SOCKET_SOCKET_RECV_ISFAILED 0x10A4032              //接受失败，内部错误
#define ERROR_NETCORE_SOCKET_SOCKET_RECV_CLIENTCLOSE 0x10A4033           //客户端已经关闭。
#define ERROR_NETCORE_SOCKET_SOCKET_SETNONEBLOC_GETFL 0x10A4040          //获取状态失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETNONEBLOC_SETNONFAILED 0x10A4041   //设置非阻塞失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETNONEBLOC_BLOCKING 0x10A4042       //设置阻塞失败
#define ERROR_NETCORE_SOCKET_SOCKET_READDR_SETFAILED 0x10A4060           //设置重用失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETTIMEOUT_SEND 0x10A4070            //设置发送超时时间失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETTIMEOUT_RECV 0x10A4071            //设置接受超时时间失败
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_OPEN 0x10A4080             //打开保活计时器失败
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_SETIDLETIME 0x10A4081      //设置空闲探测时间失败
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_SETINTERVALTIME 0x10A4082  //设置探测间隔时间
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_SETKEEPCOUNT 0x10A4083     //设置次数失败
#define ERROR_NETCORE_SOCKET_SOCKET_FASTSTART_NODELAY 0x10A40A0          //设置没有延迟失败
#define ERROR_NETCORE_SOCKET_SOCKET_FASTSTART_NOROUTE 0x10A40A1          //设置不经过路由失败
#define ERROR_NETCORE_SOCKET_SOCKET_SELECT_PARAMENT 0x10A40B0            //参数错误
#define ERROR_NETCORE_SOCKET_SOCKET_SELECT_NOREADLY 0x10A40B1            //没有设备可用
#define ERROR_NETCORE_SOCKET_SOCKET_SELECT_UNKNOW 0x10A40B2              //多路IO选择发送了未知错误
//////////////////////////////////////////////////////////////////////////
//                    TCP服务器错误导出
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                    消息选择模型服务器错误导出                        */
/************************************************************************/
#define ERROR_NETCORE_TCP_ASYCNSELECT_OPEN_ISRUNNING 0x10A4101           //服务器已经在运行//0x10A4600
#define ERROR_NETCORE_TCP_ASYCNSELECT_OPEN_SOCKET 0x10A4102              //套接字属性错误
#define ERROR_NETCORE_TCP_ASYCNSELECT_OPEN_BIND 0x10A4103                //绑定套接字失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_OPEN_WSAASYNC 0x10A4104            //消息选择模型设置失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_OPEN_LISTEN 0x10A4105              //监听失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_CLOSE_NOTRUNNING 0x10A4110         //服务器没有运行，不需要关闭
#define ERROR_NETCORE_TCP_ASYCNSELECT_CLOSE_DOWN 0x10A4111               //卸载资源失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_CLOSE_CLOSE 0x10A4112              //关闭服务器失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_GETADDR_PEERNAME 0x10A4113         //获取地址节点名称错误
#define ERROR_NETCORE_TCP_ASYCNSELECT_CLOSE_PARAMENT 0x10A4124           //参数错误无法继续
#define ERROR_NETCORE_TCP_ASYCNSELECT_GETNAME_GETNAME 0x10A4121          //获取名称失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_GETNAME_GETPEER 0x10A4122          //获取连接信息失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HELP_ISIPADDR_FALSE 0x10A4130      //不是IP地址，无法继续 
#define ERROR_NETCORE_TCP_ASYCNSELECT_CONNECTOR_ISFAILED 0x10A4141       //连接器模型错误，无法继续
#define ERROR_NETCORE_TCP_ASYCNSELECT_CONNECTOR_CONNECT 0x10A4142        //连接失败，无法继续
#define ERROR_NETCORE_TCP_ASYCNSELECT_CONNECTOR_NOTCONNECT 0x10A4143     //连接器没有任何连接
#define ERROR_NETCORE_TCP_ASYCNSELECT_CONNECTOR_DISPATCHSEND 0x10A4144   //派散发送缓冲区失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_CONNECTOR_SEND 0x10A4145           //发送数据失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_CONNECTOR_SRVCLOSED 0x10A4146      //服务器已经关闭
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_NEWFORACCEPT 0x10A4150      //为接受池新建空间失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_PAIRFORACCEPT 0x10A4151     //插入新资源到接受池失败失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_NEWFORCONNECT 0x10A4152     //为连接池申请空间失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_PAIRFORCONNECT 0x10A4153    //插入新连接到连接池失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_DELACCNOTFOUND 0x10A4154    //在接受池中没有找到需要删除的内容
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_DELCONNOTFOUND 0x10A4155    //在连接池中没有找到需要删除的内容
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_ACCEPTNOTFOUND 0x10A4160    //没有在接受池中找到指定的处理内容
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_ACCEPTCLIENT 0x10A4161      //接受客户端连接失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_NEWCLIENT 0x10A4162         //为一个客户端新建套接字连接失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_SETNEWEVENT 0x10A4163       //为新客户端设置事件失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_CONNNOTFOUND 0x10A4164      //新的连接并没有在连接池中
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_SETCONNEVENT 0x10A4165      //为连接器设置事件失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_READNOTFOUND 0x10A4166      //没有找到读取的套接字
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_SETREADEVENT 0x10A4167      //读取数据事件设置失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_RESETREADEVENT 0x10A4168    //重设读取事件失败
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_WRITENOTFOUND 0x10A4169     //写数据的时候没有找到指定的发送客户
#define ERROR_NETCORE_TCP_ASYCNSELECT_HANDLE_CLOSENOTFOUND 0x10A416A     //接受到关闭消息，但是并没有这个客户
#define ERROR_NETCORE_TCP_ASYCNSELECT_MANAGE_SENDNOTFOUND 0x10A417B      //发送地址未找到，不能继续发送
/************************************************************************/
/*                    事件选择模型服务器错误                            */
/************************************************************************/
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_ISRUNNING 0x10A4200           //服务器已经运行 无需初始化
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_SOCKET 0x10A4201              //初始化套接字句柄属性失败
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_BIND 0x10A4202                //绑定服务器失败
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_CREATEEVENT 0x10A4203         //创建事件失败
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_EVENTSELECT 0x10A4204         //设置事件模型失败
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_LISTEN 0x10A4205              //监听服务器失败
#define ERROR_NETCORE_TCP_EVENTSELECT_INIT_CREATETHREAD 0x10A4206        //创建线程失败
#define ERROR_NETCORE_TCP_EVENTSELECT_SOCKET_MALLOCFOROBJ 0x10A4210      //为套接字对象申请空间失败
#define ERROR_NETCORE_TCP_EVENTSELECT_SOCKET_NOTFOUNDEVENT 0x10A4211     //没有找到指定事件
#define ERROR_NETCORE_TCP_EVENTSELECT_SOCKET_NOTFOUNDOBJ 0x10A4212       //没有找到指定的对象
#define ERROR_NETCORE_TCP_EVENTSELECT_THREAD_MALLOCFOROBJ 0x10A4220      //为线程对象申请事件失败
#define ERROR_NETCORE_TCP_EVENTSELECT_THREAD_CREATEEVENT 0x10A4221       //为线程对象创建事件失败
#define ERROR_NETCORE_TCP_EVENTSELECT_STOP_THREAD 0x10A4230              //停止线程失败，无法继续
#define ERROR_NETCORE_TCP_EVENTSELECT_SETCALLBACK_PARAMENT 0x10A4240     //设置回调函数失败，参数错误
/************************************************************************/
/*                    Select服务器错误导出                              */
/************************************************************************/
#define ERROR_NETCORE_TCP_SELECT_INIT_ISRUNNING 0x10A4301                //服务器已经开始运行了
#define ERROR_NETCORE_TCP_SELECT_INIT_SETSOCKET 0x10A4302                //设置SOCKET协议信息失败
#define ERROR_NETCORE_TCP_SELECT_INIT_BIND 0x10A4303                     //绑定套接字失败
#define ERROR_NETCORE_TCP_SELECT_INIT_LISTEN 0x10A4304                   //监听套接字失败
#define ERROR_NETCORE_TCP_SELECT_INIT_SETSIGEMPTY 0x10A4305              //清空信号失败
#define ERROR_NETCORE_TCP_SELECT_INIT_SETSIGPIP 0x10A4306                //设置跳过信号失败
#define ERROR_NETCORE_TCP_SELECT_INIT_CREATETHREAD 0x10A4307             //创建主线程失败
#define ERROR_NETCORE_TCP_SELECT_INIT_CREATECLOSE 0x10A4308              //创建关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SEND_NOTFOUND 0x10A4310                 //发送数据失败,客户端没有找到
#define ERROR_NETCORE_TCP_SELECT_SEND_SENDISFAILED 0x10A4312             //发送数据失败
#define ERROR_NETCORE_TCP_SELECT_CONVERT_PARAMENT 0x10A4320              //转换失败，参数错误，或者服务器没有启动
#define ERROR_NETCORE_TCP_SELECT_CONVERT_GETNAME 0x10A4321               //转换失败，获取节点名称失败
#define ERROR_NETCORE_TCP_SELECT_CONVERT_NOTFOUND 0x10A4322              //没有找到转换的指定客户端
#define ERROR_NETCORE_TCP_SELECT_GETADDRINFO_PARAMENT 0x10A4350          //获取地址信息失败,参数错误
#define ERROR_NETCORE_TCP_SELECT_GETADDRINFO_ISFAILED 0x10A4351          //内部错误
#define ERROR_NETCORE_TCP_SELECT_GETFLOW_PARAMTISNULL 0x10A4360          //获取流量参数错误
#define ERROR_NETCORE_TCP_SELECT_READIO_PARAMENT 0x10A4370               //读取IO事件参数错误
#define ERROR_NETCORE_TCP_SELECT_READIO_NOIOEVNET 0x10A4371              //没有可读IO事件
#define ERROR_NETCORE_TCP_SELECT_READIO_NOTSUPPORT 0x10A4372             //不支持此方式读取IO事件，因为已经被设置为回调方式
#define ERROR_NETCORE_TCP_SELECT_READIO_NOTFOUND 0x10A4373               //没有找到指定的事件，可能没有这类事件发生
#define ERROR_NETCORE_TCP_SELECT_REMOVE_PARAMENT 0x10A4380               //移除客户端失败，参数错误
#define ERROR_NETCORE_TCP_SELECT_REMOVE_NOTFOUND 0x10A4381               //移除客户端失败，没有这个客户
#define ERROR_NETCORE_TCP_SELECT_REMOVE_DELTHREAD 0x10A4382              //删除线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_PARAMENT 0x10A4390      //启动短连接失败，参数错误
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_CREATEMAIN 0x10A4391    //启动主线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_CREATECLOSE 0x10A4392   //启动关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_CLOSE_PARAMENT 0x10A43A0      //参数错误
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_CLOSE_NOTFOUND 0x10A43A1      //关闭失败，没有找到客户端
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_CLOSE_THREAD 0x10A43A2        //关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_STOP_CLOSEMAIN 0x10A43C0      //关闭主线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_STOP_CLOSERECV 0x10A43C1      //关闭接受线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_STOP_CLOSETHREAD 0x10A43C2    //关闭关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_SEND_PARAMENT 0x10A43D0 //参数错误
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_SEND_NOTFOUND 0x10A43D1 //没有找到客户端，无法发送
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_SEND_FAILED 0x10A43D2   //发送失败
#define ERROR_NETCORE_TCP_SELECT_EX_START_ISEXIST 0x10A43E0
#define ERROR_NETCORE_TCP_SELECT_EX_START_MALLOC 0x10A43E1
#define ERROR_NETCORE_TCP_SELECT_EX_NOTFOUND 0x10A43E2
/************************************************************************/
/*                  重叠IO模型服务器 错误列表                           */
/************************************************************************/
#define ERROR_NETCORE_TCP_OVERLAPPED_START_SETSOCKET 0x10A4400           //初始化SOCKET模型失败
#define ERROR_NETCORE_TCP_OVERLAPPED_START_BIND 0x10A4401                //绑定SOCKET失败
#define ERROR_NETCORE_TCP_OVERLAPPED_START_LISTEN 0x10A4402              //监听SOCKET失败
#define ERROR_NETCORE_TCP_OVERLAPPED_START_ISRUNNING 0x10A4403           //系统已经启动，无法继续
#define ERROR_NETCORE_TCP_OVERLAPPED_START_CREATETHREAD 0x10A4405        //启动服务器主线程失败
#define ERROR_NETCORE_TCP_OVERLAPPED_SELECTLIST_MALLOC 0x10A4410         //申请空间失败
#define ERROR_NETCORE_TCP_OVERLAPPED_SELECTLIST_CREATETHREAD  0x10A4411  //创建线程失败
#define ERROR_NETCORE_TCP_OVERLAPPED_THREAD_ACCEPT 0x10A4420             //接受连接失败
#define ERROR_NETCORE_TCP_OVERLAPPED_BUILDEVENT_MALLOC 0x10A4430         //构造组合事件失败，申请空间失败
#define ERROR_NETCORE_TCP_OVERLAPPED_BUILDEVENT_REALLOC 0x10A4431        //申请申请内存失败
#define ERROR_NETCORE_TCP_OVERLAPPED_BUILDEVENT_TABLEISNULL 0x10A4432    //内存表严重错误
#define ERROR_NETCORE_TCP_OVERLAPPED_REMOTECLIENT_PARAMENT 0x10A4440     //移除客户端失败，参数错误
#define ERROR_NETCORE_TCP_OVERLAPPED_REMOTECLIENT_NOTFOUND 0x10A4441     //没有找到指定的客户端
#define ERROR_NETCORE_TCP_OVERLAPPED_REMOTECLIENT_CLOSEHANDLE 0x10A4442  //关闭句柄失败
#define ERROR_NETCORE_TCP_OVERLAPPED_REMOTECLIENT_SHUTDOWN 0x10A4443     //关闭读写失败
#define ERROR_NETCORE_TCP_OVERLAPPED_REMOTECLIENT_CLOSESOCKET 0x10A4444  //关闭套接字失败
#define ERROR_NETCORE_TCP_OVERLAPPED_REMOTECLIENT_TERMINATETHREAD 0x10A4445//关闭线程失败
#define ERROR_NETCORE_TCP_OVERLAPPED_STOP_NOTRUNNING 0x10A4450           //没有运行，不需要关闭
#define ERROR_NETCORE_TCP_OVERLAPPED_STOP_SHUTDOWN 0x10A4451             //卸载监听套接字失败
#define ERROR_NETCORE_TCP_OVERLAPPED_STOP_CLOSESOCKET 0x10A4452          //关闭套接字失败
#define ERROR_NETCORE_TCP_OVERLAPPED_STOP_TERMINATETHREAD 0x10A4453      //结束线程失败
#define ERROR_NETCORE_TCP_OVERLAPPED_SEND_NOTRUNNING 0x10A4460           //没有运行，发送数据失败
#define ERROR_NETCORE_TCP_OVERLAPPED_SEND_PARAMENT 0x10A4461             //参数错误，无法继续
#define ERROR_NETCORE_TCP_OVERLAPPED_SEND_CREATEEVENT 0x10A4462          //创建事件失败
#define ERROR_NETCORE_TCP_OVERLAPPED_SEND_NOTFOUND 0x10A4463             //没有找到这个客户端，无法发送数据
#define ERROR_NETCORE_TCP_OVERLAPPED_SEND_HASERROR 0x10A4464             //内部发生一个严重的错误，你需要自己处理错误码，NetCore_GetLastError() 参数获取系统级错误
#define ERROR_NETCORE_TCP_OVERLAPPED_GETFLOW_PARAMENT 0x10A4470          //获取流量失败，参数错误
#define ERROR_NETCORE_TCP_OVERLAPPED_SETCALL_PARAMENT 0x10A4480          //设置回调失败，参数错误
/************************************************************************/
/*                    IOCP服务器错误导出                                */
/************************************************************************/
#define ERROR_NETCORE_SOCKET_TCP_IOCP_START_CREATEIO 0x10A4500           //IOCP服务器没有初始化，无法继续
#define ERROR_NETCORE_SOCKET_TCP_IOCP_START_CREATETHREADPOOL 0x10A4501   //回调函数参数错误，有空情况
#define ERROR_NETCORE_SOCKET_TCP_IOCP_START_CREATETHREADACCEPT 0x10A4502 //启动IOCPTCP服务器失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_START_MALLOC 0x10A4503             //申请内存失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_DESTROY_CLOSETHREAD 0x10A4510      //初始化监听失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SENDMSG_PARAMENT 0x10A4520         //初始化服务失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SENDMSG_NOTFOUND 0x10A4521         //发送失败,没有找到指定用户
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SENDMSG_BREAK 0x10A4522            //此客户端被设置为跳过数据处理
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SENDMSG_UNKNOW 0x10A4523           //获取流量失败，参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SENDMSG_SENDFINISH 0x10A4524       //发送失败,发送没有完成
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SENDMSG_OVERFLOW 0x10A4525         //缓冲区满了
#define ERROR_NETCORE_SOCKET_TCP_IOCP_CLOSE_PARAMENT 0x10A4530           //没有找到此地址，可能没有连接或者查询错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETFLOW_PARAMENT 0x10A4540         //套接字是错误的，虽然找到，但是无法继续
#define ERROR_NETCORE_SOCKET_TCP_IOCP_REGCALLBACK_PARAMENT 0x10A4550     //没有找到套接字，无法继续
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SETSTATUS_PARAMENT 0x10A4560       //设置失败，参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_SETSTATUS_NOTFOUND 0x10A4561       //没有找到客户端
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETADDRSOCKET_PARAMENT 0x10A4570   //关闭指定客户端失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETADDRSOCKET_ISFAILED 0x10A4571   //没有找到可以关闭的客户端
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETSOCKET_PARAMENT 0x10A4580       //找到地址，但是SOCKET是错误的，或许已经关闭成功，或许出现问题
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETSOCKET_NOTFOUND 0x10A4581       //关闭网络IO服务器失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETSOCKET_BADSOCKET 0x10A4582      //获取数量参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_ACCEPT_ISFAILED 0x10A4590          //设置最大客户数量参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_ACCEPT_ADDCTL 0x10A4591            //获取失败，参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_ACCEPT_MALLOC 0x10A4592            //获取失败，内部错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETRECVTIME_PARAMENT 0x10A45A0     //参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETRECVTIME_NOTFOUND 0x10A45A1     //没有找到指定客户端
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETALL_PARAMENT 0x10A45B0          //获取失败，参数错误
#define ERROR_NETCORE_SOCKET_TCP_IOCP_GETALL_NOTCLIENT 0x10A45B1         //获取失败，没有客户端在线
#define ERROR_NETCORE_SOCKET_TCP_IOCP_EX_MALLOC 0x10A45F0                //申请内存失败
#define ERROR_NETCORE_SOCKET_TCP_IOCP_EX_NOTFOUND 0x10A45F1              //没有找到
//////////////////////////////////////////////////////////////////////////
//                    UDP服务器错误导出
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                    UDP-IOCP服务器错误导出                            */
/************************************************************************/
#define ERROR_NETCORE_SOCKET_UDP_IOCP_START_CREATEIO 0x10A4600            //创建事件失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_START_CREATETHREADPOOL 0x10A4601    //创建线程池失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_CREATE_BINDIOCP 0x10A4610           //绑定IOCP失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_CREATE_MALLOC 0x10A4611             //申请内存失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_CREATE_UNKNOW 0x10A4612             //未知错误 
#define ERROR_NETCORE_SOCKET_UDP_IOCP_GETFLOW_PARAMENT 0x10A4520          //获取流量参数为空
#define ERROR_NETCORE_SOCKET_UDP_IOCP_SENDMSG_PARAMENT 0x10A4630          //参数错误，发送失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_SENDMSG_ISFAILED 0x10A4631          //发送失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_SENDMSG_LEN 0x10A4632               //发送大小不一致
#define ERROR_NETCORE_SOCKET_UDP_IOCP_REGCALLBACK_PARAMENT 0x10A4640      //设置回调失败，参数错误
#define ERROR_NETCORE_SOCKET_UDP_IOCP_EX_MALLOC 0x10A46A0                 //扩展函数启动失败,申请内存失败
#define ERROR_NETCORE_SOCKET_UDP_IOCP_EX_NOTFOUND 0x10A46A1               //没有找到指定的句柄
/************************************************************************/
/*                    UDP-SELECT服务器错误导出                          */
/************************************************************************/
#define ERROR_NETCORE_UDP_SELECT_INIT_PARAMENT 0x10A4701                  //参数错误，初始化UDP失败
#define ERROR_NETCORE_UDP_SELECT_INIT_MALLOC 0x10A4702                    //申请内存失败
#define ERROR_NETCORE_UDP_SELECT_INIT_SOCKET 0x10A4703                    //设置套接字属性失败
#define ERROR_NETCORE_UDP_SELECT_INIT_BIND 0x10A4704                      //绑定服务失败
#define ERROR_NETCORE_UDP_SELECT_INIT_CREATETHREAD 0x10A4705              //创建线程失败
#define ERROR_NETCORE_UDP_SELECT_SENDTO_PARAMENT 0x10A4721                //参数错误
#define ERROR_NETCORE_UDP_SELECT_SENDTO_NOTFOUND 0x10A4722                //没有找到指定客户端
#define ERROR_NETCORE_UDP_SELECT_SENDTO_ISFAILED 0x10A4723                //发送失败，内部错误
#define ERROR_NETCORE_UDP_SELECT_RECV_PARAMENT 0x10A4730                  //参数错误
#define ERROR_NETCORE_UDP_SELECT_RECV_CALLBACK 0x10A4731                  //模式错误，不能主动接受数据
#define ERROR_NETCORE_UDP_SELECT_RECV_NOTFOUND 0x10A4732                  //没有找到指定客户端
#define ERROR_NETCORE_UDP_SELECT_RECV_ISFAILED 0x10A4733                  //接受失败，内部错误
#define ERROR_NETCORE_UDP_SELECT_RECV_NOTBIND 0x10A4734                   //没有绑定端口,无法继续
#define ERROR_NETCORE_UDP_SELECT_SETMODE_NOTFOUND 0x10A4740               //没有找到指定客户端
//////////////////////////////////////////////////////////////////////////
//                    网络引擎核心帮助函数错误导出
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                    TCP-UDP端口获取列表导出错误表                     */
/************************************************************************/
#define ERROR_NETCORE_NETHELP_ISPORTUSED_NOTFOUND 0x10A5001    //没有找到
#define ERROR_NETCORE_NETHELP_ISPORTUSED_NOTSUPPORT 0x10A5002  //不支持的协议
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_PARAMENT 0x10A5010                //参数错误
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_TCPV4_MALLOCFORTABLE 0x10A5011    //申请内存失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_TCPV4_GETTABLEFAILED 0x10A5012    //获取TCP列表失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_UDPV4_MALLOCFORTABLE 0x10A5013    //申请内存失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_UDPV4_GETTABLEFAILED 0x10A5015    //获取UDP列表失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_NOTFOUND 0x10A5016                //没有找到指定端口，这个端口没有被使用
#define ERROR_NETCORE_NETHELP_PIDTONAME_OPENPID 0x10A5020                //打开PID失败
#define ERROR_NETCORE_NETHELP_PIDTONAME_ENUMFIRST 0x10A5021              //枚举第一个列表失败
#define ERROR_NETCORE_NETHELP_PIDTONAME_NOTFOUND 0x10A5022               //没有找到指定的PID
/************************************************************************/
/*                     网络连接类型错误导出表                           */
/************************************************************************/
#define ERROR_NETCORE_NETHELP_NETCONNECTTYPE_NOCONNECT 0x10A5110         //可能没有连接，可能发生底层错误
#define ERROR_NETCORE_NETHELP_NETCONNECTTYPE_UNKNOW 0x10A5111            //无法识别的错误
#define ERROR_NETCORE_NETHELP_ROTOCOLSTATICS_IPINFO 0x10A5112            //获取IP状态信息失败
#define ERROR_NETCORE_NETHELP_ROTOCOLSTATICS_TCPINFO 0x10A5113           //获取TCP状态信息失败
#define ERROR_NETCORE_NETHELP_ROTOCOLSTATICS_UDPINFO 0x10A5114           //获取UDP状态信息失败
#define ERROR_NETCORE_NETHELP_ROTOCOLSTATICS_ICMPINFO 0x10A5115          //获取ICMP状态信息失败
//进程网络连接状态设置获取错误表
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_SET_ISFAILED 0x10A51A0   //设置进程连接状态失败
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_SET_NOTSUPPET 0x10A51A1  //不支持的状态设置
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_GETLIST_TABLE 0x10A51A2  //获取表错误
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_GETLIST_MALLOC 0x10A51A3 //申请内存错误
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_GETLIST_FIALED 0x10A51A4 //获取列表错误，无法预知的问题
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_GETLIST_NUMBER 0x10A51A5 //数量错误，无法继续
#define ERROR_NETCORE_NETHELP_PROCESSNETSTATUES_GETLIST_BUFFER 0x10A51A6 //BUFFER空间错误
/************************************************************************/
/*                     本地网络信息参数错误                             */
/************************************************************************/
#define ERROR_NETCORE_NETHELP_NETTABLE_PARAMENT 0x10A5200                //参数错误,无法继续
#define ERROR_NETCORE_NETHELP_NETTABLE_GETNETPARAMS 0x10A5201            //获取网络参数错误
/************************************************************************/
/*                     网络地址获取                                     */
/************************************************************************/
//主机地址
#define ERROR_NETCORE_NETHELP_GETHOSTNAME_PARAMENT 0x10A5320             //参数错误
#define ERROR_NETCORE_NETHELP_GETHOSTNAME_IPADDR 0x10A5321               //不是标准的地址
#define ERROR_NETCORE_NETHELP_GETHOSTNAME_ISFAILED 0x10A5322             //获取失败，内部错误
//
#define ERROR_NETCORE_NETHELP_DOMAINTOADDR_PARAMENT 0x10A5330            //解析失败,参数错误
#define ERROR_NETCORE_NETHELP_DOMAINTOADDR_NOTDOMAIN 0x10A5331           //不是域名或者没有连接网络
#define ERROR_NETCORE_NETHELP_GETNCLIST_PARAMENT 0x10A5330               //参数错误
#define ERROR_NETCORE_NETHELP_GETNCLIST_MALLOC 0x10A5331                 //申请内存失败
#define ERROR_NETCORE_NETHELP_GETNCLIST_ISFAILED 0x10A5332               //获取失败,内部错误
#define ERROR_NETCORE_NETHELP_GETNCLIST_EMPTY 0x10A5333                  //为空,没有获取到网卡信息
//////////////////////////////////////////////////////////////////////////
//                     网络高级操作导出错误类型
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                      组播管理器错误                                    */
/************************************************************************/
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_PARAMENT 0x10A6001     //参数错误，创建发送者失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_SOCKET 0x10A6002       //获取套接字失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_SETOPTLOOP 0x10A6003   //设置LOOP属性失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_SETOPTTTL 0x10A6004    //设置TTL属性失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_PARAMENT 0x10A6010     //参数错误，创建接受这失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_SOCKET 0x10A6011       //设置套接字失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_SETOPTREUSE 0x10A6012  //设置地址重用失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_SETOPTLOOP 0x10A6013   //设置LOOP属性失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_BIND 0x10A6014         //绑定端口失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_JOIN 0x10A6015         //加入组播失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_PARAMENT 0x10A6020        //参数错误，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_NOTFOUND 0x10A6021        //没有找到，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_NOTSENDER 0x10A6022       //不是发送者，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_ISFAILED 0x10A6023        //发送失败，网络错误
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_PARAMENT 0x10A6030        //参数错误
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_NOTFOUND 0x10A6031        //没有找到
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_NOTRECVER 0x10A6032       //不是接受者，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_ISFAILED 0x10A6033        //接受失败，网络错误
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_CLOSE_NOTFOUND 0x10A6040        //没有找到，无法关闭
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_CLOSE_LEAVE 0x10A6041           //离开组播失败
/************************************************************************/
/*                      广播通信管理错误导出表                              */
/************************************************************************/
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_PARAMENT 0x10A6100     //参数错误，无法继续
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_SETSOCKET 0x10A6101    //设置套接字属性失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_SETBROADCAST 0x10A6102 //初始化广播参数失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_SETOPTREUSE 0x10A6103  //设置端口复用失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_BIND 0x10A6104         //绑定服务地址端口失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_PARAMENT 0x10A6110     //参数错误
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_SETSOCKET 0x10A6111    //设置套接字失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_SETOPTREUSE 0x10A6112  //设置地址重用失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_BIND 0x10A6113         //绑定端口失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_FIND_NOTFOUNDID 0x10A6120       //没有找到套接字
#define ERROR_NETCORE_SOCKETOP_BROADCAST_CLOSE_NOTFOUND 0x10A6130        //没有找到，不需要关闭
#define ERROR_NETCORE_SOCKETOP_BROADCAST_CLOSE_CLOSESOCKET 0x10A6131     //关闭套接字失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_SEND_PARAMENT 0x10A6140         //参数错误，无法继续
#define ERROR_NETCORE_SOCKETOP_BROADCAST_SEND_NOTSENDER 0x10A6141        //不是发送者句柄，不能发送数据
#define ERROR_NETCORE_SOCKETOP_BROADCAST_SEND_SENDTOFAILED 0x10A6142     //发送失败，SOCKET错误
#define ERROR_NETCORE_SOCKETOP_BROADCAST_RECV_PARAMENT 0x10A6150         //接受失败，参数错误
#define ERROR_NETCORE_SOCKETOP_BROADCAST_RECV_NOTRECVER 0x10A6151        //不是接受者套接字
#define ERROR_NETCORE_SOCKETOP_BROADCAST_RECV_RECVISFIALED 0x10A6152     //接受失败，套接字错误
/************************************************************************/
/*                      心跳管理错误导出表                              */
/************************************************************************/
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_PARAMENT 0x10A6200          //初始化失败，参数错误，时间次数不能小于1
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_CREATETHREAD 0x10A6201      //创建线程失败
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DESTROY_TERMINATE 0x10A6210      //销毁失败。结束线程失败
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTADDR_NOTRUN 0x10A6220      //没有运行，无法继续
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTADDR_NOTSUPPORT 0x10A6221  //不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTADDR_PARAMENT 0x10A6222    //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTSOCKET_NOTRUN 0x10A6230    //没有运行
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTSOCKET_NOTSUPPORT 0x10A6231//不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTSOCKET_PARAMENT 0x10A6232  //参数错误 
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_PARAMENT 0x10A6240    //激活客户端失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_NOTRUN 0x10A6241      //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_NOTSUPPORT 0x10A6242  //不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_NOTFOUND 0x10A6243    //没有找到客户端
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_NOTRUN 0x10A6250    //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_NOTSUPPORT 0x10A6251//不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_PARAMENT 0x10A6252  //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_NOTFOUND 0x10A6253  //没有找到客户端
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_PARAMENT 0x10A6260    //删除失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_NOTFOUND 0x10A6261    //删除失败，没有找到
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_NOTRUN 0x10A6262      //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_NOTSUPPORT 0x10A6263  //不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_NOTRUN 0x10A6270    //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_NOTSUPPORT 0x10A6271//不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_PARAMENT 0x10A6272  //删除失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_NOTFOUND 0x10A6273  //删除失败，没有找到
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_PARAMENT 0x10A6280    //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_CALLBACK 0x10A6281    //设置了回调函数，不允许使用此函数
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_NOTCLIENT 0x10A6282   //没有客户端断线
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_NOTRUN 0x10A6283      //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_FORCEOUT_PARAMENT 0x10A6290      //参数错误，无法继续
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_FORCEOUT_NOTFOUND 0x10A6291      //没有找到指定的客户端地址
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_SETLOAD_PARAMENT 0x10A62A0       //设置负载失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETLOAD_PARAMENT 0x10A62A1       //获取失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETLOAD_NOTFOUND 0x10A62A2       //获取失败，没有找到
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_EX_MALLOC 0x10A62F0         //申请内存失败
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_EX_NOTFOUND 0x10A62F1       //没有找到指定的心跳管理器
//////////////////////////////////////////////////////////////////////////
//                      NETBIOS服务器导出错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_NETBIOS_ENUMLANA 0x10A7001                         //枚举BIOS列表失败
#define ERROR_NETCORE_NETBIOS_RESETLANA 0x10A7002                        //重置所有列表失败
#define ERROR_NETCORE_NETBIOS_ADDNAME 0x10A7003                          //添加名称失败
#define ERROR_NETCORE_NETBIOS_LISTEN 0x10A7004                           //监听失败
#define ERROR_NETCORE_NETBIOS_RECV 0x10A7005                             //接受数据失败
#define ERROR_NETCORE_NETBIOS_ADDGROUP 0x10A7006                         //添加到一个组失败
#define ERROR_NETCORE_NETBIOS_DELETENAME 0x10A7007                       //删除BIOS名称失败
#define ERROR_NETCORE_NETBIOS_DISCONNECT 0x10A7008                       //断开指定链接失败
#define ERROR_NETCORE_NETBIOS_CANCEL 0x10A7009                           //取消一个正在执行的未决命令失败
#define ERROR_NETCORE_NETBIOS_BCSEND 0x10A700A                           //广播消息失败
#define ERROR_NETCORE_NETBIOS_SNEDPOINT 0x10A700B                        //点对点发送失败
#define ERROR_NETCORE_NETBIOS_DGRAM_BCRECV 0x10A700C                     //接受广播数据报失败
#define ERROR_NETCORE_NETBIOS_DGRAM_RECV 0x10A700D                       //接受点对点数据报失败
#define ERROR_NETCORE_NETBIOS_STREAM_START_ALLOCFORNCB 0x10A7010         //申请NCB内存失败
#define ERROR_NETCORE_NETBIOS_STREAM_START_CREATETHREAD 0x10A7011        //为流式BIOS创建线程失败
#define ERROR_NETCORE_NETBIOS_DGRAM_START_ALLOCFORLANA 0x10A7020         //为数据包申请空间失败
#define ERROR_NETCORE_NETBIOS_DGRAM_RECVEX_WAITFORSINGLEMULTI 0x10A7021  //等待事件触发失败
#define ERROR_NETCORE_NETBIOS_DGRAM_RECVEX_WAITFORSINGLEPOINT 0x10A7022  //等待单独的事件失败
#define ERROR_NETCORE_NETBIOS_DGRAM_SETMODE_NOTSUPPED 0x10A7023          //不支持这种模式
//////////////////////////////////////////////////////////////////////////
//                    UDX传输协议导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETCORE_UDX_ACCEPT_PARAMENT 0x10A8000             //参数错误,接受处理失败
#define ERROR_NETENGINE_NETCORE_UDX_ACCEPT_MALLOC 0x10A8001               //申请内存失败
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_PARAMENT 0x10A8010              //参数错误,接受数据失败
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_NOTFOUND 0x10A8011              //没有找到指定的句柄
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_WINDOWFULL 0x10A8012            //滑动窗口已满,无法接受数据
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_PROTOCOL 0x10A8013              //协议不正确
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_SAME 0x10A8014                  //相同的序列号包
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_RETRY 0x10A8015                 //报告了一个重传错误,某一个序列号已经被丢弃
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_NOLOGIN 0x10A8016               //用户没有登录,无法继续
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_TIMEOUT 0x10A8017               //接收到一个过期的包
#define ERROR_NETENGINE_NETCORE_UDX_SEND_PARAMENT 0x10A8020               //参数错误,发送失败
#define ERROR_NETENGINE_NETCORE_UDX_SEND_NOTFOUND 0x10A8021               //没有找到指定客户端
#define ERROR_NETENGINE_NETCORE_UDX_SEND_WINDOWSIZE 0x10A8022             //对端阻塞,无法发送数据
#define ERROR_NETENGINE_NETCORE_UDX_RECV_PARAMENT 0x10A8030               //参数错误,接受数据失败
#define ERROR_NETENGINE_NETCORE_UDX_RECV_NOTFOUND 0x10A8031               //没有找到指定客户端
#define ERROR_NETENGINE_NETCORE_UDX_RECV_ISNULL 0x10A8032                 //数据队列为空
#define ERROR_NETENGINE_NETCORE_UDX_RECV_NOTDATA 0x10A8033                //没有数据或者包不完成
#define ERROR_NETENGINE_NETCORE_UDX_INIT_PARAMENT 0x10A8040               //初始化失败,参数错误
#define ERROR_NETENGINE_NETCORE_UDX_INIT_CREATETHREAD 0x10A8041           //初始化失败,创建线程失败
#define ERROR_NETENGINE_NETCORE_UDX_CBSET_PARAMENT 0x10A8050              //设置回调失败,参数错误
#define ERROR_NETENGINE_NETCORE_UDX_EX_MALLOC 0x10A80A0                   //申请内存错误
#define ERROR_NETENGINE_NETCORE_UDX_EX_NOTFOUND 0x10A80A1                 //没有找到指定服务
//////////////////////////////////////////////////////////////////////////
//                     无线通信核心模块错误导出表
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                     蓝牙API错误表                                    */
/************************************************************************/
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_BEGINLOOKUP 0x10A0001           //准备循环查找失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_PARAM 0x10A0002                 //参数传递错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_GETDEVINFO 0x10A0003            //获取设备信息错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_UNKNOW 0x10A0004                //未知错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_BTHADDRISNULL 0x10A0005         //蓝牙地址为空
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_FINDFIRSTENUM 0x10A0006         //开始枚举本地蓝牙设备失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETSTATE1 0x10A0007             //设置蓝牙状态失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETSTATE2 0x10A0008             //设置蓝牙状态失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETSTATE3 0x10A0009             //设置蓝牙状态失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_ENUMINSTALLSRV 0x10A000A        //枚举已经安装服务失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SELECTDEV 0x10A000B             //选择设备失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETSRVSTATE 0x10A000C           //设置蓝牙设备状态失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_ISOPENED 0x10A000D              //蓝牙设备已经打开了
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_COMNUMBER 0x10A0011             //串口号码范围错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_OPENCOM 0x10A0012               //打开蓝牙端口失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_GETCOMSTATE 0x10A0013           //获取蓝牙串口状态失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETTIMEOUT 0x10A0014            //设置超时时间失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETDTR 0x10A0005                //设置电位失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_GETPROPERTIES 0x10A0016         //获取通信属性失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_INPUTBUFFISSMALL 0x10A0017      //输入缓冲区太小了
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_READUNKOWN 0x10A0018            //读取缓冲区发生未知错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_WRITEBUFTOOSMALL 0x10A0019      //写入缓冲区太小了
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_WRITEUNKOWN 0x10A001A           //写入缓冲区发生未知错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_NOINIT 0x10A001B                //设备没有准备好或者没有初始化
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SETMASK 0x10A001C               //设置蓝牙串口事件失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_WAITEVENT 0x10A001D             //设置等待事件失败
/************************************************************************/
/*                     红外线错误导出列表                               */
/************************************************************************/
#define ERROR_NETCORE_WIRELESS_INFRARED_INIT_WSASOCKET 0x10A0110          //设置套接字模式失败
#define ERROR_NETCORE_WIRELESS_INFRARED_INIT_GETOPTENUMDEV 0x10A0111      //获取套接字选项失败
#define ERROR_NETCORE_WIRELESS_INFRARED_INIT_GETQUERYOPT 0x10A0112        //获取查询选项失败
//////////////////////////////////////////////////////////////////////////
//                     SPI错误导出表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_SPI_PROVIDER_INSTALL_PARAMENT 0x10AF001            //安装失败,参数错误
#define ERROR_NETCORE_SPI_PROVIDER_INSTALL_LSPUUID 0x10AF002             //获取LSP UUID失败
#define ERROR_NETCORE_SPI_PROVIDER_INSTALL_LSP 0x10AF003                 //安装LSP失败
#define ERROR_NETCORE_SPI_PROVIDER_INSTALL_GETUUID 0x10AF004             //获取安装的UUID失败
#define ERROR_NETCORE_SPI_PROVIDER_INSTALL_UUID 0x10AF005                //安装UUID失败
#define ERROR_NETCORE_SPI_PROVIDER_INSTALL_WRITEFIRST 0x10AF006          //写入第一条失败
#define ERROR_NETCORE_SPI_PROVIDER_DELETE_PARAMENT 0x10AF010             //参数错误
#define ERROR_NETCORE_SPI_PROVIDER_DELETE_NOTFOUND 0x10AF011             //没有找到协议
#define ERROR_NETCORE_SPI_PROVIDER_DELETE_REMOVEPROTO 0x10AF012          //移除协议失败
#define ERROR_NETCORE_SPI_PROVIDER_DELETE_REMOVELAYERED 0x10AF013        //移除协议层失败
#define ERROR_NETCORE_SPI_PROVIDER_GETPROTO_PARAMENT 0x10AF020           //获取协议列表失败,参数错误
#define ERROR_NETCORE_SPI_PROVIDER_GETPROTO_GETFAILED 0x10AF021          //获取错误,内部错误
#define ERROR_NETCORE_SPI_PROVIDER_GETPROVIDER_WSCENUMPROTOCOL 0x10AF030 //枚举WSC协议链失败
#define ERROR_NETCORE_SPI_PROVIDER_FREEPROVIDER_WSCENUMPROTOCOL 0x10AF040//释放空间失败