#pragma once
/********************************************************************
//	Created:	2012/1/24  16:25
//	File Name: 	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngine_NetXApi\NetXApi_Error.h
//	File Path:	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngine_NetXApi
//	File Base:	NetXApi_Error
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	网络XAPI 错误导出表
//	History:    
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                       网络地址表错误导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_NETXAPI_NETADDRESS_QQWAY_ISOPEN 0x10C0000           //已经打开了，不需要再次打开
#define ERROR_NETCORE_NETXAPI_NETADDRESS_QQWAY_OPENFILE 0x10C0002         //打开文件失败
#define ERROR_NETCORE_NETXAPI_NETADDRESS_QQWAY_CLOSEFILE 0x10C0003        //关闭文件失败
#define ERROR_NETCORE_NETXAPI_NETADDRESS_QQWAY_NOTFOUND 0x10C0004         //没有找到指定IP地址数据
//////////////////////////////////////////////////////////////////////////
//                       网络嗅探器错误导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_NETXAPI_SNIFFER_START_PARAMENT 0x10C1001            //参数错误
#define ERROR_NETCORE_NETXAPI_SNIFFER_START_MALLOC 0x10C1002              //申请内存失败
#define ERROR_NETCORE_NETXAPI_SNIFFER_START_OPENDEV 0x10C1003             //打开设备失败
#define ERROR_NETCORE_NETXAPI_SNIFFER_START_THREAD 0x10C1004              //启动线程失败
#define ERROR_NETCORE_NETXAPI_SNIFFER_FILTER_NOTFOUND 0x10C1010           //过滤器设置失败,句柄没有找到
#define ERROR_NETCORE_NETXAPI_SNIFFER_FILTER_COMPILE 0x10C1011            //编译过滤器失败,可能语法错误
#define ERROR_NETCORE_NETXAPI_SNIFFER_FILTER_SET 0x10C1012                //设置失败,内部错误
#define ERROR_NETCORE_NETXAPI_SNIFFER_WRITE_NOTFOUND 0x10C1020            //没有找到句柄
#define ERROR_NETCORE_NETXAPI_SNIFFER_WRITE_OPEN 0x10C1021                //创建文件失败
#define ERROR_NETCORE_NETXAPI_SNIFFER_GETIF_PARAMENT 0x10C1030            //参数错误
#define ERROR_NETCORE_NETXAPI_SNIFFER_GETIF_FIND 0x10C1031                //没有找到设备
#define ERROR_NETCORE_NETXAPI_SNIFFER_GETIF_EMPTY 0x10C1032               //设备列表为空
#define ERROR_NETCORE_NETXAPI_SNIFFER_GETIF_STRFORMAT 0x10C1040           //参数错误
//////////////////////////////////////////////////////////////////////////
//                       网络流量获取错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_NETXAPI_FLOW_NET_GETFLOW_PARAMENT 0x10C2001         //获取失败,参数错误
#define ERROR_NETCORE_NETXAPI_FLOW_NET_GETFLOW_ISFAILED 0x10C2002         //内部错误
#define ERROR_NETCORE_NETXAPI_FLOW_NET_GETFLOW_STATUS 0x10C2005           //找到了网卡,但是网卡状态不对(没启用或者网卡没有联网)
#define ERROR_NETCORE_NETXAPI_FLOW_NET_GETFLOW_NOTFOUND 0x10C2006         //没有找到网卡,无法继续
//流量控制错误
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_INIT_PARAMENT 0x10C3000           //初始化失败,参数错误
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_INIT_REG 0x10C3001                //初始化失败,注册失败
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_INIT_ENUM 0x10C3002               //枚举设备失败
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_INIT_MALLOC 0x10C3003             //申请内存失败
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_INIT_OPEN 0x10C3004               //打开设备失败
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_ADDFLOW_PARAMENT 0x10C3010        //添加失败,参数错误
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_ADDFLOW_NOTFOUND 0x10C3011        //没有找到主句柄
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_ADDFLOW_MALLOC 0x10C3012          //申请内存失败
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_ADDFLOW_ISFAILED 0x10C3013        //添加流量规则失败
#define ERROR_NETCORE_NETXAPI_FLOW_CTRL_ADDFLOW_FILTER 0x10C3014          //添加过滤器失败
//////////////////////////////////////////////////////////////////////////
//                       枚举局域网计算机信息错误表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_NETXAPI_LANENUM_START_PARAMENT 0x10C4001            //参数错误
#define ERROR_NETCORE_NETXAPI_LANENUM_START_MALLOC 0x10C4002              //申请内存失败
#define ERROR_NETCORE_NETXAPI_LANENUM_START_OPENDEV 0x10C4003             //打开设备失败
#define ERROR_NETCORE_NETXAPI_LANENUM_START_COMPILE 0x10C4004             //编译失败
#define ERROR_NETCORE_NETXAPI_LANENUM_START_SETFILTER 0x10C4005           //设置过滤器失败
#define ERROR_NETCORE_NETXAPI_LANENUM_START_THREAD 0x10C4006              //启动线程失败