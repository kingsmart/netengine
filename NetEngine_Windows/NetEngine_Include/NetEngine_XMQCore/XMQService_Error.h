#pragma once
/********************************************************************
//	Created:	    2017/4/20   10:05
//	Filename: 	/NetEngine_Linux/NetEngine_SourceCode/NetEngine_NetHelp/NetEngine_MQXService/MQXService_Error.h
//	File Path:	/NetEngine_Linux/NetEngine_SourceCode/NetEngine_NetHelp/NetEngine_MQXService/
//	File Base:	MQXService_Error
//	File Ext:	    h
//  Project:     NetEngine_Linux(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	    消息队列导出错误
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                       消息队列错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_XMQ_SERVICE_INIT_CREATETHREAD 0x160100            //初始化失败，创建线程失败
#define ERROR_NETENGINE_XMQ_SERVICE_POST_PARAMENT 0x160101                //参数错误，投递失败
#define ERROR_NETENGINE_XMQ_SERVICE_POST_MALLOC 0x160102                  //申请内存失败，无法继续
#define ERROR_NETENGINE_XMQ_SERVICE_PUSHLIST_ADDRKEEPNULL 0x160110        //投递失败，地址参数和时间不能同时为空
#define ERROR_NETENGINE_XMQ_SERVICE_PUSHLIST_MAXSIZE 0x160111             //最大包个数，无法继续投递
#define ERROR_NETENGINE_XMQ_SERVICE_PUSHLIST_SERIALEXIST 0x160112         //序列号已经存在，无法继续投递
#define ERROR_NETENGINE_XMQ_SERVICE_PUSHMAP_MAXSIZE  0x160120             //最大包个数，无法继续
#define ERROR_NETENGINE_XMQ_SERVICE_PUSHMAP_SERIALEXIST 0x160121          //包序列号已经存在，无法继续
#define ERROR_NETENGINE_XMQ_SERVICE_GETLIST_EMPTY 0x160130                //获取数据失败，列表容器为空
#define ERROR_NETENGINE_XMQ_SERVICE_GETLIST_NOTFOUND 0x160131             //获取失败，没有找到指定序列数据
#define ERROR_NETENGINE_XMQ_SERVICE_GETLIST_LEN 0x160132                  //长度错误，提供的缓冲区太小了
#define ERROR_NETENGINE_XMQ_SERVICE_GETMAP_EMPTY 0x160140                 //获取数据失败，KEY容器列表为空
#define ERROR_NETENGINE_XMQ_SERVICE_GETMAP_NOTFOUND 0x160141              //没有找到指定序列数据
#define ERROR_NETENGINE_XMQ_SERVICE_GETMAP_LEN 0x160142                   //数据长度不够
#define ERROR_NETENGINE_XMQ_SERVICE_GET_PARAMENT 0x160150                 //参数错误，无法继续
#define ERROR_NETENGINE_XMQ_SERVICE_DEL_PARAMENT 0x160160                 //删除失败，参数错误
//////////////////////////////////////////////////////////////////////////
//                       消息协议错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_XMQ_PROTOCOL_PACKET_PARAMENT 0x160200             //参数错误，组包失败
#define ERROR_NETENGINE_XMQ_PROTOCOL_POSTPACKET_PARAMENT 0x160210         //参数错误
#define ERROR_NETENGINE_XMQ_PROTOCOL_GETPACKET_PARAMENT 0x160220
#define ERROR_NETENGINE_XMQ_PROTOCOL_DELETEPACKET_PARAMENT 0x160230
