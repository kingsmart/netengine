#pragma once
/********************************************************************
//	Created:	2019/1/18   9:32
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_Sip\RfcSip_Error.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_Sip
//	File Base:	RfcSip_Error
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	SIP协议导出错误
//	History:    
*********************************************************************/
/************************************************************************/
/*                     SIP协议端导出错误                                */
/************************************************************************/
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSE_PARAMENT 0x120C0001        //初始化失败，参数错误
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEHDR_NOTCOMPLETE 0x120C0010  //协议不完成
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEHDR_LOST 0x120C0011         //协议丢失或者被损坏
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEHDR_FAILED 0x120C0012       //协议头错误,协议不正确
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PACKET_PARAMENT 0x120C0020       //参数错误
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEPRO_VIA 0x120C00A0          //协议错误,无法继续
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEPRO_VIANOTCOMPLETE 0x120C00A1  //协议错误,不完整
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEPRO_CONTACT 0x120C00A2      //协议错误,无法继续
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEPRO_TO 0x120C00A3           //协议错误,无法继续
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEPRO_FROM 0x120C00A4         //协议解析失败
#define ERROR_RFCCOMPONENTS_SIP_PROTOCOL_PARSEPRO_AUTH 0x120C00A5         //协议错误,无法继续
/************************************************************************/
/*                     SIP组包器导出错误                                */
/************************************************************************/
#define ERROR_RFCCOMPONENTS_SIP_PACKET_INSERT_PARAMENT 0x120C1001         //参数错误
#define ERROR_RFCCOMPONENTS_SIP_PACKET_INSERT_MALLOC 0x120C1002           //申请内存失败
#define ERROR_RFCCOMPONENTS_SIP_PACKET_INSERT_PROTOCOL 0x120C1003         //协议错误
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GET_PARAMENT 0x120C1010            //参数错误
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GET_EMPTY 0x120C1011               //数据队列什么都没有
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GET_NOTFOUND 0x120C1012            //没有找到指定客户端
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GET_NOTDATA 0x120C1013             //指定客户端没有数据
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GET_SIZE 0x120C1014                //提供的缓冲区大小不够
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GETRANDOM_PARAMENT 0x120C1020      //参数错误
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GETRANDOM_EMPTY 0x120C1021         //没有队列
#define ERROR_RFCCOMPONENTS_SIP_PACKET_GETRANDOM_NOTPKT 0x120C1022        //没有可取的数据包
/************************************************************************/
/*                     SIP组包器导出错误                                */
/************************************************************************/
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_INITCODE_NOTFOUND 0xA0B2000        //参数错误
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_INITCODE_PARSE 0xA0B2001           //解析错误
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_GETCODE_PARAMENT 0xA0B2010         //参数错误
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_GETCODE_NOTFOUND 0xA0B2011         //没有找到
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_INITMIME_PARAMENT 0xA0B2020        //参数错误，初始化失败
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_INITMIME_READFILE 0xA0B2021        //读取配置文件错误
#define ERROR_RFCCOMPONENTS_SIP_CONFIG_GETMIME_PARAMENT 0xA0B2030         //参数错误
/************************************************************************/
/*                     SIP客户端导出错误                                */
/************************************************************************/
#define ERROR_RFCCOMPONENTS_SIP_SERVER_INIT_PARAMENT 0xA0B3001            //参数错误
#define ERROR_RFCCOMPONENTS_SIP_SERVER_CREATE_PARAMENT 0xA0B3010          //参数错误
#define ERROR_RFCCOMPONENTS_SIP_SERVER_PROCESS_PARAMENT 0xA0B3020         //参数错误
#define ERROR_RFCCOMPONENTS_SIP_SERVER_PROCESS_NOTFOUND 0xA0B3021         //没有找到客户端
#define ERROR_RFCCOMPONENTS_SIP_SERVER_USER_PARAMENT 0xA0B3101            //参数错误
/************************************************************************/
/*                     SIP客户端导出错误                                */
/************************************************************************/
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_CREATE_PARAMENT 0xA0B4001          //参数错误
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_REGISTER_PARAMENT 0xA0B4010        //参数错误
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_REGISTER_NOTFOUND 0xA0B4011        //没有找到客户端
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_REGISTER_NOTSUPPORTAUTH 0xA0B4012  //服务器需要验证,客户端不支持
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_PROCESS_PARAMENT 0xA0B4020         //参数错误
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_PROCESS_NOTFOUND 0xA0B4021         //没有找到客户端
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_PROCESS_NOTTAUTH 0xA0B4022         //客户端不支持验证
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_PROCESS_NEEDAUTH 0xA0B4023         //需要验证
#define ERROR_RFCCOMPONENTS_SIP_CLIENT_PROCESS_OTHER 0xA0B4024            //服务器返回了其他错误