#pragma once
/********************************************************************
//	Created:	2019/1/18   9:32
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_Sip\RfcSip_Define.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_Sip
//	File Base:	RfcSip_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	SIP协议导出函数
//	History:    
*********************************************************************/
#define SIP_PROTOCOL_STR_TYPE_REGISTER _T("REGISTER")
//////////////////////////////////////////////////////////////////////
//                      SIP事件定义
//////////////////////////////////////////////////////////////////////
typedef enum en_RfcSip_Events
{
	//注册相关事件
	ENUM_RFCCOMPONENTS_SIP_EVENTS_REGISTRATION_PROCESS,            //注册中
	ENUM_RFCCOMPONENTS_SIP_EVENTS_REGISTRATION_SUCCESS,            //用户成功注册
	ENUM_RFCCOMPONENTS_SIP_EVENTS_REGISTRATION_FAILURE,            //用户没有注册或者注册失败
	//通话邀请相关事件
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_INVITE,                     //报告一个新的呼叫
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_REINVITE,                   //报告一个新的呼叫邀请
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_NOANSWER,                   //报告一个超时未应答消息
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_PROCEEDING,                 //报告一个正在处理的远程程序,收到100 trying消息
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_RINGING,                    //报告一个回铃,收到180 Ringing应答
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_ANSWERED,                   //开始通话,表示请求已经被成功接受，用户应答
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_REDIRECTED,                 //通告重定向
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_REQUESTFAILURE,             //请求失败
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_SERVERFAILURE,              //报告一个服务器错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_GLOBALFAILURE,              //报告一个全部失败消息
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_ACK,                        //邀请得到200ok确认
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_CANCELLED,                  //通话被取消
	//请求呼叫中的相关事件（INVITE除外）
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_NEW,                //新的传入请求
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_PROCEEDING,         //报告一个1xx请求
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_ANSWERED,           //200OK
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_REDIRECTED,         //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_REQUESTFAILURE,     //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_SERVERFAILURE,      //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_MESSAGE_GLOBALFAILURE,      //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_CLOSED,                     //此次通话收到了一个BYE
	//UAS和UAC活动
	ENUM_RFCCOMPONENTS_SIP_EVENTS_CALL_RELEASED,                   //呼叫上下文被清除
	//收到外部请求的回复
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_NEW,                     //新的传入请求
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_PROCEEDING,              //报告一个1xx请求
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_ANSWERED,                //200OK
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_REDIRECTED,              //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_REQUESTFAILURE,          //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_SERVERFAILURE,           //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_MESSAGE_GLOBALFAILURE,           //错误
	//在线状态和即时消息
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_NOANSWER,           //没有响应
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_PROCEEDING,         //报告一个1xx请求
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_ANSWERED,           //200OK
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_REDIRECTED,         //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_REQUESTFAILURE,     //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_SERVERFAILURE,      //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_GLOBALFAILURE,      //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_SUBSCRIPTION_NOTIFY,             //报告一个新的通知请求

	ENUM_RFCCOMPONENTS_SIP_EVENTS_IN_SUBSCRIPTION_NEW,             //新的订阅者

	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_NOANSWER,           //没有响应
	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_PROCEEDING,         //报告一个1xx请求
	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_ANSWERED,           //200OK
	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_REDIRECTED,         //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_REQUESTFAILURE,     //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_SERVERFAILURE,      //错误
	ENUM_RFCCOMPONENTS_SIP_EVENTS_NOTIFICATION_GLOBALFAILURE,      //错误
}ENUM_RFCCOMPONENTS_SIP_EVENTS;
//////////////////////////////////////////////////////////////////////////////////
//                         导出的数据结构
//////////////////////////////////////////////////////////////////////////////////
typedef struct
{
	BOOL bRequest;                                                    //为真表示请求,为假表示回复
	//请求消息
	struct
	{
		TCHAR tszMethod[64];                                          //请求的方法 REGISTER
		TCHAR tszClientAddr[64];                                      //客户端地址 (xyry.org:5061;transport=tls) (xyry.org)
		TCHAR tszTansport[64];                                        //传输协议 
		TCHAR tszVersion[64];                                         //版本
	}st_Request;
	//响应消息
	struct
	{
		TCHAR tszVersion[64];                                         //版本
		TCHAR tszMethod[64];                                          //响应内容
		int nCode;                                                    //返回的状态码
	}st_Response;
	//路由信息
	struct
	{
		TCHAR tszAddr[64];                                            //路由地址 192.168.1.104:52400
		TCHAR tszBranch[64];                                          //分支信息 
		TCHAR tszNetType[32];                                         //网络类型 (UDP) (TLS)
		TCHAR tszRecvAddr[32];                                        //穿越地址,只有服务器返回响应才有
		BOOL bRport;                                                  //是否启用NAT穿越
		int nPort;                                                    //穿越端口,只有服务器返回响应才有
	}st_Via;
	//包含信息
	struct
	{
		TCHAR tszAddr[64];                                            //地址 1001-0x20d1a38@192.168.1.104:52400
		TCHAR tszRinstance[64];                                       //多路ID
	}st_Contact;
	//接受者
	struct
	{
		TCHAR tszName[64];                                            //名称 
		TCHAR tszAddr[64];                                            //地址 1001@xyry.org:5061
		TCHAR tszTag[64];                                             //接受者标签,只有响应才有
	}st_To;
	//发送者
	struct
	{
		TCHAR tszName[64];                                            //名称
		TCHAR tszAddr[64];                                            //地址
		TCHAR tszTag[64];                                             //发送者标签
	}st_From;
	//会话标识
	struct
	{
		TCHAR tszUserAgent[128];                                      //用户信息
		TCHAR tszCallID[128];                                         //会话ID
		int nCSeq;                                                    //序列号
		int nExpires;                                                 //过期时间,单位秒
		int nKeepAlive;                                               //包活计时
		int nForward;                                                 //转发跳跃限制
	}st_Session;
	//允许选项
	struct
	{
		BOOL bInvite;                                                 //呼叫
		BOOL bAck;                                                    //确认
		BOOL bBye;                                                    //关闭
		BOOL bCancel;                                                 //挂断
		BOOL bOption;                                                 //选项
		BOOL bRefer;                                                  //隐式订阅
		BOOL bNotify;                                                 //通知消息
		BOOL bSubscribe;                                              //订阅
		BOOL bInfo;                                                   //信息
		BOOL bMessage;                                                //消息
		BOOL bUPDate;                                                 //更新
		BOOL bPublish;                                                //发布
	}st_Allow;
	//支持选项
	struct  
	{
		BOOL bTimer;                                                  //计时器
		BOOL bPath;                                                   //路径选择
		BOOL bReplaces;                                               //替换
	}st_Support;
	//验证结构
	struct
	{
		TCHAR tszRealm[128];                                      
		TCHAR tszNonce[128];
		TCHAR tszOPaque[128];
		TCHAR tszQop[32];
		//以下字段只有请求才有
		TCHAR tszUserName[64];
		TCHAR tszUri[64];
		TCHAR tszNC[64];
		TCHAR tszCNonce[64];
		TCHAR tszResponse[128];
	}st_Authenticate;
	//会话数据大小
	struct
	{
		int nHdrLen;                                                  //协议头大小
		int nBodyLen;                                                 //后续大小 
		TCHAR tszContentType[64];                                     //数据类型
		TCHAR tszBodyBuffer[2048];                                    //后续数据缓冲区
	}st_Context;
}SIPPROTOCOL_HDRINFO;
//////////////////////////////////////////////////////////////////////////////////
//                         导出的函数
//////////////////////////////////////////////////////////////////////////////////
extern "C" BOOL RfcSip_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                     SIP协议端导出函数                                */
/************************************************************************/
/********************************************************************
函数名称：RfcComponents_SipProtocol_Parse
函数功能：解析SIP协议
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要解析的缓冲区
 参数.二：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要解析的缓冲区大小
 参数.三：pSt_SIPProtocol
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：输出解析好的协议
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipProtocol_Parse(LPCTSTR lpszMsgBuffer, int nMsgLen, SIPPROTOCOL_HDRINFO *pSt_SIPProtocol);
/********************************************************************
函数名称：RfcComponents_SipProtocol_PacketRequest
函数功能：SIP请求打包函数
 参数.一：pSt_SIPProtocol
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入协议结构
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出打好包的缓冲区
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：一般的,名字和地址以及协议是必填的,其他可以选填
*********************************************************************/
extern "C" BOOL RfcComponents_SipProtocol_PacketRequest(SIPPROTOCOL_HDRINFO *pSt_SIPProtocol, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：RfcComponents_SipProtocol_PacketResponse
函数功能：SIP响应打包函数
 参数.一：pSt_SIPProtocol
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入协议结构
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出打好包的缓冲区
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：请求的解析结构需要输入到这个参数里面
*********************************************************************/
extern "C" BOOL RfcComponents_SipProtocol_PacketResponse(SIPPROTOCOL_HDRINFO *pSt_SIPProtocol, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/************************************************************************/
/*                     SIP协议端导出函数                                */
/************************************************************************/
/********************************************************************
函数名称：RfcComponents_SipPacket_Init
函数功能：初始化一个SIP组包器
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipPacket_Init();
/********************************************************************
函数名称：RfcComponents_SipPacket_Insert
函数功能：插入一段数据到SIP组包器里面
 参数.一：lpszClientID
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要组包的客户端
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要组包的缓冲区
 参数.二：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：可以把分片的数据组成一个完成的包,如果你确定每次接受的包都是完成的,可以不使用这些函数
*********************************************************************/
extern "C" BOOL RfcComponents_SipPacket_Insert(LPCTSTR lpszClientID, LPCTSTR lpszMsgBuffer, int nMsgLen);
/********************************************************************
函数名称：RfcComponents_SipPacket_Get
函数功能：获取一个指定客户端未处理的包
 参数.一：lpszClientID
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要获得数据的客户端
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的数据
 参数.二：pInt_MsgLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供缓冲区大小,输出获取到的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipPacket_Get(LPCTSTR lpszClientID, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：RfcComponents_SipPacket_GetRandom
函数功能：随机获取一个数据包
 参数.一：ptszClientID
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出这个包是属于哪个客户端
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的数据
 参数.二：pInt_MsgLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供缓冲区大小,输出获取到的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipPacket_GetRandom(TCHAR *ptszClientID, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：RfcComponents_SipPacket_Close
函数功能：销毁一个客户端资源
 参数.一：lpszClientID
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要销毁的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipPacket_Close(LPCTSTR lpszClientID);
/********************************************************************
函数名称：RfcComponents_SipPacket_Destroy
函数功能：销毁所有资源
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipPacket_Destroy();
/************************************************************************/
/*                     SIP协议端导出函数                                */
/************************************************************************/
/********************************************************************
函数名称：RfcComponents_SipConfig_InitCode
函数功能：初始化SIP状态码映射表
 参数.一：lpszFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入CODE映射表文件所在路径
返回值
  类型：逻辑型
  意思：是否成功
备注：不需要销毁，如果状态码文件改变，可以重新加载进行动态更改
*********************************************************************/
extern "C" BOOL RfcComponents_SipConfig_InitCode(LPCTSTR lpszFile);
/********************************************************************
函数名称：RfcComponents_SipConfig_InitMime
函数功能：初始化SIP MIME类型,可以使用HTTP的MIME文件
 参数.一：lpszFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入映MIME映射文件所在路径
返回值
  类型：逻辑型
  意思：是否成功
备注：不需要销毁，如果MIME文件改变，可以重新加载进行动态更改
*********************************************************************/
extern "C" BOOL RfcComponents_SipConfig_InitMime(LPCTSTR lpszFile);
/********************************************************************
函数名称：RfcComponents_SipConfig_GetCode
函数功能：通过HTTP CODE来获得返回的状态信息字符串
 参数.一：nHttpCode
  In/Out：In
  类型：整数型
  可空：N
  意思：输入HTTP状态码
 参数.二：ptszApiType
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出状态码对应字符串信息
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipConfig_GetCode(int nHttpCode, TCHAR *ptszCodeMsg);
/********************************************************************
函数名称：RfcComponents_SipConfig_GetMime
函数功能：通过MIME类型来获得返回的MIME描述字符串
 参数.一：lpszMimeType
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：类型名称，比如 html jpeg bin exe等等
 参数.二：ptszMimeDes
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出对应的MIMEHTTP描述符
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipConfig_GetMime(LPCTSTR lpszMimeType, TCHAR *ptszMimeDes);
/************************************************************************/
/*                     SIP服务端导出函数                                */
/************************************************************************/
/********************************************************************
函数名称：RfcComponents_SipServer_Init
函数功能：初始化SIP服务器
 参数.一：lpszDomain
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入服务器的地址或者域名
 参数.二：lpszSIPCode
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入SIPCode文件地址,如果为NULL,你需要自己调用RfcComponents_SipConfig_InitCode初始化
 参数.三：lpszSIPMime
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入SIPMime文件地址,如果为NULL,你需要自己调用RfcComponents_SipConfig_InitMime初始化
 参数.四：bAnonymous
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否允许匿名注册
返回值
  类型：逻辑型
  意思：是否成功
备注：注意,此模块不包含网络服务
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_Init(LPCTSTR lpszDomain, LPCTSTR lpszSIPCode = NULL, LPCTSTR lpszSIPMime = NULL, BOOL bAnonymous = FALSE);
/********************************************************************
函数名称：RfcComponents_SipServer_Destory
函数功能：销毁服务器
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_Destory();
/********************************************************************
函数名称：RfcComponents_SipServer_Create
函数功能：创建一个客户端
 参数.一：lpszClientID
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要创建的客户端信息
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_Create(LPCTSTR lpszClientID);
/********************************************************************
函数名称：RfcComponents_SipServer_Delete
函数功能：删除一个指定的客户端
 参数.一：lpszClientID
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_Delete(LPCTSTR lpszClientID);
/********************************************************************
函数名称：RfcComponents_SipServer_Process
函数功能：处理一条完成的客户端请求
 参数.一：lpszClientID
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要操作的客户端
 参数.二：pSt_SIPHdr
  In/Out：In/Out
  类型：数据结构指针
  可空：N
  意思：输入通过RfcComponents_SipProtocol_Parse解析后的请求.
		输出可以通过RfcComponents_SipProtocol_PacketResponse打包的结构
返回值
  类型：逻辑型
  意思：是否成功
备注：必须解析完毕后的数据才能进行处理,处理后,返回成功你需要把结构体继续打包返回给客户端告诉其结果
	  如果你想知道这个客户端请求是否成功,你可以通过此函数调用结束后的pSt_SIPHdr->st_Response.nCode
	  值来确定
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_Process(LPCTSTR lpszClientID, SIPPROTOCOL_HDRINFO *pSt_SIPHdr);
/********************************************************************
函数名称：RfcComponents_SipServer_UserInsert
函数功能：插入一个可注册的用户到服务管理器中
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入用户名
 参数.二：lpszUserPass
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入密码
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_UserInsert(LPCTSTR lpszUserName, LPCTSTR lpszUserPass);
/********************************************************************
函数名称：RfcComponents_SipServer_UserDelete
函数功能：删除一个用户
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入删除的用户名
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipServer_UserDelete(LPCTSTR lpszUserName);
/************************************************************************/
/*                     SIP客户端导出函数                                */
/************************************************************************/
/********************************************************************
函数名称：RfcComponents_SipClient_Create
函数功能：创建一个客户端
 参数.一：pxhToken
  In/Out：Out
  类型：句柄
  可空：N
  意思：输出客户端句柄
 参数.二：lpszServerAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入服务器地址或者域名信息
 参数.二：lpszUser
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入用户名
 参数.二：lpszPass
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入密码.如果密码为NULL,表示客户端只支持匿名登录
 参数.二：bEnableHold
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：是否启用穿透,需要服务器支持
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipClient_Create(XNETHANDLE *pxhToken, LPCTSTR lpszServerAddr, LPCTSTR lpszUser, LPCTSTR lpszPass = NULL, BOOL bEnableHold = FALSE);
/********************************************************************
函数名称：RfcComponents_SipClient_Delete
函数功能：移除客户端
 参数.一：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要操作的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_SipClient_Delete(XNETHANDLE xhToken);
/********************************************************************
函数名称：RfcComponents_SipClient_Register
函数功能：客户端注册协议填充
 参数.一：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要操作的客户端
 参数.二：pSt_SIPProtocol
  In/Out：In/Out
  类型：数据结构指针
  可空：N
  意思：输入已经填充好的部分协议,输出系统填充的协议
返回值
  类型：逻辑型
  意思：是否成功
备注：此函数会根据RfcComponents_SipProtocol_Parse解析的请求继续填充需要的信息.
	  导出的参数需要配合RfcComponents_SipProtocol_PacketRequest函数使用
*********************************************************************/
extern "C" BOOL RfcComponents_SipClient_Register(XNETHANDLE xhToken, SIPPROTOCOL_HDRINFO *pSt_SIPProtocol);
/********************************************************************
函数名称：RfcComponents_SipClient_Process
函数功能：开始处理服务器返回的信息
 参数.一：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入要操作的客户端
 参数.二：pSt_SIPProtocol
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入已经解析好的协议结构
返回值
  类型：逻辑型
  意思：是否成功
备注：此函数会根据RfcComponents_SipProtocol_Parse解析的请求来处理
*********************************************************************/
extern "C" BOOL RfcComponents_SipClient_Process(XNETHANDLE xhToken, SIPPROTOCOL_HDRINFO *pSt_SIPHdr);