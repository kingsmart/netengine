#pragma once
/********************************************************************
//	Created:	    2017/03/15  11:06
//	File Name: 	G:\U_DISK_Path\NetSocketEngine\NetEngine_Rfc\NetEngine_RfcProtocol\NetEngine_Rfc_WSFrame\WSFrame_Define.h
//	File Path:	G:\U_DISK_Path\NetSocketEngine\NetEngine_Rfc\NetEngine_RfcProtocol\NetEngine_Rfc_WSFrame
//	File Base:	WSFrame_Define
//	File Ext:	    h
//  Project:     NetSocketEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:     WEBSOCKET帧协议解析器导出定义
//	History:
*********************************************************************/
///////////////////////////////////////////////////////////////////////////////
//                               导出的数据结构
///////////////////////////////////////////////////////////////////////////////
#pragma pack(push)
#pragma pack(1)
typedef enum en_WEBSocket_Frame_OPCode
{
    ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_CONTINUE = 0x00,
    ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_TEXT = 0x01,
    ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_BINARY = 0x02,
    ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_CLOSE = 0x08,
    ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_PING = 0x09,
    ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_PONG = 0x0A
}WEBSOCKET_FRAME_OPCODE, *LPWEBSOCKET_FRAME_OPCODE;

typedef struct tag_WSProtocol_Frame
{
    BOOL bIsFin;
    BOOL bIsMask;
    int nRsv1 : 1;
    int nRsv2 : 1;
    int nRsv3 : 1;
    int nOPCode;
    int nDATAOffSet;
    __int64 nPayloadLen;
    CHAR tszMaskKey[4];
}WSPROTOCOL_FRAME, *LPWSPROTOCOL_FRAME;
#pragma pack(pop)
///////////////////////////////////////////////////////////////////////////////
//                               导出的函数
///////////////////////////////////////////////////////////////////////////////
extern "C" DWORD WSFrame_GetLastError(int *pInt_SysError = NULL);
/******************************************************************************
                                导出WS帧解析函数
******************************************************************************/
/******************************************************************************
								导出WS帧解析函数
******************************************************************************/
/********************************************************************
函数名称：RfcComponents_WSCodec_DecodeMsg
函数功能：解码一个封包
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解码的数据缓冲区
 参数.二：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要解码缓冲区的大小
 参数.三：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出解码好的缓冲区
 参数.四：pSt_WSProtocolFrame
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：输入websocket协议头，如果这个参数为NULL，下面的参数必须为真
 参数.五：bIsPacket
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否需要打包
返回值
  类型：逻辑型
  意思：是否处理成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_WSCodec_DecodeMsg(LPCTSTR lpszMsgBuffer, int nLen, CHAR *ptszMsgBuffer, WSPROTOCOL_FRAME *pSt_WSProtocolFrame = NULL, BOOL bIsPacket = FALSE);
/********************************************************************
函数名称：RfcComponents_WSCodec_EncodeMsg
函数功能：编码一个封包，不编码数据包
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要编码的数据缓冲区
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出编码后的数据缓冲区
 参数.三：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出编码后的数据缓冲区大小
返回值
  类型：逻辑型
  意思：是否处理成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_WSCodec_EncodeMsg(LPCTSTR lpszMsgBuffer, CHAR *ptszMsgBuffer, int *pInt_Len);
/********************************************************************
函数名称：RfcComponents_WSCodec_EncodeMaskMsg
函数功能：编码一个待数据包的封包
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要编码的数据缓冲区
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出编码后的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入要编码数据缓冲区大小，输出编码后的数据缓冲区大小
返回值
  类型：逻辑型
  意思：是否处理成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_WSCodec_EncodeMaskMsg(LPCTSTR lpszMsgBuffer, CHAR *ptszMsgBuffer, int *pInt_Len);
/********************************************************************
函数名称：RfcComponents_WSCodec_DecodeHdr
函数功能：解析websocket协议头
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要解析的缓冲区大小
 参数.三：pSt_WSProtocolFrame
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出解析好的协议头
返回值
  类型：逻辑型
  意思：是否解析成功
备注：
*********************************************************************/
extern "C" BOOL RfcComponents_WSCodec_DecodeHdr(LPCTSTR lpszMsgBuffer, int nMsgLen, WSPROTOCOL_FRAME *pSt_WSProtocolFrame);
/********************************************************************
函数名称：RfcComponents_WSCodec_Poing
函数功能：发送一个ping或者pong消息
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出封装好的ping或者pong消息
 参数.二：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出封装好的长度
 参数.三：enWSFrame_OPCode
  In/Out：In
  类型：枚举型
  可空：N
  意思：要封装的消息类型
返回值
  类型：逻辑型
  意思：是否处理成功
备注：如果收到的是ping，那么需要回复pong
*********************************************************************/
extern "C" BOOL RfcComponents_WSCodec_Poing(CHAR *ptszMsgBuffer, int *pInt_Len, WEBSOCKET_FRAME_OPCODE enWSFrame_OPCode = ENUM_RFC_WEBSOCKET_DATAFRAME_OPCODE_PING);
/******************************************************************************
								导出WS连接器处理函数
******************************************************************************/
/********************************************************************
函数名称：RfcComponents_WSConnector_HandShake
函数功能：处理一段数据判断是否是websocket的连接信息数据
 参数.一：lpszKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入一段客户端唯一的KEY，GUID字符串
 参数.二：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入缓冲区的数据
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入缓冲区数据长度
 参数.四：ptszBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出编码后的客户端信息数据
返回值
  类型：逻辑型
  意思：是否处理成功
备注：返回真，表示这个数据是websocket的连接数据。返回假表示不是websocket数据
	 你需要自己启动一个TCP服务器，并且websocket的第一个发送过来的数据使用此函数处理
	 也就是第一次recv的数据
*********************************************************************/
extern "C" BOOL RfcComponents_WSConnector_HandShake(LPCTSTR lpszKey, LPCTSTR lpszBuffer, int nLen, TCHAR *ptszBuffer);
/********************************************************************
函数名称：RfcComponents_WSConnector_ConnectReturn
函数功能：封装一个连接成功的数据包给websocket客户端
 参数.一：lpszClientKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：客户端的唯一值，输入处理函数返回的内容
 参数.二：ptszBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出封装好的要返回的数据缓冲区
 参数.三：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出封装好的数据长度
返回值
  类型：逻辑型
  意思：是否处理成功
备注：如果连接处理成功，你需要调用这个函数返回一个包给客户端告诉客户端连接成功
	 否则客户端会断开连接，并且认为服务器故障，处理完连接信息后，下个数据才是websocket帧数据
*********************************************************************/
extern "C" BOOL RfcComponents_WSConnector_ConnectReturn(LPCTSTR lpszClientKey, TCHAR *ptszBuffer, int *pInt_Len);
