#pragma once
/********************************************************************
//	Created:	    2017/03/15  11:06
//	File Name: 	G:\U_DISK_Path\NetSocketEngine\NetEngine_Rfc\NetEngine_RfcProtocol\NetEngine_Rfc_WSFrame\WSFrame_Error.h
//	File Path:	G:\U_DISK_Path\NetSocketEngine\NetEngine_Rfc\NetEngine_RfcProtocol\NetEngine_Rfc_WSFrame
//	File Base:	WSFrame_Error
//	File Ext:	    h
//  Project:     NetSocketEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:     WEBSOCKET帧协议解析器导出错误
//	History:
*********************************************************************/
///////////////////////////////////////////////////////////////////////////////
//                      分析器错误
///////////////////////////////////////////////////////////////////////////////
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CODEC_DEMSG_PARAMENT 0x120D0001          //解码失败，参数错误
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CODEC_ENMSG_PARAMENT 0x120D0010          //编码失败，参数错误
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CODEC_ENMASKMSG_PARAMENT 0x120D0020      //编码失败，参数错误
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CODEC_DEHDR_PARAMENT 0x120D0030          //参数错误
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CODEC_POING_PARAMENT 0x120D0040          //参数错误
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CODEC_POING_NOTSUPPORT 0x120D0041        //不支持的类型
///////////////////////////////////////////////////////////////////////////////
//                      连接器错误
///////////////////////////////////////////////////////////////////////////////
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CONNECTOR_HANDSHAKE_PARAMENT 0x120D1001  //参数错误
#define ERROR_RFCCOMPONENTS_WEBSOCKET_CONNECTOR_HANDSHAKE_TYPE 0x120D1002      //解析类型错误，不是WEBSOCKET
