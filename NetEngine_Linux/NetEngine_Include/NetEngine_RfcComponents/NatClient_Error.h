#pragma once
/********************************************************************
//	Created:	2019/2/26   9:57
//	Filename: 	G:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_NatClient\NatClient_Error.h
//	File Path:	G:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_NatClient
//	File Base:	NatClient_Error
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	NAT客户端导出错误定义
//	History:
*********************************************************************/
/************************************************************************/
/*                      STUN客户端协议导出错误码                        */
/************************************************************************/
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_REQUEST_PARAMENT 0x120F0001    //参数错误
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_REQUEST_SETREPPORT 0x120F0002  //设置回复端口失败
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_REQUEST_CHANGEADDR 0x120F0003  //设置改变地址失败
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_RESPONSE_PARAMENT 0x120F0010   //回复处理失败,参数错误
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_RESPONSE_NOTSTUNMSG 0x120F0011 //不是标准的STUN协议
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_RESPONSE_NOTREPMSG 0x120F0012  //不是回复的协议
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_RESPONSE_NOSUCC 0x120F0013     //回复已经处理,但是未完成,参考错误码
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_RESPONSE_BUILD 0x120F0014      //重构回复包失败
#define ERROR_RFCCOMPONENTS_NATCLIENT_STUN_RESPONSE_GETADDR 0x120F0015    //获取地址失败
