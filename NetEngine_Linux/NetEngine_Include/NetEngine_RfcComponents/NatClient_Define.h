#pragma once
/********************************************************************
//	Created:	2019/2/26   9:56
//	Filename: 	G:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_NatClient\NatClient_Define.h
//	File Path:	G:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_RfcComponents\RfcComponents_NatClient
//	File Base:	NatClient_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	NAT客户端导出函数定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////////////
//                         导出的函数
//////////////////////////////////////////////////////////////////////////////////
extern "C" DWORD NatClient_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*              STUN客户端导出函数                                      */
/************************************************************************/
/********************************************************************
函数名称：RfcComponents_StunNat_Request
函数功能：构建一个请求包
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出组好的STUN协议请求缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出组包缓冲区大小
 参数.三：nRepPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：指定回复端口,默认不使用
 参数.三：bChangeAddr
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否需要修改过后的地址
 参数.四：bChangePort
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否需要修改过后的端口
返回值
  类型：逻辑型
  意思：是否成功
备注：STUN标准协议请求包构建函数,你需要自己使用UDP套接字发送给STUN服务器
*********************************************************************/
extern "C" BOOL RfcComponents_StunNat_Request(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, int nRepPort = -1, BOOL bChangeAddr = FALSE, BOOL bChangePort = FALSE);
/********************************************************************
函数名称：RfcComponents_StunNat_Response
函数功能：处理一个接收到的STUN协议包
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要处理的数据包缓冲区
 参数.二：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入缓冲区大小
 参数.三：ptszAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的外网通信地址和端口
 参数.四：pInt_ErrCode
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：如果有错误发生,可以导出处理结果的错误码,默认不获取
返回值
  类型：逻辑型
  意思：是否成功
备注：接受到的数据直接使用此函数来处理
*********************************************************************/
extern "C" BOOL RfcComponents_StunNat_Response(LPCTSTR lpszMsgBuffer, int nMsgLen, TCHAR *ptszAddr, int *pInt_ErrCode = NULL);
