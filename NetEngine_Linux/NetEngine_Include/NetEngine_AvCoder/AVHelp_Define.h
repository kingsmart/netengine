#pragma once
/********************************************************************
//	Created:	2017/5/4   10:39
//	Filename: 	P:\NetEngine_Windows\NetEngine_SourceCode\NetEngien_AvCoder\NetEngine_AVHelp\AVHelp_Define.h
//	File Path:	P:\NetEngine_Windows\NetEngine_SourceCode\NetEngien_AvCoder\NetEngine_AVHelp
//	File Base:	AVHelp_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	音视频编解码帮助模块导出定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                     导出的数据结构
//////////////////////////////////////////////////////////////////////////
typedef struct tag_AVHelp_MetaInfo
{
    TCHAR tszKey[128];                                                    //获取到的键值,当作为设备列表获取时,这个表示缩写名称
    TCHAR tszValue[256];                                                  //获取到的键值对应信息,当作为设备列表获取时,这个表示完整名称
}AVHELP_METAINFO;
typedef struct
{
    TCHAR tszCodecName[64];                                               //编码器名称
    __int64 nFrameCount;                                                  //总帧数
    __int64 nAVBitRate;                                                   //比特率
    int nAVFrameRate;                                                     //帧率,单位.KB
    int nCodecId;                                                         //编码器类型,音视频编解码定义的ENUM_AVCODEC_AUDIOTYPE和ENUM_AVCODEC_VEDIOTYPE
    int nAVFormat;                                                        //采样格式,音频AVCOLLECT_AUDIOSAMPLEFORMAT,视频AVCOLLECT_VIDEOSAMPLEFORMAT
    int nWidth;                                                           //图像宽度
    int nHeight;                                                          //图像高度
}AVHELP_AVMETA;
typedef struct
{
    TCHAR tszPacketName[128];                                             //封装格式名称
    __int64 nStartTime;                                                   //数据开始时间
    __int64 nCountTime;                                                   //音视频文件总播放时间
    int nNBStream;                                                        //流个数
    AVHELP_AVMETA st_VideoInfo;
    AVHELP_AVMETA st_AudioInfo;
}AVHELP_METADATA;
//设备信息
typedef struct
{
    int nCardNumber;                                                      //所属采集卡编号，编号从0开始
    int nDeviceNumber;                                                    //所属设备编号
    TCHAR tszName[64];                                                    //设备名称
}AVHELP_DEVICEINFO;
//////////////////////////////////////////////////////////////////////////
//                     导出的函数
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD AVHelp_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                     设备列表获取                                     */
/************************************************************************/
/********************************************************************
函数名称：AVHelp_Device_DeviceList
函数功能：显示所有支持的设备列表
 参数.一：pStl_ListDevice
  In/Out：Out
  类型：LIST容器
  可空：Y
  意思：导出获取到的设备列表
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AVHelp_Device_DeviceList(list<AVHELP_METAINFO> *pStl_ListDevice);
/********************************************************************
函数名称：AVHelp_Device_GetInput
函数功能：自定义获取输入设备信息
 参数.一：lpszInputName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入支持的输入端设备名称
 参数.二：pStl_ListDevice
  In/Out：Out
  类型：LIST容器
  可空：N
  意思：输出获取到的输入设备端名称信息
返回值
  类型：逻辑型
  意思：是否成功
备注：参数一名称可以参考我们的文档
*********************************************************************/
extern "C" BOOL AVHelp_Device_GetInput(LPCTSTR lpszInputName, list<AVHELP_METAINFO> *pStl_ListDevice);
/********************************************************************
函数名称：AVHelp_Device_EnumDevice
函数功能：枚举可用设备
 参数.一：pStl_ListAudioDevice
  In/Out：Out
  类型：STL容器指针
  可空：Y
  意思：输出可用的音频设备
 参数.二：pStl_ListVideoDevice
  In/Out：Out
  类型：STL容器指针
  可空：Y
  意思：输出可用的视频设备
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AVHelp_Device_EnumDevice(list<AVHELP_DEVICEINFO> *pStl_ListAudioDevice, list<AVHELP_DEVICEINFO> *pStl_ListVideoDevice);
/************************************************************************/
/*                     媒体信息接口                                     */
/************************************************************************/
/********************************************************************
函数名称：AVHelp_MetaInfo_Get
函数功能：获取ID3V2的媒体信息
 参数.一：lpszFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要读取的文件路径
 参数.二：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出媒体信息头总大小,你跳过这个大小就是数据文件了
 参数.三：pSt_MetaData
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出获取到的媒体数据信息
 参数.四：pStl_ListMetaInfo
  In/Out：Out
  类型：LIST容器指针
  可空：N
  意思：导出获取到的参数信息
 参数.五：ptszPICBuffer
  In/Out：In/Out
  类型：字符指针
  可空：Y
  意思：导出媒体文件的封面图片，如果有的话,为NULL不导出
 参数.六：pInt_PICLen
  In/Out：In/Out
  类型：整数型指针
  可空：Y
  意思：输入提供的缓冲区大小，输出获取到的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AVHelp_MetaInfo_Get(LPCTSTR lpszFile, int *pInt_Len,AVHELP_METADATA *pSt_MetaData, list<AVHELP_METAINFO> *pStl_ListMetaInfo,TCHAR *ptszPICBuffer = NULL,int *pInt_PICLen = NULL);
/********************************************************************
函数名称：AVHelp_MetaInfo_Set
函数功能：设置媒体信息
 参数.一：lpszSrcFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要设置的原始音视频文件
 参数.二：lpszDstFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：设置后保存的音视频文件
 参数.二：pStl_ListMetaInfo
  In/Out：In
  类型：list容器指针
  可空：N
  意思：要设置的值列表
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AVHelp_MetaInfo_Set(LPCTSTR lpszSrcFile, LPCTSTR lpszDstFile, list<AVHELP_METAINFO> *pStl_ListMetaInfo);
/********************************************************************
函数名称：AVHelp_MetaInfo_GetStream
函数功能：获取流信息
 参数.一：lpszFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取的音视频文件地址
 参数.二：pInt_ACount
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出可用的音频流个数
 参数.三：pInt_VCount
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出可用的视频流个数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AVHelp_MetaInfo_GetStream(LPCTSTR lpszFile, int *pInt_ACount, int *pInt_VCount);
/********************************************************************
函数名称：AVHelp_MetaInfo_GetSPSPPS
函数功能：获取一个视频的SPS和PPS信息
 参数.一：lpszMemory
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入缓冲区
 参数.二：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要解析的缓冲区大小
 参数.三：puszSPSBuffer
  In/Out：Out
  类型：无符号字符指针
  可空：Y
  意思：导出获取到的SPS缓冲区
 参数.四：puszPPSBuffer
  In/Out：Out
  类型：无符号字符指针
  可空：Y
  意思：导出获取到的PPS缓冲区
 参数.五：puszIDLeave
  In/Out：Out
  类型：无符号字符指针
  可空：Y
  意思：导出SDP需要用的ID级别配置信息,这个缓冲区大小固定为3
 参数.六：pInt_SPSLen
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：导出SPS缓冲区大小
 参数.七：pInt_PPSLen
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：导出PPS缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AVHelp_MetaInfo_GetSPSPPS(LPCTSTR lpszMemory, int nMsgLen, UCHAR *puszSPSBuffer = NULL, UCHAR *puszPPSBuffer = NULL, UCHAR *puszIDLeave = NULL, int *pInt_SPSLen = NULL, int *pInt_PPSLen = NULL);
