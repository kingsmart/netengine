#pragma once
/********************************************************************
//	Created:	2018/9/15   10:53
//	Filename: 	H:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_AvCoder\NetEngine_AVCollect\AVCollect_Error.h
//	File Path:	H:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_AvCoder\NetEngine_AVCollect
//	File Base:	AVCollect_Error
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	采集器导出错误码
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                        屏幕录像错误导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_PARAMENT 0x40B0001    //参数错误,初始化失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_MALLOCAVFMT 0x40B0002 //申请音视频内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_OPENDEVICE 0x40B0003  //打开设备失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_FINDSTREAM 0x40B0004  //查找AV流失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_NOTFOUNDVEDIO 0x40B0005//查找视频流失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_MALLOCCODEC 0x40B0006 //申请编解码器内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_FINDCODEC 0x40B0007   //查找编解码器失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_OPENCODEC 0x40B0008   //打开编解码器失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_MALLOCFRAME 0x40B0009 //申请帧内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_OPENFILE 0x40B000A    //打开文件失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_CREATETHREAD 0x40B000B//创建线程失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_INIT_MALLOC 0x40B000C      //初始化失败,申请内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_GETINFO_PARAMENT 0x40B0010 //获取信息失败,参数错误
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_GETINFO_NOTFOUND 0x40B0011 //没有找到句柄
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_START_NOTFOUND 0x40B0020   //开始失败,没有找到
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_SCREEN_STOP_NOTFOUND 0x40B0030    //停止失败,没有找到
//////////////////////////////////////////////////////////////////////////
//                        摄像头错误导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_PARAMENT 0x40B1001    //参数错误,初始化失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_MALLOCAVFMT 0x40B1002 //申请音视频内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_OPENDEVICE 0x40B1003  //打开设备失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_FINDSTREAM 0x40B1004  //查找AV流失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_NOTFOUNDVEDIO 0x40B1005//查找视频流失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_MALLOCCODEC 0x40B1006 //申请编解码器内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_FINDCODEC 0x40B1007   //查找编解码器失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_OPENCODEC 0x40B1008   //打开编解码器失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_MALLOCFRAME 0x40B1009 //申请帧内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_OPENFILE 0x40B100A    //打开文件失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_CREATETHREAD 0x40B100B//创建线程失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_INIT_MALLOC 0x40B100C      //申请内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_GETINFO_PARAMENT 0x40B1010 //参数错误
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_GETINFO_NOTFOUND 0x40B1011 //没有找到句柄
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_START_NOTFOUND 0x40B1020
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_CAMERA_STOP_NOTFOUND 0x40B1021
//////////////////////////////////////////////////////////////////////////
//                        声音录制错误导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_PARAMENT 0x40B2001    //参数错误,初始化失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_MALLOC 0x40B2002      //申请内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_MALLOCAVFMT 0x40B2003 //申请音视频内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_OPENDEVICE 0x40B2004  //打开设备失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_FINDSTREAM 0x40B2005  //查找AV流失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_NOTFOUNDVEDIO 0x40B2006//查找视频流失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_MALLOCCODEC 0x40B2006 //申请编解码器内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_FINDCODEC 0x40B2007   //查找编解码器失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_OPENCODEC 0x40B2008   //打开编解码器失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_MALLOCFRAME 0x40B2009 //申请帧内存失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_OPENFILE 0x40B200A    //打开文件失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_INIT_CREATETHREAD 0x40B200B//创建线程失败
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_GETINFO_PARAMENT 0x40B2010 //获取参数错误
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_GETINFO_NOTFOUND 0x40B2011 //没有找到句柄
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_START_NOTFOUND 0x40B2020   //启动失败,没有找到
#define ERROR_NETENGINE_AVCODEC_AVCOLLECT_AUDIO_STOP_NOTFOUND 0x40B2021    //停止失败,没有找到
