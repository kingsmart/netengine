#pragma once
/********************************************************************
//	Created:	2011/10/29  15:19
//	Filename: 	../NetSocketEngine/NetEngine_uPnP/NetEngine_uPnP_Define.h
//	File Path:	../NetSocketEngine/NetEngine_uPnP/
//	File Base:	NetEngine_uPnP
//	File Ext:	h
//      Project:        血与荣誉网络通信引擎 For Linux
//	Author:		Dowflyon
//	Purpose:	UPNP组件导出定义
//	History:
*********************************************************************/
//回调函数，意思：第几个映射，从1开始 协议 外部端口 内部地址 内部端口 描述信息 远程主机 其他信息（最后更新时间） 自定义参数
typedef void(*CALLBACK_NETENGINE_NETHELP_UPNP_ENTERPORTMAPPING)(int nCountTimer,LPCTSTR lpszProtocol,LPCTSTR lpszExtPort,LPCTSTR lpszIntClient,LPCTSTR lpszIntPort,LPCTSTR lpszDesc,LPCTSTR lpszHost,LPCTSTR lpszDuration,LPVOID lParam);
typedef void(*CALLBACK_NETENGINE_NETHELP_UPNP_ENTERPORTMAPPINGEX)(int nCountTimer,LPCTSTR lpszProtocol,unsigned short usExtPort,LPCTSTR lpszIntClient,unsigned short usIntPort,LPCTSTR lpszDesc,LPCTSTR lpszHost,unsigned int unLeaseTime,LPVOID lParam);
//////////////////////////////////////////////////////////////////////////
//                  跨平台UPNP版本
//////////////////////////////////////////////////////////////////////////
/*************************************************************************
                       UPNP客户端数据结构导出定义
*************************************************************************/
//初始化时候获取的值
typedef struct tag_UPnp_InitVerInfo
{
    int nIPVer;                                                           //0表示IPv4
    TCHAR tszLanIP[64];                                                   //本地IP地址
    union un_DevInfo
    {
        BOOL bIsOk;                                                       //找到正常工作设备
        BOOL bIsConnected;                                                //找到设备但是没有连接
        BOOL bIsFounduPnP;                                                //是否找到UPNP设备
    }UN_DEVIGD;
}UPNP_INITVERINFO,*LPUPNP_INITVERINFO;
//获取连接信息，IPV4使用
typedef struct tag_UPnp_GetDisplayInfo
{
    TCHAR tszConnectType[128];                                            //连接类型
    TCHAR tszStatus[128];                                                 //连接状态
    int UpTimes;                                                          //更新时间
    TCHAR tszLastConnectError[128];                                       //最后的出错信息
    TCHAR tszStartTime[64];                                               //开始时间
    TCHAR tszExternalIP[64];                                              //外部IP地址

    long int ubrDown;                                                     //下载大小，自己计算，大于 1000000，公式为：Mbps = ubrDown / 1000000,Mbps = (ubrDown / 100000) % 10);
    long int ubrUp;                                                       //上传大小，计算公式如上

    long int nlSendByte;                                                  //发送字节
    long int nlReceivedByte;                                              //接受字节
    long int nlSendPacket;                                                //发送包数量
    long int nlReceivedPacket;                                            //接受包数量
}UPNP_GETDISPLAYINFO,*LPUPNP_GETDISPLAYINFO;
//获取防火墙信息，IPV6使用
typedef struct tag_UPnp_GetFirewallStatus
{
    BOOL bFireWallEnble;                                                  //防火墙是否启动
    BOOL bInBoundPinholeAllowed;                                          //是否允许入站

    unsigned int unSentByte;                                              //发送字节
    unsigned int unReceivedByte;                                          //接受字节
    unsigned int unSentPacket;                                            //发送包
    unsigned int unReceivedPatcket;                                       //接受包
}UPNP_GETFIREWALLSTATUS,*LPUPNP_GETFIREWALLSTATUS;
//获取打洞信息
typedef struct tag_UPnp_GetPinholeStatus
{
    TCHAR tszUniqueID[128];                                               //设备ID
    BOOL bWorking;                                                        //是否正在工作
    TCHAR tszLeastTime[64];                                               //租约时间
}UPNP_PINHOLESTATUS,*LPUPNP_PINHOLESTATUS;
//////////////////////////////////////////////////////////////////////////
//                导出函数定义
//////////////////////////////////////////////////////////////////////////
/************************************************************************
函数名称：NetEngine_uPnPClient_GetLastError
函数功能：获取错误码
返回值
  类型：双字
  意思：返回获取到的错误码
备注：
************************************************************************/
extern "C" DWORD NetEngine_uPnP_GetLastError(int *pInt_ErrorCode = NULL);
/************************************************************************
函数名称：NetEngine_uPnPClient_Init
函数功能：初始化UPNP设备
  参数一：pSt_InitVerInfo
   In/Out：In/Out
   类型：结构体
   可空：N
   意思：获取初始化的设备信息
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：如果失败请勿继续操作
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_Init(LPUPNP_INITVERINFO pSt_InitInfo);
/************************************************************************
函数名称：NetEngine_uPnPClient_UnInit
函数功能：释放UPNP内存空间
返回值
  类型：逻辑型
  意思：是否卸载成功
备注：在不使用的时候必须调用此函数，否则会引起内存泄露
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_UnInit();
//IPV4
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV4AddPortMap
函数功能：添加一个UPNP映射
  参数一：lpszIntAddr
   In/Out：In
   类型：字符串
   可空：N
   意思：内部地址
  参数二：lpszIntPort
   In/Out：In
   类型：字符串
   可空：N
   意思：内部端口
  参数三：lpszExtPort
   In/Out：In
   类型：字符串
   可空：N
   意思：外部端口
  参数四：lpszProto
   In/Out：In
   类型：字符串
   可空：N
   意思：协议
  参数五：lpszLeaseDuration
   In/Out：In
   类型：字符串
   可空：N
   意思：描述信息
返回值
  类型：逻辑型
  意思：是否设置添加映射信息成功
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV4AddPortMap(LPCTSTR lpszIntAddr,LPCTSTR lpszIntPort,LPCTSTR lpszExtPort,LPCTSTR lpszProto,LPCTSTR lpszLeaseDuration);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV4RemovePortMap
函数功能：删除映射信息
  参数一：lpszExtPort
   In/Out：In
   类型：字符串指针
   可空：N
   意思：外部端口
  参数二：lpszProto
   In/Out：In
   类型：字符串指针
   可空：N
   意思：协议
返回值
  类型：逻辑型
  意思：是否成功删除
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV4RemovePortMap(LPCTSTR lpszExtPort,LPCTSTR lpszProto);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV4GetConnectStatus
函数功能：获取传输速度
  参数一：pSt_GetConnectInfo
   In/Out：In/Out
   类型：结构体指针
   可空：N
   意思：获取的连接信息
返回值
  类型：逻辑型
  意思：是否成功执行
备注：依赖于NetEngine_uPnPClient_Display这个函数
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV4GetConnectStatus(LPUPNP_GETDISPLAYINFO pSt_GetConnectInfo);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV4ListRedirections
函数功能：获取端口映射信息
  参数一：fpCall_PortMappting
   In/Out：In/Out
   类型：回调函数
   可空：N
   意思：用来导出信息
  参数二：lParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功获取映射信息表
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV4ListRedirections(CALLBACK_NETENGINE_NETHELP_UPNP_ENTERPORTMAPPING fpCall_PortMappting,LPVOID lParam);
//IPV6
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6GetConnectStatus
函数功能：获取防火墙状态
  参数一：pSt_FireWallStatus
   In/Out：In/Out
   类型：结构体指针
   可空：N
   意思：获取到的信息
返回值
  类型：逻辑型
  意思：是否成功获取到
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6GetConnectStatus(LPUPNP_GETFIREWALLSTATUS pSt_FireWallStatus);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6AddPortMap
函数功能：设置打洞信息，IPV6添加端口映射
  参数一：lpszRemoteAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：远程地址
  参数二：lpszExtPort
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：远程端口
  参数三：lpszIntAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：本地地址
  参数四：lpszIntPort
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：本地端口
  参数五：lpszProto
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：协议类型
  参数六：lpszLeaseTime
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：租凭时间
  参数七：ptszUniQuid
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置成功后此设置的ID信息
返回值
  类型：逻辑型
  意思：是否设置成功
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6AddPortMap(LPCTSTR lpszRemoteAddr,LPCTSTR lpszExtPort,LPCTSTR lpszIntAddr,LPCTSTR lpszIntPort,LPCTSTR lpszProto,LPCTSTR lpszLeaseTime,TCHAR *ptszUniQuid);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6GetPinholeAndUpdate
函数功能：更新映射事件，续约时间
  参数一：pSt_PinholeStatus
   In/Out：In/Out
   类型：结构体
   可空：N
   意思：输入：tszUniqueID 值必须输入，你添加的时候返回的ID，tszLeastTime，要更新的时间，bWorking 返回FALSE 表示没有工作，那么视为失败
返回值
  类型：逻辑型
  意思：是否更新成功
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6GetPinholeAndUpdate(LPUPNP_PINHOLESTATUS pSt_PinholeStatus);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6GetPinholeOutboundTimeout
函数功能：获取打洞信息超时时间
  参数一：lpszRemoteAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：远程地址
  参数二：lpszExtPort
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：外部端口
  参数三：lpszIntaddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：内部地址
  参数四：lpszIntPort
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：内部端口
  参数五：lpszProto
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：协议
  参数六：pInt_TimedOut
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出获取到的超时时间
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6GetPinholeOutboundTimeout(LPCTSTR lpszRemoteAddr,LPCTSTR lpszExtPort,LPCTSTR lpszIntaddr,LPCTSTR lpszIntPort,LPCTSTR lpszProto,int *pInt_TimedOut);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6GetPinholePackets
函数功能：获取打洞后的数据包
  参数一：lpszUniQueID
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：唯一设备ID
  参数二：pInt_PinHolePackets
   In/Out：Out
   类型：整数指针
   可空：N
   意思：打洞的数据包数
返回值
  类型：逻辑型
  意思：是否成功获取
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6GetPinholePackets(LPCTSTR lpszUniQueID,int *pInt_PinHolePackets);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6CheckPinhole
函数功能：检查指定打洞的ID是否正在工作
  参数一：lpszUniQueID
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：唯一设备ID
  参数二：pbWorking
   In/Out：Out
   类型：逻辑型指针
   可空：N
   意思：是否正在工作
返回值
  类型：逻辑型
  意思：检测执行是否成功
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6CheckPinhole(LPCTSTR lpszUniQueID,BOOL *pbWorking);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6RemotePortMap
函数功能：删除IPV6的打洞信息
  参数一：lpszUniQueID
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：唯一设备ID
返回值
  类型：逻辑型
  意思：是否删除成功
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6RemotePortMap(LPCTSTR lpszUniQueID);
/************************************************************************
函数名称：NetEngine_uPnPClient_IPV6ListRedirections
函数功能：扩展方式获取，可以获取到UDP和TCP信息
  参数一：fpCall_PortMapptingEx
   In/Out：In、Out
   类型：回调函数
   可空：N
   意思：用来导出信息
  参数二：lParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：自定义参数
返回值
  类型：逻辑型
  意思：是否成功获取到UDP和TCP信息
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_IPV6ListRedirections(CALLBACK_NETENGINE_NETHELP_UPNP_ENTERPORTMAPPINGEX fpCall_PortMapptingEx,LPVOID lParam);
//-------------帮助函数
/************************************************************************
函数名称：NetEngine_uPnPClient_Display
函数功能：获取连接信息
  参数一：pSt_GetConnectInfo
   In/Out：In/Out
   类型：结构体指针
   可空：N
   意思：获取的连接信息
返回值
  类型：逻辑型
  意思：是否成功执行
备注：
************************************************************************/
extern "C" BOOL NetEngine_uPnPClient_Display(LPUPNP_GETDISPLAYINFO pSt_GetConnectInfo);
