#pragma once
/********************************************************************
//	Created:	2012/1/21  18:10
//	File Name: 	J:\U_DISK_Path\NetSocketEngine\NetEngine_DownLoad\NetEngine_DownLoad\DownLoad_Error.h
//	File Path:	J:\U_DISK_Path\NetSocketEngine\NetEngine_DownLoad\NetEngine_DownLoad
//	File Base:	DownLoad_Error
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	网络下载引擎，错误导出表
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                       HTTP下载错误,需要NetEngine_PublicOperatorSting 的支持
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_DOWNLOAD_HTTP_CREATE_PARAMENT 0x0BAF0001          //创建HTTP下载参数错误
#define ERROR_NETENGINE_DOWNLOAD_HTTP_CREATE_MALLOC 0x0BAF0002            //申请空间失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_CREATE_INIT 0x0BAF0003              //初始化下载引擎失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_CREATE_PARAMENTISTOOLONG 0x0BAF0004 //参数传递的太上了，无法继续
#define ERROR_NETENGINE_DOWNLOAD_HTTP_CREATE_FILEEXIST 0xBBF0005          //指定的保存文件路径已经有文件存在
#define ERROR_NETENGINE_DOWNLOAD_HTTP_CREATE_CREATETHREAD 0x0BAF0006      //创建下载线程失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_QUERY_NOTSUPPORT 0x0BAF0010         //查询失败，不支持此查询方式，你需要使用回调
#define ERROR_NETENGINE_DOWNLOAD_HTTP_DELETE_NOTFOUND 0x0BAF0020          //没有找到，无法继续！
#define ERROR_NETENGINE_DOWNLOAD_HTTP_DELETE_TERMINATETHREAD 0x0BAF0021   //删除失败，结束任务线程失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_URLADDR 0x0BAF1030        //设置下载地址失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_CONNTIMEDOUT 0x0BAF1031   //设置连接超时失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_RWTIMEDOUT 0x0BAF1032     //设置读写超时失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_WRITEFUNC 0x0BAF1033      //设置写操作函数失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_WRITEPARAMENT 0x0BAF1034  //设置写操作函数参数失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_SETPROGRESS 0x0BAF1035    //设置过程失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_PROGRESSFUNC 0x0BAF1036   //设置过程函数失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_PROGRESSPARAMENT 0x0BAF1037//设置过程函数参数失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETOPTION_SETRANGE 0x0BAF1038       //设置文件写入位置失败，文件续传出现错误
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GETFILE_WRITEOPEN 0x0BAF1041        //打开失败，可能权限有问题
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GETFILE_OPENREAD 0x0BAF1042         //打开追加文件失败，内部错误
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GETFILE_SEEKPOS 0x0BAF1043          //移动文件指针失败，无法继续，续传出错
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_NOTFOUND 0x0BAF1050           //暂停失败，没有找到任务
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_NOTDOWNLOADING 0x0BAF1051     //暂停失败，找到任务，但是任务没有下载
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_ISSUSPENDED 0x0BAF1052        //暂停失败，任务已经是暂停中
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_PAUSEALL 0x0BAF1053           //暂停下载失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_ISDOWNLOADING 0x0BAF1054      //已经在下载中，无法恢复下载
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_NOTSUSPENDED 0x0BAF1054       //不是暂停状态，状态错误，无法恢复
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_UNPAUSEALL 0x0BAF1055         //恢复下载失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_PAUSE_NOTFOUNDTASK 0x0BAF1057       //没有找到任务信息
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_PARAMENT 0x0BAF1060    //参数错误
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_INIT 0x0BAF1061        //初始化化失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_SETURL 0x0BAF1062      //设置URL失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_NOTHEADER 0x0BAF1063   //设置头失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_NOTBODY 0x0BAF1064     //设置内容失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_EXEC 0x0BAF1065        //执行失败，内部错误
#define ERROR_NETENGINE_DOWNLOAD_HTTP_GET_FILESIZE_LENGTH 0x0BAF1066      //获取长度失败，内部错误
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETSPEED_NOTFOUND 0x0BAF1070        //设置速度失败，没有找到任务
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETSPEED_MAXRECV 0x0BAF1071         //设置最大接受速度失败
#define ERROR_NETENGINE_DOWNLOAD_HTTP_SETSPEED_MAXRSEND 0x0BAF1072        //设置最大发送速度失败
//////////////////////////////////////////////////////////////////////////
//                       ftp下载错误,需要NetEngine_PublicOperatorSting 的支持
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_DOWNLOAD_FTP_CREATE_PARAMENT 0x0BBF1001           //创建HTTP下载参数错误
#define ERROR_NETENGINE_DOWNLOAD_FTP_CREATE_MALLOC 0x0BBF1002             //申请空间失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_CREATE_INIT 0x0BBF1003               //初始化下载引擎失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_CREATE_PARAMENTISTOOLONG 0x0BBF1004  //参数传递的太上了，无法继续
#define ERROR_NETENGINE_DOWNLOAD_FTP_CREATE_CREATETHREAD 0x0BBF1005       //创建下载线程失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_QUERY_NOTSUPPORT 0x0BBF1010          //查询失败，不支持此查询方式，你需要使用回调
#define ERROR_NETENGINE_DOWNLOAD_FTP_DELETE_NOTFOUND 0x0BBF1020           //没有找到，无法继续！
#define ERROR_NETENGINE_DOWNLOAD_FTP_DELETE_TERMINATETHREAD 0x0BBF1021    //删除失败，结束任务线程失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_URLADDR 0x0BBF1030         //设置下载或者上传地址失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_CONNTIMEDOUT 0x0BBF1031    //连接超时设定失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_RESPONSE_TIMEOUT 0x0BBF1A32//响应超时时间
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_RWTIMEDOUT 0x0BBF1032      //传输超时设定失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_PASV 0x0BBF1034            //设置PASV传输模式失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_WRITEFUNC 0x0BBF1035       //设置写函数失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_WRITEPARAMENT 0x0BBF1036   //设置写函数参数失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_SETPUT 0x0BBF1037          //设置上传模式失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_UPFILE 0x0BBF1038          //设置上传文件失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_SETSIZE 0x0BBF1039         //设置上传文件大小失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_REDFUNC 0x0BBF103A         //读取函数设置失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_MISSIONDIR 0x0BBF103B      //跳过文件夹设置失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_READDATA 0x0BBF103C        //读取函数数据失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_SETPROGRESS 0x0BBF103D     //设置过程失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_PROGRESSFUNC 0x0BBF103E    //设置过程函数失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_PROGRESSPARAMENT 0x0BBF103F//设置过程函数参数失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETOPTION_SETRANGE 0x0BBF1A3F        //设置下载起始地址失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_GETFILE_SIZE 0x0BBF1040              //获取文件大小失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_GETFILE_NOTFOUND 0x0BBF1041          //文件不存在
#define ERROR_NETENGINE_DOWNLOAD_FTP_GETFILE_WRITEOPEN 0x0BBF1042         //写入文件打开失败，请检查文件是否有权限写
#define ERROR_NETENGINE_DOWNLOAD_FTP_GETFILE_READOPEN 0x0BBF1043          //读取文件打开失败，请检查文件是否有权限读
#define ERROR_NETENGINE_DOWNLOAD_FTP_GETFILE_SEEKPOS 0x0BBF1044           //移动指针失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_NOTFOUND 0x0BBF1050            //没有找到无法继续
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_NOTDOWNLOADING 0x0BBF1051      //没有下载中，无法继续
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_ISSUSPENDED 0x0BBF1052         //已经是暂停状态，无法继续
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_PAUSEALL 0x0BBF1053            //暂停失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_ISDOWNLOADING 0x0BBF1054       //已经在运行，无法继续
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_NOTSUSPENDED 0x0BBF1055        //不是暂停状态，无法恢复
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_UNPAUSEALL 0x0BBF1056          //恢复下载失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_PAUSE_NOTFOUNDTASK 0x0BBF1057        //没有找到任务信息
#define ERROR_NETENGINE_DOWNLOAD_FTP_REMOTEFEXIST_PARAMENT 0x0BBF1060     //参数错误
#define ERROR_NETENGINE_DOWNLOAD_FTP_REMOTEFEXIST_INIT 0x0BBF1061         //初始化错误
#define ERROR_NETENGINE_DOWNLOAD_FTP_REMOTEFEXIST_NOBODY 0x0BBF1062       //数据取回设置失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_REMOTEFEXIST_EXEC 0x0BBF1063         //执行失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_REMOTEFEXIST_NOTFOUND 0x0BBF1064     //没有找到文件，可能文件不存在
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETSPEED_NOTFOUND 0x0BBF1070         //设置速度失败，没有找到任务
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETSPEED_MAXRECV 0x0BBF1071          //设置最大接受速度失败
#define ERROR_NETENGINE_DOWNLOAD_FTP_SETSPEED_MAXRSEND 0x0BBF1072         //设置最大发送速度失败
