#pragma once
/********************************************************************
//	Created:	2018/9/7   17:17
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_StreamMedia\StreamMedia_RtmpClient\RtmpClient_Define.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_StreamMedia\StreamMedia_RtmpClient
//	File Base:	RtmpClient_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	RTMP流媒体推送导出函数
//	History:
*********************************************************************/
typedef void(CALLBACK *CALLBACK_NETENGINE_STREAMMEDIA_RTMPCLIENT_PULL_STREAM)(XNETHANDLE xhStream, uint8_t *lpszBuffer,int nLen, LPVOID lParam);
///////////////////////////////////////////////////////////////////////////////
//                               导出的函数
///////////////////////////////////////////////////////////////////////////////
extern "C" DWORD RtmpClient_GetLastError(int *pInt_SysError = NULL);
/******************************************************************************
                                 导出RTMP实时流推送函数
******************************************************************************/
/********************************************************************
函数名称：RtmpClient_StreamPush_Push
函数功能：初始化一个实时流推送服务器
 参数.一：pxhNet
  In/Out：Out
  类型：通道句柄
  可空：N
  意思：推送后导出这个通道的唯一句柄
 参数.二：lpszRtmpUrl
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：推送的URL地址,播放也是这个地址
 参数.三：lpCall_RtmpStream
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：导出当前需要输入的缓冲区数据和长度以及通道句柄
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtmpClient_StreamPush_Init(XNETHANDLE *pxhNet, LPCTSTR lpszRtmpUrl, int nWidth, int nHeight, int enAvCodec, int nBitRate = 500000);
/********************************************************************
函数名称：RtmpClient_StreamPush_Push
函数功能：推送一个流
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化的RTMP推流句柄
 参数.二：punYBuffer
  In/Out：In
  类型：无符号整数型指针
  可空：N
  意思：yuv中的y数据
 参数.三：nYLen
  In/Out：In
  类型：整数型
  可空：N
  意思：y数据大小
 参数.四：punUBuffer
  In/Out：In
  类型：无符号整数型指针
  可空：N
  意思：yuv中的u数据
 参数.五：nULen
  In/Out：In
  类型：整数型
  可空：N
  意思：u数据大小
 参数.七：punVBuffer
  In/Out：In
  类型：无符号整数型指针
  可空：N
  意思：yuv中的v数据
 参数.八：nVLen
  In/Out：In
  类型：整数型
  可空：N
  意思：v数据大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtmpClient_StreamPush_Push(XNETHANDLE xhNet, uint8_t *punYBuffer, int nYLen, uint8_t *punUBuffer, int nULen, uint8_t *punVBuffer, int nVLen);
/********************************************************************
函数名称：RtmpClient_StreamPush_Close
函数功能：关闭一个RTMP实时推流通道
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要关闭的通道句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：销毁资源必须调用
*********************************************************************/
extern "C" BOOL RtmpClient_StreamPush_Close(XNETHANDLE xhNet);
/******************************************************************************
                                 导出RTMP文件流推送函数
******************************************************************************/
/********************************************************************
函数名称：RtmpClient_FilePush_Push
函数功能：推送一个H264文件到RTMP服务器中去
 参数.一：pxhNet
  In/Out：Out
  类型：通道句柄
  可空：N
  意思：推送后导出这个通道的唯一句柄
 参数.二：lpszFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要推送哪个H264文件,文件路径
 参数.三：lpszRtmpUrl
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：推送的URL地址,播放也是这个地址
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtmpClient_FilePush_Push(XNETHANDLE *pxhNet, LPCTSTR lpszFile, LPCTSTR lpszRtmpUrl);
/********************************************************************
函数名称：RtmpClient_FilePush_GetStatus
函数功能：获取一个通道的传输状态
 参数.一：xhNet
  In/Out：In
  类型：通道句柄
  可空：N
  意思：要获取的通道状态的句柄
 参数.二：pbPush
  In/Out：Out
  类型：逻辑指针
  可空：N
  意思：导出是否正在传输,真是,假否
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtmpClient_FilePush_GetStatus(XNETHANDLE xhNet, BOOL *pbPush);
/********************************************************************
函数名称：RtmpClient_FilePush_Close
函数功能：关闭一个RTMP文件推流通道
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要关闭的通道句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：销毁资源必须调用
*********************************************************************/
extern "C" BOOL RtmpClient_FilePush_Close(XNETHANDLE xhNet);
/******************************************************************************
                                 拉取RTMP实时流函数
******************************************************************************/
/********************************************************************
函数名称：RtmpClient_StreamPull_Init
函数功能：初始化一个实时流拉取服务
 参数.一：pxhNet
  In/Out：Out
  类型：通道句柄
  可空：N
  意思：拉取流导出这个通道的唯一句柄
 参数.二：lpszRtmpUrl
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取的URL地址
 参数.三：fpCall_RtmpStream
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：导出当前流的每一帧的信息
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
 参数.五：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入要保存的H264文件路径,为NULL不保存为文件
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtmpClient_StreamPull_Init(XNETHANDLE *pxhNet, LPCTSTR lpszRtmpUrl, CALLBACK_NETENGINE_STREAMMEDIA_RTMPCLIENT_PULL_STREAM fpCall_RtmpStream, LPVOID lParam = NULL, LPCTSTR lpszFileName = NULL);
/********************************************************************
函数名称：RtmpClient_StreamPull_Close
函数功能：关闭一个RTMP拉取流流通道
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要关闭的通道句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：销毁资源必须调用
*********************************************************************/
extern "C" BOOL RtmpClient_StreamPull_Close(XNETHANDLE xhNet);
