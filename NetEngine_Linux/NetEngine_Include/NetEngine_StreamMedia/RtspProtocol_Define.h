#pragma once
/********************************************************************
//	Created:	2019/8/21   15:31
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_StreamMedia\StreamMedia_RtspProtocol\RtspProtocol_Define.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_StreamMedia\StreamMedia_RtspProtocol
//	File Base:	RtspProtocol_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	RTSP协议处理模块头文件
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                       导出的枚举型
//////////////////////////////////////////////////////////////////////////
//RTSP方法
typedef enum en_RtspProtocol_Method
{
    ENUM_RTSPPROTOCOL_METHOD_TYPE_OPTIONS = 1,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_DESCRIBE,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_SETUP,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_PLAY,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_PASUE,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_GETPARAMETER,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_SETPARAMENT,
    ENUM_RTSPPROTOCOL_METHOD_TYPE_TEARDOWN
}ENUM_RTSPPROTOCOL_METHOD;
//H264分包模式
typedef enum en_RtspProtocol_Packet
{
    ENUM_RTSPPROTOCOL_PACKET_TYPE_NOPACKET = 0,           //不需要分包
    ENUM_RTSPPROTOCOL_PACKET_TYPE_FAST,                   //使用STAP-A, FU-A,快速低延迟
    ENUM_RTSPPROTOCOL_METHOD_TYPE_CACHE                   //传输顺序与解码顺序可能不一致, 需要缓冲一些数据, 重新排序
}ENUM_RTSPPROTOCOL_PACKET;
//////////////////////////////////////////////////////////////////////////
//                       导出的数据结构
//////////////////////////////////////////////////////////////////////////
//客户端请求信息
typedef struct tag_RtspProtocol_Request
{
    ENUM_RTSPPROTOCOL_METHOD enMethod;   //方法类型
    CHAR tszMethod[64];                  //访问的方法名称
    CHAR tszVersion[64];                 //版本
    CHAR tszUrl[MAX_PATH];               //地址

    int nCseq;                           //序列号
    CHAR tszUserAgent[MAX_PATH];         //用户信息
    //通道信息
    struct
    {
        CHAR tszChannels[64];            //通道内容
        CHAR tszChannelName[64];         //通道名称
        int nChannelNumber;              //通道号
    }st_ChannelInfo;
    //附加信息
    struct
    {
        BOOL bKeyFrame;                  //设置参数的关键帧字段值
        CHAR tszAccept[64];              //客户端接受类型
        CHAR tszTransport[64];           //客户端传输信息
        CHAR tszSession[64];             //客户端会话ID
        CHAR tszRange[64];               //开始位置
    }st_ExtInfo;
    //内容信息
    struct
    {
        CHAR tszCtxBuffer[2048];         //附加内容缓冲区
        CHAR tszCtxType[64];             //附加内容信息
        BOOL bContext;                   //是否有负载内容
        int nCtxLen;                     //附加信息长度
    }st_CtxInfo;
    //验证信息
    struct
    {
        CHAR tszUser[64];                //验证的用户名
        CHAR tszPass[64];                //验证的密码
        BOOL bAuth;                      //是否启用了验证
    }st_AuthInfo;
}RTSPPROTOCOL_REQUEST;
//支持方法开关
typedef struct tag_RtspProtocol_Options
{
    BOOL bOptions;                       //选项
    BOOL bDescribe;                      //描述
    BOOL bSetup;                         //配置
    BOOL bTeardown;                      //关闭
    BOOL bPlay;                          //播放
    BOOL bPause;                         //暂停
    BOOL bGetParam;                      //获取参数
    BOOL bSetParam;                      //设置参数
}RTSPPROTOCOL_OPTIONS;
//SETUP网络配置
typedef struct tag_RtspProtocol_Setup
{
    TCHAR tszSourceAddr[32];             //原始地址
    TCHAR tszDestAddr[32];               //目标IP地址
    int nClientPortStart;                //客户端起始端口,RTP端口
    int nClientPortEnd;                  //客户端结束端口,RTCP端口
    int nServerPortStart;                //服务器开始端口,RTP绑定端口
    int nServerPortEnd;                  //服务器结束端口,RTCP绑定端口
}RTSPPROTOCOL_SETUP;
//媒体信息
typedef struct tag_RtspProtocol_MediaInfo
{
    ENUM_RTSPPROTOCOL_PACKET enPacket;
    CHAR tszLeaveId[3];                        //H.264的sps的第1个字节之后的3个字节(不包括第一个字节)
    CHAR tszSPSBase[256];                      //SPS的信息,PPS和SPS作为SDP组包的时候,系统会自动为你进行BASE64编码
    CHAR tszPPSBase[256];                      //PPS的信息
    int nSPSLen;                               //输入SPS获取的大小
    int nPPSLen;                               //输入PPS获取的大小
}RTSPPROTOCOL_MEDIAINFO;
//////////////////////////////////////////////////////////////////////////
//                       导出的函数
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD RtspProtocol_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                      解析函数导出                                    */
/************************************************************************/
/********************************************************************
函数名称：RtspProtocol_Parse_Create
函数功能：创建一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符串指针
  可空：N
  意思：要创建的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Parse_Create(LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：RtspProtocol_Parse_Delete
函数功能：删除一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的地址
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Parse_Delete(LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：RtspProtocol_Parse_Insert
函数功能：解析缓冲区并且插入队列
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：创建的客户端地址
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要解析的缓冲区
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Parse_Insert(LPCTSTR lpszClientAddr, LPCTSTR lpszMsgBuffer, int nMsgLen);
/********************************************************************
函数名称：RtspProtocol_Parse_Get
函数功能：从队列里面获取一个请求包
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取的客户端地址
 参数.二：pSt_RtspRequest
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出获取到的RTSP请求包
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Parse_Get(LPCTSTR lpszClientAddr, RTSPPROTOCOL_REQUEST *pSt_RtspRequest);
/********************************************************************
函数名称：RtspProtocol_Parse_Parse
函数功能：解析缓冲区
 参数.一：pSt_RtspRequest
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：输出解析出来的RTSP请求
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要解析的缓冲区
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：此函数可单独使用
*********************************************************************/
extern "C" BOOL RtspProtocol_Parse_Parse(RTSPPROTOCOL_REQUEST *pSt_RtspRequest, LPCTSTR lpszMsgBuffer, int nMsgLen);
/************************************************************************/
/*                      帮助函数导出                                    */
/************************************************************************/
/********************************************************************
函数名称：RtspProtocol_Help_Transport
函数功能：传输端口解析帮助函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：ptszProto
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出客户端需要的包类型(RTP/AVP)
 参数.三：ptszType
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出客户端的数据传输类型(unicast)
 参数.四：pInt_PortStart
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出客户端接受数据开始端口,一般表示RTP
 参数.五：pInt_PortEnd
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出客户端接受数据结束端口,一般表示RTCP
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Help_Transport(LPCTSTR lpszMsgBuffer, TCHAR *ptszProto, TCHAR *ptszType, int *pInt_PortStart, int *pInt_PortEnd);
/********************************************************************
函数名称：RtspProtocol_Help_Transport
函数功能：传输端口解析帮助函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：ptszType
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出客户端时间类型npt
 参数.三：plfTimeStart
  In/Out：Out
  类型：双精度浮点型指针
  可空：N
  意思：导出客户端需要的开始时间节点
 参数.四：plfTimeEnd
  In/Out：Out
  类型：双精度浮点型指针
  可空：N
  意思：导出客户端需要的结束时间节点,如果为0,表示,没有结束
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Help_Range(LPCTSTR lpszMsgBuffer, TCHAR *ptszType, double *plfTimeStart, double *plfTimeEnd);
/********************************************************************
函数名称：RtspProtocol_Help_Url
函数功能：URL请求的地址分割
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：ptszFileName
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：导出获取到的文件或者串流地址
 参数.三：ptszUrl
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：导出获取到的URL地址
 参数.四：ptszType
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：导出获取到的请求头
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Help_Url(LPCTSTR lpszMsgBuffer, TCHAR *ptszFileName = NULL, TCHAR *ptszUrl = NULL, TCHAR *ptszType = NULL);
/********************************************************************
函数名称：RtspProtocol_Help_SDPPacket
函数功能：组装SDP信息
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出封装好的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
 参数.三：xhToken
  In/Out：In
  类型：句柄
  可空：N
  意思：输入会话ID
 参数.四：lpszIPAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入本地网络IP地址
 参数.五：lpszStreamName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入流名称
 参数.六：pSt_MediaInfo
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入媒体信息
 参数.七：nChannels
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入此流通道编号
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Help_SDPPacket(CHAR *ptszMsgBuffer, int *pInt_MsgLen, XNETHANDLE xhToken, LPCSTR lpszIPAddr, LPCSTR lpszStreamName, RTSPPROTOCOL_MEDIAINFO *pSt_MediaInfo, int nChannel = 0);
/************************************************************************/
/*                      打包函数导出                                    */
/************************************************************************/
/********************************************************************
函数名称：RtspProtocol_Packet_Option
函数功能：选项方法打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：pSt_Options
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入你服务器支持的方法
 参数.四：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入客户端请求的序列号
 参数.五：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Option(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, RTSPPROTOCOL_OPTIONS *pSt_Options, int nCseq, int nCode = 200);
/********************************************************************
函数名称：RtspProtocol_Packet_Describe
函数功能：描述方法打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszRtspUrl
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入RTSP请求的URL地址
 参数.四：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入返回的SDP信息
 参数.五：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.六：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
返回值
  类型：逻辑型
  意思：是否成功
备注：如果状态码不为200,那么参数三和四将不起作用
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Describe(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszRtspUrl, LPCTSTR lpszMsgBuffer, int nCseq, int nCode = 200);
/********************************************************************
函数名称：RtspProtocol_Packet_Setup
函数功能：配置方法打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszSession
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：创建的RTP会话ID
 参数.四：lpszProto
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入RTSP传输负载类型
 参数.五：lpszType
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入传输方式
 参数.六：pSt_RtspSetup
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入RTSP网络配置信息
 参数.七：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.八：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
返回值
  类型：逻辑型
  意思：是否成功
备注：如果状态码不为200,那么参数四,五,六将不起作用
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Setup(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszSession, LPCTSTR lpszProto, LPCTSTR lpszType, RTSPPROTOCOL_SETUP *pSt_RtspSetup, int nCseq, int nCode = 200);
/********************************************************************
函数名称：RtspProtocol_Packet_Play
函数功能：播放方法打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszSession
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入请求的会话ID
 参数.四：lpszRange
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入播放时间范围
 参数.五：lpszRtspUrl
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入请求的URL地址
 参数.六：lpszTrack
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：通道名称
 参数.七：nRtpSeq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入RTP包序号
 参数.八：nRtpTime
  In/Out：In
  类型：整数型
  可空：N
  意思：输入RTP包时间
 参数.九：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.十：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Play(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszSession, LPCTSTR lpszRange, LPCTSTR lpszRtspUrl, LPCTSTR lpszTrack, int nRtpSeq, int nRtpTime, int nCseq, int nCode = 200);
/********************************************************************
函数名称：RtspProtocol_Packet_Pasue
函数功能：暂停操作打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszSession
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入请求的会话ID
 参数.四：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.五：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Pasue(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszSession, int nCseq, int nCode = 200);
/********************************************************************
函数名称：RtspProtocol_Packet_Teardown
函数功能：关闭方法打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszSession
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入请求的会话ID
 参数.四：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.五：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Teardown(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszSession, int nCseq, int nCode = 200);
/********************************************************************
函数名称：RtspProtocol_Packet_Parament
函数功能：参数方法打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszSession
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入请求的会话ID
 参数.四：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.五：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
 参数.六：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：附加负载数据,如果有的话
返回值
  类型：逻辑型
  意思：是否成功
备注：获取参数方法可以用来表示心跳
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Parament(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszSession, int nCseq, int nCode = 200, LPCTSTR lpszMsgBuffer = NULL);
/********************************************************************
函数名称：RtspProtocol_Packet_CodeMsg
函数功能：打包一个指定消息
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出打好包的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入请求的序列号
 参数.四：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入返回的处理结果码
 参数.五：lpszSession
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入请求的会话ID
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_CodeMsg(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, int nCseq, int nCode = 404, LPCTSTR lpszSession = NULL);
/********************************************************************
函数名称：RtspProtocol_Packet_Auth
函数功能：请求对方进行鉴权
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出封装好的缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
 参数.三：nCseq
  In/Out：In
  类型：整数型
  可空：N
  意思：输入序列号
返回值
  类型：逻辑型
  意思：是否成功
备注：返回此包给客户端,客户端需要进行HTTP BASIC鉴权
*********************************************************************/
extern "C" BOOL RtspProtocol_Packet_Auth(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, int nCseq);
