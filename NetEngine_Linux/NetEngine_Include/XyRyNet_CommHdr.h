﻿#pragma once
/********************************************************************
//	Created:	2011/9/14   15:14
//	Filename: 	/NetEngine_Linux/NetEngine_SourceCode/XyRyNet_Comm.h
//	File Path:	/NetEngine_Linux/NetEngine_SourceCode/
//	File Base:	XyRyNet_Comm
//	File Ext:	h
//  Project:    网络通信引擎-LINUX版本
//	Author:		dowflyon
//	Purpose:	公用头文件-必须加载
//	History:
*********************************************************************/
///////////////////////////////////////////////////////////////////////
//                  自定义类型
///////////////////////////////////////////////////////////////////////
typedef long (LONG);                                                   //有长整数型
typedef unsigned long int (ULONG);                                     //无符号长整数型
typedef unsigned long long (DWORD64);                                  //四字
typedef unsigned long (DWORD);                                         //双字
typedef unsigned short (WORD);                                         //字
typedef unsigned char (BYTE);                                          //无符号字符
typedef BYTE (byte);                                                   //无符号字符
typedef int BOOL;                                                      //逻辑型
typedef int (HFILE);                                                   //文件句柄
typedef int (HDEVICE);                                                 //设备句柄
typedef void* (LPVOID);                                                //无类型指针
typedef int (SOCKET);                                                  //网络套接字句柄
typedef unsigned long int (HANDLE);                                    //线程句柄
typedef void * (HMODULE);                                              //模块句柄
typedef unsigned int (UINT);                                           //无符号整数型
typedef char (CHAR);                                                   //字符
typedef unsigned char (UCHAR);                                         //无符号字符型
typedef wchar_t (WCHAR);                                               //宽字符
typedef const char* (LPCSTR);                                          //常量字符指针
typedef const wchar_t* (LPCWSTR);                                      //常量宽字符指针
typedef unsigned short int (USHORT);                                   //无符号短整数型
typedef unsigned long long XNETHANDLE,ULONGLONG;                       //网络句柄
typedef void* XHANDLE;
typedef unsigned long long* (PXNETHANDLE);                             //网络句柄指针
typedef unsigned long* DWORD_PTR,*PDWORD_PTR;                          //双字指针
///////////////////////////////////////////////////////////////////////
//                  自定义定义
///////////////////////////////////////////////////////////////////////
#define INVALID_HANDLE_VALUE -1                                        //失败的文件句柄
#define INVALID_SOCKET -1                                              //失败的SOCKET句柄
#define SOCKET_ERROR -1                                                //错误的SOCKET处理
#ifndef FALSE
#define FALSE   0
#endif
#ifndef TRUE
#define TRUE    1
#endif
#define __int64 int64_t                                                //有符号64位整数型
#define MAX_PATH 260                                                   //文件夹最大深度
#define _MAX_DRIVE 3                                                   //设备路径类型个数
#define __stdcall
#define CALLBACK
///////////////////////////////////////////////////////////////////////
//                  自定义预处理
///////////////////////////////////////////////////////////////////////
#ifdef _UNICODE
typedef const wchar_t* LPCTSTR;
typedef wchar_t TCHAR;
#else
typedef char TCHAR;
typedef const char* LPCTSTR;
typedef const void* LPCVOID;
#endif
#ifdef _UNICODE
#define _T(x)      L ## x
#else
#define _T(x)      x
#endif

#ifdef XYRY_NETENGINE_API_CALL
#define XYRY_NETENGINE_APICALL __stdcall
#else
#define XYRY_NETENGINE_APICALL
#endif
///////////////////////////////////////////////////////////////////////
//                  自定义操作
///////////////////////////////////////////////////////////////////////
#define MAKEVERSION(a,b) ((WORD)(((BYTE) (a)) | ((WORD)((BYTE) (b))) << 8))
#define MAKEWORD(a,b) ((WORD)(((BYTE)(((DWORD)(a)) & 0xff)) | ((WORD)((BYTE)(((DWORD)(b)) & 0xff))) << 8))
