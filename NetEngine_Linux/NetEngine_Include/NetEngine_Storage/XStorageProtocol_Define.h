#pragma once
/********************************************************************
//	Created:	2019/6/28   16:08
//	Filename: 	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_Storage\NetEngine_XStorageProtocol\XStorageProtocol_Define.h
//	File Path:	E:\NetEngine_Windows\NetEngine_SourceCode\NetEngine_Storage\NetEngine_XStorageProtocol
//	File Base:	XStorageProtocol_Define
//	File Ext:	h
//  Project:    NetEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	协议模块导出函数
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                      导出函数定义
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD XStorageProtocol_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                      公用协议操作导出函数                            */
/************************************************************************/
/********************************************************************
函数名称：XStorageProtocol_Comm_ParseFile
函数功能：通用解析函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要解析的缓冲区
 参数.二：byVersion
  In/Out：In
  类型：字节
  可空：N
  意思：输入解析协议的版本号,支持1结构体,2JSON
 参数.三：pSt_ProtocolFile
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出解析后的数据
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Comm_ParseFile(LPCTSTR lpszMsgBuffer, BYTE byVersion, NETENGINE_PROTOCOLFILE *pSt_ProtocolFile);
/********************************************************************
函数名称：XStorageProtocol_Comm_Response
函数功能：通用回复协议打包函数
 参数.一：pSt_ProtocolHdr
  In/Out：In/Out
  类型：数据结构指针
  可空：N
  意思：输入要返回的协议,协议头的后续大小字段会被修改
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出组好包的缓冲区
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
 参数.四：nMsgCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入操作的结果,0为真
 参数.五：lpszMsgInfo
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入CODE字段解释,可以不填
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Comm_Response(NETENGINE_PROTOCOLHDR *pSt_ProtocolHdr, TCHAR *ptszMsgBuffer, int *pInt_MsgLen, int nMsgCode = 0, LPCTSTR lpszMsgInfo = NULL);
/********************************************************************
函数名称：XStorageProtocol_Comm_ParseClient
函数功能：通用客户端解析函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要解析的缓冲区
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字节指针
  可空：N
  意思：输出获取到的后续数据
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出后续数据大小
 参数.三：pSt_ProtocolHdr
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：导出协议头
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Comm_ParseClient(LPCTSTR lpszMsgBuffer, TCHAR *ptszMsgBuffer, int *pInt_MsgLen, NETENGINE_PROTOCOLHDR *pSt_ProtocolHdr = NULL);
/************************************************************************/
/*                      客户端协议操作导出函数                          */
/************************************************************************/
/********************************************************************
函数名称：XStorageProtocol_Client_REQQuery
函数功能：查询请求函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出组好包的请求缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszTimeStart
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：查询文件所属开始时间
 参数.四：lpszTimeEnd
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：查询文件所属结束时间
 参数.五：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入要查询的文件名
 参数.六：lpszFileMD5
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入要查询的文件MD5
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQQuery(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszTimeStart, LPCTSTR lpszTimeEnd, LPCTSTR lpszFileName = NULL, LPCTSTR lpszFileMD5 = NULL);
/********************************************************************
函数名称：XStorageProtocol_Client_REQDelete
函数功能：删除文件请求函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出组好包的请求缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入要删除的文件名
 参数.四：lpszFileMD5
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入要删除的文件MD5
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQDelete(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszFileName = NULL, LPCTSTR lpszFileMD5 = NULL);
/********************************************************************
函数名称：XStorageProtocol_Client_REQLogin
函数功能：登录请求协议封装函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出组好包的请求缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：pSt_ProtocolAuth
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入要打包的登录协议
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQLogin(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, NETENGINE_PROTOCOL_USERAUTH *pSt_ProtocolAuth);
/********************************************************************
函数名称：XStorageProtocol_Client_REQUPFile
函数功能：请求上传文件协议打包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出请求协议缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要打包的文件路径
 参数.四：lpszFilePath
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入要保存的位置
 参数.五：xhToken
  In/Out：In
  类型：网络句柄
  可空：Y
  意思：输入上传数据的令牌
 参数.六：bUPFile
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：上传还是下载,默认上传
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQFile(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszFileName, LPCTSTR lpszFilePath = NULL, XNETHANDLE xhToken = 0, BOOL bUPFile = TRUE);
//////////////////////////////////////////////////////////////////////////
/********************************************************************
函数名称：XStorageProtocol_Client_REQDirOperator
函数功能：请求创建或者删除一个文件夹
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出组好包的协议缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
 参数.三：lpszUserDir
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要操作的目录
 参数.四：bCreate
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：真为创建,假为删除
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQDirOperator(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszUserDir, BOOL bCreate = TRUE);
/********************************************************************
函数名称：XStorageProtocol_Client_REQDirQuery
函数功能：查询用户目录
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出组好包的协议缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQDirQuery(TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：XStorageProtocol_Client_REQRegister
函数功能：用户注册
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出封装好的缓冲区信息
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
 参数.三：lpszUser
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要注册的用户名
 参数.四：lpszPass
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要注册的密码
 参数.五：nPerimission
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：权限级别
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Client_REQRegister(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPCTSTR lpszUser, LPCTSTR lpszPass, int nPerimission = 0);
/************************************************************************/
/*                      服务核心协议操作导出函数                        */
/************************************************************************/
/********************************************************************
函数名称：XStorageProtocol_Core_REQQueryFile
函数功能：文件查询请求解析函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：ptszTimeStart
  In/Out：Out
  类型：字符指针
  可空：N
  意思：查询开始时间
 参数.三：ptszTimeEnd
  In/Out：Out
  类型：字符指针
  可空：N
  意思：查询结束时间
 参数.四：ptszFileName
  In/Out：Out
  类型：字符指针
  可空：N
  意思：查询的文件名
 参数.五：ptszFileMD5
  In/Out：Out
  类型：字符指针
  可空：N
  意思：查询的文件MD5
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Core_REQQueryFile(LPCTSTR lpszMsgBuffer, TCHAR *ptszTimeStart, TCHAR *ptszTimeEnd, TCHAR *ptszFileName = NULL, TCHAR *ptszFileMD5 = NULL);
/********************************************************************
函数名称：XStorageProtocol_Core_REPQueryFile
函数功能：查询回复打包协议
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出组好包的请求缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：lParam
  In/Out：In
  类型：无类型指针
  可空：N
  意思：输入list<XSTORAGECORE_DBFILE> 查询的STL
 参数.四：lpszTimeStart
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入查询请求的开始时间
 参数.五：lpszTimeEnd
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入查询请求的结束时间
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Core_REPQueryFile(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, LPVOID lParam, LPCTSTR lpszTimeStart = NULL, LPCTSTR lpszTimeEnd = NULL);
/********************************************************************
函数名称：XStorageProtocol_Core_REPUPFile
函数功能：查询返回文件上传请求确认协议打包
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出组好包的请求缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出缓冲区大小
 参数.三：xhToken
  In/Out：In
  类型：网络句柄
  可空：Y
  意思：输入客户端的令牌,如果有的话
 参数.四：bUPFile
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是上传还是下载,默认上传
 参数.五：nCode
  In/Out：In
  类型：整数型
  可空：Y
  意思：返回的处理完毕CODE值
 参数.六：lpszCodeMsg
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入CODE的文字说明,如果需要的话
 参数.七：pSt_ProtcolFile
  In/Out：In
  类型：数据结构指针
  可空：Y
  意思：输入文件附加信息,如果需要的话!
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Core_REPFile(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, XNETHANDLE xhToken = 0, BOOL bUPFile = TRUE, int nCode = 0, LPCTSTR lpszCodeMsg = NULL, NETENGINE_PROTOCOLFILE *pSt_ProtcolFile = NULL);
/********************************************************************
函数名称：XStorageProtocol_Core_REQCreateDir
函数功能：创建文件夹协议
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：ptszUserDir
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的文件夹
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Core_REQCreateDir(LPCTSTR lpszMsgBuffer, TCHAR *ptszUserDir);
/********************************************************************
函数名称：XStorageProtocol_Core_REPQueryDir
函数功能：查询用户目录返回处理封包函数
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出组好包的协议缓冲区
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出缓冲区大小
 参数.三：pStl_ListEnum
  In/Out：In
  类型：STL容器指针
  可空：N
  意思：输入查找到的文件夹
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Core_REPQueryDir(TCHAR *ptszMsgBuffer, int *pInt_MsgLen, list<tstring> *pStl_ListEnum);
/********************************************************************
函数名称：XStorageProtocol_Core_REQUserReg
函数功能：用户注册协议解析函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解析的缓冲区
 参数.二：ptszUser
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的用户名
 参数.三：ptszPass
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的密码
 参数.四：pInt_Permission
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的用户权限级别
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL XStorageProtocol_Core_REQUserReg(LPCTSTR lpszMsgBuffer, TCHAR *ptszUser, TCHAR *ptszPass, int *pInt_Permission);
