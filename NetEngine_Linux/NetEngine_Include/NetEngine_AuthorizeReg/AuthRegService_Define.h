#pragma once
/********************************************************************
//	Created:	2013/1/21  13:22
//	File Name: 	G:\U_DISK_Path\NetSocketEngine\NetEngine_AuthorizeReg\NetEngine_AuthRegService\AuthRegService_Define.h
//	File Path:	G:\U_DISK_Path\NetSocketEngine\NetEngine_AuthorizeReg\NetEngine_AuthRegService
//	File Base:	AuthRegService_Define
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		qyt
//	Purpose:	导出定义和函数
//	History:
*********************************************************************/
#define NETENGINE_AUTHREG_SERVICE_SQL_MAX_USERNAME 32
//////////////////////////////////////////////////////////////////////////
//                         导出的回调函数
//////////////////////////////////////////////////////////////////////////
//用户在线时间事件回调处理函数，用户名 在线时间 剩余时间（分,天） 注册的卡类型 自定义参数
typedef void(*CALLBACK_NETENGIEN_AUTHREG_SERVICE_EVENTS)(LPCTSTR lpszUserName,__int64 nOnlineTimer,__int64 nLeftTimer,ENUM_AUTHREG_GENERATESERIALTYPE en_AuthRegSerialType,LPVOID lParam);
//////////////////////////////////////////////////////////////////////////
//                         导出的数据结构
//////////////////////////////////////////////////////////////////////////
//充值协议
typedef struct
{
    TCHAR tszUserName[NETENGINE_AUTHREG_SERVICE_SQL_MAX_USERNAME];        //用户
    TCHAR tszSerialNumber[128];                                           //序列号
}AUTHREG_PROTOCOL_USERPAY,*LPAUTHREG_PROTOCOL_USERPAY;
//时间信息协议
typedef struct
{
    TCHAR tszUserName[NETENGINE_AUTHREG_SERVICE_SQL_MAX_USERNAME];        //用户
    __int64 nTimeONLine;
    __int64 nTimeLeft;
    ENUM_AUTHREG_GENERATESERIALTYPE enSerialType;
}AUTHREG_PROTOCOL_TIME,*LPAUTHREG_PROTOCOL_TIME;
//用户表
typedef struct tag_AuthReg_UserTable
{
    TCHAR tszUserName[NETENGINE_AUTHREG_SERVICE_SQL_MAX_USERNAME];        //用户
    TCHAR tszPassword[NETENGINE_AUTHREG_SERVICE_SQL_MAX_USERNAME];        //用户密码
    TCHAR tszRegData[64];                                                 //注册日期 日期格式：2013/1/5-12:33:33
    TCHAR tszLeftTime[64];                                                //剩余日期
    TCHAR tszEmailAddr[64];                                               //电子邮件地址
    TCHAR tszHardCode[32];                                                //硬件码
    ENUM_AUTHREG_GENERATESERIALTYPE en_AuthRegSerialType;                 //充值卡类型
    __int64 dwIDCard;                                                     //身份证
    __int64 dwQQNumber;                                                   //QQ号
}AUTHREG_USERTABLE,*LPAUTHREG_USERTABLE;
//注册序列号表
typedef struct tag_AuthReg_SerialTable
{
    TCHAR tszUserName[NETENGINE_AUTHREG_SERVICE_SQL_MAX_USERNAME];        //使用者是谁
    TCHAR tszSerialNumber[128];                                           //序列号
    TCHAR tszMaxTime[64];                                                 //使用时间
    ENUM_AUTHREG_GENERATESERIALTYPE en_AuthRegSerialType;                 //充值卡类型
    BOOL bIsUsed;                                                         //是否已经使用
}AUTHREG_SERIALTABLE,*LPAUTHREG_SERIALTABLE;
//////////////////////////////////////////////////////////////////////////
//                         导出的函数
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD AuthRegService_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                         数据库服务导出函数                           */
/************************************************************************/
/********************************************************************
函数名称：AuthRegService_SQL_Init
函数功能：初始化数据库服务
 参数.一：lpszSqlFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：数据库文件路径
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：先初始化数据库服务，在初始化网络服务，才可以使用本验证服务器！
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_Init(LPCTSTR lpszSqlFile);
/********************************************************************
函数名称：AuthRegService_Sql_Destroy
函数功能：销毁数据库服务
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_Destroy();
/********************************************************************
函数名称：AuthRegService_Sql_UserDelete
函数功能：删除一个用户从数据库中
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的用户
返回值
  类型：逻辑型
  意思：是否删除成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_UserDelete(LPCTSTR lpszUserName);
/********************************************************************
函数名称：AuthRegService_Sql_UserRegister
函数功能：用户注册处理数据库语句函数
 参数.一：pSt_UserInfo
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：要插入的用户数据
返回值
  类型：逻辑型
  意思：是否插入成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_UserRegister(LPAUTHREG_USERTABLE pSt_UserInfo);
/********************************************************************
函数名称：AuthRegService_Sql_UserQuery
函数功能：查询用户相对应的值
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要查询的指定用户
 参数.二：pSt_UserInfo
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：如果为空NULL，那么将只判断此用户是否存在
返回值
  类型：逻辑型
  意思：是否查询成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_UserQuery(LPCTSTR lpszUserName,LPAUTHREG_USERTABLE pSt_UserInfo);
/********************************************************************
函数名称：AuthRegService_Sql_UserPay
函数功能：用户充值函数
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要充值的用户名
 参数.二：lpszSerialName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：充值使用的序列号
返回值
  类型：逻辑型
  意思：是否成功充值
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_UserPay(LPCTSTR lpszUserName,LPCTSTR lpszSerialName);
/********************************************************************
函数名称：AuthRegService_Sql_UserLeave
函数功能：用户离开处理事件
 参数.一：pSt_UserTable
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：离开的用户信息
 参数.二：nLeftTime
  In/Out：In
  类型：整数型
  可空：N
  意思：用户的剩余时间，根据卡的类型处理得到相应的结果
返回值
  类型：逻辑型
  意思：是否处理成功
备注：只有分钟和天数才需要处理
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_UserLeave(LPAUTHREG_USERTABLE pSt_UserTable,__int64 nLeftTime);
/********************************************************************
函数名称：AuthRegService_Sql_SerialInsert
函数功能：插入一个序列号到数据库
 参数.一：lpszSerialNumber
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要插入的序列号
返回值
  类型：逻辑型
  意思：是否插入成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_SerialInsert(LPCTSTR lpszSerialNumber);
/********************************************************************
函数名称：AuthRegService_Sql_SerialDelete
函数功能：从数据库删除指定序列号
 参数.一：lpszSerialNumber
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的序列号
返回值
  类型：逻辑型
  意思：是否删除成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_SerialDelete(LPCTSTR lpszSerialNumber);
  /********************************************************************
函数名称：AuthRegService_Sql_SerialQuery
函数功能：查询一个指定的序列号信息
 参数.一：lpszSerialNumber
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要查询的序列号
 参数.二：pSt_SerialTable
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：导出查询到的信息，如果为NULL，此参数将不起作用
返回值
  类型：逻辑型
  意思：是否查询成功，如果第二个参数为NULL，那么将只返回是否有这个序列号
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_SerialQuery(LPCTSTR lpszSerialNumber,LPAUTHREG_SERIALTABLE pSt_SerialTable);
/********************************************************************
函数名称：AuthRegService_Sql_SerialQueryAll
函数功能：查询序列卡表中的所有序列号
 参数.一：pInt_Number
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：导出有多少张卡！如果为空将不获取
返回值
  类型：逻辑型
  意思：是否查询成功
备注：查询成功后调用AuthRegService_Sql_SerialGetAll 来获取查询的结果
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_SerialQueryAll(int *pInt_Number = NULL);
/********************************************************************
函数名称：AuthRegService_Sql_SerialGetAll
函数功能：执行了获取操作后，通过此函数获取一个卡的信息
 参数.一：pSt_SerialTable
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出获取到的序列卡内容信息
返回值
  类型：逻辑型
  意思：是否获取成功
备注：一直执行下去，直到返回错误，错误码为 ERROR_AUTHORIZEREG_SERVICE_DATABASE_QUERYALL_NONE，表示没有记录了
*********************************************************************/
extern "C" BOOL AuthRegService_Sql_SerialGetAll(LPAUTHREG_SERIALTABLE pSt_SerialTable);
/************************************************************************/
/*                         网络服务导出函数                             */
/************************************************************************/
/********************************************************************
函数名称：AuthRegService_Net_Init
函数功能：初始化网络库
 参数.一：fpCall_AuthEvent
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：验证服务器事件时间回调函数，通过此来通知上层用户时间更新
 参数.二：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_Init(CALLBACK_NETENGIEN_AUTHREG_SERVICE_EVENTS fpCall_AuthEvent,LPVOID lParam = NULL);
/********************************************************************
函数名称：AuthRegService_Net_GetClient
函数功能：获取客户端信息
 参数.一：pStl_ListClient
  In/Out：Out
  类型：LIST容器
  可空：N
  意思：导出获取到的客户端信息
 参数.二：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要获取的客户端地址，为空表示获取所有
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_GetClient(list<AUTHREG_USERTABLE> *pStl_ListClient,LPCTSTR lpszClientAddr = NULL);
/********************************************************************
函数名称：AuthRegService_Net_GetTimer
函数功能：获取客户端时间信息
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要查找用户名
 参数.二：pSt_AuthTime
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：用户时间信息结构体
返回值
  类型：逻辑型
  意思：是否获取成功
备注：通过卡类型来判断导出的时间是分钟还是天
*********************************************************************/
extern "C" BOOL AuthRegService_Net_GetTimer(LPCTSTR lpszUserName,AUTHREG_PROTOCOL_TIME *pSt_AuthTime);
/********************************************************************
函数名称：AuthRegService_Net_SendMsg
函数功能：发送验证协议数据填充函数
 参数.一：pSt_ProtocolHdr
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入要打包的协议头
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：发送数据填充后的缓冲区
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出发送数据缓冲区大小
 参数.四：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：发送缓冲区内容,如果没有附加信息可以为NULL
 参数.五：nMsgLen
  In/Out：In
  类型：整数型
  可空：Y
  意思：发送数据长度
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_SendMsg(NETENGINE_PROTOCOLHDR *pSt_ProtocolHdr,TCHAR *ptszMsgBuffer,int *pInt_MsgLen,LPCTSTR lpszMsgBuffer = NULL,int nMsgLen = 0);
/********************************************************************
函数名称：AuthRegService_Net_RecvMsg
函数功能：分析协议处理器
 参数.一：lpszRecvMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要分析的数据
 参数.二：pInt_Protocl
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：导出协议
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：导出后续数据长度
返回值
  类型：逻辑型
  意思：是否分析成功
备注：要分析的数据必须是一个完整的包
*********************************************************************/
extern "C" BOOL AuthRegService_Net_RecvMsg(LPCTSTR lpszMsgBuffer,int *pInt_Protocl = NULL,int *pInt_MsgLen = NULL);
/********************************************************************
函数名称：AuthRegService_Net_GetAddrForUser
函数功能：通过用户名获取对应地址
 参数.一：lpszClientUser
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入用户名
 参数.二：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的用户套接字地址
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_GetAddrForUser(LPCTSTR lpszClientUser,TCHAR *ptszClientAddr);
/********************************************************************
函数名称：AuthRegService_Net_GetUserForAddr
函数功能：通过IP地址获取对应用户名
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入IP地址端口
 参数.二：ptszClientUser
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的用户名
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_GetUserForAddr(LPCTSTR lpszClientAddr, TCHAR *ptszClientUser);
/********************************************************************
函数名称：AuthRegService_Net_CloseClient
函数功能：移除一个客户端
 参数.一：lpszClientUser
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要移除的用户名
返回值
  类型：逻辑型
  意思：是否移除成功
备注：此函数会自动调用AuthRegService_Sql_UserLeave来处理离开时间
*********************************************************************/
extern "C" BOOL AuthRegService_Net_CloseClient(LPCTSTR lpszClientUser);
/********************************************************************
函数名称：AuthRegService_Net_Destroy
函数功能：销毁网络服务
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_Destroy();
/********************************************************************
函数名称：AuthRegService_Net_LoginAnalysis
函数功能：用户登陆协议分析
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：用户的IP地址加端口
 参数.二：pSt_AuthProtocol
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：用户传递的协议数据
返回值
  类型：逻辑型
  意思：是否允许登陆
备注：如果成功，服务器会自动进行计时
*********************************************************************/
extern "C" BOOL AuthRegService_Net_ProLogin(LPCTSTR lpszClientAddr,NETENGINE_PROTOCOL_USERAUTH *pSt_AuthProtocol);
/********************************************************************
函数名称：AuthRegService_Net_ProDelete
函数功能：用户删除自身协议处理
 参数.一：pSt_AuthProtocol
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：用户传递的协议数据
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_ProDelete(AUTHREG_USERTABLE *pSt_UserTable);
/********************************************************************
函数名称：AuthRegService_Net_ProDelete
函数功能：用户删除自身协议处理
 参数.一：pSt_AuthProtocol
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：用户传递的协议数据
 参数.二：pSt_AuthProtocol
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出查询的用户名和密码，这个结构体需要返回给客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL AuthRegService_Net_ProGetPass(AUTHREG_USERTABLE *pSt_UserTable,NETENGINE_PROTOCOL_USERAUTH *pSt_AuthProtocol);
