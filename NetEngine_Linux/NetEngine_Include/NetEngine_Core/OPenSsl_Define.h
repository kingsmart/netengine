#pragma once
/********************************************************************
//	Created:	2011/7/18  20:53
//	Filename: 	j:\U_DISK_Path\NetSocketEngine\CryptOpenSSL\OpenSSL_OutDefine.h
//	File Path:	j:\U_DISK_Path\NetSocketEngine\CryptOpenSSL
//	File Base:	OpenSSL_OutDefine
//	File Ext:	h
//      Project:        NetSocketEnginer(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	网络通信引擎PKI开发组件导出定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                   导入定义
//////////////////////////////////////////////////////////////////////////
//SSL证书文件证书编码类型
#define NETENGINE_OPENSSL_OPENSSL_PEM_FILE 1                              //PEM编码格式：BASE64。调用者无需关心编码实现部分，只需要传递即可
#define NETENGINE_OPENSSL_OPENSSL_ASN1_FILE 2                             //ASN1编码格式：DER
//////////////////////////////////////////////////////////////////////////
//        数据类型定义
//////////////////////////////////////////////////////////////////////////
//摘要算法库
typedef enum en_NetEngine_OpenSsl_Digest
{
    NETENGINE_OPENSSL_API_DIGEST_MD4 = 1,                                 //MD4摘要算法
    NETENGINE_OPENSSL_API_DIGEST_MD5,                                     //MD5摘要算法
    NETENGINE_OPENSSL_API_DIGEST_SHA1,                                    //SHA1摘要算法
    NETENGINE_OPENSSL_API_DIGEST_SHA256,                                  //SHA256摘要算法
    NETENGINE_OPENSSL_API_DIGEST_SHA512,                                  //SHA512摘要算法
    NETENGINE_OPENSSL_API_DIGEST_RIPEMD160                                //RIPEMD160摘要算法
}ENUM_NETENGINE_OPENSSL_DIGEST,*LPENUM_NETENGINE_OPENSSL_DIGEST;
//非对称加解密库
typedef enum en_NetEngine_OpenSsl_Crypt
{
    NETENGINE_OPENSSL_API_CRYPT_AES = 1,                                  //AES加解密
    NETENGINE_OPENSSL_API_CRYPT_DES = 2,                                  //DES加解密
    NETENGINE_OPENSSL_API_CRYPT_3DES = 3,                                 //3DES加解密算法
    NETENGINE_OPENSSL_API_CRYPT_RC4 = 4                                   //RC4加解密
}ENUM_NETENGINE_OPENSSL_CRYPT,*LPENUM_NETENGINE_OPENSSL_CRYPT;
//SSL安全传输协议库,用于服务器
typedef enum en_NetEngine_OpenSsl_Protocol
{
    NETENGINE_OPENSSL_PROTOCOL_SSL_SERVER,                                //同时支持SSL V2和V3版本的协议
    NETENGINE_OPENSSL_PROTOCOL_TLS_SERVER,                                //TLS V1 V2协议
    NETENGINE_OPENSSL_PROTOCOL_DTL_SERVER                                 //DTL V1 V2协议
}ENUM_NETENGINE_OPENSSL_PROTOCOL,*LPENUM_NETENGINE_OPENSSL_PROTOCOL;
//证书查询内部结构体
typedef struct tag_NetEngine_OpenSsl_X509CCInl
{
    TCHAR tszCountryName[128];                                            //国家
    TCHAR tszProvinceName[128];                                           //省
    TCHAR tszLocalityName[128];                                           //地区
    TCHAR tszOrganizationName[128];                                       //组织
    TCHAR tszOrgUnitName[128];                                            //单位
    TCHAR tszCommonName[128];                                             //通用名
    TCHAR tszEmailAddress[128];                                           //电子邮件
    struct
    {
        CHAR tszName[128];                                               //名字
        CHAR tszTitle[128];                                              //头衔
        CHAR tszDesCription[128];                                        //描述
    }st_ExtInfo;                                                         //可空信息
}OPENSSL_X509CCINL,*LPOPENSSL_X509CCINL;
//查询证书信息函数输入结构体，输出结构体
typedef struct tag_OpenSSL_X509CCInfo
{
    int nCertType;                                                        //证书类型,1用于加密,2用于签名
    long lVersion;                                                        //保存证书版本
    time_t nTimeStart;                                                    //证书生效时间
    time_t nTimeEnd;                                                      //证书过期时间
    CHAR tszSerialNumber[64];                                             //证书序列号
    CHAR tszAlgorithm[64];                                                //签名算法
    CHAR tszPubKey[1024];                                                 //证书的公钥
    CHAR tszPriKey[1024];                                                 //证书的私钥

    OPENSSL_X509CCINL st_Issuer;                                          //证书颁发者信息
    OPENSSL_X509CCINL st_SubJect;                                         //证书拥有者信息
}OPENSSL_X509CCINFO,*LPOPENSSL_X509CCINFO;
//查询证书信息函数输入结构体，输出结构体
typedef struct
{
    CHAR tszExtName[256];                                                 //名称
    CHAR tszExtValue[256];                                                //值
}OPENSSL_X509EXT, *LPOPENSSL_X509EXT;
//////////////////////////////////////////////////////////////////////////
//               函数导出定义
//////////////////////////////////////////////////////////////////////////
//获取错误码
extern "C" DWORD OPenSsl_GetLastError(int *pInt_ErrorCode = NULL);
/************************************************************************/
/*               数据编码算法导出函数                                      */
/************************************************************************/
/********************************************************************
函数名称：OPenSsl_Codec_UrlEnCodec
函数功能：URL编码
 参数.一：lpszSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要编码的URL字符串
 参数.二：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入原始字符串长度
 参数.三：pszDest
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出编码后的字符串
返回值
  类型：无
  意思：
备注：如果是传送HTTP服务器的字符串需要根据服务器类型做编码转换,比如LINUX是UTF8,然后在进行URL编码
*********************************************************************/
extern "C" void OPenSsl_Codec_UrlEnCodec(LPCSTR lpszSource,int nLen,CHAR *ptszDest);
/********************************************************************
函数名称：OPenSsl_Codec_UrlDeCodec
函数功能：URL解码
 参数.一：lpszSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要解码的URL字符串
 参数.二：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入原始字符串长度
 参数.三：pszDest
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出解码后的字符串
返回值
  类型：无
  意思：
备注:
*********************************************************************/
extern "C" void OPenSsl_Codec_UrlDeCodec(LPCSTR lpszSource,int nLen,CHAR *ptszDest);
/********************************************************************
函数名称：OPenSsl_Codec_Base64
函数功能：BASE64编解码
 参数.一：lpszSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要编解码的原始数据
 参数.二：ptszDest
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出编解码后的数据
 参数.三：pInt_Len
  In/Out：In
  类型：整数型指针
  可空：N
  意思：输入：原始数据大小，输出：编解码后的数据大小
 参数.四：bIsEnCodec
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：要编码还是解码，默认编码
 参数.五：bIsLine
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：编码的数据是否启用换行,默认不启用
返回值
  类型：逻辑型
  意思：是否编解码成功
备注：
*********************************************************************/
extern "C" BOOL OPenSsl_Codec_Base64(LPCTSTR lpszSource,TCHAR *ptszDest,int *pInt_Len,BOOL bIsEnCodec = TRUE, BOOL bIsLine = FALSE);
/********************************************************************
函数名称：OPenSsl_Codec_CRC32Codec
函数功能：CRC32公开算法文件获取源码
 参数.一：lpszSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入一段要编码的数据
 参数.二：nSize
  In/Out：In
  类型：整数型
  可空：N
  意思：编码数据大小
 参数.三：ptszBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出编码好的数据
返回值
  类型：逻辑型
  意思：是否编码成功
备注：
*********************************************************************/
extern "C" BOOL OPenSsl_Codec_CRC32Codec(LPCSTR lpszSource,int nSize,TCHAR *ptszBuffer);
/********************************************************************
函数名称：OPenSsl_Codec_2BytesToBCD
函数功能：两个字符转BCD编码
 参数.一：lpszSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入一段要编码的数据
 参数.二：chBCD
  In/Out：Out
  类型：无符号字符
  可空：N
  意思：输出编码好的数据
返回值
  类型：无
  意思：
备注：
*********************************************************************/
extern "C" void OPenSsl_Codec_2BytesToBCD(LPCTSTR lpszSource,UCHAR &chBCD);
/********************************************************************
函数名称：OPenSsl_Codec_BCDTo2Bytes
函数功能：BCD编码转字符
 参数.一：chBCD
  In/Out：In
  类型：无符号字符
  可空：N
  意思：输入要解码的BCD字符
 参数.二：ptszDest
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出解码后的字符串
返回值
  类型：无
  意思：
备注：
*********************************************************************/
extern "C" void OPenSsl_Codec_BCDTo2Bytes(UCHAR chBCD,TCHAR *ptszDest);
/********************************************************************
函数名称：OPenSsl_Codec_BCDToInt
函数功能：BCD编码转整数型
 参数.一：chBCD
  In/Out：In
  类型：无符号字符
  可空：N
  意思：输入要解码的BCD字符
返回值
  类型：整数型
  意思：输出解码数据
备注：
*********************************************************************/
extern "C" int OPenSsl_Codec_BCDToInt(UCHAR chBCD);
/********************************************************************
函数名称：OPenSsl_Codec_IntToBCD
函数功能：将00-99的整数存放成1个字节的BCD
 参数.一：uszInt
  In/Out：In
  类型：无符号字符
  可空：N
  意思：输入要编码的整数
返回值
  类型：无符号字符
  意思：输出编码数据
备注：
*********************************************************************/
extern "C" UCHAR OPenSsl_Codec_IntToBCD(UCHAR uszInt);
/************************************************************************/
/*               OPENSSL加解密算法API                                   */
/************************************************************************/
/************************************************************************
函数名称：OPenSsl_Api_CryptEncodec
函数功能：非对称加密
 参数一：lpszEnCodecString
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：待加密的数据
 参数二：ptszOutCodec
  In/Out：In/Out
  类型：无符号字符指针
  可空：N
  意思：加密后的字符指针，请不要尝试做任何编码转换。编码转换肯定认不出这个数据内容
 参数三：pInt_StrLen
  In/Out：In/Out
  类型：整数指针
  可空：N
  意思：输入待加密的字符串大小，输出加密后的数据长度
 参数四：lpszKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：表明要加密的KEY密钥。32位无符号，必须是8的倍数 要么8位要么16位
 参数五：en_CryptType
  In/Out：In
  类型：枚举型
  可空：Y
  意思：要使用哪种方式加密，默认为3DES
 返回值
  类型：逻辑型
  意思：是否加密成功
备注：
************************************************************************/
extern "C" BOOL OPenSsl_Api_CryptEncodec(LPCTSTR lpszEnCodecString,UCHAR *ptszOutCodec,int *pInt_StrLen,LPCTSTR lpszKey,ENUM_NETENGINE_OPENSSL_CRYPT en_CryptType = NETENGINE_OPENSSL_API_CRYPT_3DES);
/************************************************************************
函数名称：OPenSsl_Api_CryptDecodec
函数功能：非对称解密函数
 参数一：lpszDeCodecString
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：待解密的加密数据
 参数二：ptszOutCodec
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：解密后的数据，初始化足够的大小。返回解密后的数据
 参数三：pInt_StrLen
  In/Out：In/Out
  类型：整数指针
  可空：N
  意思：输入带解密的字符串空间大小，输出解密后数据长度
 参数四：lpszKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：解密KEY的值，32位值
 参数五：en_CryptType
  In/Out：In
  类型：枚举型
  可空：Y
  意思：要使用哪个加解密库
返回值
  类型：逻辑型
  意思：是否成功解密
备注：
************************************************************************/
extern "C" BOOL OPenSsl_Api_CryptDecodec(const UCHAR *lpszDeCodecString,TCHAR *ptszOutCodec,int *pInt_StrLen,LPCTSTR lpszKey,ENUM_NETENGINE_OPENSSL_CRYPT en_CryptType = NETENGINE_OPENSSL_API_CRYPT_3DES);
/************************************************************************
函数名称：OPenSsl_Api_Digest
函数功能：信息摘要算法实现函数
 参数一：lpszMD_Value
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要加密的数据
 参数二：ptszStr_Value
  In/Out：In/Out
  类型：无符号字符指针
  可空：N
  意思：输入一个足够的大的内存，输出加密后的数据
 参数三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入待加密的数据大小，输出加密后的数据大小,文件方式这个参数可以为NULL
 参数四：bIsFile
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：文件还是缓冲区,默认缓冲区
 参数五：dwDigestLib
  In/Out：In
  类型：双字
  可空：Y
  意思：加密方式，默认为MD5
返回值
  类型：逻辑型
  意思：是否加密成功
备注：
************************************************************************/
extern "C" BOOL OPenSsl_Api_Digest(LPCTSTR lpszMD_Value,UCHAR *ptszStr_Value,int *pInt_Len,BOOL bIsFile = FALSE,DWORD dwDigestLib = NETENGINE_OPENSSL_API_DIGEST_MD5);
/********************************************************************
函数名称：OPenSsl_Api_RsaGenerater
函数功能：生成RSA公钥和私钥文件
 参数.一：lpszPubFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要保存的公钥文件路径
 参数.二：lpszPriFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要保存的私钥文件路径，如果这个文件存在，将为其在增加一个公钥。
 参数.三：lpszPriPass
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要加密的私钥密码,可以不加密
返回值
  类型：逻辑型
  意思：是否生成成功
备注：RSA的私钥和公钥都可以用来加解密，但是他们的加解密的KEY要对称公钥加密-私钥解密 或者 私钥加密-公钥解密
*********************************************************************/
extern "C" BOOL OPenSsl_Api_RsaGenerater(LPCTSTR lpszPubFile,LPCTSTR lpszPriFile, LPCTSTR lpszPriPass = NULL);
/********************************************************************
函数名称：OPenSsl_Api_RsaEnCodec
函数功能：RSAKEY加密数据
 参数.一：lpszKeyFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：KEY文件路径
 参数.二：lpszSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要加密的原始数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入：原始数据长度，输出加密后的数据长度
 参数.四：ptszDest
  In/Out：Out
  类型：无符号字符指针
  可空：N
  意思：输出加密后的数据
 参数.五：bKeyType
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：为真表示使用公钥加密，为假表示使用私钥加密
 参数.六：lpszPriPass
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果私钥加过密,那么必须输入私钥密码
返回值
  类型：逻辑型
  意思：是否加密成功
备注：
*********************************************************************/
extern "C" BOOL OPenSsl_Api_RsaEnCodec(LPCTSTR lpszKeyFile,LPCTSTR lpszSource,int *pInt_Len,UCHAR *ptszDest,BOOL bKeyType = TRUE, LPCTSTR lpszPriPass = NULL);
/********************************************************************
函数名称：OPenSsl_Api_RsaEnCodec
函数功能：使用RSAKEY解密数据
 参数.一：lpszKeyFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：KEY文件路径
 参数.二：puszSource
  In/Out：In
  类型：常量无符号字符指针
  可空：N
  意思：要解密的原始数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入：原始数据长度，输出解密后的数据长度
 参数.四：ptszDest
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出解密后的数据
 参数.五：bKeyType
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：为真表示使用公钥解密，为假表示使用私钥解密
 参数.六：lpszPriPass
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果私钥加过密,那么必须输入私钥密码
返回值
  类型：逻辑型
  意思：是否解密成功
备注：
*********************************************************************/
extern "C" BOOL OPenSsl_Api_RsaDeCodec(LPCTSTR lpszKeyFile,const UCHAR *puszDest,int *pInt_Len,TCHAR *ptszDest,BOOL bKeyType = TRUE, LPCTSTR lpszPriPass = NULL);
/************************************************************************/
/*                 证书操作导出函数                                     */
/************************************************************************/
/********************************************************************
函数名称：OPenSSL_Cert_SignEncoder
函数功能：签名数据
 参数.一：lpszSignSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要签名的数据
 参数.二：ptszSignDest
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：输入足够大缓冲区，输出验证后的内容
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入，签名数据大小，输出，签名后的数据大小
 参数.四：dwDigestLib
  In/Out：In
  类型：双字
  可空：Y
  意思：为空取MD5，摘要算法
返回值
  类型：逻辑型
  意思：是否签名成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_SignEncoder(LPCTSTR lpszKeyFile, LPCTSTR lpszKeyPass, LPCSTR lpszSignSource, CHAR *ptszSignDest, int *pInt_Len, DWORD dwDigestLib = NETENGINE_OPENSSL_API_DIGEST_MD5);
/********************************************************************
函数名称：OPenSSL_Cert_DignVerifly
函数功能：验证签名
 参数.一：lpszSignSource
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要验证的数据
 参数.二：ptszSignDest
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：输入足够大缓冲区，输出验证后的内容
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入，验证数据大小，输出，验证后的数据大小
 参数.四：dwDigestLib
  In/Out：In
  类型：双字
  可空：Y
  意思：为空取MD5，摘要算法
返回值
  类型：逻辑型
  意思：为真为验证成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_SignVerifly(LPCTSTR lpszKeyFile, LPCTSTR lpszKeyPass, LPCTSTR lpszSignSource, int nSrcLen, LPCTSTR lpszSignDest, int nDstLen, DWORD dwDigestLib = NETENGINE_OPENSSL_API_DIGEST_MD5);
/********************************************************************
函数名称：OPenSSL_Cert_MakeCACert
函数功能：制作一个CA证书,用于签发证书请求文件
 参数.一：lpszCertFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要生成的CA根证书保存目录
 参数.二：lpszKeyFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要生成的CA根证书的密钥目录
 参数.三：lpszKeyPass
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要生成证书私钥密码,如果不想设置密码,传递NULL
 参数.四：nBits
  In/Out：In
  类型：整数型
  可空：N
  意思：RSA长度.1024 2048
 参数.五：nlSerial
  In/Out：In
  类型：长整数型
  可空：N
  意思：密钥序列号
 参数.六：nlTimeSecond
  In/Out：In
  类型：长整数型
  可空：N
  意思：跟证书时间
 参数.七：pSt_X509CerInfo
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：要写入的信息
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_MakeCACert(LPCTSTR lpszCertFile, LPCTSTR lpszKeyFile, LPCTSTR lpszKeyPass, int nBits, long nlSerial, long nlTimeSecond, OPENSSL_X509CCINL *pSt_X509CerInfo);
/********************************************************************
函数名称：OPenSSL_Cert_X509GenRequest
函数功能：根据密钥生成证书请求文件
 参数.一：lpszDstFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入生成成功的文件保存路径
 参数.二：pSt_X509CerInfo
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：输入主题信息
 参数.三：lpszRSAKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入私钥文件路径
 参数.四：lpszKeyPass
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入私钥文件密码,如果没有密码,传递NULL
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_X509GenRequest(LPCTSTR lpszDstFile, OPENSSL_X509CCINL *pSt_X509CerInfo, LPCTSTR lpszRSAKey, LPCTSTR lpszKeyPass = NULL);
/********************************************************************
函数名称：OPenSSL_Cert_X509SignVer
函数功能：对一个证书请求文件进行签名验证
 参数.一：lpszCACert
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：CA根证书目录
 参数.二：lpszCAKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：CA根证书的密钥目录
 参数.三：lpszCAPass
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：证书的私钥密码,如果没有传递NULL
 参数.四：lpszREQFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：生成的请求文件路径
 参数.五：lpszREQPass
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：请求的文件密钥,如果没有传递NULL
 参数.六：lpszDstFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：签发后的证书保存路径
 参数.七：nlSerial
  In/Out：In
  类型：长整数型
  可空：N
  意思：密钥序列号
 参数.八：nlTimeSecond
  In/Out：In
  类型：长整数型
  可空：N
  意思：跟证书时间,单位秒
 参数.九：pStl_ListExt
  In/Out：In
  类型：数据结构指针
  可空：Y
  意思：证书附加信息
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_X509SignVer(LPCTSTR lpszCACert, LPCTSTR lpszCAKey, LPCTSTR lpszCAPass, LPCTSTR lpszREQFile, LPCTSTR lpszREQPass, LPCTSTR lpszDstFile, long nlSerial, long nlTimeSecond, list<OPENSSL_X509EXT> *pStl_ListExt);
/********************************************************************
函数名称：OPenSSL_Cert_X509Verifly
函数功能：验证证书是否正确
 参数.一：lpszCARoot
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：根证书保存路径
 参数.二：lpszCAUser
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：个人证书路径
返回值
  类型：逻辑型
  意思：证书是否验证成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_X509Verifly(LPCTSTR lpszCARoot, LPCTSTR lpszCAUser);
/********************************************************************
函数名称：OPenSSL_Cert_GetCerInfomachine
函数功能：获取个人证书信息
 参数.一：lpszCerFile
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要查询的证书路径
 参数.二：pSt_X509CCInfo
  In/Out：In
  类型：结构体指针
  可空：N
  意思：输出个人证书所查询到的信息
返回值
  类型：逻辑型
  意思：是否查询成功
备注：
*********************************************************************/
extern "C" BOOL OPenSSL_Cert_GetCerInfomachine(LPCSTR lpszCerFile, LPOPENSSL_X509CCINFO pSt_X509CCInfo);
/************************************************************************/
/*                 SSL安全传输认证函数                                  */
/************************************************************************/
/********************************************************************
函数名称：OPenSsl_Server_Init
函数功能：初始化SSL上下文环境
 参数.一：lpszCACert
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：CA签名的证书
 参数.二：lpszServerCert
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：服务端证书
 参数.三：lpszServerKey
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：服务器端私钥
 参数.十：enProtocol
  In/Out：In
  类型：枚举型
  可空：Y
  意思：默认支持SSL_V2和V3版本自动切换。枚举型里面的成员，协议类型
 参数.五：dwCoderType
  In/Out：In
  类型：双字
  可空：Y
  意思：编码类型，默认PEM编码
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：启用这个服务器，你可以使用安全的传输模式，你发送和接受到的数据都是明文，底层我们已经为你做好了加解密工作
*********************************************************************/
extern "C" BOOL OPenSsl_Server_Init(LPCTSTR lpszCACert,LPCTSTR lpszServerCert,LPCTSTR lpszServerKey,ENUM_NETENGINE_OPENSSL_PROTOCOL enProtocol = NETENGINE_OPENSSL_PROTOCOL_SSL_SERVER,DWORD dwCoderType = NETENGINE_OPENSSL_OPENSSL_PEM_FILE);
/********************************************************************
函数名称：OPenSsl_Server_Accept
函数功能：接受一个SSL连接
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：输入你的ACCEPT返回的套接字句柄
 参数.二：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入你的客户端地址的信息IP:PORT
 参数.三：ptszSslSubJect
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出客户端拥有的证书信息
 参数.四：ptszSslIssuer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出客户端发布的整数型
 参数.五：ptszSslAlgorithm
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出客户端拥有的加密算法
返回值
  类型：逻辑型
  意思：是否成功
备注：如果后面三个参数没有导出值,说明客户端没有使用证书连接
*********************************************************************/
extern "C" BOOL OPenSsl_Server_Accept(SOCKET hSocket, LPCSTR lpszClientAddr, CHAR *ptszSslSubJect, CHAR *ptszSslIssuer, CHAR *ptszSslAlgorithm);
/********************************************************************
函数名称：OPenSsl_Server_Recv
函数功能：接受一条SSL数据
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要操作的客户端地址
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出接受到的数据缓冲区
 参数.三：pInt_MsgLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小,输出接收到的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：可读事件自己处理,当有可读事件的时候调用此函数来接受数据
*********************************************************************/
extern "C" BOOL OPenSsl_Server_Recv(LPCSTR lpszClientAddr, CHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：OPenSsl_Server_SendMsg
函数功能：发送安全数据
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送给谁
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送的消息
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：发送消息大小
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL OPenSsl_Server_SendMsg(LPCTSTR lpszClientAddr,LPCTSTR lpszMsgBuffer,int nLen);
/************************************************************************
函数名称：OPenSsl_Server_Stop
函数功能：关闭SSL服务器
返回值
  类型：逻辑型
  意思：是否成功关闭并且销毁SSL服务数据
备注：
************************************************************************/
extern "C" BOOL OPenSsl_Server_Stop();
/************************************************************************
函数名称：OPenSsl_Server_CloseClient
函数功能：主动关闭一个客户
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：客户端地址
返回值
  类型：逻辑型
  意思：是否成功释放相关资源
备注：回调的模式会自动关闭客户端并且释放资源
************************************************************************/
extern "C" BOOL OPenSsl_Server_CloseClient(LPCTSTR lpszClientAddr);
