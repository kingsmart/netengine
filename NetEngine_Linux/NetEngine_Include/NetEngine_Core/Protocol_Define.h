#pragma once
/********************************************************************
//	Created:	2012/1/8  12:15
//	File Name: 	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngine_Protocol\Protocol_Define.h
//	File Path:	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngine_Protocol
//	File Base:	Protocol_Define
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	协议模块导出函数定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                              回调函数定义
//////////////////////////////////////////////////////////////////////////
//ICMP PING函数接受回调
typedef void(*CALLBACK_PROTOCOL_ICMP_PING)(LPCTSTR lpszAddr,int nByteLen,int nTime,int nTTL,LPVOID lParam);
//ICMP 路由过程函数回调
typedef void(*CALLBACK_PROTOCOL_ICMP_ROUTERTRACER)(int nCurrentRouter,LPCTSTR lpszRouter,int nTime,LPVOID lParam);
//////////////////////////////////////////////////////////////////////////
//                               导出函数定义
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD Protocol_GetLastError(int *pInt_SysError = NULL);
/************************************************************************/
/*                     ICMP协议导出函数                                 */
/************************************************************************/
/********************************************************************
函数名称：Protocol_Icmp_Ping
函数功能：PING协议实现
 参数.一：lpszPindAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要PING的IP地址
 参数.二：fpCall_ProtocolIcmp_Ping
  In/Out：Out
  类型：函数指针
  可空：N
  意思：PING后的数据回调
 参数.三：lParam
  In/Out：In
  类型：无类型指针
  可空：Y
  意思：回调函数参数
返回值
  类型：逻辑型
  意思：是否成功执行PING
备注：
*********************************************************************/
extern "C" BOOL Protocol_Icmp_Ping(LPCTSTR lpszPindAddr,CALLBACK_PROTOCOL_ICMP_PING fpCall_ProtocolIcmp_Ping,LPVOID lParam);
/********************************************************************
函数名称：Protocol_Icmp_RouterTracer
函数功能：路由路径查询协议
 参数.一：lpszDestAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取的IP地址路由
 参数.二：fpCall_IcmpTracer
  In/Out：Out
  类型：回调函数
  可空：N
  意思：路由过程回调
 参数.三：lParam
  In/Out：In
  类型：无类型指针
  可空：Y
  意思：回调函数参数
返回值
  类型：逻辑型
  意思：时候成功执行路由获取
备注：
*********************************************************************/
extern "C" BOOL Protocol_Icmp_RouterTracer(LPCTSTR lpszDestAddr,CALLBACK_PROTOCOL_ICMP_ROUTERTRACER fpCall_IcmpTracer,LPVOID lParam);
/************************************************************************/
/*                     SOCKS5协议导出函数                               */
/************************************************************************/
/********************************************************************
函数名称：RfcProtocol_Socks_RequestConnect
函数功能：创建一个连接请求协议包
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出创建好的包结构体
 参数.二：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出包大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcProtocol_Socks_RequestConnect(BYTE *ptszMsgBuffer, int *pInt_Len);
/********************************************************************
函数名称：RfcProtocol_Socks_RequestLogin
函数功能：创建一个连接用户名密码登录的包
 参数.一：lpszUserName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：用户名
 参数.二：lpszUserPass
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：密码
 参数.三：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出创建好的包结构体
 参数.四：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出包大小
返回值
  类型：逻辑型
  意思：是否成功
备注：如果服务器确认是0x01 用户名密码验证方式,那么你需要先投递这个包才可以继续下一步,否则可以跳过这步
*********************************************************************/
extern "C" BOOL RfcProtocol_Socks_RequestLogin(LPCSTR lpszUserName, LPCSTR lpszUserPass, BYTE *ptszMsgBuffer, int *pInt_Len);
/********************************************************************
函数名称：RfcProtocol_Socks_RequestInfo
函数功能：创建一个最后请求建立代理信息包
 参数.一：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：IP地址或者域名,为了服务器处理速度,建议将域名转为IP地址
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要访问的服务提供的端口
 参数.三：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出创建好的包结构体
 参数.四：pInt_Len
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出包大小
 参数.五：byRequestCmd
  In/Out：In
  类型：字符
  可空：Y
  意思：0x01 = 连接代理 0x02 = BIND代理 0x03 = UDP关联代理,默认0x01即可
 参数.六：bIsAddr
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否输入的是IP地址，否则参数1输入的是域名
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL RfcProtocol_Socks_RequestInfo(LPCSTR lpszAddr, u_short nPort, BYTE *ptszMsgBuffer, int *pInt_Len, BYTE byRequestCmd = 0x01,BOOL bIsAddr = TRUE);
/********************************************************************
函数名称：RfcProtocol_Socks_HandleConnect
函数功能：处理连接请求返回的数据
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：代理服务器返回的数据
 参数.二：pInt_AuthMothed
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出服务器返回需要的验证为哪个方法0x00匿名 0x02需要用户密码
返回值
  类型：逻辑型
  意思：是否成功
备注：如果服务器要求匿名,那么直接调用RfcProtocol_Socks_RequestInfo即可,而不需要登录
*********************************************************************/
extern "C" BOOL RfcProtocol_Socks_HandleConnect(LPCSTR lpszMsgBuffer, int *pInt_AuthMothed);
/********************************************************************
函数名称：RfcProtocol_Socks_HandleLogin
函数功能：处理用户密码登录后返回的协议数据
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：代理服务器返回的数据
返回值
  类型：逻辑型
  意思：是否成功
备注：登录成功返回真,登录失败返回假
*********************************************************************/
extern "C" BOOL RfcProtocol_Socks_HandleLogin(LPCSTR lpszMsgBuffer);
/********************************************************************
函数名称：RfcProtocol_Socks_HandleLogin
函数功能：处理用户密码登录后返回的协议数据
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：代理服务器返回的数据
 参数.二：pInt_ErrorCode
  In/Out：Out
  类型：整数型指针
  可空：Y
  意思：失败,服务器返回错误码:0x01普通SOCKS服务器连接失败 0x02现有规则不允许连接 0x03网络不可达 0x04主机不可达 0x05连接被拒 0x06 TTL超时 0x07不支持的命令 0x08不支持的地址类型 0x09 - 0xFF未定义
返回值
  类型：逻辑型
  意思：是否成功
备注：成功参数2不起作用,执行完这一步骤,可以直接转发数据了
*********************************************************************/
extern "C" BOOL RfcProtocol_Socks_HandleInfo(LPCSTR lpszMsgBuffer, int *pInt_ErrorCode = NULL);
