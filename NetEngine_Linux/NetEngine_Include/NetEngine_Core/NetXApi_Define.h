#pragma once
/********************************************************************
//	Created:	2011/10/15   14:22
//	Filename: 	NetSocketEngine/NetSocketHelp/NetGetInfo/NetGetInfo_Define.h
//	File Path:	NetSocketEngine/NetSocketHelp/NetGetInfo/
//	File Base:	NetGetInfo
//	File Ext:	h
//      Project:        血与荣誉网络通信引擎 For Linux
//	Author:		dowflyon
//	Purpose:	获取网络信息导出函数定义
//	History:
*********************************************************************/
//nETHProtoType:内部解析器支持的协议，不支持的解析器将由用户自己解析
#define NETENGINE_NETXAPI_SNIFFER_PROTOCOL_TYPE_IPV4 0x0800
#define NETENGINE_NETXAPI_SNIFFER_PROTOCOL_TYPE_ARP 0x0806
//////////////////////////////////////////////////////////////////////////
//                         导出的数据结构
//////////////////////////////////////////////////////////////////////////
//网卡信息结构
typedef struct tag_NetXApi_Sniffer_If
{
    TCHAR tszIFName[MAX_PATH];                                            //名称
    TCHAR tszIFDes[MAX_PATH];                                             //描述
}NETXAPI_SNIFFERIF,*LPNETXAPI_SNIFFERIF;
//抓包信息结构
typedef struct tag_NetXApi_ProtocolInfo
{
    TCHAR tszSourceMac[32];                                               //源MAC地址
    TCHAR tszDestMac[32];                                                 //目标MAC地址
    TCHAR tszSourceAddr[32];                                              //源地址
    TCHAR tszDestAddr[32];                                                //目标地址,IP协议有效
    int nSourcePort;                                                      //源端口
    int nDestPort;                                                        //目的端口

    int nPktLen;                                                          //消息总大小
    int nCapLen;                                                          //抓到的包大小
    int nMsgLen;                                                          //数据大小，如果有的话

    int nETHProtoType;                                                    //上层协议.ARP RARP IP
    int nIPProtoType;                                                     //IP或者ARP操作协议类型
    UCHAR uFlags;                                                         //最终操作标记,TCP(FIN,PST等)
}NETXAPI_PROTOCOLINFO, *LPNETXAPI_PROTOCOLINFO;
//获取网卡流量信息
typedef struct tag_NetInfo_Flow_State
{
    TCHAR tszDevName[64];                    //设备名称

    struct
    {
        ULONGLONG ullBytes;                  //接受的流量
        ULONGLONG ullPackets;                //接受包个数
        ULONGLONG ullError;                  //错误的包
        ULONGLONG ullDrop;                   //被丢弃的包
        ULONGLONG ullFifo;                   //出入队列
        ULONGLONG ullFrame;                  //帧
        ULONGLONG ullCompress;               //压缩的包
        ULONGLONG ullMultiCast;              //多播包
    }st_RecvPackets;
    struct
    {
        ULONGLONG ullBytes;                  //接受的流量
        ULONGLONG ullPackets;                //接受包个数
        ULONGLONG ullError;                  //错误的包
        ULONGLONG ullDrop;                   //被丢弃的包
        ULONGLONG ullFifo;                   //出入队列
        ULONGLONG ullFrame;                  //帧
        ULONGLONG ullCompress;               //压缩的包
        ULONGLONG ullMultiCast;              //多播包
    }st_SendPackets;
}NETXAPI_FLOWSTATE,*LPNETXAPI_FLOWSTATE;
//////////////////////////////////////////////////////////////////////////
//                        导出回调
//////////////////////////////////////////////////////////////////////////
//声明回调函数，参数:抓抱器句柄，导出的协议信息，抓到的原始数据（你可以自己解析协议，包是以太网帧头）,自定义参数
typedef void(CALLBACK *CALLBACK_NETXAPI_SNIFFER_DATAPACKET)(XNETHANDLE xhNet,NETXAPI_PROTOCOLINFO *pSt_ProtoInfo,LPCTSTR lpszMsgBuffer,LPVOID lParam);
//网络枚举回调函数,源IP地址,源MAC地址
typedef void(CALLBACK *CALLBACK_NETXAPI_LANENUM_RESLIST)(XNETHANDLE xhNet, LPCTSTR lpszIPAddr, LPCTSTR lpszMacAddr, LPVOID lParam);
//////////////////////////////////////////////////////////////////////
//                        导出函数
//////////////////////////////////////////////////////////////////////
/************************************************************************
函数名称：NetXApi_GetLastError
函数功能：获取最后发生的错误
返回值
  类型：双字
  意思：错误码
备注：
************************************************************************/
extern "C" DWORD NetXApi_GetLastError(int *pInt_ErrorCode = NULL);
/************************************************************************/
/*                     地址查询函数导出                                    */
/************************************************************************/
/************************************************************************
函数名称：NetXApi_Address_OpenQQWry
函数功能：打开QQIP数据库
  参数一：lpszFileName
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：QQWry数据库位置
返回值
  类型：逻辑型
  意思：是否成功打开
备注：
************************************************************************/
extern "C" BOOL NetXApi_Address_OpenQQWry(LPCTSTR lpszFileName);
/************************************************************************
函数名称：NetXApi_Address_CloseQQWry
函数功能：关闭IP查询文件
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：
************************************************************************/
extern "C" BOOL NetXApi_Address_CloseQQWry();
/************************************************************************
函数名称：NetXApi_Address_IPtoAddr
函数功能：获取IP对应的国家和地址
  参数一：lpszIpAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：指定文件偏移量
  参数二：pszAddrCountry
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出国家信息
  参数三：pszAddrLocation
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出地址信息
返回值
  类型：逻辑型
  意思：是否成功获取到IP对应的地址信息
备注：
************************************************************************/
extern "C" BOOL NetXApi_Address_IPtoAddr(LPCTSTR lpszIpAddr,TCHAR *pszAddrCountry,TCHAR *pszAddrLocation);
//同上功能
extern "C" BOOL NetXApi_Address_IPtoAddr2(DWORD dwAddr,TCHAR *ptszAddrCountry,TCHAR *ptszAddrLocation);
/************************************************************************/
/*                     流量获取函数导出                                    */
/************************************************************************/
/************************************************************************
函数名称：NetXApi_NetFlow_GetAll
函数功能：获取网络流量信息
  参数.一：pSt_FlowState
   In/Out：Out
   类型：数据结构指针
   可空：N
   意思：网络流量信息结构体
  参数.二：lpszEth_Name
   In/Out：In
   类型：常量字符指针
   可空：Y
   意思：要获取的网卡名称，比如：eth0
  参数.三：nNumEntries
   In/Out：In
   类型：整数型
   可空：Y
   意思：要获取的网卡编号
返回值
  类型：逻辑型
  意思：是否成功获取
备注：最后两个参数不能同时为空,每秒获取一次,这一次减去上一次的流量就得到当前每秒流量
************************************************************************/
extern "C" BOOL NetXApi_NetFlow_GetAll(NETXAPI_FLOWSTATE *pSt_FlowState,LPCTSTR lpszDevName = NULL,int nNumEntries = 0);
/************************************************************************/
/*                     网络流量控制导出函数                             */
/************************************************************************/
/********************************************************************
函数名称：NetXApi_CtrlFlow_Init
函数功能：初始化流量控制程序
 参数.一：pxhNet
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出初始化成功的句柄
 参数.二：lpszDevName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入你的网卡名称
 参数.三：bIsClear
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否清理老旧的流量控制服务设定
返回值
  类型：逻辑型
  意思：是否成功
备注：此函数需要管理员权限
*********************************************************************/
extern "C" BOOL NetXApi_CtrlFlow_Init(XNETHANDLE *pxhNet,LPCTSTR lpszDevName,BOOL bIsClear = TRUE);
/********************************************************************
函数名称：NetXApi_CtrlFlow_AddFlow
函数功能：为一条连接添加一个流量控制程序
 参数.一：xhNet
  In/Out：n
  类型：网络句柄
  可空：N
  意思：输入要操作的流量控制程序
 参数.二：pxhNet
  In/Out：Out
  类型：网络句柄
  可空：N
  意思：导出添加的过滤器句柄
 参数.三：nLimitByte
  In/Out：In
  类型：整数型
  可空：N
  意思：设置要限制的速度，每秒Byte,不能超过你的网卡负载大小
 参数.四：nRecvByte
  In/Out：In
  类型：整数型
  可空：N
  意思：Linux版本此参数无效！暂时无效
 参数.五：nDstPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：设置要控制通道的目标端口
 参数.六：nSrcPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：设置要控制流量通道的源端口,可以为空,只用目标端口控制某一条通道流量信息
返回值
  类型：逻辑型
  意思：是否成功
备注：参数5和6不能同时为0
*********************************************************************/
extern "C" BOOL NetXApi_CtrlFlow_AddFlow(XNETHANDLE xhNet,XNETHANDLE *pxhFilter,int nLimitByte, int nRecvByte, int nDstPort = 0, int nSrcPort = 0);
/********************************************************************
函数名称：NetXApi_CtrlFlow_DelFlow
函数功能：删除一条流量控制信息
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要操作的流量控制句柄
 参数.二：xhFilter
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要删除的过滤器句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_CtrlFlow_DelFlow(XNETHANDLE xhNet,XNETHANDLE xhFilter);
/********************************************************************
函数名称：NetXApi_CtrlFlow_Destory
函数功能：销毁流量控制程序
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要销毁的流量控制句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_CtrlFlow_Destory(XNETHANDLE xhNet);
/************************************************************************/
/*                     网络嗅探器函数导出                                  */
/************************************************************************/
/********************************************************************
函数名称：NetXApi_Sniffer_Start
函数功能：启动一个网络抓包器
 参数.一：pxhNet
  In/Out：Out
  类型：网络句柄
  可空：N
  意思：导出网络监听句柄
 参数.二：lpszDevName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要监听哪个网卡
 参数.三：fpCall_NetXSniffer
  In/Out：Out
  类型：回调函数
  可空：N
  意思：设置回调函数地址
 参数.四：lParam
  In/Out：In
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_Sniffer_Start(XNETHANDLE *pxhNet, LPCTSTR lpszDevName, CALLBACK_NETXAPI_SNIFFER_DATAPACKET fpCall_NetXSniffer, LPVOID lParam = NULL);
/********************************************************************
函数名称：NetXApi_Sniffer_Stop
函数功能：停止网络嗅探器
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：要释放的嗅探器句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_Sniffer_Stop(XNETHANDLE xhNet);
/********************************************************************
函数名称：NetXApi_Sniffer_Filter
函数功能：设置网络嗅探过滤器
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入嗅探器句柄
 参数.二：lpszFilter
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入过滤器字符串,参考TCPDUMP格式
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_Sniffer_Filter(XNETHANDLE xhNet, LPCTSTR lpszFilter);
/********************************************************************
函数名称：NetXApi_Sniffer_WriteDump
函数功能：抓包的数据保存为文件
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入嗅探器句柄
 参数.二：lpszFilter
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要保存的路径
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_Sniffer_WriteDump(XNETHANDLE xhNet, LPCTSTR lpszFileName);
/********************************************************************
函数名称：NetXApi_Sniffer_GetIFAll
函数功能：获取网络嗅探器允许的设备列表名称
 参数.一：pStl_IFSniffer
  In/Out：Out
  类型：容器指针
  可空：N
  意思：导出获取到的网卡设备列表名和描述
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_Sniffer_GetIFAll(list<NETXAPI_SNIFFERIF> *pStl_IFSniffer);
/********************************************************************
函数名称：NetXApi_Sniffer_HexFormat
函数功能：格式化字符串函数
 参数.一：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要转换的缓冲区
 参数.二：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要转换缓冲区大小
 参数.三：ptszLineBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出行数缓冲区（使用\r\n来分割，如果你需要的话）
 参数.四：ptszHexBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出转换后的二进制格式
 参数.五：ptszStrBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出转换后的单个字符串
 参数.六：nLine
  In/Out：In
  类型：整数型
  可空：Y
  意思：输入缓冲区输出每行允许的字节数
返回值
  类型：逻辑型
  意思：是否转换成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_Sniffer_StrFormat(LPCTSTR lpszMsgBuffer, int nLen, TCHAR *ptszLineBuffer, TCHAR *ptszHexBuffer, TCHAR *ptszStrBuffer, int nLine);
//////////////////////////////////////////////////////////////////////////
//                                枚举局域网IP和MAC地址
//////////////////////////////////////////////////////////////////////////
/********************************************************************
函数名称：NetXApi_LANEnum_Start
函数功能：启动一个局域网扫描服务
 参数.一：pxhNet
  In/Out：Out
  类型：网络句柄
  可空：N
  意思：导出启动成功的局域网扫描服务
 参数.二：lpszDevName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：网卡名称
 参数.三：lpszSrcAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发起者的IP地址
 参数.四：lpszSrcMac
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发起者的网卡MAC地址
 参数.五：lpszStartAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要扫描的IP起始地址
 参数.六：lpszEndAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要扫描的IP结束地址
 参数.七：fpCall_ResList
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：枚举资源函数地址
 参数.八：lParam
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_LANEnum_Start(XNETHANDLE *pxhNet, LPCTSTR lpszDevName, LPCTSTR lpszSrcAddr, LPCTSTR lpszSrcMac, LPCTSTR lpszStartAddr, LPCTSTR lpszEndAddr, CALLBACK_NETXAPI_LANENUM_RESLIST fpCall_ResList, LPVOID lParam = NULL);
/********************************************************************
函数名称：NetXApi_LANEnum_Close
函数功能：关闭一个枚举资源
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：输入要操作的枚举资源句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetXApi_LANEnum_Close(XNETHANDLE xhNet);
