﻿#pragma once
/********************************************************************
//	Created:	2011/10/10   15:14
//	Filename: 	NetSocketEngine/NETCORE/Net/Engine_Core/NetCore_Error.h
//	File Path:	NetSocketEngine/NETCORE/Engine_Core/
//	File Base:	NETCORE_Core
//	File Ext:	h
//  Project:    血与荣誉网络通信引擎 For Linux
//	Author:		dowflyon
//	Purpose:	网络通信服务器引擎核心错误导出
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                         高速缓存导出错误
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                          文件缓存池错误                                */
/************************************************************************/
#define ERROR_NETCORE_CACHE_FILE_CREATE_PARAMENT 0x10A0000               //参数错误，无法继续
#define ERROR_NETCORE_CACHE_FILE_CREATE_CREATED 0x10A0001                //已经创建过了，除非你关闭，不然无法继续！
#define ERROR_NETCORE_CACHE_FILE_CREATE_ISFAILED 0x10A0002               //创建文件失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_SEEK 0x10A0003                   //移动文件指针失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_WRITE 0x10A0004                  //占用文件大小失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_MAP 0x10A0005                    //映射文件到内存失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_THREAD 0x10A0006                 //创建线程失败
#define ERROR_NETCORE_CACHE_FILE_CREATE_CALLBACK 0x10A0007               //设置回调函数失败
#define ERROR_NETCORE_CACHE_FILE_WRITE_PARAMENT 0x10A0000                //参数错误，请仔细检查
#define ERROR_NETCORE_CACHE_FILE_WRITE_NOTCREATE 0x10A0011               //写入失败，没有创建
#define ERROR_NETCORE_CACHE_FILE_READ_PARAMENT 0x10A0000                 //参数错误，无法绩溪
#define ERROR_NETCORE_CACHE_FILE_READ_SIZESMALL 0x10A0010                //你输入的缓冲区太小了，无法容纳
#define ERROR_NETCORE_CACHE_FILE_READ_NOTCREATE 0x10A0011                //没有创建。
#define ERROR_NETCORE_CACHE_FILE_FLUSH_NOTCREATE 0x10A0021               //没有创建。
#define ERROR_NETCORE_CACHE_FILE_FLUSH_ISFAILED 0x10A0022                //刷新失败，内部错误
#define ERROR_NETCORE_CACHE_FILE_FLUSH_CHANGESIZE 0x10A0023              //刷新失败，改变文件大小失败
#define ERROR_NETCORE_CACHE_FILE_CLOSE_NOTCREATE 0x10A0040               //没有创建，无法关闭
#define ERROR_NETCORE_CACHE_FILE_CLOSE_UNMAP 0x10A0041                   //关闭内存失败
#define ERROR_NETCORE_CACHE_FILE_CLOSE_FILE 0x10A0042                    //关闭文件失败
#define ERROR_NETCORE_CACHE_FILE_CLOSE_THREAD 0x10A0043                  //关闭线程失败
#define ERROR_NETCORE_CACHE_FILE_RESIZE_NOTCREATE 0x10A0050              //重置失败，没有创建缓存文件
#define ERROR_NETCORE_CACHE_FILE_RESIZE_RESIZE 0x10A0051                 //重置文件大小失败，系统错误
/************************************************************************/
/*                          文件缓存池扩展错误                             */
/************************************************************************/
#define ERROR_NETCORE_CAHCE_EX_FILE_CREATE_MALLOC 0x10A0101              //申请内存失败
#define ERROR_NETCORE_CAHCE_EX_FILE_WRITE_FIND 0x10A0110                 //写失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_READ_FIND 0x10A0120                  //读失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_FLUSH_FIND 0x10A0130                 //刷新失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_CLEAR_FIND 0x10A0140                 //清理失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_CLOSE_FIND 0x10A0150                 //关闭失败，没有找到
#define ERROR_NETCORE_CAHCE_EX_FILE_RESIZE_FIND 0x10A0160                //没有找到，重置失败
/************************************************************************/
/*                          数据缓存池错误                                */
/************************************************************************/
#define ERROR_NETCORE_CACHE_DATA_PUSHSEND_PARAMENT 0x10A0201             //参数错误
#define ERROR_NETCORE_CACHE_DATA_PUSHSEND_RESIZE 0x10A0202               //改变文件大小失败
#define ERROR_NETCORE_CACHE_DATA_PUSHRECV_PARAMENT 0x10A0210             //参数错误
#define ERROR_NETCORE_CACHE_DATA_PUSHRECV_RESIZE 0x10A0211               //改变文件大小失败
#define ERROR_NETCORE_CACHE_DATA_GETSEND_PARAMENT 0x10A0220              //参数错误
#define ERROR_NETCORE_CACHE_DATA_GETSEND_NOTFOUND 0x10A0221              //获取失败，没有找到唯一标识符
#define ERROR_NETCORE_CACHE_DATA_GETSEND_NOTENABLE 0x10A0222             //没有找到可用的数据回溯包
#define ERROR_NETCORE_CACHE_DATA_GETSEND_LEN 0x10A0223                   //长度错误，不够
#define ERROR_NETCORE_CACHE_DATA_GETRECV_PARAMENT 0x10A0230              //参数错误
#define ERROR_NETCORE_CACHE_DATA_GETRECV_NOTFOUND 0x10A0231              //获取失败，没有找到唯一标识符
#define ERROR_NETCORE_CACHE_DATA_GETRECV_NOTENABLE 0x10A0232             //没有找到可用的数据回溯包
#define ERROR_NETCORE_CACHE_DATA_GETRECV_LEN 0x10A0233                   //长度错误，不够
#define ERROR_NETCORE_CACHE_DATA_CLOSE_PARAMENT 0x10A0240                //参数错误
#define ERROR_NETCORE_CACHE_DATA_CREATE_SNDFILE 0x10A0250                //创建发送缓存回溯文件失败
#define ERROR_NETCORE_CACHE_DATA_CREATE_RCVFILE 0x10A0251                //创建接受缓存回溯文件失败
/************************************************************************/
/*                          数据缓存池扩展错误                             */
/************************************************************************/
#define ERROR_NETCORE_CAHCE_EX_DATA_NOTFOUND 0x10A0300                   //没有找到指定句柄
#define ERROR_NETCORE_CAHCE_EX_DATA_CREATE_MALLOC 0x10A0310              //申请内存失败
//////////////////////////////////////////////////////////////////////////
//                     进程通信机制错误导出表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_PIPCOMMUNICATION_CMD_OPEN 0x10A1001                //打开命令行失败
/************************************************************************/
/*                     匿名管道                                          */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_PARAMENT 0x10A1003      //创建匿名管道检查到参数错误
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_CREATEPIPE 0x10A1004    //创建匿名管道失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_NOTEXIST 0x10A1009      //不存在，不需要删除
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_FIND_NOTEXIST 0x10A1006 //匿名管道不存在，没有找到
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_READ 0x10A1007          //读取数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_WRITE 0x10A1008         //写入数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_ANONYMOUS_WRITESIZE 0x10A1009     //写入数据大小不一同
/************************************************************************/
/*                     命名管道                                          */
/************************************************************************/
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_ISEXIST 0x10A1101           //命名管道已经存在，无法创建
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATEFILE 0x10A1102        //创建文件失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATEPIPE 0x10A1103        //创建命名管道失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CREATETHREAD 0x10A1105      //创建线程失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CALLPARAMENT 0x10A1108      //设置回调函数的时候参数为空
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_NOTFOUND 0x10A1106          //没有找到指定的命名管道
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_TERMINATETHREAD 0x10A1107   //结束线程失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_CALLPARAMENT 0x10A1108      //设置回调函数的时候参数为空
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_NOTSUPPEDREAD 0x10A1109     //不支持此读取方法，因为已经设置为回调方式
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_READ 0x10A110A              //读取数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_WRITE 0x10A110B             //写入数据失败
#define ERROR_NETCORE_PIPCOMMUNICATION_NAMED_WRITELEN 0x10A110C          //写入数据的实际大小与传输的大小不一致，失败
/************************************************************************/
/*                     内存映射错误表                                     */
/************************************************************************/
//IPC版本
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_CREATEMAP 0x10A1202     //创建内存映射失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_MEMORYSIZE 0x10A1203    //MAP映射文件大小内存错误，内存大小不能被页面大小整除。
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_GETMEMADDR 0x10A1204    //获取我们操作的内存地址失败！
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_DETACH 0x10A1205        //脱离内存块失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_FREE 0x10A1206          //释放内存块失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_NOTFOUNDFORCLOSE 0x10A1207 //没有找到需要关闭的内存块
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_PARAMENT 0x10A1208      //参数错误，不能为NULL
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_NOTFOUNDFORGET 0x10A1209   //获取失败，没有找到指定KEY
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYIPC_GETFAILED 0x10A120A     //获取失败，系统错误
//文件内存映射
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_OPENFILE 0x10A1301      //打开文件失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_GETFILESTAT 0x10A1302   //获取文件状态失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_CREATEMAP 0x10A1303     //创建内存映射文件失败
#define ERROR_NETCORE_PIPCOMMUNICATION_MEMORYMAP_UNMAP 0x10A1304         //卸载文件内存映射失败
//////////////////////////////////////////////////////////////////////////
//                         原始套接字错误表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_RAWSOCKET_IPHDR_PARAMENT 0x10A2000                 //参数错误
#define ERROR_NETCORE_RAWSOCKET_IPHDR_NOTSUPPORT 0x10A2001               //不支持的协议类型
#define ERROR_NETCORE_RAWSOCKET_TCPHDR_PARAMENT 0x10A2010                //参数错误
#define ERROR_NETCORE_RAWSOCKET_UDPHDR_PARAMENT 0x10A2020
#define ERROR_NETCORE_RAWSOCKET_ICMPHDR_PARAMENT 0x10A2030
//TCP
#define ERROR_NETCORE_RAWSOCKET_TCP_INIT_PARAMENT 0x10A2100              //参数错误
#define ERROR_NETCORE_RAWSOCKET_TCP_INIT_SOCKET 0x10A2101                //获取套接字失败
#define ERROR_NETCORE_RAWSOCKET_TCP_INIT_SET 0x10A2102                   //设置套机字失败
#define ERROR_NETCORE_RAWSOCKET_TCP_SET_PARAMENT 0x10A2110               //参数错误
//UDP
#define ERROR_NETCORE_RAWSOCKET_UDP_INIT_PARAMENT 0x10A2200              //参数错误
#define ERROR_NETCORE_RAWSOCKET_UDP_INIT_SOCKET 0x10A2201                //初始化套接字失败
#define ERROR_NETCORE_RAWSOCKET_UDP_INIT_SET 0x10A2202                   //设置套接字属性失败
#define ERROR_NETCORE_RAWSOCKET_UDP_SET_PARAMENT 0x10A2210               //参数错误
//ICMP
#define ERROR_NETCORE_RAWSOCKET_ICMP_INIT_PARAMENT 0x10A2300             //参数错误
#define ERROR_NETCORE_RAWSOCKET_ICMP_INIT_SOCKET 0x10A2301               //初始化套接字失败
#define ERROR_NETCORE_RAWSOCKET_ICMP_INIT_SET 0x10A2302                  //设置套接字属性失败
#define ERROR_NETCORE_RAWSOCKET_ICMP_SET_PARAMENT 0x10A2310              //参数错误
//ARP
#define ERROR_NETCORE_RAWSOCKET_ARP_INIT_PARAMENT 0x10A2400              //参数错误
#define ERROR_NETCORE_RAWSOCKET_ARP_INIT_SOCKET 0x10A2401                //设置套接字失败
#define ERROR_NETCORE_RAWSOCKET_ARP_SET_PARAMENT 0x10A2410               //设置属性失败，参数错误
//////////////////////////////////////////////////////////////////////////
//                     串口错误导出类型定义
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_SERIALPORT_FORMATSTR_PARAMISNOTNUMBER 0x10A3001    //参数不是数字
#define ERROR_NETCORE_SERIALPORT_OPENDEV_ISOPENNED 0x10A3002             //串口已经打开了！
#define ERROR_NETCORE_SERIALPORT_OPENDEV_OPENTOFAILED 0x10A3003          //打开串口失败
#define ERROR_NETCORE_SERIALPORT_OPENDEV_CREATETHREAD 0x10A3004          //创建线程失败
#define ERROR_NETCORE_SERIALPORT_SENDDATA_NOTFINDTHEPORT 0x10A3005       //没有找到指定的串口
#define ERROR_NETCORE_SERIALPORT_SELECT_SELECTISFAILED 0x10A3006         //多路IO函数出错
#define ERROR_NETCORE_SERIALPORT_SELECT_NODEVISREADLY 0x10A3007          //没有任何设备准备好
#define ERROR_NETCORE_SERIALPORT_SELECT_FALGISNOTSET 0x10A3008           //标志位没有被设置
#define ERROR_NETCORE_SERIALPORT_SENDDATA_HDEVISFAILED 0x10A3009         //设备标志错误
#define ERROR_NETCORE_SERIALPORT_SENDDATA_DATALENGTH 0x10A300A           //数据长度不合适
#define ERROR_NETCORE_SERIALPORT_SENDDATA_WRITEFAILED 0x10A300B          //写入数据失败
#define ERROR_NETCORE_SERIALPORT_SENDDATA_ASYNCWRITE 0x10A301B           //异步操作失败，无法写入
#define ERROR_NETCORE_SERIALPORT_SENDDATA_WRITESIZE 0x10A302B            //写入的大小与指定大小不同
#define ERROR_NETCORE_SERIALPORT_CLOSE_WAITFORTHREADEXIT 0x10A300C       //等待线程退出失败
#define ERROR_NETCORE_SERIALPORT_CLOSE_UNKNOW 0x10A300D                  //无法识别的错误
#define ERROR_NETCORE_SERIALPORT_READDATA_NOTFOUND 0x10A300E             //没有找到可以读取的串口
#define ERROR_NETCORE_SERIALPORT_READDATA_NOTSUPPORT 0x10A300F           //不支持此方式读取数据，请通过回调方式读取
#define ERROR_NETCORE_SERIALPORT_READDATA_NOTREADY 0x10A301F             //设备没有准备好，有可能是没有数据可读
#define ERROR_NETCORE_SERIALPORT_READDATA_READ 0x10A302F                 //读取失败
//属性设置
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_SETSPEED 0x10A3010            //设置速率失败
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_GETDEFAULTSET 0x10A3011       //获取默认设置失败
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_FLUSHSP 0x10A3012             //刷新缓冲区失败
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_NOTSUPPORTOFDATASIZE 0x10A3013//不支持的数据位数
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_NOTSUPPORTOFPARITY 0x10A3014  //不支持的校验位方法
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_NOTSUPPORTOFSTOPBIT 0x10A3015 //不支持的停止位数
#define ERROR_NETCORE_SERIALPORT_ATTRIBUTE_WRITEATTFAILED 0x10A3016      //写入设置到串口属性失败
//////////////////////////////////////////////////////////////////////////
//                         网络核心套接字错误表
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_PARAMENT 0x10A4501            //参数错误
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_ISFAILED 0x10A4502            //创建失败，内部错误
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_SIGEMPTY 0x10A4503            //清空信号失败
#define ERROR_NETCORE_SOCKET_SOCKET_CREATE_SIGACTION 0x10A4504           //设置信号失败
#define ERROR_NETCORE_SOCKET_SOCKET_BIND_LISTEN 0x10A4510                //监听端口失败
#define ERROR_NETCORE_SOCKET_SOCKET_BIND_BIND 0x10A4511                  //绑定端口失败
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_ISFULL 0x10A4521                //发送缓冲区已满，需要等待
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_ISFAILED 0x10A4522              //发送失败，内部错误
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_LEN 0x10A4523                   //发送的数据长度与实际的长度不合适
#define ERROR_NETCORE_SOCKET_SOCKET_SEND_ISCLOSED 0x10A4525              //发送的套接字被对方强制关闭
#define ERROR_NETCORE_SOCKET_SOCKET_RECV_RETRY 0x10A4531                 //接受失败，需要重试
#define ERROR_NETCORE_SOCKET_SOCKET_RECV_ISFAILED 0x10A4532              //接受失败，内部错误
#define ERROR_NETCORE_SOCKET_SOCKET_RECV_CLIENTCLOSE 0x10A4533           //客户端已经关闭。
#define ERROR_NETCORE_SOCKET_SOCKET_SETNONEBLOC_GETFL 0x10A4540          //获取状态失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETNONEBLOC_SETNONFAILED 0x10A4541   //设置非阻塞失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETNONEBLOC_BLOCKING 0x10A4542       //设置阻塞失败
#define ERROR_NETCORE_SOCKET_SOCKET_READDR_SETFAILED 0x10A4560           //设置重用失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETTIMEOUT_SEND 0x10A4570            //设置发送超时时间失败
#define ERROR_NETCORE_SOCKET_SOCKET_SETTIMEOUT_RECV 0x10A4571            //设置接受超时时间失败
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_OPEN 0x10A4580             //打开保活计时器失败
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_SETIDLETIME 0x10A4581      //设置空闲探测时间失败
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_SETINTERVALTIME 0x10A4582  //设置探测间隔时间
#define ERROR_NETCORE_SOCKET_SOCKET_KEEPALIVE_SETKEEPCOUNT 0x10A4583     //设置次数失败
#define ERROR_NETCORE_SOCKET_SOCKET_FASTSTART_NODELAY 0x10A45A0          //设置没有延迟失败
#define ERROR_NETCORE_SOCKET_SOCKET_FASTSTART_NOROUTE 0x10A45A1          //设置不经过路由失败
#define ERROR_NETCORE_SOCKET_SOCKET_SELECT_PARAMENT 0x10A40B0            //参数错误
#define ERROR_NETCORE_SOCKET_SOCKET_SELECT_NOREADLY 0x10A40B1            //没有设备可用
#define ERROR_NETCORE_SOCKET_SOCKET_SELECT_UNKNOW 0x10A40B2              //多路IO选择发送了未知错误
//////////////////////////////////////////////////////////////////////////
//                    网络服务函数错误
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                    Select服务器错误导出                                */
/************************************************************************/
#define ERROR_NETCORE_TCP_SELECT_INIT_ISRUNNING 0x10A4101                //服务器已经开始运行了
#define ERROR_NETCORE_TCP_SELECT_INIT_SETSOCKET 0x10A4102                //设置SOCKET协议信息失败
#define ERROR_NETCORE_TCP_SELECT_INIT_BIND 0x10A4103                     //绑定套接字失败
#define ERROR_NETCORE_TCP_SELECT_INIT_LISTEN 0x10A4104                   //监听套接字失败
#define ERROR_NETCORE_TCP_SELECT_INIT_SETSIGEMPTY 0x10A4105              //清空信号失败
#define ERROR_NETCORE_TCP_SELECT_INIT_SETSIGPIP 0x10A4106                //设置跳过信号失败
#define ERROR_NETCORE_TCP_SELECT_INIT_CREATETHREAD 0x10A4107             //创建主线程失败
#define ERROR_NETCORE_TCP_SELECT_INIT_CREATECLOSE 0x10A4108              //创建关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SEND_NOTFOUND 0x10A4110                 //发送数据失败,客户端没有找到
#define ERROR_NETCORE_TCP_SELECT_SEND_SENDISFAILED 0x10A4112             //发送数据失败
#define ERROR_NETCORE_TCP_SELECT_CONVERT_PARAMENT 0x10A4320              //转换失败，参数错误，或者服务器没有启动
#define ERROR_NETCORE_TCP_SELECT_CONVERT_GETNAME 0x10A4321               //转换失败，获取节点名称失败
#define ERROR_NETCORE_TCP_SELECT_CONVERT_NOTFOUND 0x10A4322              //没有找到转换的指定客户端
#define ERROR_NETCORE_TCP_SELECT_GETADDRINFO_PARAMENT 0x10A4150          //获取地址信息失败,参数错误
#define ERROR_NETCORE_TCP_SELECT_GETADDRINFO_ISFAILED 0x10A4151          //内部错误
#define ERROR_NETCORE_TCP_SELECT_GETFLOW_PARAMTISNULL 0x10A4160          //获取流量参数错误
#define ERROR_NETCORE_TCP_SELECT_READIO_PARAMENT 0x10A4170               //读取IO事件参数错误
#define ERROR_NETCORE_TCP_SELECT_READIO_NOIOEVNET 0x10A4171              //没有可读IO事件
#define ERROR_NETCORE_TCP_SELECT_READIO_NOTSUPPORT 0x10A4172             //不支持此方式读取IO事件，因为已经被设置为回调方式
#define ERROR_NETCORE_TCP_SELECT_READIO_NOTFOUND 0x10A4173               //没有找到指定的事件，可能没有这类事件发生
#define ERROR_NETCORE_TCP_SELECT_REMOVE_PARAMENT 0x10A4180               //移除客户端失败，参数错误
#define ERROR_NETCORE_TCP_SELECT_REMOVE_NOTFOUND 0x10A4181               //移除客户端失败，没有这个客户
#define ERROR_NETCORE_TCP_SELECT_REMOVE_DELTHREAD 0x10A4182              //删除线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_PARAMENT 0x10A4190      //启动短连接失败，参数错误
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_CREATEMAIN 0x10A4191    //启动主线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_CREATECLOSE 0x10A4192   //启动关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_CLOSE_PARAMENT 0x10A41A0      //参数错误
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_CLOSE_NOTFOUND 0x10A41A1      //关闭失败，没有找到客户端
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_CLOSE_THREAD 0x10A41A2        //关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_STOP_CLOSEMAIN 0x10A41C0      //关闭主线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_STOP_CLOSERECV 0x10A41C1      //关闭接受线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_STOP_CLOSETHREAD 0x10A41C2    //关闭关闭线程失败
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_SEND_PARAMENT 0x10A41D0 //参数错误
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_SEND_NOTFOUND 0x10A41D1 //没有找到客户端，无法发送
#define ERROR_NETCORE_TCP_SELECT_SHORTCONN_START_SEND_FAILED 0x10A41D2   //发送失败
#define ERROR_NETCORE_TCP_SELECT_EX_START_ISEXIST 0x10A41E0
#define ERROR_NETCORE_TCP_SELECT_EX_START_MALLOC 0x10A41E1
#define ERROR_NETCORE_TCP_SELECT_EX_NOTFOUND 0x10A41E2
/************************************************************************/
/*          POLL服务器错误导出类型定义                                      */
/************************************************************************/
#define ERROR_NETCORE_TCP_POLL_START_ISRUNNING 0x10A4201                 //服务器已经在运行了
#define ERROR_NETCORE_TCP_POLL_START_SETSOCKET 0x10A4202                 //设置SOCKET信息失败
#define ERROR_NETCORE_TCP_POLL_START_BIND 0x10A4203                      //绑定套接字失败咯
#define ERROR_NETCORE_TCP_POLL_START_LISTEN 0x10A4204                    //监听套接字失败
#define ERROR_NETCORE_TCP_POLL_START_CRAETETHREADLISTEN 0x10A4205        //创建接受者线程失败
#define ERROR_NETCORE_TCP_POLL_POLL_NODEVREADY 0x10A4210                 //没有设备准备好
#define ERROR_NETCORE_TCP_POLL_POLL_ISFAILED 0x10A4211                   //查询函数失败
#define ERROR_NETCORE_TCP_POLL_THREAD_SETATTRPOLICY 0x10A4233            //设置线程调度属性失败
#define ERROR_NETCORE_TCP_POLL_THREAD_SETATTRLEAVE 0x10A4234             //设置线程调度级别失败
#define ERROR_NETCORE_TCP_POLL_THREAD_SETATTROK 0x10A4235                //调整线程属性生效失败
#define ERROR_NETCORE_TCP_POLL_DOULISTT_GETCOUNT_NOTUSEDNODE 0x10A4236   //没有可用的节点了
#define ERROR_NETCORE_TCP_POLL_POLL_MALLOC 0x10A4237                     //申请空间失败
#define ERROR_NETCORE_TCP_POLL_SEND_NOTFOUNDCLIENT 0x10A4239             //没有找到指定要发送的客户地址数据
#define ERROR_NETCORE_TCP_POLL_SEND_SOCKETISFAILED 0x10A423A             //套接字句柄错误，无法发送
#define ERROR_NETCORE_TCP_POLL_SEND_SENDISFAILED 0x10A423B               //发送失败
#define ERROR_NETCORE_TCP_POLL_SETKEEPALIVE_OPENORCLOSE 0x10A4240        //开启或者关闭保活计时器失败
#define ERROR_NETCORE_TCP_POLL_SETKEEPALIVE_SETFIRST 0x10A4241           //设置首次探测失败
#define ERROR_NETCORE_TCP_POLL_SETKEEPALIVE_SETTIME 0x10A4242            //设置间隔时间失败
#define ERROR_NETCORE_TCP_POLL_SETKEEPALIVE_SETCOUNT 0x10A4243           //设置探测次数失败
#define ERROR_NETCORE_TCP_POLL_DOULISTT_SET_NOTSUPPTYPE 0x10A4244        //无法支持的设置查找类型
#define ERROR_NETCORE_TCP_POLL_DOULISTT_INSERT_NEWINITNODE 0x10A4245     //为新树初始化叶子空间失败
#define ERROR_NETCORE_TCP_POLL_CALLBACK_REG_PARAMENT 0x10A4250           //设置回调数据失败，参数错误
/************************************************************************/
/*          EPOLL TCP服务器错误导出类型定义                                 */
/************************************************************************/
#define ERROR_NETCORE_TCP_EPOLL_START_EMPTYSIGN 0x10A4301                //清空信号失败
#define ERROR_NETCORE_TCP_EPOLL_START_SETSIGN 0x10A4302                  //设置信号失败
#define ERROR_NETCORE_TCP_EPOLL_CREATE_MALLOC 0x10A4310                  //申请内存失败
#define ERROR_NETCORE_TCP_EPOLL_CREATE_EPOLL 0x10A4311                   //创建数据传输EPOLL失败
#define ERROR_NETCORE_TCP_EPOLL_CREATE_ADD 0x10A4312                     //添加到EPOLL事件失败
#define ERROR_NETCORE_TCP_EPOLL_CLOSE_PARAMENT 0x10A4330                 //参数错误
#define ERROR_NETCORE_TCP_EPOLL_CLOSE_NOTFOUND 0x10A4331                 //没有找到客户端
#define ERROR_NETCORE_TCP_EPOLL_CLOSE_CTLDEL 0x10A4332                   //删除事件失败
#define ERROR_NETCORE_TCP_EPOLL_GETFLOW_PARAMENT 0x10A4340               //获取流量失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_CALLBACK_PARAMENT 0x10A4350              //参数错误，设置回调失败
#define ERROR_NETCORE_TCP_EPOLL_ACCEPT_ISFAILED 0x10A4360                //接受链接失败
#define ERROR_NETCORE_TCP_EPOLL_ACCEPT_ADDCTL 0x10A4361                  //加入的用户添加到EPOLL集合中失败
#define ERROR_NETCORE_TCP_EPOLL_RECV_CLIENTLEAVE 0x10A4370               //接受到客户端离开事件数据
#define ERROR_NETCORE_TCP_EPOLL_RECV_ISFAILED 0x10A4371                  //接受失败，内部错误
#define ERROR_NETCORE_TCP_EPOLL_RECV_BUFFISNULL 0x10A4372                //缓冲区已经空了
#define ERROR_NETCORE_TCP_EPOLL_GETSOCKET_PARAMENT 0x10A4380             //获取套接字失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_GETSOCKET_NOTFOUND 0x10A4381             //没有找到指定的客户端
#define ERROR_NETCORE_TCP_EPOLL_GETSOCKET_BADSOCKET 0x10A4382            //客户端套接字有错误，请关闭此客户端
#define ERROR_NETCORE_TCP_EPOLL_GETADDR_PARAMENT 0x10A4383               //获取地址失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_GETADDR_NOTFOUND 0x10A4384               //获取地址失败，没有这个客户端
#define ERROR_NETCORE_TCP_EPOLL_SENDMSG_OVERFLOW 0x10A4390               //发送失败，缓冲区已经满了
#define ERROR_NETCORE_TCP_EPOLL_SENDMSG_UNKNOW 0x10A4391                 //无法识别的错误
#define ERROR_NETCORE_TCP_EPOLL_SENDMSG_PARAMENT 0x10A4392               //投递数据失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_SENDMSG_NOTFOUND 0x10A4393               //没有找到指定客户端
#define ERROR_NETCORE_TCP_EPOLL_SENDMSG_BREAK 0x10A4394                  //此客户端被设置为跳过数据处理
#define ERROR_NETCORE_TCP_EPOLL_SENDALL_PARAMENT 0x10A4395               //发送数据失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_GETRECVTIME_PARAMENT 0x10A43A0           //参数错误
#define ERROR_NETCORE_TCP_EPOLL_GETRECVTIME_NOTFOUND 0x10A43A1           //没有找到指定客户端
#define ERROR_NETCORE_TCP_EPOLL_GETALL_PARAMENT 0x10A43B0                //获取失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_GETALL_NOTCLIENT 0x10A43B1               //获取失败，没有客户端在线
#define ERROR_NETCORE_TCP_EPOLL_SETSTATUS_PARAMENT 0x10A43C0             //设置失败，参数错误
#define ERROR_NETCORE_TCP_EPOLL_SETSTATUS_NOTFOUND 0x10A43C1             //没有找到客户端
//扩展函数错误
#define ERROR_NETCORE_TCP_EPOLL_EX_MALLOC 0x10A43A0                      //申请内存失败
#define ERROR_NETCORE_TCP_EPOLL_EX_NOTFOUND 0x10A43A1                    //没有找到
/************************************************************************/
/*          SELECT UDP服务器错误导出类型定义                                 */
/************************************************************************/
#define ERROR_NETCORE_UDP_SELECT_INIT_PARAMENT 0x10A4401                  //参数错误，初始化UDP失败
#define ERROR_NETCORE_UDP_SELECT_INIT_MALLOC 0x10A4402                    //申请内存失败
#define ERROR_NETCORE_UDP_SELECT_INIT_SOCKET 0x10A4403                    //设置套接字属性失败
#define ERROR_NETCORE_UDP_SELECT_INIT_BIND 0x10A4404                      //绑定服务失败
#define ERROR_NETCORE_UDP_SELECT_INIT_CREATETHREAD 0x10A4405              //创建线程失败
#define ERROR_NETCORE_UDP_SELECT_SENDTO_PARAMENT 0x10A4421                //参数错误
#define ERROR_NETCORE_UDP_SELECT_SENDTO_NOTFOUND 0x10A4422                //没有找到指定客户端
#define ERROR_NETCORE_UDP_SELECT_SENDTO_ISFAILED 0x10A4423                //发送失败，内部错误
#define ERROR_NETCORE_UDP_SELECT_RECV_PARAMENT 0x10A4430                  //参数错误
#define ERROR_NETCORE_UDP_SELECT_RECV_CALLBACK 0x10A4431                  //模式错误，不能主动接受数据
#define ERROR_NETCORE_UDP_SELECT_RECV_NOTFOUND 0x10A4432                  //没有找到指定客户端
#define ERROR_NETCORE_UDP_SELECT_RECV_ISFAILED 0x10A4433                  //接受失败，内部错误
#define ERROR_NETCORE_UDP_SELECT_RECV_NOTBIND 0x10A4434                   //没有绑定端口,无法继续
#define ERROR_NETCORE_UDP_SELECT_SETMODE_NOTFOUND 0x10A4440               //没有找到指定客户端
/************************************************************************/
/*          EPOLL UDP服务器错误导出类型定义                                 */
/************************************************************************/
#define ERROR_NETCORE_UDP_EPOLL_START_MALLOC 0x10A4501                   //申请内存失败，启动错误
#define ERROR_NETCORE_UDP_EPOLL_START_CREATEEPOLL 0x10A4502              //创建EPOLL事件失败
#define ERROR_NETCORE_UDP_EPOLL_START_CTLADD 0x10A4503                   //加入套接字集合失败
#define ERROR_NETCORE_UDP_EPOLL_SENDMSG_PARAMENT 0x10A4510               //参数错误，发送失败
#define ERROR_NETCORE_UDP_EPOLL_SENDMSG_FAILED 0x10A4511                 //发送失败
#define ERROR_NETCORE_UDP_EPOLL_SENDMSG_SNEDSIZE 0x10A4512               //发送大小不一致
#define ERROR_NETCORE_UDP_EPOLL_GETFLOW_PARAMENT 0x10A4520               //获取流量参数为空
#define ERROR_NETCORE_UDP_EPOLL_CALLBACK_PARAMENT 0x10A4530              //设置回调失败，参数错误
#define ERROR_NETCORE_UDP_EPOLL_EX_MALLOC 0x10A45A0                      //扩展函数启动失败,申请内存失败
#define ERROR_NETCORE_UDP_EPOLL_EX_NOTFOUND 0x10A45A1                    //没有找到指定的句柄
/************************************************************************/
/*          SCTP服务器错误导出类型定义                                      */
/************************************************************************/
#define ERROR_NETCORE_SCTP_START_EMPTYSIGN 0x10A4600                      //启动失败，设置空信号失败
#define ERROR_NETCORE_SCTP_START_SETSIGN 0x10A4601                        //设置信号量失败
#define ERROR_NETCORE_SCTP_START_CREATETHREAD 0x10A4602                   //创建线程失败
#define ERROR_NETCORE_SCTP_DESTORY_TERMINATE 0x10A4610                    //结束线程失败
#define ERROR_NETCORE_SCTP_SEND_PARAMENT 0x10A4620                        //发送数据失败，参数错误
#define ERROR_NETCORE_SCTP_SEND_ISFAILED 0x10A4621                        //发送失败，内部错误
#define ERROR_NETCORE_SCTP_CLOSE_PARAMENT 0x10A4630                       //关闭失败，参数错误
#define ERROR_NETCORE_SCTP_CLOSE_NOTFOUND 0x10A4631                       //关闭失败，没有找到
#define ERROR_NETCORE_SCTP_CLOSE_CTRL 0x10A4632                           //关闭失败，删除表失败
#define ERROR_NETCORE_SCTP_GETFLOW_PARAMENT 0x10A4640                     //获取流量失败
#define ERROR_NETCORE_SCTP_CALLBACK_PARAMENT 0x10A4650                    //设置回调函数失败
#define ERROR_NETCORE_SCTP_GETSTRUCT_PARAMENT 0x10A4660                   //获取属性失败，参数错误
#define ERROR_NETCORE_SCTP_GETSTRUCT_NOTFOUND 0x10A4661                   //没有找到
#define ERROR_NETCORE_SCTP_GETSTRUCT_BADSOCKET 0x10A4662                  //错误的套接字
#define ERROR_NETCORE_SCTP_GETSOCKET_PARAMENT 0x10A4670                   //参数错误
#define ERROR_NETCORE_SCTP_GETSOCKET_NOTFOUND 0x10A4671                   //没有找到
#define ERROR_NETCORE_SCTP_GETSOCKET_BADSOCKET 0x10A4672                  //错误的套机字
#define ERROR_NETCORE_SCTP_GETADDR_PARAMENT 0x10A4680                     //参数错误
#define ERROR_NETCORE_SCTP_GETADDR_ISFAILED 0x10A4681                     //内部错误
#define ERROR_NETCORE_SCTP_ACCEPT_ISFAILED 0x10A4690                      //内部错误，结束链接失败
#define ERROR_NETCORE_SCTP_ACCEPT_GETOPT_STATUS 0x10A4696                 //获取状态失败
#define ERROR_NETCORE_SCTP_ACCEPT_ADDCTL 0x10A4697                        //添加到EPOLL中失败
#define ERROR_NETCORE_SCTP_RECV_CLIENTLEAVE 0x10A46A0                     //接受失败，客户端离开
#define ERROR_NETCORE_SCTP_RECV_BUFFISNULL 0x10A46A1                      //数据为空，无法继续
#define ERROR_NETCORE_SCTP_RECV_ISFAILED 0x10A46A2                        //内部错误
#define ERROR_NETCORE_SCTP_CREATE_SOCKET 0x10A46B0                        //创建套接字失败
#define ERROR_NETCORE_SCTP_CREATE_BIND 0x10A46B2                          //绑定端口失败
#define ERROR_NETCORE_SCTP_CREATE_SETOPTMSG 0x10A46B3                     //设置消息选项失败
#define ERROR_NETCORE_SCTP_CREATE_LISTEN 0x10A46B4                        //监听失败
#define ERROR_NETCORE_SCTP_CREATE_CREATE 0x10A46B5                        //创建EPOLL失败
#define ERROR_NETCORE_SCTP_CREATE_CTRL 0x10A46B6                          //创建ET事件失败
#define ERROR_NETCORE_SCTP_GETRECVTIME_PARAMENT 0x10A46C0                 //参数错误
#define ERROR_NETCORE_SCTP_GETRECVTIME_NOTFOUND 0x10A46C1                 //没有找到
//////////////////////////////////////////////////////////////////////////
//                     网络帮助API
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                     网络状态获取                                       */
/************************************************************************/
#define ERROR_NETCORE_NETHELP_ISPORTUSED_NOTFOUND 0x10A5101              //端口没有找到
#define ERROR_NETCORE_NETHELP_ISPORTUSED_NOTSUPPORT 0x10A5102            //不支持的选项
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_PARAMENT 0x10A5110
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_NOTFOUND 0x10A5111            //没有找到指定端口，可能是没有占用
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_GETAPPNAME 0x10A5112          //获取应用程序名称失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_GETAPPPID 0x10A5113           //获取应用程序PID失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_GETUSERNAME 0x10A5114         //获取所属用户失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_GETIPVERSION 0x10A5115        //获取协议版本失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_GETPROTOCOL 0x10A5116         //获取协议类型失败
#define ERROR_NETCORE_NETHELP_GETPORTSTATE_GETSTATE 0x10A5117            //获取状态失败

#define ERROR_NETCORE_NETHELP_DOMAINTOADDR_PARAMENT 0x20D0010           //参数错误，不能为空
#define ERROR_NETCORE_NETHELP_DOMAINTOADDR_NOTDOMAIN 0x20D0011          //不是域名或者你没有连接网络
#define ERROR_NETCORE_NETHELP_GETNCLIST_PARAMENT 0x20D0020              //参数错误
#define ERROR_NETCORE_NETHELP_GETNCLIST_ISFAILED 0x20D0021              //内部错误，无法继续
#define ERROR_NETCORE_NETHELP_GETNCLIST_EMPTY 0x20D0022                 //没有获取到网卡列表
//主机地址
#define ERROR_NETCORE_NETHELP_GETHOSTNAME_PARAMENT 0x20D0020             //参数错误
#define ERROR_NETCORE_NETHELP_GETHOSTNAME_IPADDR 0x20D0021               //不是标准的地址
#define ERROR_NETCORE_NETHELP_GETHOSTNAME_ISFAILED 0x20D0022             //获取失败，内部错误
#define ERROR_NETCORE_NETHELP_NETTABLE_PARAMENT 0x20D0030                //参数错误,无法继续
#define ERROR_NETCORE_NETHELP_NETTABLE_GETNETPARAMS 0x20D0031            //获取网络参数错误
//////////////////////////////////////////////////////////////////////////
//                     网络高级操作导出错误类型
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                      组播管理器错误                                    */
/************************************************************************/
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_PARAMENT 0x10A6001     //参数错误，创建发送者失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_SOCKET 0x10A6002       //获取套接字失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_SETOPTLOOP 0x10A6003   //设置LOOP属性失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDCREATE_SETOPTTTL 0x10A6004    //设置TTL属性失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_PARAMENT 0x10A6010     //参数错误，创建接受这失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_SOCKET 0x10A6011       //设置套接字失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_SETOPTREUSE 0x10A6012  //设置地址重用失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_SETOPTLOOP 0x10A6013   //设置LOOP属性失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_BIND 0x10A6014         //绑定端口失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVCREATE_JOIN 0x10A6015         //加入组播失败
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_PARAMENT 0x10A6020        //参数错误，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_NOTFOUND 0x10A6021        //没有找到，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_NOTSENDER 0x10A6022       //不是发送者，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_SDEND_ISFAILED 0x10A6023        //发送失败，网络错误
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_PARAMENT 0x10A6030        //参数错误
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_NOTFOUND 0x10A6031        //没有找到
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_NOTRECVER 0x10A6032       //不是接受者，无法继续
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_RVECV_ISFAILED 0x10A6033        //接受失败，网络错误
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_CLOSE_NOTFOUND 0x10A6040        //没有找到，无法关闭
#define ERROR_NETCORE_SOCKETOP_GROUPCAST_CLOSE_LEAVE 0x10A6041           //离开组播失败
/************************************************************************/
/*                      广播通信管理错误导出表                              */
/************************************************************************/
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_PARAMENT 0x10A6100     //参数错误，无法继续
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_SETSOCKET 0x10A6101    //设置套接字属性失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_SETBROADCAST 0x10A6102 //初始化广播参数失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_BIND 0x10A6103         //绑定服务地址端口失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITSEND_SETOPTREUSE 0x10A6104  //设置地址重用失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_PARAMENT 0x10A6110     //参数错误
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_SETSOCKET 0x10A6111    //设置套接字失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_SETOPTREUSE 0x10A6112  //设置地址重用失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_INITRECV_BIND 0x10A6113         //绑定端口失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_FIND_NOTFOUNDID 0x10A6120       //没有找到套接字
#define ERROR_NETCORE_SOCKETOP_BROADCAST_CLOSE_NOTFOUND 0x10A6130        //没有找到，不需要关闭
#define ERROR_NETCORE_SOCKETOP_BROADCAST_CLOSE_CLOSESOCKET 0x10A6131     //关闭套接字失败
#define ERROR_NETCORE_SOCKETOP_BROADCAST_SEND_PARAMENT 0x10A6140         //参数错误，无法继续
#define ERROR_NETCORE_SOCKETOP_BROADCAST_SEND_NOTSENDER 0x10A6141        //不是发送者句柄，不能发送数据
#define ERROR_NETCORE_SOCKETOP_BROADCAST_SEND_SENDTOFAILED 0x10A6142     //发送失败，SOCKET错误
#define ERROR_NETCORE_SOCKETOP_BROADCAST_RECV_PARAMENT 0x10A6150         //接受失败，参数错误
#define ERROR_NETCORE_SOCKETOP_BROADCAST_RECV_NOTRECVER 0x10A6151        //不是接受者套接字
#define ERROR_NETCORE_SOCKETOP_BROADCAST_RECV_RECVISFIALED 0x10A6152     //接受失败，套接字错误
/************************************************************************/
/*                      心跳管理错误导出表                                 */
/************************************************************************/
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_PARAMENT 0x10A6200          //初始化失败，参数错误，时间次数不能小于1
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_CREATETHREAD 0x10A6201      //创建线程失败
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DESTROY_TERMINATE 0x10A6210      //销毁失败。结束线程失败
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTADDR_NOTRUN 0x10A6220      //没有运行，无法继续
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTADDR_NOTSUPPORT 0x10A6221  //不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTADDR_PARAMENT 0x10A6222    //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTSOCKET_NOTRUN 0x10A6230    //没有运行
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTSOCKET_NOTSUPPORT 0x10A6231//不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INSERTSOCKET_PARAMENT 0x10A6232  //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_PARAMENT 0x10A6240    //激活客户端失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_NOTRUN 0x10A6241      //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_NOTSUPPORT 0x10A6242  //不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVEADDR_NOTFOUND 0x10A6243    //没有找到指定的客户端
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_NOTRUN 0x10A6250    //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_NOTSUPPORT 0x10A6251//不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_PARAMENT 0x10A6252  //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_ACTIVESOCKET_NOTFOUND 0x10A6253  //没有找到指定的客户端
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_PARAMENT 0x10A6260    //删除失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_NOTFOUND 0x10A6261    //删除失败，没有找到
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_NOTRUN 0x10A6262      //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETEADDR_NOTSUPPORT 0x10A6263  //不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_NOTRUN 0x10A6270    //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_NOTSUPPORT 0x10A6271//不支持的模式
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_PARAMENT 0x10A6272  //删除失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_DELETESOCKET_NOTFOUND 0x10A6273  //删除失败，没有找到
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_PARAMENT 0x10A6280    //参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_CALLBACK 0x10A6281    //设置了回调函数，不允许使用此函数
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_NOTCLIENT 0x10A6282   //没有客户端断线
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETTIMEOUT_NOTRUN 0x10A6283      //没有初始化
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_FORCEOUT_PARAMENT 0x10A6290      //参数错误，无法继续
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_FORCEOUT_NOTFOUND 0x10A6291      //没有找到指定的客户端地址
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_SETLOAD_PARAMENT 0x10A62A0       //设置负载失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETLOAD_PARAMENT 0x10A62A1       //获取失败，参数错误
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_GETLOAD_NOTFOUND 0x10A62A2       //获取失败，没有找到
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_EX_MALLOC 0x10A62F0         //申请内存失败
#define ERROR_NETCORE_SOCKOPT_HEARTBEAT_INIT_EX_NOTFOUND 0x10A62F1       //没有找到指定的心跳管理器
//////////////////////////////////////////////////////////////////////////
//                     UNIX域协议C/S模式导出错误码
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETCORE_UNIXDOMAIN_START_PARAMENT 0x10A7000                //参数错误，为空
#define ERROR_NETCORE_UNIXDOMAIN_START_MALLOC 0x10A70A0                  //申请空间失败
#define ERROR_NETCORE_UNIXDOMAIN_START_ISREADLYUNLINK 0x10A7001          //已经有一个名称存在，并且已经移除，你需要自己判断这个名称是否有用，请再次调用START启动
#define ERROR_NETCORE_UNIXDOMAIN_START_SETTCPV4SOCKET 0x10A7002          //设置套接子IPV4 TCP失败
#define ERROR_NETCORE_UNIXDOMAIN_START_SETUDPV4SOCKET 0x10A7003          //设置套接子IPV4 UDP失败
#define ERROR_NETCORE_UNIXDOMAIN_START_NOTSUPPORT 0x10A7004              //不支持的模式
#define ERROR_NETCORE_UNIXDOMAIN_START_BIND 0x10A7005                    //绑定服务器失败
#define ERROR_NETCORE_UNIXDOMAIN_START_LISTEN 0x10A7006                  //监听服务器失败
#define ERROR_NETCORE_UNIXDOMAIN_START_CREATETHREAD 0x10A7007            //创建线程失败
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_PARAMENT 0x10A7010              //接受数据失败，参数有NULL
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_NOTFOUNDSERVER 0x10A7011        //没有找到指定服务器，请检查你的服务器参数是否传错
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_NOTFOUNDCLIENT 0x10A7012        //没有找到指定客户端，请检查你的客户端参数是否传错
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_SELECTSERVER 0x10A7013          //选择服务器失败，你指定的客户端并非属于这个服务器
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_RECVMSG 0x10A7014               //接受数据失败，内部网络发生错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_CLIENTISLEAVE 0x10A7015         //接受数据失败，客户端已经离开服务器
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_RECVHDR 0x10A7016               //接受数据失败，接受到的头有问题
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_NOTALIGNMENT 0x10A7017          //内存对其有问题，接受失败，数据内存错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_MSGLEVEL 0x10A7018              //数据的消息等级不属于我们这一级
#define ERROR_NETCORE_UNIXDOMAIN_RECVDES_MSGRIGHT 0x10A7019              //接受失败，数据不正确
#define ERROR_NETCORE_UNIXDOMAIN_SENDDES_PARAMENT 0x10A7020              //发送数据失败，参数有NULL
#define ERROR_NETCORE_UNIXDOMAIN_SENDDES_NOTFOUNDSERVER 0x10A7021        //没有找到指定服务器，请检查你的服务器参数是否传错
#define ERROR_NETCORE_UNIXDOMAIN_SENDDES_NOTFOUNDCLIENT 0x10A7022        //没有找到指定客户端，请检查你的客户端参数是否传错
#define ERROR_NETCORE_UNIXDOMAIN_SENDDES_SELECTSERVER 0x10A7023          //选择服务器失败，你指定的客户端并非属于这个服务器
#define ERROR_NETCORE_UNIXDOMAIN_SENDDES_SENDMSG 0x10A7024               //发送失败，发送内部错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_PARAMENT 0x10A7030             //接受失败，参数有空
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_NOTFOUNDSERVER 0x10A7031       //没有找到指定服务器
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_NOTFOUNDCLIENT 0x10A7032       //没有指定的客户端
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_SELECTSERVER 0x10A7033         //选择错误，指定客户端没有找到对应的服务器
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_CLIENTISLEAVE 0x10A7034        //客户端已经离开，无法继续
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_RECVMSG 0x10A7035              //接受内部错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_LEN 0x10A7036                  //接受到的数据长度有问题
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_NOTCREDLEN 0x10A7037           //接受到的数据协议长度有问题
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_SOCKETLEAVE 0x10A7038          //接受到的数据不是属于同一级别
#define ERROR_NETCORE_UNIXDOMAIN_RECVCRED_NOTCRED 0x10A7039              //不是凭证消息
#define ERROR_NETCORE_UNIXDOMAIN_SENDCRED_PARAMENT 0x10A7040             //发送失败，参数错误
#define ERROR_NETCORE_UNIXDOMAIN_SENDCRED_NOTFOUNDSERVER 0x10A7041       //没有找到指定服务器
#define ERROR_NETCORE_UNIXDOMAIN_SENDCRED_NOTFOUNDCLIENT 0x10A7042       //发送失败，没有找到指定客户端
#define ERROR_NETCORE_UNIXDOMAIN_SENDCRED_SELECTSERVER 0x10A7043         //指定的客户端不属于此服务器
#define ERROR_NETCORE_UNIXDOMAIN_SENDCRED_MALLOC 0x10A7044               //申请空间失败
#define ERROR_NETCORE_UNIXDOMAIN_SENDCRED_SENDMSG 0x10A7045              //发送失败，内部错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVMSG_PARAMENT 0x10A7050              //接受数据失败，参数错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVMSG_NOTFOUNDSERVER 0x10A7051        //接受失败，没有找到服务器
#define ERROR_NETCORE_UNIXDOMAIN_RECVMSG_NOTFOUNDCLIENT 0x10A7052        //没有找到指定客户端
#define ERROR_NETCORE_UNIXDOMAIN_RECVMSG_SELECTSERVER 0x10A7053          //选择的服务器错误，服务器没有此客户端
#define ERROR_NETCORE_UNIXDOMAIN_RECVMSG_RECVMSG 0x10A7054               //接受数据失败，内部错误
#define ERROR_NETCORE_UNIXDOMAIN_RECVMSG_CLIENTISLEAVE 0x10A7055         //客户端已经离开服务器
#define ERROR_NETCORE_UNIXDOMAIN_SENDMSG_PARAMENT 0x10A7060              //发送数据失败，参数错误
#define ERROR_NETCORE_UNIXDOMAIN_SENDMSG_NOTFOUNDSERVER 0x10A7061        //没有找到指定服务器
#define ERROR_NETCORE_UNIXDOMAIN_SENDMSG_NOTFOUNDCLIENT 0x10A7062        //没有找到指定客户端
#define ERROR_NETCORE_UNIXDOMAIN_SENDMSG_SELECTSERVER 0x10A7063          //选择了错误的服务器
#define ERROR_NETCORE_UNIXDOMAIN_SENDMSG_RECVMSG 0x10A7064               //发送数据内部错误
#define ERROR_NETCORE_UNIXDOMAIN_RECV_NOTFOUNDCLIENT 0x10A7070           //没有找到客户端
#define ERROR_NETCORE_UNIXDOMAIN_RECV_NOTSUPPORTMSGTYPE 0x10A7071        //不支持此接受消息类型
#define ERROR_NETCORE_UNIXDOMAIN_SEND_NOTFOUNDCLIENT 0x10A7072           //没有找到客户端
#define ERROR_NETCORE_UNIXDOMAIN_SEND_NOTSUPPORTMSGTYPE 0x10A7073        //不支持此发送消息类型
#define ERROR_NETCORE_UNIXDOMAIN_REMOTE_PARAMENT 0x10A7080               //参数错误，移除客户端失败
#define ERROR_NETCORE_UNIXDOMAIN_REMOTE_NOTFOUNDCLIENT 0x10A7081         //移除客户端失败，没有找到客户端
#define ERROR_NETCORE_UNIXDOMAIN_REMOTE_SHUTDOWN 0x10A7082               //关闭读写失败
#define ERROR_NETCORE_UNIXDOMAIN_REMOTE_CLOSE 0x10A7083                  //关闭描述符失败
#define ERROR_NETCORE_UNIXDOMAIN_SHUTDOWN_NOTFOUNDCLIENT 0x10A7084       //没有找到，关闭服务器失败
#define ERROR_NETCORE_UNIXDOMAIN_SHUTDOWN_NOTRUNNING 0x10A7085           //服务器没有运行，不需要关闭
#define ERROR_NETCORE_UNIXDOMAIN_SHUTDOWN_TERMINATETHREAD 0x10A7086      //结束服务线程失败
#define ERROR_NETCORE_UNIXDOMAIN_SHUTDOWN_SHUTDOWN 0x10A7087             //关闭读写操作失败
#define ERROR_NETCORE_UNIXDOMAIN_SHUTDOWN_CLOSE 0x10A7088                //关闭描述符失败
//////////////////////////////////////////////////////////////////////////
//                    UDX传输协议导出
//////////////////////////////////////////////////////////////////////////
#define ERROR_NETENGINE_NETCORE_UDX_ACCEPT_PARAMENT 0x10A8000             //参数错误,接受处理失败
#define ERROR_NETENGINE_NETCORE_UDX_ACCEPT_MALLOC 0x10A8001               //申请内存失败
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_PARAMENT 0x10A8010              //参数错误,接受数据失败
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_NOTFOUND 0x10A8011              //没有找到指定的句柄
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_WINDOWFULL 0x10A8012            //滑动窗口已满,无法接受数据
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_PROTOCOL 0x10A8013              //协议不正确
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_SAME 0x10A8014                  //相同的序列号包
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_RETRY 0x10A8015                 //报告了一个重传错误,某一个序列号已经被丢弃
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_NOLOGIN 0x10A8016               //用户没有登录,无法继续
#define ERROR_NETENGINE_NETCORE_UDX_PARSE_TIMEOUT 0x10A8017               //接收到一个过期的包
#define ERROR_NETENGINE_NETCORE_UDX_SEND_PARAMENT 0x10A8020               //参数错误,发送失败
#define ERROR_NETENGINE_NETCORE_UDX_SEND_NOTFOUND 0x10A8021               //没有找到指定客户端
#define ERROR_NETENGINE_NETCORE_UDX_SEND_WINDOWSIZE 0x10A8022             //对端阻塞,无法发送数据
#define ERROR_NETENGINE_NETCORE_UDX_RECV_PARAMENT 0x10A8030               //参数错误,接受数据失败
#define ERROR_NETENGINE_NETCORE_UDX_RECV_NOTFOUND 0x10A8031               //没有找到指定客户端
#define ERROR_NETENGINE_NETCORE_UDX_RECV_ISNULL 0x10A8032                 //数据队列为空
#define ERROR_NETENGINE_NETCORE_UDX_RECV_NOTDATA 0x10A8033                //没有数据或者包不完成
#define ERROR_NETENGINE_NETCORE_UDX_INIT_PARAMENT 0x10A8040               //初始化失败,参数错误
#define ERROR_NETENGINE_NETCORE_UDX_INIT_CREATETHREAD 0x10A8041           //初始化失败,创建线程失败
#define ERROR_NETENGINE_NETCORE_UDX_CBSET_PARAMENT 0x10A8050              //设置回调失败,参数错误
#define ERROR_NETENGINE_NETCORE_UDX_EX_MALLOC 0x10A80A0                   //申请内存错误
#define ERROR_NETENGINE_NETCORE_UDX_EX_NOTFOUND 0x10A80A1                 //没有找到指定服务
//////////////////////////////////////////////////////////////////////////
//                         无线通信导出错误
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                          蓝牙设备API错误                               */
/************************************************************************/
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SCAN_NOTDEVICE 0x10A9000          //没有找到本地蓝牙设备
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SCAN_GETHANDLE 0x10A9001          //打开本地蓝牙设备失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SCAN_MALLOC 0x10A9002             //申请内存空间失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_SCAN_NOSEARCH 0x10A9003           //没有搜索到别的蓝牙设备
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDCONNECT_PARAMENT 0x10A90010    //参数错误，无法连接
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDCONNECT_SETSOCKET 0x10A90011   //设置套接字属性失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDCONNECT_BIND 0x10A90012        //绑定网络失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDCONNECT_ISFAILED 0x10A90013    //连接失败，内部错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDSEND_PARAMENT 0x10A90020       //参数错误，发送数据失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDSEND_NOTFOUND 0x10A90021       //没有找到网络句柄
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDSEND_ISFAILED 0x10A90022       //发送数据失败，内部错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDRECV_PARAMENT 0x10A90030       //接受数据失败，参数错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDRECV_NOTFOUND 0x10A90031       //没有找到网络句柄
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDRECV_ISFAILED 0x10A90032       //接受数据失败，内部错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CMDCLOSE_NOTFOUND 0x10A90040      //没有找到句柄，关闭失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_START_PARAMENT 0x10A90050         //启动失败，参数错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_START_SETSOCKET 0x10A90051        //设置套接字失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_START_BIND 0x10A90052             //绑定服务失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_START_SETOPT 0x10A90053           //设置蓝牙属性失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_START_LISTEN 0x10A90054           //监听失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_START_CREATETHREAD 0x10A90055     //创建线程失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CONNECT_PARAMENT 0x10A90060       //连接失败，参数错误
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CONNECT_SETSOCKET 0x10A90061      //设置套接字失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CONNECT_BIND 0x10A90062           //绑定服务器失败
#define ERROR_NETCORE_WIRELESS_BLUETOOTH_CONNECT_ISFAILED 0x10A90063       //连接失败，内部错误
/************************************************************************/
/*                          红外设备API错误                               */
/************************************************************************/
#define ERROR_NETCORE_WIRELESS_INFRARED_INIT_PARAMENT 0x10A9100           //初始化失败，参数错误
#define ERROR_NETCORE_WIRELESS_INFRARED_INIT_ISFAILED 0x10A9101           //初始化失败，内部错误
#define ERROR_NETCORE_WIRELESS_INFRARED_INIT_READCONFIG 0x10A9102         //读取配置文件失败，无法继续
#define ERROR_NETCORE_WIRELESS_INFRARED_GETCODE_NOTFOUND 0x10A9110        //没有找到TOKEN
#define ERROR_NETCORE_WIRELESS_INFRARED_GETCODE_ISFAILED 0x10A9111        //获取CODE失败，内部错误
#define ERROR_NETCORE_WIRELESS_INFRARED_GETCODE_ISNULL 0x10A9112          //获取CODE失败，红外没有接收到任何数据
#define ERROR_NETCORE_WIRELESS_INFRARED_GETMSG_NOTFOUND 0x10A9120         //获取消息失败，没有找到
#define ERROR_NETCORE_WIRELESS_INFRARED_GETMSG_ISFAILED 0x10A9121         //内部错误
#define ERROR_NETCORE_WIRELESS_INFRARED_GETMSG_ISNULL 0x10A9122           //获取失败，这个CODE关联的数据已经空了
#define ERROR_NETCORE_WIRELESS_INFRARED_SEND_PARAMENT 0x10A9130           //发送数据失败，参数错误
#define ERROR_NETCORE_WIRELESS_INFRARED_SEND_OPEN 0x10A9131               //打开红外设备失败
#define ERROR_NETCORE_WIRELESS_INFRARED_SEND_ISFAILED 0x10A9132           //内部错误
#define ERROR_NETCORE_WIRELESS_INFRARED_DESTORY_NOTFOUND 0x10A9140        //没有找到，不需要销毁
