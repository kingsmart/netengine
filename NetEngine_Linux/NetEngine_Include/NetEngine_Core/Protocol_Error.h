#pragma once
/********************************************************************
//	Created:	2012/1/8  12:15
//	File Name: 	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngine_Protocol\Protocol_Error.h
//	File Path:	J:\U_DISK_Path\NetSocketEngine\NetEngineCore\NetEngine_Protocol
//	File Base:	Protocol_Error
//	File Ext:	h
//  Project:    NetSocketEngine(网络通信引擎)
//	Author:		Dowflyon
//	Purpose:	协议模块导出错误定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                          ICMP协议导出错误
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*                          PING                                        */
/************************************************************************/
#define ERROR_PROTOCOL_ICMP_PING_ISRUNPING 0x10E0001                    //已经在运行PING了
#define ERROR_PROTOCOL_ICMP_SETTIMEOUT_ISFAILED 0x10E0002               //设置套接字超时时间失败
#define ERROR_PROTOCOL_ICMP_PING_SETSOCKET 0x10E0003                    //设置套接字模式失败
#define ERROR_PROTOCOL_ICMP_PING_SENDTO 0x10E0004                       //发送ICMP包失败
#define ERROR_PROTOCOL_ICMP_PING_TIMEDOUT 0x10E0005                     //接受包超时
#define ERROR_PROTOCOL_ICMP_PING_RECVFROMISFAILED 0x10E0006             //接受封包失败
#define ERROR_PROTOCOL_ICMP_PING_NONECHO 0x10E0007                      //数据回显失败
#define ERROR_PROTOCOL_ICMP_PING_OTHERPACKET 0x10E0008                  //非ICMP PING包，无法解析
#define ERROR_PROTOCOL_ICMP_SETTTL 0x10E0009                            //设置套接字TTL值失败
/************************************************************************/
/*                  TRACER路由路径过程                                  */
/************************************************************************/
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_ISRUN 0x10E0020                //已经在运行路由传输过程协议
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_SETSOCKETRAW 0x10E0021         //设置原始套接字属性失败
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_BIND 0x10E0022                 //绑定本地端口失败
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_SETUDPSOCKET 0x10E0023         //设置UDP套接字属性失败
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_SENDTOUDP 0x10E0024            //发送UDP数据包失败
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_TIMEDOUT 0x10E0025             //接受UDP数据包超时
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_RECVFROM 0x10E0026             //接受数据包失败
#define ERROR_PROTOCOL_ICMP_ROUTERTRACER_PROTOCOLISFAILED 0x10E0027     //协议被损坏，无法继续
//////////////////////////////////////////////////////////////////////////
//                          SOCKS5协议包导出错误
//////////////////////////////////////////////////////////////////////////
#define ERROR_PROTOCOL_SOCKS_REQUEST_CONNECT_PARAMENT 0x10E2001          //参数错误
#define ERROR_PROTOCOL_SOCKS_REQUEST_LOGIN_PARAMENT 0x10E2010            //参数错误
#define ERROR_PROTOCOL_SOCKS_REQUEST_LOGIN_USERPASS 0x10E2011            //用户名或者密码输入参数有问题
#define ERROR_PROTOCOL_SOCKS_REQUEST_INFO_PARAMENT 0x10E2020             //参数错误
#define ERROR_PROTOCOL_SOCKS_HANDLE_CONNECT_PARAMENT 0x10E2030           //参数错误
#define ERROR_PROTOCOL_SOCKS_HANDLE_CONNECT_NOSOCKS 0x10E2031            //不是SOCKS协议无法继续
#define ERROR_PROTOCOL_SOCKS_HANDLE_LOGIN_PARAMENT 0x10E2040             //登录失败,参数错误
#define ERROR_PROTOCOL_SOCKS_HANDLE_LOGIN_FAILED 0x10E2041               //登录失败,用户密码错误
#define ERROR_PROTOCOL_SOCKS_HANDLE_INFO_PARAMENT 0x10E2050              //参数错误
#define ERROR_PROTOCOL_SOCKS_HANDLE_INFO_FAILED 0x10E2051                //处理失败,服务器发生错误
