﻿#pragma once
/********************************************************************
//	Created:	2011/10/10   15:14
//	Filename: 	NetSocketEngine/NetEngine/NetEngine_Define.h
//	File Path:	NetSocketEngine/NetEngine/
//	File Base:	NetEngine_Define
//	File Ext:	h
//      Project:        血与荣誉网络通信引擎 For Linux
//	Author:		dowflyon
//	Purpose:	网络通信引擎服务器导出定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//                        导出类型定义
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*          串口操作导出定义                                              */
/************************************************************************/
#define NETENGINE_NETCORE_SERIALPORT_PARITYNONE 0x000000A1                //没有校验位
#define NETENGINE_NETCORE_SERIALPORT_PARITYODD 0x000000A2                 //设置为奇效验
#define NETENGINE_NETCORE_SERIALPORT_PARITYEVEN 0x000000A3                //偶校验
/************************************************************************/
/*                      网络核心帮助函数定义                               */
/************************************************************************/
//网络协议版本号定义
#define NETCORE_SOCKETHELP_STATE_VERSION_IPV4 0x11110010                     //IPV4版本
#define NETCORE_SOCKETHELP_STATE_VERSION_IPV6 0x11110011                     //IPV6版本
#define NETCORE_SOCKETHELP_STATE_VERSION_UNKNOW 0x1111001F                   //无法识别的协议版本
//网络端口占用者信息
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_TCP 0x11110020               //TCP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_UDP 0x11110021               //UDP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_RAW 0x11110022               //RAW
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_UNIX 0x11110023              //UNIX
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_ICMP 0x11110024              //ICMP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_IP 0x11110025                //IP
#define NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_UNKNOW 0x1111002F            //无法识别的协议类型
//网络连接类型定义
#define NETCORE_SOCKETHELP_CONNECTTYPE_CONNECTOKOFNOTCONNECTED 0x11110001    //已经连接但是可能并没有网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_LANTOINTERNET 0x11110002              //通过局域网连接到网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_MODEMTOINTERNET 0x11110003            //通过MODEM连接到网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_NOTCONNECTED 0x11110004               //没有连接到网络
#define NETCORE_SOCKETHELP_CONNECTTYPE_PROXYTOINTERNET 0x11110005            //通过代理连接到网络
//网络状态信息
#define NETCORE_SOCKETHELP_STATE_NET_CLOSED 1                                //网络关闭
#define NETCORE_SOCKETHELP_STATE_NET_LISTEN 2                                //监听
#define NETCORE_SOCKETHELP_STATE_NET_SYNSENT 3                               //再发送连接请求后等待匹配的连接请求
#define NETCORE_SOCKETHELP_STATE_NET_SYNRECEIVED 4                           //再收到和发送一个连接请求后等待对方对连接请求的确认
#define NETCORE_SOCKETHELP_STATE_NET_ESTABLISHED 5                           //代表一个打开的连接
#define NETCORE_SOCKETHELP_STATE_NET_FINWAIT1 6                              //等待远程TCP连接中断请求，或先前的连接中断请求的确认
#define NETCORE_SOCKETHELP_STATE_NET_FINWAIT2 7                              //从远程TCP等待连接中断请求
#define NETCORE_SOCKETHELP_STATE_NET_CLOSEWAIT 8                             //等待从本地用户发来的连接中断请求
#define NETCORE_SOCKETHELP_STATE_NET_CLOSING 9                               //等待远程TCP对连接中断的确认
#define NETCORE_SOCKETHELP_STATE_NET_LASTACK 10                              //等待原来的发向远程TCP的连接中断请求的确认
#define NETCORE_SOCKETHELP_STATE_NET_TIMEWAIT 11                             //等待足够的时间以确保远程TCP接收到连接中断请求的确认
/***********************************************************a*************/
/*                      选择模型-主动获取事件类型                           */
/************************************************************************/
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_ALL 0xABCD0010                 //所有事件，输入作用，不支持输出
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_LOGIN 0xABCD0011               //用户登录事件，用户连接到服务器，此事件无数据
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_LEAVE 0xABCD0012               //用户离开事件，用户断开连接，此事件无数据
#define NETENGINE_NETCORE_TCP_SELECT_EVENT_RECV 0xABCD0013                //数据到达事件，用户发送数据给服务器
/************************************************************************/
/*                      其他CS模式定义                                    */
/************************************************************************/
#define NETENGINE_NETCORE_PROTOCOL_TCP_VERSION4 0xABCE0001                //TCP协议IPV4
#define NETENGINE_NETCORE_PROTOCOL_UDP_VERSION4 0xABCE0002                //UDP协议IPV4
#define NETENGINE_NETCORE_PROTOCOL_TCP_VERSION6 0xABCE0003                //TCP协议IPV6
#define NETENGINE_NETCORE_PROTOCOL_UDP_VERSION6 0xABCE0004                //UDP协议IPV6
/************************************************************************/
/*                      无线通信导出结构体                                 */
/************************************************************************/
//蓝牙通信传输协议类型
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_REJ 0x01
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_CONN_REQ 0x02
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_CONN_RSP 0x03
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_CONF_REQ 0x04
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_CONF_RSP 0x05
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_DISCONN_REQ 0x06
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_DISCONN_RSP 0x07
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_ECHO_REQ 0x08
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_ECHO_RSP 0x09
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_INFO_REQ 0x0a
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_INFO_RSP 0x0b
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_CREATE_REQ 0x0c
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_CREATE_RSP 0x0d
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_MOVE_REQ 0x0e
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_MOVE_RSP 0x0f
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_MOVE_CFM 0x10
#define NETCORE_WIRELESS_BLUETOOTH_SINGLECMD_MOVE_CFM_RSP 0x11
/************************************************************************/
/*                        枚举类型                                       */
/************************************************************************/
//UNIX域套接字消息
typedef enum en_NetCore_UnixDomain_MsgType
{
    NETCORE_DOMAIN_MSGTYPE_DATA = 1,                                      //仅仅传递数据
    NETCORE_DOMAIN_MSGTYPE_DES,                                           //传递文件描述符
    NETCORE_GROUPCAST_MT_CRED                                             //传递凭证
}ENUM_NETCORE_UNIXDOMAIN_MSGTYPE,*LPENUM_NETCORE_UNIXDOMAIN_MSGTYPE;
//负载状态
typedef enum en_NetCore_SockOpt_HBLoad
{
    ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_UNKNOW = 0,                          //未知状态
    ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_IDLE = 1,                            //负载空闲
    ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_IDEAL,                               //理想负载
    ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_NORMAL,                              //正常负载
    ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_BUSY,                                //忙碌的负载
    ENUM_NETCORE_SOCKOPT_HBLOAD_RATE_DANGER                               //危险负载，资源一直在90%以上
}ENUM_NETCORE_SOCKOPT_HBLOAD,*LPENUM_NETCORE_SOCKOPT_HBLOAD;
//////////////////////////////////////////////////////////////////////////
//                        导出的回调函数
//////////////////////////////////////////////////////////////////////////
/************************************************************************/
/*          Select TCP 服务器回调函数                                     */
/************************************************************************/
typedef void(*CALLBACK_NETCORE_TCP_SERVER_SELECT_LOGIN)(LPCTSTR lpszClientAddr,LPVOID lParam);  //有客户进入，返回字符串客户地址和端口
typedef void(*CALLBACK_NETCORE_TCP_SERVER_SELECT_RECV)(LPCTSTR lpszClientAddr,LPCTSTR lpszMsgBuffer,int nMsgLen,LPVOID lParam);     //接受到数据，返回字符串的客户地址和接受到的数据
typedef void(*CALLBACK_NETCORE_TCP_SERVER_SELECT_LEAVE)(LPCTSTR lpszClientAddr,LPVOID lParam);  //客户离开，返回客户离开的地址和端口号码=
//轻量级服务器回调函数，参数：套接字句柄,客户端地址 客户端消息 消息长度 自定义数据
typedef void(*CALLBACK_NETCORE_TCP_SERVER_SELECT_SCONNECT)(SOCKET m_Socket,LPCTSTR lpszClientAddr,LPCTSTR lpszMsgBuffer,int nMsgLen,LPVOID lParam);
/************************************************************************/
/*          POLL TCP 服务器回调函数                                       */
/************************************************************************/
typedef void(*CALLBACK_NETCORE_TCP_SERVER_POLL_LOGIN)(LPCTSTR,LPVOID);    //有客户进入
typedef void(*CALLBACK_NETCORE_TCP_SERVER_POLL_RECV)(LPCTSTR,LPCTSTR,int,LPVOID);   //有数据到达，返回字符串的客户地址和接受到的数据以及数据包长度
typedef void(*CALLBACK_NETCORE_TCP_SERVER_POLL_LEAVE)(LPCTSTR,LPVOID);    //客户离开，返回地址和端口号
/************************************************************************/
/*          EPOLL TCP 服务器回调函数                                      */
/************************************************************************/
//用户连接
typedef BOOL(*CALLBACK_NETCORE_TCP_SERVER_EPOLL_LOGIN)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPVOID lParam);
//接受到数据nMsgLen == -1 表示客户端被设置为跳过
typedef void(*CALLBACK_NETCORE_TCP_SERVER_EPOLL_RECV)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPCTSTR lpszRecvMsg,int nMsgLen,LPVOID lParam);
//用户离开
typedef void(*CALLBACK_NETCORE_TCP_SERVER_EPOLL_LEAVE)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPVOID lParam);
/************************************************************************/
/*          SCTP      服务回调函数                                        */
/************************************************************************/
typedef BOOL(*CALLBACK_NETCORE_SCTP_SERVER_LOGIN)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPVOID lParam);   //用户连接
typedef void(*CALLBACK_NETCORE_SCTP_SERVER_RECV)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPCTSTR lpszRecvMsg,int nMsgLen,int nChannel,LPVOID lParam); //接受到数据
typedef void(*CALLBACK_NETCORE_SCTP_SERVER_LEAVE)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPVOID lParam);          //用户离开
/************************************************************************/
/*          SELECT UDP 服务器回调函数                                      */
/************************************************************************/
typedef void(*CALLBACK_NETCORE_UDP_SERVER_SELEC_NETEVENT)(XNETHANDLE xhNet,LPCTSTR lpszRecvAddr,LPCTSTR lpszMsgBuffer,int nLen,LPVOID lParam);
/************************************************************************/
/*          EPOLL UDP 服务器回调函数                                      */
/************************************************************************/
//有数据到达，返回字符串的客户地址和接受到的数据以及数据包长度 nMsgLen -1 表示错误发生,下面的SOCKET参数不起作用
typedef void(*CALLBACK_NETCORE_UDP_SERVER_EPOLL_RECV)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPCTSTR lpszMsgBuffer,int nMsgLen,LPVOID lParam);
/************************************************************************/
/*          串口数据接受回调                                              */
/************************************************************************/
typedef void(*CALLBACK_NETCORE_SERIALPORT_RECV)(LPCTSTR,LPVOID,int,LPVOID);//函数指针，返回接受到的数据的串口号，接受到的数据，接受数据的长度，自定义参数
/************************************************************************/
/*                      管道数据回调函数                                   */
/************************************************************************/
//命名管道：接受到的数据消息，参数： 哪个命名管道返回的数据消息，读取的数据缓冲区，缓冲区数据长度，用户自定义参数
typedef void(*CALLBACK_NETCORE_PIPECOMMUNICATIONS_NAMED_READ)(LPCTSTR,LPCTSTR,int,LPVOID);
/************************************************************************/
/*                      UNIX域协议用户登录回调函数                          */
/************************************************************************/
typedef void (*CALLBACK_NETCORE_UNIXDOMAIN_LOGIN)(XNETHANDLE xhServer,LPCTSTR lpszClientAddr,LPVOID lParam);
/************************************************************************/
/*         套接字选项管理回调函数                                          */
/************************************************************************/
/*                        心跳管理                                       */
//心跳离开事件
typedef void(*CALLBACK_NETCORE_SOCKOPT_HEARTBEAT_EVENT)(LPCTSTR lpszClientAddr,SOCKET hSocket,int nStatus,LPVOID lParam);
/************************************************************************/
/*         高速缓存事件触发回调函数                                          */
/************************************************************************/
//告知用户缓存写到文件中的大小
typedef void(*CALLBACK_NETCORE_CACHEFILE_FLUSHEVENT)(LPCSTR lpszFileName,int nMemLength,LPVOID lParam);
/************************************************************************/
/*         无线通信事件触发回调函数                                         */
/************************************************************************/
//蓝牙数据回调接口，第一个参数是客户端地址，第二个参数是客户端句柄，倒数第二个参数 0 表示用户进入，-1 表示用户离开，>0表示有数据到达(参数二会生效)
//回调通知的客户端离开，服务器会自动删除客户端资源，不需要再次删除
typedef void(*CALLBACK_NETCORE_WIRELESS_BLUETOOTH_NETEVENT)(LPCTSTR lpszClientAddr,SOCKET hSocket,LPCTSTR lpszMsgBuffer,int nLen,LPVOID lParam);
/************************************************************************/
/*                      UDX回调函数                                     */
/************************************************************************/
//客户进入
typedef void(CALLBACK* CALLBACK_NETCORE_UDX_LOGIN)(LPCSTR lpszClientAddr, LPVOID lParam);
//客户离开
typedef void(CALLBACK* CALLBACK_NETCORE_UDX_LEAVE)(LPCSTR lpszClientAddr, LPVOID lParam);
//////////////////////////////////////////////////////////////////////////
//                        导出的数据结构
//////////////////////////////////////////////////////////////////////////
//网络信息状态查询
typedef struct tag_NetCore_NetHelp_NetState
{
    TCHAR tszAppName[128];                                                //应用程序名称
    int nPid;                                                             //应用程序PID
    TCHAR tszUserName[32];                                                //应用程序所属用户
    DWORD dwProVersion;                                                   //协议版本
    DWORD dwProtocol;                                                     //协议类新
    DWORD dwNetState;                                                     //网络状态
}NETCORE_NETHELP_NETSTATE,*LPNETCORE_NETHELP_NETSTATE;
typedef struct tag_NetCore_NetHelp_NetCard
{
    TCHAR tszIFName[128];                                                 //网卡名称
    TCHAR tszIPAddr[32];                                                  //网卡IP地址
    TCHAR tszBroadAddr[32];                                               //网卡的广播地址
    TCHAR tszMaskAddr[32];                                                //网卡的子网地址
}NETCORE_NETHELP_NETCARD,*LPNETCORE_NETHELP_NETCARD;
//网络参数信息
typedef struct tag_NetCore_SocketHelp_NetParam
{
    TCHAR tszHostName[64];                                                 //本地电脑的主机名称
    TCHAR tszDomainNmae[64];                                               //本地电脑注册的域名，LINUX暂时不支持
    list<tstring> stl_ListDns;                                             //NDS服务器列表
}NETCORE_SOCKHELP_NETPARAM,*LPNETCORE_SOCKHELP_NETPARAM;
//凭证传递接口数据结构
typedef struct tag_Netcore_UnixDomain_UserCred
{
    pid_t nPid;			                                                  //PID 进程ID
    uid_t nUid;			                                                  //UID 用户ID
    gid_t nGid;			                                                  //GID 所属组ID
}NETCORE_UNIXDOMAIN_USERCRED,*LPNETCORE_UNIXDOMAIN_USERCRED;
/************************************************************************/
/*                      套接字操作导出结构体                               */
/************************************************************************/
//心跳机器负载
typedef struct
{
    ENUM_NETCORE_SOCKOPT_HBLOAD en_CPULoad;
    ENUM_NETCORE_SOCKOPT_HBLOAD en_MEMLoad;
    ENUM_NETCORE_SOCKOPT_HBLOAD en_NETLoad;
    ENUM_NETCORE_SOCKOPT_HBLOAD en_DISKLoad;
    ENUM_NETCORE_SOCKOPT_HBLOAD en_GRAPHLoad;
}NETCORE_SOCKOPT_HBLOAD,*LPNETCORE_SOCKOPT_HBLOAD;
typedef struct
{
    int nCPURate;
    int nMemRate;
    int nNetRate;
    int nDiskRate;
    int nGraphRate;
}NETCORE_SOCKOPT_LOADRATE,*LPNETCORE_SOCKOPT_LOADRATE;
/************************************************************************/
/*                      无线通信导出结构体                                 */
/************************************************************************/
typedef struct
{
    uint8_t unBTDevType[3];                                               //蓝牙设备类型
    TCHAR tszBTAddr[6];                                                   //蓝牙设备地址
    TCHAR tszBTName[MAX_PATH];                                            //蓝牙设备名称
}NETCORE_WIRELESSBLUETOOTH,*LPNETCORE_WIRELESSBLUETOOTH;
/************************************************************************/
/*                      UDX协议数据结构参数                             */
/************************************************************************/
//UDX配置信息
typedef struct tag_NetCore_UDXConfig
{
    BOOL bEnableLogin;                                                    //是否启用登录离开模式
    BOOL bEnableReorder;                                                  //是否启用乱序重组
    BOOL bEnableRryTime;                                                  //是否启用重传超时
    BOOL bEnableLost;                                                     //是否允许最小丢包,如果不允许,丢包后将强制断开
    BOOL bEnableMtap;                                                     //是否启用聚合包发送数据,启用后将允许低延迟发送,不启用将无延迟发送
    int nWindowSize;                                                      //是否启用滑动窗口,0为不启用,大于0表示启用字节大小
}NETCORE_UDXCONFIG, *LPNETCORE_UDXCONFIG;
/************************************************************************/
/*                      原始套接字编程数据结构参数                          */
/************************************************************************/
typedef struct tag_NetCore_RawSocket_NetParam
{
    TCHAR tszSrcMac[8];                                                   //原始MAC地址(ARP协议需要)
    TCHAR tszDstMac[8];                                                   //目的MAC地址
    TCHAR tszSrcAddr[64];                                                 //原始地址
    TCHAR tszDstAddr[64];                                                 //目标地址
    int nSrcPort;                                                         //原端口
    int nDstPort;                                                         //目标端口
    USHORT usLen;                                                         //负载数据包大小，如果没有填入0
    //负载协议
    struct
    {
        int nWinSize;                                                     //滑动窗口大小
        int nSequeue;                                                     //当前序列号
        int nAck;                                                         //确认号
        TCHAR cFlags;                                                     //当前标记
    }st_TCPHdr;
    struct
    {
        int nSequeue;
    }st_ICMPHdr;
}NETCORE_RAWSOCKET_NETPARAM,*LPNETCORE_RAWSOCKET_NETPARAM;
//////////////////////////////////////////////////////////////////////////
//                        导出函数定义
//////////////////////////////////////////////////////////////////////////
/************************************************************************
函数名称：NetEngine_GetLastError
函数功能：获得错误码
返回值
  类型：双字
  意思：返回错误码
备注：
************************************************************************/
extern "C" DWORD NetCore_GetLastError(int *pInt_ErrorCode = NULL);
/************************************************************************/
/*          网络帮助导出函数                                               */
/************************************************************************/
/********************************************************************
函数名称：NetCore_SocketHelp_IsPortOccupation
函数功能：端口是否被使用
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要检查的端口号码
 参数.二：nProto
  In/Out：In
  类型：整数型
  可空：Y
  意思：协议类型，要查找端口的所属协议
返回值
  类型：逻辑型
  意思：是否被使用，假为没有
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_IsPortOccupation(int nPort,int nProto = NETCORE_SOCKETHELP_NETSTATE_PROTOCOL_TCP);
/********************************************************************
函数名称：NetCore_SocketHelp_GetPortState
函数功能：获取端口状态
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要检查的端口号码
 参数.二：pSt_NetState
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出相关的信息
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetPortState(int nPort,NETCORE_NETHELP_NETSTATE *pSt_NetState);
/********************************************************************
函数名称：NetCore_SocketHelp_DomainToAddr
函数功能：域名转IP地址
 参数.一：lpszDomain
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入一个标准的域名地址
 参数.二：ptszIPAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出转为到的IP地址
返回值
  类型：逻辑型
  意思：是否转换成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_DomainToAddr(LPCTSTR lpszDomain,TCHAR *ptszIPAddr);
/********************************************************************
函数名称：NetCore_SocketHelp_GetNetCardList
函数功能：获取网卡列表信息
 参数.一：pSt_ListIFInfo
  In/Out：Out
  类型：STL列表容器指针
  可空：Y
  意思：输出获取到的网卡信息列表
 参数.二：lpszIPAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：判断这个IP地址是否存在于当前网卡（是否为本地IP地址），真表示存在
返回值
  类型：逻辑型
  意思：是否获取成功
备注：两个参数不能同时使用，也不能同时不使用
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetNetCardList(list<NETCORE_NETHELP_NETCARD> *pSt_ListIFInfo = NULL,LPCTSTR lpszIPAddr = NULL);
/********************************************************************
函数名称：NetCore_SocketHelp_GetNetParam
函数功能：获取网络接口参数
 参数.一：pSt_NetParam
  In/Out：Out
  类型：结构体指针
  可空：N
  意思：导出的网络信息，参考结构体定义
返回值
  类型：逻辑型
  意思：是否成功获取
备注：
*********************************************************************/
extern "C" BOOL NetCore_SocketHelp_GetNetParam(NETCORE_SOCKHELP_NETPARAM *pSt_NetParam);
/************************************************************************/
/*          通信模块导出函数定义                                           */
/************************************************************************/
/************************************************************************
函数名称：NetCore_PipCommunications_ReadCmdReturn
函数功能：读取CMD命令管道返回的内容
参数.一：lpszCmd
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要执行CMD命令
参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出的内容
参数.三：nCountLine
  In/Out：In
  类型：整数型
  可空：Y
  意思：要读取几行,0表示全部读取
参数.四：nReadLen
  In/Out：In
  类型：整数型
  可空：Y
  意思：从第几行开始读取0表示不关心
参数.五：pInt_Len
  类型：整数型指针
  可空：Y
  意思：输出读取到的返回内容大小，可以为NULL，不获取
返回值
  类型：逻辑性
  意思：是否执行成功
备注：
*************************************************************************/
extern "C" BOOL NetCore_PipCommunications_ReadCmdReturn(LPCTSTR lpszCmd,TCHAR *tszOutBuffer,int nCountLine = 0,int nReadLen = 0,int *pInt_Len = NULL);
//---------------------------------匿名管道：
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Create
函数功能：创建匿名管道
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的匿名管道进程名称
返回值
  类型：逻辑型
  意思：是否创建成功
备注：支持子进程通信
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Create(LPCTSTR lpszProcessName);
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Close
函数功能：关闭指定的匿名管道
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要关闭的匿名管道名称
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Close(LPCTSTR lpszProcessName);
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Read
函数功能：读取匿名管道中的数据
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个匿名管道
 参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：读取到的数据缓冲区
 参数.三：pDwLen
  In/Out：Out
  类型：双字指针
  可空：N
  意思：读取到的数据长度
返回值
  类型：逻辑型
  意思：是否有数据成功读取得到
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Read(LPCTSTR lpszProcessName,TCHAR *ptszOutBuffer,DWORD *pDwLen);
/********************************************************************
函数名称：NetCore_PipCommunications_Anonymous_Read
函数功能：读取匿名管道中的数据
 参数.一：lpszProcessName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个匿名管道
 参数.二：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写入的数据缓冲区
 参数.三：pDwLen
  In/Out：In/Out
  类型：双字指针
  可空：N
  意思：要写入的数据长度，导出实际写入的数据长度
返回值
  类型：逻辑型
  意思：数据是否成功写入匿名管道
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Anonymous_Write(LPCTSTR lpszProcessName,LPCTSTR lpszBuffer,DWORD *pDwLen);
//----------------------------------命名管道：
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Create
函数功能：创建命名管道
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的命名管道名称，表示一个文件路径
 参数.二：bIsCallBack
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否通过回调方式来获取数据，默认否
返回值
  类型：逻辑型
  意思：是否创建成功
备注：支持多进程，严格按照FIFO模式读取数据
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Create(LPCTSTR lpszHostName,BOOL bIsCallBack = FALSE);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_SetCallBack
函数功能：设置回调函数
 参数.一：fpCall_Read
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：回调函数地址，导出回调数据
 参数.二：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：N
  意思：回调函数的用户自定义参数
返回值
  类型：逻辑型
  意思：是否设置成功
备注：如果你不打算使用回调函数，可以不调用此函数，如果你像通过回调方式来接受数据，那么请必须调用此函数
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_SetCallBack(CALLBACK_NETCORE_PIPECOMMUNICATIONS_NAMED_READ fpCall_NamedRead,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Close
函数功能：关闭指定的命名管道或者关闭全部命名管道
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果为空，将全部关闭，否则，请指定一个命名管道名称
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Close(LPCTSTR lpszHostName = NULL);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Read
函数功能：读取指定命名管道中的数据
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个命名管道中的数据
 参数.二：ptszOutBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：读取到的数据缓冲区
 参数.三：pdw_OutLen
  In/Out：In/Out
  类型：双字
  可空：N
  意思：输入：要读取的大小，输出，实际读取到的大小
返回值
  类型：逻辑型
  意思：是否成功读取到数据
备注：如果你已经设置了指定的命名管道为回调接受数据，那么你不能调用此函数
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Read(LPCTSTR lpszHostName,TCHAR *ptszOutBuffer,DWORD *pdw_OutLen);
/********************************************************************
函数名称：NetCore_PipCommunications_Named_Write
函数功能：写入数据到指定的命名管道
 参数.一：lpszHostName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写入到哪个命名管道中
 参数.二：ptszOutBuffer
  In/Out：In
  类型：字符指针
  可空：N
  意思：写入到的数据缓冲区
 参数.三：dwMsgLen
  In/Out：In
  类型：双字
  可空：N
  意思：要写入的大小
返回值
  类型：逻辑型
  意思：是否成功写入数据
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_Named_Write(LPCTSTR lpszHostName,LPCTSTR lpszInBuffer,DWORD dwMsgLen);
//-----------------------------------内存共享 IPC:
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryIpc_Create
函数功能：创建一个内存映射-IPC版本
 参数.一：nMemorySize
  In/Out：In
  类型：整数型
  可空：N
  意思：文件内存大小，必须是能被页面内存除余0
 参数.二：lPMemoryAddr
  In/Out：Out
  类型：无类型指针
  可空：N
  意思：导出我们要操作的IPC内存地址。
 参数.三：pInt_MapKey
  In/Out：In/Out
  类型：整数型指针
  可空：Y
  意思：这个值可以表示几个意思，为NULL：不关心这个KEY。其值为0，0表示我们来分配一个KEY，然后导出，或者你指定一个值，如果你指定的值已经存在，那么会打开他！
返回值
  类型：逻辑型
  意思：是否创建成功
备注：你必须自己控制读写锁，在多进程中操作，非进程自身空间，最后一个参数不能为NULL，获取了内存地址后，你可以像操作字符串那样操作这个地址
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryIpc_Create(int nMemorySize,LPVOID lPMemoryAddr,int *pInt_MapKey);
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryIpc_GetSize
函数功能：获取IPC共享内存的指定KEY内存大小
 参数.一：nMapKey
  In/Out：In
  类型：整数型
  可空：N
  意思：要获取的内存索引
 参数.二：pInt_BufferSize
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出获取到的大小
 参数.三：pInt_MapKey
返回值
  类型：逻辑型
  意思：是否成功获取
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryIpc_GetSize(int nMapKey,int *pInt_BufferSize);
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryIpc_Close
函数功能：关闭销毁一个IPC内存映射
 参数.一：pInt_MapKey
  In/Out：In
  类型：整数型指针
  可空：Y
  意思：为NULL：全部关闭，不然其指定一个值
返回值
  类型：逻辑型
  意思：是否删除成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryIpc_Close(int *pInt_MapKey = NULL);
//-----------------------------------内存共享 文件映射:
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryMap_Create
函数功能：创建一个文件内存映射
 参数.一：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要映射的文件
 参数.二：lpMemroyAddr
  In/Out：Out
  类型：无符号指针
  可空：N
  意思：映射到的内存地址
返回值
  类型：逻辑型
  意思：是否创建成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryMap_Create(LPCTSTR lpszFileName,LPVOID lpMemroyAddr);
/********************************************************************
函数名称：NetCore_PipCommunications_MemoryMap_Close
函数功能：卸载一个文件内存映射
 参数.一：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要卸载的文件
 参数.二：lpMemroyAddr
  In/Out：In
  类型：无符号指针
  可空：N
  意思：映射到的内存地址
返回值
  类型：逻辑型
  意思：是否成功卸载
备注：
*********************************************************************/
extern "C" BOOL NetCore_PipCommunications_MemoryMap_Close(LPCTSTR lpszFileName,LPVOID lpMemroyAddr);
/************************************************************************/
/*          原始套机字接口定义                                             */
/************************************************************************/
/************************************************************************
函数名称：NetCore_ICMPRaw_Init
函数功能：初始化一个ICMP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_ICMPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_ICMPRaw_Get
函数功能：设置ICMP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入ICMP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（IP+ICMP）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_ICMPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket,TCHAR *ptszMsgBuffer,int *pInt_Len);
/************************************************************************
函数名称：NetCore_TCPRaw_Init
函数功能：初始化一个TCP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_TCPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_TCPRaw_Get
函数功能：设置TCP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入TCP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（IP+TCP）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：导出的数据只是协议头，不包含附加数据，你需要自己附加数据后发送
************************************************************************/
extern "C" BOOL NetCore_TCPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket,TCHAR *ptszMsgBuffer,int *pInt_Len);
/************************************************************************
函数名称：NetCore_UDPRaw_Init
函数功能：初始化一个UDP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_UDPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_UDPRaw_Get
函数功能：设置UDP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入UDP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（IP+UDP）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_UDPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket,TCHAR *ptszMsgBuffer,int *pInt_Len);
/************************************************************************
函数名称：NetCore_ARPRaw_Init
函数功能：初始化一个ARP原始套接字
 参数.一：phSocket
   In/Out：Out
   类型：套机字句柄
   可空：N
   意思：导出初始化成功的套接字句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己CLOSE，你需要调用sendto和recvmsg来发送和接受数据
************************************************************************/
extern "C" BOOL NetCore_ARPRaw_Init(SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_ARPRaw_Get
函数功能：设置ARP协议属性并且获取设置好的协议缓冲区
 参数.一：pSt_RawSocket
   In/Out：In
   类型：数据结构指针
   可空：N
   意思：输入ARP套接字的属性
 参数.二：ptszMsgBuffer
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出设置号的协议头缓冲区（以太网头+ARP协议）
 参数.三：pInt_Len
   In/Out：Out
   类型：整数型指针
   可空：N
   意思：导出的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_ARPRaw_Get(NETCORE_RAWSOCKET_NETPARAM *pSt_RawSocket,TCHAR *ptszMsgBuffer,int *pInt_Len);
/************************************************************************/
/*          串口函数导出定义                                              */
/************************************************************************/
/********************************************************************
函数名称：NetCore_SerialPort_OpenDev
函数功能：初始化异步串口操作模块
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要初始化哪个串口号
 参数.二：dwBaudRate
  In/Out：In
  类型：双字
  可空：Y
  意思：波特率，默认2400
 参数.三：byParity
  In/Out：In
  类型：无符号字符
  可空：Y
  意思：串口事件，默认无事件
 参数.四：byStopBits
  In/Out：In
  类型：无符号字符
  可空：Y
  意思：停止位，默认1个停止位
 参数.五：byByteSize
  In/Out：In
  类型：无符号字符
  可空：Y
  意思：初始化大小，数据大小，默认8
 参数.六：fpCall_SerialPortRecv
  In/Out：Out
  类型：回调函数
  可空：Y
  意思：回调函数地址，可空，如果空将通过主动方式接受数据
 参数.七：lParam
  In/Out：In
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_OpenDev(LPCTSTR lpszComPort,unsigned int dwBaudRate = 2400,BYTE byParity = NETENGINE_NETCORE_SERIALPORT_PARITYNONE,unsigned int byStopBits = 1,unsigned int byByteSize = 8,CALLBACK_NETCORE_SERIALPORT_RECV fpCall_SerialPortRecv = NULL,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_SerialPort_CloseDev
函数功能：关闭指定串口
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：要关闭的串口号，如果为NULL，关闭所有此模块保存的串口表
返回值
  类型：逻辑型
  意思：是否成功关闭
备注：不在使用串口的时候可以关闭它。不然会造成下次使用出现问题
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_CloseDev(LPCTSTR lpszComPort = NULL);
/********************************************************************
函数名称：NetCore_SerialPort_SendData
函数功能：异步发送串口数据
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据字符串指针
 参数.二：byDates
  In/Out：In
  类型：无符号字节型指针
  可空：N
  意思：要发送的数据
 参数.三：nWriteLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：如果错误你需要获取错误码来知道为什么错误
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_SendData(LPCTSTR lpszComPort,BYTE *byDates,int nWriteLen);
/********************************************************************
函数名称：NetCore_SerialPort_RecvData
函数功能：异步读取数据
 参数.一：lpszComPort
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要读取哪个串口的数据
 参数.二：byDates
  In/Out：Out
  类型：无符号字符指针
  可空：N
  意思：导出读取到的数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入要读取多大的数据，输出读取到的实际数据大小
返回值
  类型：逻辑型
  意思：是否读取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_SerialPort_RecvData(LPCTSTR lpszComPort,BYTE *byDates,int *pInt_Len);
/************************************************************************
函数名称：NetCore_SerialPort_IsOpenDev
函数功能：串口是否被打开
  参数一：lpszComPort
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：串口号
返回值
  类型：逻辑型
  意思：假为打开，真为没有打开
备注：
************************************************************************/
extern "C" BOOL NetCore_SerialPort_IsOpenDev(LPCTSTR lpszComPort);
/************************************************************************/
/*          Select TCP服务器函数导出定义                                   */
/************************************************************************/
/************************************************************************
函数名称：NetCore_TCPSelect_Start
函数功能：初始化这个选择模型并且启动这个服务
  参数一：nPort
   In/Out：In
   可空：N
   意思：要绑定的端口
  参数二：nTimeOut
   In/Out：In
   可空：Y
   意思：超时时间设置。默认0.1秒
  参数三：bKeepAlive
   In/Out：In
   可空：Y
   意思：超时时间设置，默认不设置
返回值
  类型：BOOL
  意思：是否启动成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_Start(int nPort,int nTimeOut = 100,BOOL bKeepAlive = FALSE);
/************************************************************************
函数名称：NetCore_TCPSelect_Send
函数功能：异步IO发送数据给客户端
 参数.一：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的客户端，如果为NULL，那么表示发送给所有客户，如果不为NULL，那么发送给指定用户
 参数.二：lpszBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：发送缓冲区长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_Send(LPCTSTR lpszAddr,LPCTSTR SendBuf,int BufLen);
/************************************************************************
函数名称：NetCore_TCPSelect_SetCallBack
函数功能：用户注册函数，如果你不关心这些事件，可以不用管！
  参数一：fpCall_Login
   In/Out：In
   类型：回调函数
   可空：N
   意思：用户进入事件
  参数二：fpCallS_Recv
   In/Out：In
   类型：回调函数
   可空：N
   意思：接受到数据事件
  参数三：fpCall_Leave
   In/Out：In
   类型：回调函数
   可空：N
   意思：用户离开事件
  参数四：lPLogin
   In/Out：In
   类型：无类型指针
   可空：N
   意思：用户登录回调函数自定义参数
  参数五：lPRecv
   In/Out：In
   类型：无类型指针
   可空：N
   意思：接受数据自定义参数
  参数六：lPLeave
   In/Out：In
   类型：无类型指针
   可空：N
   意思：用户离开自定义参数
返回值
  类型：无
  意思：
备注：在服务器启动的时候，你应该主动调用这个函数来处理事件，如果你不想通过回调的方式获取这些事件
      也就是说不想通过异步方式，那么你可以调用读取IO事件的函数来主动获取下面的事件
************************************************************************/
extern "C" void NetCore_TCPSelect_SetCallBack(CALLBACK_NETCORE_TCP_SERVER_SELECT_LOGIN fpCall_Login,CALLBACK_NETCORE_TCP_SERVER_SELECT_RECV fpCallS_Recv,CALLBACK_NETCORE_TCP_SERVER_SELECT_LEAVE fpCall_Leave,LPVOID lPConnect = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/************************************************************************
函数名称：NetCore_TCPSelect_stop
函数功能：停止选择模型服务器
返回值
  类型：逻辑型
  意思：是否成功停止
备注：只会返回真
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_stop();
/************************************************************************
函数名称：NetCore_TCPSelect_GetClientCount
函数功能：获取客户端链接数量
返回值
  类型：整数型
  意思：返回获取到的客户数量
备注：
************************************************************************/
extern "C" int NetCore_TCPSelect_GetClientCount();
/************************************************************************
函数名称：NetCore_TCPSelect_GetFlow
函数功能：获取服务器发送和接受到的流量，单位字节
  参数一：pdwFlow_Up
   In/Out：Out
   类型：指向无符号长整型的指针
   可空：N
   意思：上传的流量
  参数二：pdwFlow_Down
   In/Out：Out
   类型：指向无符号长整型的指针
   可空：N
   意思：下载的流量
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" int NetCore_TCPSelect_GetFlow(DWORD *pdwFlow_Up,DWORD *pdwFlow_Down);
/********************************************************************
函数名称：NetCore_TCPSelect_RemoveClient
函数功能：移除一个指定的客户
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要移除的客户IP地址+端口 ip:port
返回值
  类型：逻辑型
  意思：是否成功移除
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_RemoveClient(LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：NetCore_TCPSelect_ReadIOEvent
函数功能：读取IO事件，主动模式
 参数.一：ptszAddr
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：导出发生事件的客户地址,输入：只有在接受数据事件才有用，表示查找这个客户地址的第一个数据
 参数.二：ptszBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出事件消息内容，只有RECV事件有效
 参数.三：pInt_MsgLen
  In/Out：Out
  类型：整数型
  可空：N
  意思：导出消息长度
 参数.四：pdwEvent
  In/Out：In/Out
  类型：双字
  可空：N
  意思：输入 要获取的事件类型，可以是ALL，ALL表示所有类型，那么将弹出事件驱动表中第一个事件，输出，如果是ALL，那么输出这个事件是什么事件
        当然，你也可以输入指定的事件类型，那么这个参数导出将没有作用
返回值
  类型：逻辑型
  意思：是否成功获取
备注：如果设置了回调，那么此函数将失效，就算你使用主动方式获取IO网络事件，网络内部也不会阻塞。请放心使用
      但是你也要及时处理，因为内部数据过多会造成服务器内存占用过大
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_ReadIOEvent(TCHAR *ptszAddr,TCHAR *ptszBuffer,int *pInt_MsgLen,DWORD *pdwEvent);
/********************************************************************
函数名称：NetCore_TCPSelect_ConvertToNetAddr
函数功能：把指定IP地址转换为网络地址
 参数.一：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要转换的IP地址
 参数.二：pdw_Addr
  In/Out：Out
  类型：双字指针
  可空：N
  意思：导出转换成功后的网络地址
返回值
  类型：逻辑型
  意思：是否转换成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_ConvertToNetAddr(LPCTSTR lpszAddr,DWORD *pdw_Addr);
/********************************************************************
函数名称：NetCore_TCPSelect_GetAddrInfo
函数功能：获取服务器地址信息
 参数.一：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出绑定的IP地址
 参数.二：pInt_Port
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出绑定的端口号码
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPSelect_GetAddrInfo(TCHAR *ptszClientAddr,int *pInt_Port);
//--------扩展SELECT TCP服务器函数-支持多个服务器，参数意思和上面的一样，多个了句柄参数而已
extern "C" BOOL NetCore_TCPSelect_StartEx(XNETHANDLE *pxNetId,int nPort,int nTimeOut = 100,BOOL bKeepAlive = FALSE);
extern "C" BOOL NetCore_TCPSelect_SendEx(XNETHANDLE xNetId,LPCTSTR lpszAddr,LPCTSTR lpszBuffer,int nLen);
extern "C" BOOL NetCore_TCPSelect_SendAllEx(XNETHANDLE xNetId,LPCTSTR lpszBuffer,int nLen);
extern "C" BOOL NetCore_TCPSelect_GetFlowEx(XNETHANDLE xNetId,DWORD *pdwFlow_Up,DWORD *pdwFlow_Down);
extern "C" BOOL NetCore_TCPSelect_RemoveClientEx(XNETHANDLE xNetId,LPCTSTR lpszClientAddr);
extern "C" BOOL NetCore_TCPSelect_ReadIOEventEx(XNETHANDLE xNetId,TCHAR *ptszAddr,TCHAR *ptszBuffer,int *pInt_MsgLen,DWORD *pdwEvent);
extern "C" BOOL NetCore_TCPSelect_StopEx(XNETHANDLE xNetId,BOOL bIsClearFlow = TRUE);
extern "C" BOOL NetCore_TCPSelect_GetAddrInfoEx(XNETHANDLE xNetId,TCHAR *ptszClientAddr,int *pInt_Port);
extern "C" int NetCore_TCPSelect_GetClientCountEx(XNETHANDLE xNetId);
extern "C" void NetCore_TCPSelect_SetCallBackEx(XNETHANDLE xNetId,CALLBACK_NETCORE_TCP_SERVER_SELECT_LOGIN fpCall_Login,CALLBACK_NETCORE_TCP_SERVER_SELECT_RECV fpCall_Recv,CALLBACK_NETCORE_TCP_SERVER_SELECT_LEAVE fpCall_Leave,LPVOID lPLogin = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
extern "C" BOOL NetCore_TCPSelect_ConvertToNetAddrEx(XNETHANDLE xNetId,LPCTSTR lpszAddr,DWORD *pdw_Addr);
/************************************************************************/
/*          TCP轻量级服务函数导出定义                                       */
/************************************************************************/
/************************************************************************
函数名称：NetCore_TCPSelect_ShortStart
函数功能：启动一个轻量级服务器
  参数一：nPort
   In/Out：In
   类型：整数型
   可空：N
   意思：要绑定的端口号
  参数二：lpCall_TcpSConnect
   In/Out：In
   类型：回调函数
   可空：N
   意思：导出用户发送过来的数据
  参数三：lParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：自定义数据
  参数四：bNotify
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否仅仅通知数据到达,如果是,那么将又你自己recv数据
返回值
  类型：逻辑型
  意思：是否启动成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortStart(int nPort,CALLBACK_NETCORE_TCP_SERVER_SELECT_SCONNECT lpCall_TcpSConnect,LPVOID lParam = NULL,BOOL bNotify = FALSE);
/************************************************************************
函数名称：NetCore_TCPSelect_ShortSend
函数功能：发送一段数据
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要给哪个客户端发送数据
  参数二：lpszMsgBuffer
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要发送的数据缓冲区
  参数三：nMsgLen
   In/Out：In
   类型：整数型
   可空：N
   意思：要发送数据的大小
返回值
  类型：逻辑型
  意思：是否发送成功
备注：发送完毕后客户端会被关闭，资源会被销毁
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortSend(LPCTSTR lpszClientAddr,LPCTSTR lpszMsgBuffer,int nMsgLen);
/************************************************************************
函数名称：NetCore_TCPSelect_ShortClose
函数功能：关闭客户端
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要关闭的客户端
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortClose(LPCTSTR lpszClientAddr);
/************************************************************************
函数名称：NetCore_TCPSelect_ShortDestroy
函数功能：销毁短连接服务器
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPSelect_ShortDestroy();
/********************************************************************
函数名称：NetCore_TCPSelect_ShortGetSocket
函数功能：获得监听句柄
返回值
  类型：套接字句柄
  意思：返回套接字监听句柄
备注：
*********************************************************************/
extern "C" SOCKET NetCore_TCPSelect_ShortGetSocket();
/************************************************************************/
/*          POLL服务器函数导出定义                                        */
/************************************************************************/
/************************************************************************
函数名称：NetCore_TCPPoll_Start
函数功能：初始化这个选择模型并且启动这个服务
  参数一：nPort
   In/Out：In
   类型：整数型
   可空：N
   意思：要绑定的端口
  参数二：bKeepAlive
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否开启保活计时器，默认不开启
返回值
  类型：逻辑型
  意思：是否启动成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPPoll_Start(int nPort,BOOL bKeepAlive = FALSE);
/************************************************************************
函数名称：NetCore_TCPPoll_Send
函数功能：为指定客户发送数据
  参数一：lpszAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：发送的客户地址
  参数二：lpszSendBuf
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要发送的数据缓冲区地址
  参数三：pnBufLen
   In/Out：In/Out
   类型：整数指针
   可空：N
   意思：输入：需要发送的数据长度，输出：发送成功的数据长度
返回值
  类型：逻辑型
  意思：是否成功停止
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPPoll_Send(LPCTSTR lpszAddr,LPCTSTR lpszSendBuf,int *pnBufLen);
/************************************************************************
函数名称：NetCore_TCPPoll_Stop
函数功能：关闭POLL服务器
  参数一：bIsClearFLow
   In/Out：In
   类型：逻辑型
   可空：N
   意思：是否清空上传下载流量
返回值
  类型：逻辑型
  意思：是否成功停止
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPPoll_Stop(BOOL bIsClearFLow = TRUE);
/************************************************************************
函数名称：NetCore_TCPPoll_GetNetFlow
函数功能：关闭POLL服务器
  参数一：pdwUp
   In/Out：Out
   类型：双字
   可空：N
   意思：服务器发送流量
  参数二：pdwDown
   In/Out：Out
   类型：双字
   可空：N
   意思：服务器接受到的流量
返回值
  类型：逻辑型
  意思：是否成功获取
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPPoll_GetNetFlow(DWORD *pdwUp,DWORD *pdwDown);
/************************************************************************
函数名称：Poll_LPCallBack
函数功能：注册服务器回调事件
  参数一：fpCall_PollLogin
   In/Out：In/Out
   类型：函数指针
   可空：N
   意思：客户连接的事件
  参数二：fpCall_PollRecv
   In/Out：In/Out
   类型：函数指针
   可空：N
   意思：服务器接受到的数据
  参数三：fpCall_PollLeave
   In/Out：In/Out
   类型：函数指针
   可空：N
   意思：客户离开的事件
  参数四：lPLogin
   In/Out：In
   类型：无类型指针
   可空：N
   意思：客户连接回调函数参数
  参数五：lPRecv
   In/Out：In
   类型：无类型指针
   可空：N
   意思：数据到达回调函数参数
  参数六：lPLeave
   In/Out：In
   类型：无类型指针
   可空：N
   意思：客户离开回调函数参数
返回值
  类型：逻辑型
  意思：是否成功获取
备注：
************************************************************************/
extern "C" void NetCore_TCPPoll_SetCallBack(CALLBACK_NETCORE_TCP_SERVER_POLL_LOGIN fpCall_PollLogin,CALLBACK_NETCORE_TCP_SERVER_POLL_LEAVE fpCall_PollLeave,CALLBACK_NETCORE_TCP_SERVER_POLL_RECV fpCall_PollRecv,LPVOID lPLogin = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/************************************************************************/
/*          EPOLL TCP服务器函数导出定义                                                                       */
/************************************************************************/
/********************************************************************
函数名称：NetCore_TCPEPoll_Start
函数功能：初始化这个EPOLL模型并且启动这个服务
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：要绑定的服务器端口
 参数.二：nMaxClient
  In/Out：In
  类型：整数型
  可空：Y
  意思：最大允许的客户端数量
 参数.三：nThreads
  In/Out：In
  类型：整数型
  可空：Y
  意思：设置网络事件线程池启动数量,0将根据CPU个数启动
 参数.四：bKeepAlive
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否开启TCP心跳功能,默认不开启
返回值
  类型：逻辑型
  意思：是否启动成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_TCPEPoll_StartEx(XNETHANDLE *pxhNet,int nPort = 5000,int nMaxClient = 10000,int nThreads = 0,BOOL bKeepAlive = FALSE);
/************************************************************************
函数名称：NetCore_TCPEPoll_Destroy
函数功能：停止EPOLL服务器
  参数一：bIsClearFlow
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否清楚流量信息，默认清除
返回值
  类型：逻辑型
  意思：是否停止成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_DestroyEx(XNETHANDLE xhNet,BOOL bIsClearFlow = TRUE);
/************************************************************************
函数名称：NetCore_TCPEPoll_Send
函数功能：发送数据给客户端
  参数一：lpszSendAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要发送的地址，格式：IP:PORT
  参数二：lpszSendMsg
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：发送缓冲区，要发送的数据
  参数三：nMsgLen
   In/Out：In
   类型：整数型
   可空：N
   意思：发送的数据缓冲区长度
返回值
  类型：逻辑型
  意思：是否投递成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_SendEx(XNETHANDLE xhNet,LPCTSTR lpszSendAddr,LPCTSTR lpszSendMsg,int nMsgLen);
/************************************************************************
函数名称：NetCore_TCPEPoll_SendAll
函数功能：发送数据给所有客户端
  参数.一：lpszSendMsg
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：发送缓冲区，要发送的数据
  参数.二：nMsgLen
   In/Out：In
   类型：整数型
   可空：N
   意思：发送的数据缓冲区长度
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_SendAllEx(XNETHANDLE xhNet,LPCTSTR lpszSendMsg,int nMsgLen);
/************************************************************************
函数名称：NetCore_TCPEPoll_GetAll
函数功能：获取所有客户端列表
  参数.一：pStl_ListClient
   In/Out：Out
   类型：LIST容器指针
   可空：N
   意思：导出获取到的客户端地址列表
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_GetAllEx(XNETHANDLE xhNet,list<tstring> *pStl_ListClient);
/************************************************************************
函数名称：NetCore_TCPEPoll_CloseForClient
函数功能：强制关闭一个已建立连接的用户
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要关闭的客户地址信息
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_CloseForClientEx(XNETHANDLE xhNet,LPCTSTR lpszClientAddr);
/************************************************************************
函数名称：NetCore_TCPEPoll_GetFlow
函数功能：获取服务器网络流量信息
  参数一：pullFlow_Up
   In/Out：Out
   类型：四字
   可空：N
   意思：服务器上传的流量
  参数二：pullFlow_Down
   In/Out：Out
   类型；四字
   可空：N
   意思：服务器接受的流量信息
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_GetFlowEx(XNETHANDLE xhNet,DWORD64 *pullFlow_Up,DWORD64 *pullFlow_Down);
/************************************************************************
函数名称：NetCore_TCPEPoll_SetStatus
函数功能：设置客户端状态
 参数.一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：客户端地址
 参数.二：bIsBreak
   In/Out：In
   类型；逻辑型
   可空：N
   意思：为真设置客户端跳过，不处理发送和接受，为假不跳过
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_SetStatusEx(XNETHANDLE xhNet,LPCTSTR lpszClientAddr,BOOL bIsBreak);
/************************************************************************
函数名称：NetCore_TCPePoll_SetCallBack
函数功能：注册数据处理回调函数
  参数一：fpCallePoll_Login
   In/Out：In/Out
   类型：回调函数
   可空：N
   意思：用户登录事件回调
  参数二：fpCallePoll_Recv
   In/Out：In/Out
   类型：回调函数指针
   可空：N
   意思：收到用户发送数据事件回调
  参数三：fpCallePoll_Leave
   In/Out：In/Out
   类型：回调函数
   可空：N
   意思：用户离开事件回调
  参数四：lLoginParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：用户登录回调自定义参数
  参数五：lRecvParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：收到数据回调函数自定义参数
  参数六：lLeaveParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：用户离开回调自定义参数
返回值
  类型：逻辑型
  意思：是否成功注册
备注：必须调用，在开启之前
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_RegisterCallBackEx(XNETHANDLE xhNet,CALLBACK_NETCORE_TCP_SERVER_EPOLL_LOGIN fpCallePoll_Login,CALLBACK_NETCORE_TCP_SERVER_EPOLL_RECV fpCallePoll_Recv,CALLBACK_NETCORE_TCP_SERVER_EPOLL_LEAVE fpCallePoll_Leave,LPVOID lPLogin = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/************************************************************************
函数名称：NetCore_TCPEPoll_GetAddrForSocket
函数功能：通过SOCKET找到IP地址信息
  参数一：hSocket
   In/Out：In
   类型：套接字句柄
   可空：N
   意思：要获取的套接字
  参数二：ptszClientAddr
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出客户端的地址
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_GetAddrForSocketEx(XNETHANDLE xhNet,SOCKET hSocket,TCHAR *ptszClientAddr);
/************************************************************************
函数名称：NetCore_TCPEPoll_GetSocketForAddr
函数功能：通过IP地址查找对应的用户套接字
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：用户地址信息
  参数二：phSocket
   In/Out：Out
   类型：套节字句柄
   可空：N
   意思：导出获取到的套节字
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_GetSocketForAddrEx(XNETHANDLE xhNet,LPCTSTR lpszClientAddr,SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_TCPEPoll_GetRecvTime
函数功能：获取一个客户端距离上次接受数据相差多少秒
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：用户地址
 参数.二：pInt_Timer
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出获取的相差时间，如果是0,表示还没有接受过数据
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_TCPEPoll_GetRecvTimeEx(XNETHANDLE xhNet,LPCTSTR lpszClientAddr,int *pInt_Timer);
/******************************************************************************
                                 调度UDP导出函数
******************************************************************************/
/************************************************************************/
/*          SELECT UDP服务器函数导出定义                                    */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UDPSelect_Init
函数功能：初始化一个UDP服务或者客户端
 参数.一：pxhNet
  In/Out：Out
  类型：句柄
  可空：N
  意思：导出初始化成功的UDP服务句柄
 参数.二：nBindPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：是否需要提供一个服务，默认不需要
 参数.三：bIsCall
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：设置启动模式，直接回调还是主动接受
 参数.四：fpCall_UDPEvent
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：输入数据接受回调函数地址
 参数.五：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功
备注：模式设置必须要回调函数有值才有作用
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_Init(XNETHANDLE *pxhNet,int nBindPort = 0,BOOL bIsCall = FALSE,CALLBACK_NETCORE_UDP_SERVER_SELEC_NETEVENT fpCall_UDPEvent = NULL,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_UDPSelect_SendTo
函数功能：发送一条UDP数据给指定的服务器
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要发送的缓冲区数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型
  可空：N
  意思：输入要发送数据大小，输出真实发送数据大小
 参数.四：lpszSendAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要发送到的UDP服务地址
 参数.五：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要发送数据到的UDP服务器端口
返回值
  类型：逻辑型
  意思：是否成功
备注：你需要自己判断实际发送大小是否等于需要发送大小
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_SendTo(XNETHANDLE xhNet,LPCTSTR lpszMsgBuffer,int *pInt_Len,LPCTSTR lpszSendAddr,int nPort);
/********************************************************************
函数名称：NetCore_UDPSelect_Recv
函数功能：接受一条UDP数据
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
 参数.四：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出接受数据的UDP发送过来的地址和端口
 参数.三：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出接受到的数据缓冲区
 参数.四：pInt_Len
  In/Out：In/Out
  类型：整数型
  可空：N
  意思：输入要接受数据大小，输出真实接受数据大小
 参数.五：bSelect
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否启用SELECT判断,默认不使用
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_Recv(XNETHANDLE xhNet,TCHAR *ptszClientAddr,TCHAR *ptszMsgBuffer,int *pInt_Len,BOOL bSelect = FALSE);
/********************************************************************
函数名称：NetCore_UDPSelect_Stop
函数功能：停止一个UDP服务
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_Stop(XNETHANDLE xhNet);
/********************************************************************
函数名称：NetCore_UDPSelect_Stop
函数功能：停止一个UDP服务
 参数.一：xhNet
  In/Out：In
  类型：句柄
  可空：N
  意思：输入初始化成功的UDP句柄
 参数.二：bIsCall
  In/Out：In
  类型：逻辑型
  可空：N
  意思：输入要设置的模式，真为被动回调，假为主动接受
返回值
  类型：逻辑型
  意思：是否成功
备注：这个函数的作用必须是初始化设置了回调函数地址才有作用
*********************************************************************/
extern "C" BOOL NetCore_UDPSelect_SetMode(XNETHANDLE xhNet,BOOL bIsCall = TRUE);
/************************************************************************/
/*          EPOLL UDP服务器函数导出定义                                    */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UDPEPoll_Start
函数功能：启动一个EPOLL服务器
 参数.一：nPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：要绑定的服务器端口
 参数.二：nThreads
  In/Out：In
  类型：整数型
  可空：Y
  意思：要设置网络事件处理线程数量,0为使用CPU个数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDPEPoll_StartEx(XNETHANDLE *pxhNet, int nListenPort,int nThread = 0);
/************************************************************************
函数名称：NetCore_UDPEPoll_Destroy
函数功能：停止EPOLL服务器
  参数一：bIsClearFlow
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否清楚流量信息，默认清除
返回值
  类型：逻辑型
  意思：是否停止成功
备注：
************************************************************************/
extern "C" BOOL NetCore_UDPEPoll_DestroyEx(XNETHANDLE xhNet,BOOL bIsClearFlow = TRUE);
/************************************************************************
函数名称：NetCore_UDPEPoll_SendMsg
函数功能：发送数据给客户端
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要发送的地址，格式：IP:PORT
  参数二：lpszMsgBuffer
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：发送缓冲区，要发送的数据
  参数三：pInt_Len
   In/Out：In/Out
   类型：整数型指针
   可空：N
   意思：输入发送的数据缓冲区长度，输出真实发送数据长度
返回值
  类型：逻辑型
  意思：是否发送数据成功
备注：
************************************************************************/
extern "C" BOOL NetCore_UDPEPoll_SendMsgEx(XNETHANDLE xhNet,LPCTSTR lpszClientAddr,LPCTSTR lpszMsgBuffer,int *pInt_Len);
/************************************************************************
函数名称：NetCore_UDPEPoll_GetFlow
函数功能：获取服务器网络流量信息
  参数一：pdwFlow_Up
   In/Out：Out
   类型：双字
   可空：N
   意思：服务器上传的流量
  参数二：pdwFlow_Down
   In/Out：Out
   类型；双字
   可空：N
   意思：服务器接受的流量信息
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_UDPEPoll_GetFlowEx(XNETHANDLE xhNet,DWORD64 *pdwFlow_Up,DWORD64 *pdwFlow_Down);
/************************************************************************
函数名称：NetCore_UDPEPoll_RegisterCallBackEx
函数功能：注册数据处理回调函数
  参数一：fpCallePoll_Recv
   In/Out：In/Out
   类型：回调函数指针
   可空：N
   意思：收到用户发送数据事件回调
  参数二：lRecv
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：收到数据回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否成功注册
备注：开启服务器成功后，立即调用此函数
************************************************************************/
extern "C" BOOL NetCore_UDPEPoll_RegisterCallBackEx(XNETHANDLE xhNet,CALLBACK_NETCORE_UDP_SERVER_EPOLL_RECV fpCallePoll_Recv,LPVOID lRecv = NULL);
/************************************************************************/
/*          SCTP服务器函数导出定义                                         */
/************************************************************************/
/************************************************************************
函数名称：NetCore_SCTP_Start
函数功能：初始化这个SCTP服务器模型并且启动这个服务
 参数一：nPort
  In/Out：In
  类型：整数型
  可空：Y
  意思：端口，默认5000
 参数二：nMaxClient
  In/Out：In
  类型；整数型
  可空：Y
  意思：最大客户数量，默认10000
 参数三：nStreamChannel
  In/Out：In
  类型；整数型
  可空：Y
  意思：最多允许数据类型通道个数，1-10
 参数.四：nThreads
  In/Out：In
  类型：整数型
  可空：Y
  意思：设置网络事件线程池启动数量,0将根据CPU个数启动
返回值
  类型：逻辑型
  意思：是否启动成功
备注：nStreamChannel 将在你发送和接受数据的时候用到
************************************************************************/
extern "C" BOOL NetCore_SCTP_Start(int nPort = 5000,int nMaxClient = 10000,int nStreamChannel = 5,int nThreads = 0);
/************************************************************************
函数名称：NetCore_SCTP_Destroy
函数功能：停止SCTP服务器
  参数一：bIsClearFlow
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否清楚流量信息，默认清除
返回值
  类型：逻辑型
  意思：是否停止成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_Destroy(BOOL bIsClearFlow = TRUE);
/************************************************************************
函数名称：NetCore_SCTP_SendMsg
函数功能：发送数据给客户端
  参数一：lpszSendAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要发送的地址，格式：IP:PORT
  参数二：lpszSendMsg
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：发送缓冲区，要发送的数据
  参数三：nMsgLen
   In/Out：In
   类型：整数型
   可空：N
   意思：发送的数据缓冲区长度
  参数四：bAck
   In/Out：In
   类型：逻辑型
   可空：Y
   意思：是否使用有序发送相当于TCP
  参数四：nStreamChannel
   In/Out：In
   类型：整数型
   可空：Y
   意思：这个数据在通道的哪一个，不能大于等于设置的通道
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_SendMsg(LPCTSTR lpszSendAddr,LPCTSTR lpszSendMsg,int nMsgLen,BOOL bAck = TRUE,int nStreamChannel = 1);
/************************************************************************
函数名称：NetCore_SCTP_CloseClient
函数功能：强制关闭一个已建立连接的用户
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：要关闭的客户地址信息
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_CloseClient(LPCTSTR lpszClientAddr);
/************************************************************************
函数名称：NetCore_SCTP_GetFlow
函数功能：获取服务器网络流量信息
  参数一：pullFlow_Up
   In/Out：Out
   类型：四字
   可空：N
   意思：服务器上传的流量
  参数二：pullFlow_Down
   In/Out：Out
   类型；四字
   可空：N
   意思：服务器接受的流量信息
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_GetFlow(DWORD64 *pullFlow_Up,DWORD64 *pullFlow_Down);
/************************************************************************
函数名称：NetCore_SCTP_SetCallBack
函数功能：注册数据处理回调函数
  参数一：fpCall_SCTPLogin
   In/Out：In/Out
   类型：回调函数
   可空：N
   意思：用户登录事件回调
  参数二：fpCall_SCTPRecv
   In/Out：In/Out
   类型：回调函数指针
   可空：N
   意思：收到用户发送数据事件回调
  参数三：fpCall_SCTPLeave
   In/Out：In/Out
   类型：回调函数
   可空：N
   意思：用户离开事件回调
  参数四：lLoginParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：用户登录回调自定义参数
  参数五：lRecvParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：收到数据回调函数自定义参数
  参数六：lLeaveParam
   In/Out：In
   类型：无类型指针
   可空：Y
   意思：用户离开回调自定义参数
返回值
  类型：逻辑型
  意思：是否成功注册
备注：必须调用，在开启之前
************************************************************************/
extern "C" BOOL NetCore_SCTP_RegisterCallBack(CALLBACK_NETCORE_SCTP_SERVER_LOGIN fpCall_SCTPLogin,CALLBACK_NETCORE_SCTP_SERVER_RECV fpCall_SCTPRecv,CALLBACK_NETCORE_SCTP_SERVER_LEAVE fpCall_SCTPLeave,LPVOID lPLogin = NULL,LPVOID lPRecv = NULL,LPVOID lPLeave = NULL);
/************************************************************************
函数名称：NetCore_SCTP_GetAddrForSocket
函数功能：通过SOCKET找到IP地址信息
  参数一：hSocket
   In/Out：In
   类型：套接字句柄
   可空：N
   意思：要获取的套接字
  参数二：ptszClientAddr
   In/Out：Out
   类型：字符指针
   可空：N
   意思：导出客户端的地址
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_GetAddrForSocket(SOCKET hSocket,TCHAR *ptszClientAddr);
/************************************************************************
函数名称：NetCore_SCTP_GetSocketForAddr
函数功能：通过IP地址查找对应的用户套接字
  参数一：lpszClientAddr
   In/Out：In
   类型：常量字符指针
   可空：N
   意思：用户地址信息
  参数二：phSocket
   In/Out：Out
   类型：套节字句柄
   可空：N
   意思：导出获取到的套节字
返回值
  类型：逻辑型
  意思：是否获取成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_GetSocketForAddr(LPCTSTR lpszClientAddr,SOCKET *phSocket);
/************************************************************************
函数名称：NetCore_SCTP_GetRecvTime
函数功能：获取一个客户端距离上次接受数据相差多少秒
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：用户地址
 参数.二：pInt_Timer
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：导出获取的相差时间，如果是0,表示还没有接受过数据
返回值
  类型：逻辑型
  意思：是否成功
备注：
************************************************************************/
extern "C" BOOL NetCore_SCTP_GetRecvTime(LPCTSTR lpszClientAddr,int *pInt_Timer);
/************************************************************************/
/*                      网络套接字高级操作导出函数                          */
/************************************************************************/
/*                      组播通信函数导出                                 */
/********************************************************************
函数名称：NetCore_GroupCast_SDCreate
函数功能：创建一个发送者组播
 参数.一：phSocket
  In/Out：Out
  类型：网络套接字
  可空：N
  意思：导出创建好的组播发送套接字
 参数.二：lpszSendAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送到的组播地址，搜索获得用户可使用的组播地址范围
 参数.三：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送到的端口
 参数.四：nTTL
  In/Out：In
  类型：整数型
  可空：N
  意思：要设置组播跳转的TTL值，可以为空，不设置，采用默认
 参数.五：bLoop
  In/Out：In
  类型：整数型
  可空：Y
  意思：是否把数据发送给本地回环网络。Windows下这个参数无效
返回值
  类型：逻辑型
  意思：是否创建成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_SDCreate(SOCKET *phSocket,LPCTSTR lpszSendAddr,int nPort,int nTTL = 0,BOOL bLoop = TRUE);
/********************************************************************
函数名称：NetCore_GroupCast_RVCreate
函数功能：创建一个接受组播服务
 参数.一：phSocket
  In/Out：Out
  类型：网络套接字
  可空：N
  意思：导出创建好的组播套接字
 参数.二：lpszRecvAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要接受的组播地址，必须和发送创建的一样的地址
 参数.三：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要接受到的端口，和发送者一样的端口
 参数.四：bLoop
  In/Out：In
  类型：整数型
  可空：N
  意思：是否接受回环数据。Linux下这个参数无效
返回值
  类型：逻辑型
  意思：是否创建成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_RVCreate(SOCKET *phSocket,LPCTSTR lpszRecvAddr,int nPort,BOOL bLoop = TRUE);
/********************************************************************
函数名称：NetCore_GroupCast_SDend
函数功能：发送者发送消息
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：操作哪个组播套接字
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送的消息
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：发送消息的长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_SDend(SOCKET hSocket,LPCTSTR lpszMsgBuffer,int nLen);
/********************************************************************
函数名称：NetCore_GroupCast_RVecv
函数功能：接收者接受组播消息
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：操作哪个组播套接字
 参数.二：ptszMsgBuffer
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：要接受的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出接受到的数据缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功接受
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_RVecv(SOCKET hSocket,TCHAR *ptszMsgBuffer,int *pInt_Len);
/********************************************************************
函数名称：NetCore_GroupCast_Close
函数功能：关闭一个组播服务
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：要关闭哪个组播
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_GroupCast_Close(SOCKET hSocket);
/*                      广播通信函数导出定义                               */
/********************************************************************
函数名称：NetCore_BroadCast_SendInit
函数功能：初始化发送端
 参数.一：phSocket
  In/Out：Out
  类型：套接字指针
  可空：N
  意思：导出的套接字
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送端口
 参数.三：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：如果有多张网卡，那么需要指定哪张网卡IP地址发送广播包
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_SendInit(SOCKET *phSocket,int nPort,LPCTSTR lpszAddr = NULL);
/********************************************************************
函数名称：NetCore_BroadCast_Send
函数功能：发送广播消息
 参数.一：hSocket
  In/Out：In
  类型：套接字
  可空：N
  意思：要给哪个套接字发送数据
 参数.二：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的内容
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送的长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：UDP发送方式，如果超过MTU，会被分片，建议大数据包使用组包起模块配合使用.
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_Send(SOCKET hSocket,LPCTSTR lpszSendMsg,int nLen);
/********************************************************************
函数名称：NetCore_BroadCast_RecvInit
函数功能：初始化接受数据
 参数.一：phSocket
  In/Out：Out
  类型：套接字指针
  可空：N
  意思：导出的套接字
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要接受数据的端口
返回值
  类型：逻辑型
  意思：是否成功初始化
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_RecvInit(SOCKET *phSocket,int nPort);
/********************************************************************
函数名称：NetCore_BroadCast_Recv
函数功能：接受广播数据
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：要接受哪个套接字的数据
 参数.二：ptszBuffer
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：输入足够大的缓冲区，输出接受到的数据
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入要接受的缓冲区大小，输出接受到的缓冲区大小
 参数.四：ptszAddr
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：输出这条数据发送者的IP地址信息
返回值
  类型：逻辑型
  意思：是否成功接受到数据
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_Recv(SOCKET hSocket,TCHAR *ptszBuffer,int *pInt_Len,TCHAR *ptszAddr = NULL);
/********************************************************************
函数名称：NetCore_BroadCast_Close
函数功能：关闭一个指定的广播服务
 参数.一：hSocket
  In/Out：In
  类型：网络套接字
  可空：N
  意思：要关闭的套接字句柄
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_BroadCast_Close(SOCKET hSocket);
/*                      心跳管理功能导出函数                               */
/********************************************************************
函数名称：SocketOpt_HeartBeat_Init
函数功能：初始化心跳服务
 参数.一：nTimeOut
  In/Out：In
  类型：套接字指针
  可空：Y
  意思：每隔多少秒检测一次心跳
 参数.二：nTimeNumber
  In/Out：In
  类型：整数型
  可空：Y
  意思：超过多少次没有心跳认为断线
 参数.三：fpCall_HeartBeatEvent
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：心跳超时后的回调函数
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数的参数
 参数.五：bIsAddr
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否启用客户端地址模式,只允许同时使用一个模式
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：如果回调函数没有设置，你需要通过获取超时函数来得到超时的用户
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_InitEx(XNETHANDLE *pxhNet, int nTimeOut = 5, int nTimeNumber = 3, CALLBACK_NETCORE_SOCKOPT_HEARTBEAT_EVENT fpCall_HeartBeatEvent = NULL, LPVOID lParam = NULL, BOOL bIsAddr = TRUE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_Destory
函数功能：销毁心跳管理
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_DestoryEx(XNETHANDLE xhNet);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ForceOutAddr
函数功能：强制让一个客户端心跳超时
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要超时的客户端地址ID
 参数.二：nStatus
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：强制退出用户信息状态
 参数.三：bIgnore
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否忽略这个地址是否在心跳管理器中，默认不忽略
返回值
  类型：逻辑型
  意思：是否成功
备注：在某些极端情况下，开发人员可能需要这样的功能
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ForceOutAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, int nStatus = 0, BOOL bIgnore = FALSE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ForceOutSkt
函数功能：强制让一个客户端心跳超时
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要超时的客户端句柄
 参数.二：nStatus
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：强制退出用户信息状态
 参数.三：bIgnore
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否忽略这个地址是否在心跳管理器中，默认不忽略
返回值
  类型：逻辑型
  意思：是否成功
备注：在某些极端情况下，开发人员可能需要这样的功能
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ForceOutSktEx(XNETHANDLE xhNet, SOCKET hSocket, int nStatus = 0, BOOL bIgnore = FALSE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_InsertAddr
函数功能：插入一个客户端到心跳管理器
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要激活的客户端，唯一标识符，可以是任意字符串
返回值
  类型：逻辑型
  意思：是否激活成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_InsertAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：SocketOpt_HeartBeat_InsertSocket
函数功能：插入一个客户端到心跳管理器
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要激活的客户端，唯一标识符
返回值
  类型：逻辑型
  意思：是否激活成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_InsertSocketEx(XNETHANDLE xhNet, SOCKET hSocket);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ActiveAddr
函数功能：激活一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要激活的客户端地址，唯一标识符，可以是任意字符串
 参数.二：pSt_ProtocolHeartBeat
  In/Out：In
  类型：数据结构指针
  可空：Y
  意思：心跳协议,可以为NULL
返回值
  类型：逻辑型
  意思：是否激活成功
备注：每收到一次心跳消息，都需要调用此函数进行激活，超过设定时间将被认为断线
      如果客户端不存在，此函数会调用SocketOpt_HeartBeat_Insert插入一个新的客户端
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ActiveAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, NETENGINE_PROTOCOLHEARTBEAT *pSt_ProtocolHeartBeat = NULL);
/********************************************************************
函数名称：SocketOpt_HeartBeat_ActiveSocket
函数功能：激活一个客户端
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要激活的客户端，唯一标识符
 参数.二：pSt_ProtocolHeartBeat
  In/Out：In
  类型：数据结构指针
  可空：Y
  意思：心跳协议
返回值
  类型：逻辑型
  意思：是否激活成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_ActiveSocketEx(XNETHANDLE xhNet, SOCKET hSocket, NETENGINE_PROTOCOLHEARTBEAT *pSt_ProtocolHeartBeat = NULL);
/********************************************************************
函数名称：SocketOpt_HeartBeat_DeleteAddr
函数功能：从心跳管理中删除一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的唯一标识符
返回值
  类型：逻辑型
  意思：是否删除成功
备注：心跳被动离开不需要调用此函数，内部会自动删除。只有你主动删除才需要调用
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_DeleteAddrEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：SocketOpt_HeartBeat_DeleteSocket
函数功能：从心跳管理中删除一个客户端
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：要删除的唯一标识符
返回值
  类型：逻辑型
  意思：是否删除成功
备注：心跳被动离开不需要调用此函数，内部会自动删除。只有你主动删除才需要调用
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_DeleteSocketEx(XNETHANDLE xhNet, SOCKET hSocket);
/********************************************************************
函数名称：SocketOpt_HeartBeat_GetTimeOut
函数功能：从心跳管理中获取一个超时的客户端标识符
 参数.一：ptszClientAddr
  In/Out：Out
  类型：字符指针
  可空：Y
  意思：获取到的唯一标识符
 参数.二：phSocket
  In/Out：Out
  类型：套接字句柄
  可空：Y
  意思：获取到的唯一标识符
返回值
  类型：逻辑型
  意思：是否获取成功
备注：如果设置了回调函数，这个函数将不起作用，此方式你需要手动调用函数删除超时客户端
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_GetTimeOutEx(XNETHANDLE xhNet, TCHAR *ptszClientAddr = NULL, SOCKET *phSocket = NULL);
/********************************************************************
函数名称：SocketOpt_HeartBeat_SetLoadAttr
函数功能：设置负载属性
 参数.一：nTimeCal
  In/Out：In
  类型：整数型
  可空：N
  意思：要设置每次计算负载属性的时间间隔（秒）
 参数.二：bOPen
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：是否开启还是关闭
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_SetLoadAttrEx(XNETHANDLE xhNet,int nTimeCal,BOOL bOPen = FALSE);
/********************************************************************
函数名称：SocketOpt_HeartBeat_GetLoadAttr
函数功能：获取负载属性
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：可以为NULL，表示通过参数二套接字获取客户端负载属性
 参数.二：hSocket
  In/Out：In
  类型：套接字句柄
  可空：Y
  意思：可以为0,表示通过参数一客户端地址获取负载属性
 参数.三：pSt_HBLoad
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：导出客户端负载属性
 参数.四：pSt_LoadRate
  In/Out：Out
  类型：数据结构指针
  可空：Y
  意思：导出客户端负载率
返回值
  类型：逻辑型
  意思：是否成功
备注：参数1和参数2不能同时为NULL
*********************************************************************/
extern "C" BOOL SocketOpt_HeartBeat_GetLoadAttrEx(XNETHANDLE xhNet,LPCTSTR lpszClientAddr,SOCKET hSocket,NETCORE_SOCKOPT_HBLOAD *pSt_HBLoad,NETCORE_SOCKOPT_LOADRATE *pSt_LoadRate = NULL);
/************************************************************************/
/*                      UNIX域协议服务器创建                               */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UnixDomain_Start
函数功能：开始一个UNIX域服务器
 参数.一：pxhNet
  In/Out：Out
  类型：网络句柄
  可空：N
  意思：导出创建成功的服务器句柄
 参数.二：lpszUnixName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要创建的UNIX域名称路径，尽可能使用绝对路径
 参数.三：fpCall_UnixDomainLogin
  In/Out：In
  类型：回调函数
  可空：N
  意思：回调客户登录消息
 参数.四：dwProtocol
  In/Out：In
  类型：双字
  可空：Y
  意思：要创建的服务所属协议，默认IPV4TCP
 参数.五：lParam
  In/Out：In
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否创建成功
备注：在创建完成服务器，你的读写操作要使用非数据传递方式，你必须让客户端优先发送一段随机数据到服务器，让服务器保存这个客户
*********************************************************************/
extern "C" BOOL NetCore_UnixDomain_Start(PXNETHANDLE pxhNet,LPCTSTR lpszUnixName,CALLBACK_NETCORE_UNIXDOMAIN_LOGIN fpCall_UnixDomainLogin,DWORD dwProtocol = NETENGINE_NETCORE_PROTOCOL_TCP_VERSION4,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_UnixDomain_Recv
函数功能：接受消息
 参数.一：ptszClientAddr
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：要接受的客户端地址，如果是UDP，而且这个客户不存在，那么会加入我们的服务器，那么这个参数无效，会被显示导出客户端内容，并且回调函数会被触发
 参数.二：ptszRecvMsg
  In/Out：In/Out
  类型：字符指针
  可空：N
  意思：输入：接受数据的缓冲区，输出：接受到的内容
 参数.三：pInt_MsgSize
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入：缓冲区的大小，输出：接受到的缓冲区大小
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：N
  意思：根据你的第五个参数来判断此参数的作用，如果是仅仅进行消息传递，那么此参数为NULL，如果要接受数据凭证，需要强制转换，或者传递描述符 都需要强制转换并且导出
 参数.五：en_MsgType
  In/Out：In
  类型：枚举型
  可空：Y
  意思：要操作的类型，接受，默认为仅仅进行进程消息传递
返回值
  类型：逻辑型
  意思：是否接受成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UnixDomain_Recv(TCHAR *ptszClientAddr,TCHAR *ptszRecvMsg,int *pInt_MsgSize,LPVOID lParam,ENUM_NETCORE_UNIXDOMAIN_MSGTYPE en_MsgType = NETCORE_DOMAIN_MSGTYPE_DATA);
/********************************************************************
函数名称：NetCore_UnixDomain_Send
函数功能：发送数据
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的客户端地址，函数内部会自动选择所属服务器
 参数.二：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：发送的数据缓冲区
 参数.三：pInt_MsgSize
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入：缓冲区的大小，输出：实际发送的大小
 参数.四：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：N
  意思：根据你的第五个参数来判断此参数的作用，如果是仅仅进行消息传递，那么此参数为NULL，如果要接受数据凭证，需要强制转换，或者传递描述符 都需要强制转换并且导出，凭证传递此参数可以为NULL，那么将传递此进行的凭证信息
 参数.五：en_MsgType
  In/Out：In
  类型：枚举型
  可空：Y
  意思：要操作的类型，接受，默认为仅仅进行进程消息传递
返回值
  类型：逻辑型
  意思：是否发送成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UnixDomain_Send(LPCTSTR lpszClientAddr,LPCTSTR lpszSendMsg,int *pInt_MsgSize,LPVOID lParam,ENUM_NETCORE_UNIXDOMAIN_MSGTYPE en_MsgType = NETCORE_DOMAIN_MSGTYPE_DATA);
/********************************************************************
函数名称：NetCore_UnixDomain_RemoteClinet
函数功能：移除一个客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要移除的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：你不想让一个客户端连接到服务器或者一个客户端离开服务器，都需要调用此函数
*********************************************************************/
extern "C" BOOL NetCore_UnixDomain_RemoteClinet(LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：NetCore_UnixDOmain_Shutdown
函数功能：移除一个服务器
 参数.一：xhServer
  In/Out：In
  类型：网络服务句柄
  可空：N
  意思：要移除的服务器
返回值
  类型：逻辑型
  意思：是否成功
备注：这个函数将会关闭与之关联的客户端并且清楚自身服务和资源
*********************************************************************/
extern "C" BOOL NetCore_UnixDOmain_Shutdown(XNETHANDLE xhServer);
/************************************************************************/
/*                  高速缓存导出函数                                       */
/************************************************************************/
/********************************************************************
函数名称：NetCore_FileCacheEx_Create
函数功能：创建文件缓存
 参数.一：lpszFileName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：文件缓存路径
 参数.二：nSize
  In/Out：In
  类型：整数型
  可空：N
  意思：文件缓存大小
 参数.三：nFlushTime
  In/Out：In
  类型：整数型
  可空：Y
  意思：自动刷新时间，刷新到文件中去，如果为0,默认，将不启用
 参数.四：fpCall_CacheFileTimer
  In/Out：In/Out
  类型：回调函数
  可空：Y
  意思：如果刷新时间不启用，此参数没有作用，如果启用了刷新时间，那么此参数必须设置
 参数.五：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数自定义参数
返回值
  类型：逻辑型
  意思：是否创建成功
备注：我们提供了强制刷新的功能，一般情况下你不需要调用下面的强制刷新，我们内部已经给你做好了你只需要调用 创建 读 写 关闭即可
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Create(PXNETHANDLE pxhCache,LPCTSTR lpszFileName,int nSize,int nFlushTime = 0,CALLBACK_NETCORE_CACHEFILE_FLUSHEVENT fpCall_CacheFileTimer = NULL,LPVOID lParam = NULL);
/********************************************************************
函数名称：NetCore_FileCacheEx_Write
函数功能：写入文件缓存
 参数.一：lpszWriteBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要写的内容
 参数.二：nWriteLen
  In/Out：In
  类型：整数型
  可空：N
  意思：写入数据大小
返回值
  类型：逻辑型
  意思：是否写入成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Write(XNETHANDLE xhCache,LPCTSTR lpszWriteBuffer,int nWriteLen);
/********************************************************************
函数名称：NetCore_FileCacheEx_Read
函数功能：读取文件缓存
 参数.一：ptszBuffer
  In/Out：In
  类型：字符指针
  可空：N
  意思：读到的内存缓冲区
 参数.二：pInt_Len
  In/Out：In
  类型：整数型指针
  可空：N
  意思：输入读的内存缓冲区大小，输出读到的内存缓冲区大小
返回值
  类型：逻辑型
  意思：是否读取成功
备注：直接从内存读的，所以速度非常快！直接读取完毕一次性
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Read(XNETHANDLE xhCache,TCHAR *ptszBuffer,int *pInt_Len);
/********************************************************************
函数名称：NetCore_FileCacheEx_Flush
函数功能：主动刷新内存到文件
返回值
  类型：逻辑型
  意思：是否刷新成功
备注：主动强制更新才需要调用 同步刷新。
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Flush(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_FileCacheEx_ReSize
函数功能：重置大小，每次刷新后清理后，需要调用这个函数还原到以前的大小
返回值
  类型：逻辑型
  意思：还原成功
备注：主动强制更新才需要调用
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_ReSize(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_FileCacheEx_Clear
函数功能：清理高速缓存中的数据
返回值
  类型：逻辑型
  意思：是否清理成功
备注：主动强制更新才需要调用
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Clear(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_FileCacheEx_Close
函数功能：把内存中的数据写到文件并且关闭文件
返回值
  类型：逻辑型
  意思：是否关闭成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_FileCacheEx_Close(XNETHANDLE xhCache);
/************************************************************************/
/*                  数据缓存导出函数                                      */
/************************************************************************/
//拥有扩展函数的第一个参数都是句柄
/********************************************************************
函数名称：NetCore_DataCache_Init
函数功能：初始化数据回溯缓冲池
 参数.一：nBufferCount
  In/Out：In
  类型：整数型
  可空：N
  意思：支持可以回溯的队列个数
 参数.二：bClear
  In/Out：In
  类型：逻辑型
  可空：Y
  意思：如果满了后，是否清理最开始的包，默认清理，否则将无法继续写入回溯
 参数.三：nMemSize
  In/Out：In
  类型：整数型
  可空：Y
  意思：要创建的回溯队列中每个缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：使用了内存预加载技术。
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_Init(XNETHANDLE *pxhCache,int nBufferCount,BOOL bClear = TRUE,int nMemSize = 1024000,BOOL bFile = FALSE);
/********************************************************************
函数名称：NetCore_DataCache_Destory
函数功能：销毁回溯缓冲池
返回值
  类型：逻辑型
  意思：是否销毁成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_Destory(XNETHANDLE xhCache);
/********************************************************************
函数名称：NetCore_DataCache_PushSend
函数功能：压入一个发送的数据到回溯队列中
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的唯一标识符
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的缓冲区（内部有自己的内存空间）
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要压入的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：压入的数据后，这个回溯系统会自动管理内存和维护队列
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_PushSend(XNETHANDLE xhCache,LPCTSTR lpszClientId,LPCTSTR lpszBuffer, int nLen);
/********************************************************************
函数名称：NetCore_DataCache_PushRecv
函数功能：压入一个接受的数据到回溯队列中
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的唯一标识符
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要压入的数据的缓冲区（内部有自己的内存空间）
 参数.三：nLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要压入的缓冲区大小
返回值
  类型：逻辑型
  意思：是否成功
备注：压入的数据后，这个回溯系统会自动管理内存和维护队列
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_PushRecv(XNETHANDLE xhCache,LPCTSTR lpszClientId,LPCTSTR lpszBuffer, int nLen);
/********************************************************************
函数名称：NetCore_DataCache_GetSend
函数功能：从一个发送回溯缓冲队列中获得一个指定的数据包
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取数据的队列唯一标识符
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出获得的缓冲区大小
 参数.四：nPktSerial
  In/Out：In
  类型：整数型
  可空：Y
  意思：要获得可用队列中的第几个回溯包，默认最近的第一个
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_GetSend(XNETHANDLE xhCache,LPCTSTR lpszClientId,TCHAR *ptszBuffer,int *pInt_Len,int nPktSerial = 0);
/********************************************************************
函数名称：NetCore_DataCache_GetRecv
函数功能：从一个接受回溯缓冲队列中获得一个指定的数据包
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要获取数据的队列唯一标识符
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出获取到的数据缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出获得的缓冲区大小
 参数.四：nPktSerial
  In/Out：In
  类型：整数型
  可空：Y
  意思：要获得可用队列中的第几个回溯包，默认最近的第一个
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_GetRecv(XNETHANDLE xhCache,LPCTSTR lpszClientId,TCHAR *ptszBuffer,int *pInt_Len,int nPktSerial = 0);
/********************************************************************
函数名称：NetCore_DataCache_Close
函数功能：关闭一个指定的回溯队列并且清理相关资源
 参数.一：lpszClientId
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要删除的回溯队列唯一标识符
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_DataCacheEx_Close(XNETHANDLE xhCache,LPCTSTR lpszClientId);
/************************************************************************/
/*                  无线通信导出函数                                       */
/************************************************************************/
//蓝牙通信导出函数
extern "C" BOOL NetCore_Bluetooth_Scan(NETCORE_WIRELESSBLUETOOTH *pSt_BTWireless,int *pInt_BTCount = NULL);
/********************************************************************
函数名称：NetCore_Bluetooth_CMDConnect
函数功能：连接到一个蓝牙设备，命令通信模式
 参数.一：pxhNet
  In/Out：Out
  类型：网络句柄指针
  可空：N
  意思：导出连接成功的网络句柄
 参数.二：lpszBTRemoteAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要连接到的远端蓝牙地址
返回值
  类型：逻辑型
  意思：是否连接成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_CMDConnect(XNETHANDLE *pxhNet,LPCTSTR lpszBTRemoteAddr);
/********************************************************************
函数名称：NetCore_Bluetooth_CMDSend
函数功能：发送一条命令到蓝牙设备
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：已经连接上的蓝牙设备网络句柄
 参数.二：lpszSendMsg
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送数据的缓冲区
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：要发送数据的长度
 参数.四：nMsgId
  In/Out：In
  类型：整数型
  可空：N
  意思：发送消息的ID，大于1开始，每次+1
 参数.五：nMsgCode
  In/Out：In
  类型：整数型
  可空：N
  意思：消息的类型，参考导出的蓝牙消息类型定义
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_CMDSend(XNETHANDLE xhNet,LPCTSTR lpszSendMsg,int nMsgLen,int nMsgId,int nMsgCode);
/********************************************************************
函数名称：NetCore_Bluetooth_CMDRecv
函数功能：从蓝牙设备接受一条消息命令
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：已经连接上的蓝牙设备网络句柄
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：接受到数据的缓冲区
 参数.三：pInt_Len
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入提供的缓冲区大小，输出获取到的缓冲区大小
 参数.四：pInt_MsgId
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：接受到的消息的ID，与发送对应
 参数.五：pInt_MsgCode
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：接受到的消息的类型
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_CMDRecv(XNETHANDLE xhNet,TCHAR *ptszMsgBuffer,int *pInt_Len,int *pInt_MsgId,int *pInt_MsgCode);
/********************************************************************
函数名称：NetCore_Bluetooth_CMDClose
函数功能：关闭一个连接上的蓝牙设备句柄
 参数.一：xhNet
  In/Out：In
  类型：网络句柄
  可空：N
  意思：已经连接上的蓝牙设备网络句柄
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_CMDClose(XNETHANDLE xhNet);
/********************************************************************
函数名称：NetCore_Bluetooth_Start
函数功能：启动一个蓝牙数据通信服务
 参数.一：fpCall_BTEvent
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：事件回调接受函数指针
 参数.二：lParam
  In/Out：In/Out
  类型：无类型指针
  可空：Y
  意思：回调函数的参数
 参数.三：nProtoPsm
  In/Out：In
  类型：整数型
  可空：Y
  意思：蓝牙通信层自定义协议前缀，必须大于0x1000
返回值
  类型：逻辑型
  意思：是否成功
备注：导出的xhSocket句柄你可以通过 send和recv函数发送或者接受数据
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_Start(CALLBACK_NETCORE_WIRELESS_BLUETOOTH_NETEVENT fpCall_BTEvent,LPVOID lParam = NULL,int nProtoPsm = 0x1001);
/********************************************************************
函数名称：NetCore_Bluetooth_Connect
函数功能：蓝牙客户端连接到一个蓝牙服务器
 参数.一：pxhSocket
  In/Out：Out
  类型：网络句柄指针
  可空：N
  意思：导出可操作连接好的句柄
 参数.二：lpszAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要连接到的蓝牙服务地址
 参数.三：nProtoPsm
  In/Out：In
  类型：整数型
  可空：N
  意思：蓝牙通信层自定义协议前缀，与服务器绑定的一致
返回值
  类型：逻辑型
  意思：是否成功
备注：导出的pxhSocket句柄你可以通过 send和recv函数发送或者接受数据
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_Connect(SOCKET *pxhSocket,LPCTSTR lpszAddr,int nProtoPsm);
/********************************************************************
函数名称：NetCore_Bluetooth_Close
函数功能：关闭一个蓝牙资源
 参数.一：xhSocket
  In/Out：In
  类型：网络句柄
  可空：Y
  意思：输入要删除的客户端蓝牙句柄，可以为0，跳过。
 参数.二：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：输入服务器连接进来的客户端地址，可以为NULL，跳过
返回值
  类型：逻辑型
  意思：是否成功
备注：这个函数可以删除客户端或者服务器关联的客户端的资源。
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_Close(SOCKET xhSocket = 0,LPCTSTR lpszClientAddr = NULL);
/********************************************************************
函数名称：NetCore_Bluetooth_Stop
函数功能：停止蓝牙服务器
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Bluetooth_Stop();
//红外通信导出函数
/********************************************************************
函数名称：NetCore_Infrared_Init
函数功能：初始化红外设备
 参数.一：lpszToken
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：初始化后要操作的TOKEN
 参数.二：lpszConfig
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：红外配置文件，如果没有，那么为NULL，采用默认
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Infrared_Init(LPCTSTR lpszToken,LPCTSTR lpszConfig = NULL);
/********************************************************************
函数名称：NetCore_Infrared_GetNextCode
函数功能：获取一个接收到的红外识别码
 参数.一：lpszToken
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要操作红外设备的TOKEN
返回值
  类型：逻辑型
  意思：是否成功
备注：首先调用此获取CODE，在获取STRING
*********************************************************************/
extern "C" BOOL NetCore_Infrared_GetNextCode(LPCTSTR lpszToken);
/********************************************************************
函数名称：NetCore_Infrared_GetNextMsg
函数功能：获取一个红外接收到的数据
 参数.一：lpszToken
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要操作红外设备的TOKEN
 参数.二：ptszString
  In/Out：Out
  类型：字符指针
  可空：N
  意思：导出接收到的红外数据
返回值
  类型：逻辑型
  意思：是否成功
备注：直到返回错误，才继续调用NetCore_Infrared_GetNextCode
*********************************************************************/
extern "C" BOOL NetCore_Infrared_GetNextMsg(LPCTSTR lpszToken,TCHAR *ptszString);
/********************************************************************
函数名称：NetCore_Infrared_Send
函数功能：使用红外发送数据给一个远程设备
 参数.一：lpszRemoteName
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送给哪个设备的名称
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据消息
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Infrared_Send(LPCTSTR lpszRemoteName,LPCTSTR lpszMsgBuffer);
/********************************************************************
函数名称：NetCore_Infrared_Destory
函数功能：释放红外设备资源
 参数.一：lpszToken
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要操作红外设备的TOKEN
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_Infrared_Destory(LPCTSTR lpszToken);
/************************************************************************/
/*                  UDX导出函数                                         */
/************************************************************************/
/********************************************************************
函数名称：NetCore_UDXSocket_Init
函数功能：初始化UDX
 参数.一：pSt_UDXConfig
  In/Out：In
  类型：数据结构指针
  可空：N
  意思：设置UDX的服务器选项
 参数.二：nPort
  In/Out：In
  类型：整数型
  可空：N
  意思：要绑定的端口号
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_InitEx(XNETHANDLE *pxhNet, NETCORE_UDXCONFIG *pSt_UDXConfig, int nPort);
/********************************************************************
函数名称：NetCore_UDXSocket_CBSet
函数功能：设置回调函数
 参数.一：fpCall_UDXLogin
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：用户连接处理回调函数
 参数.二：fpCall_UDXLeave
  In/Out：In/Out
  类型：回调函数
  可空：N
  意思：用户离开回调处理函数
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_CBSetEx(XNETHANDLE xhNet, CALLBACK_NETCORE_UDX_LOGIN fpCall_UDXLogin, CALLBACK_NETCORE_UDX_LEAVE fpCall_UDXLeave, LPVOID lPLogin = NULL, LPVOID lPLeave = NULL);
/********************************************************************
函数名称：NetCore_UDXSocket_Close
函数功能：关闭一个指定客户端
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要操作的客户端
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_CloseEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr);
/********************************************************************
函数名称：NetCore_UDXSocket_Send
函数功能：发送数据函数
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要操作的客户端
 参数.二：lpszMsgBuffer
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：要发送的数据
 参数.三：nMsgLen
  In/Out：In
  类型：整数型
  可空：N
  意思：输入要发送的数据大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_SendEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, LPCTSTR lpszMsgBuffer, int nMsgLen);
/********************************************************************
函数名称：NetCore_UDXSocket_Recv
函数功能：获取数据
 参数.一：lpszClientAddr
  In/Out：In
  类型：常量字符指针
  可空：N
  意思：输入要操作的客户端
 参数.二：ptszMsgBuffer
  In/Out：Out
  类型：字符指针
  可空：N
  意思：输出获取到的数据
 参数.二：pInt_MsgLen
  In/Out：Out
  类型：整数型指针
  可空：N
  意思：输出数据大小
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_RecvEx(XNETHANDLE xhNet, LPCTSTR lpszClientAddr, TCHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：NetCore_UDXSocket_Destroy
函数功能：销毁UDX资源
返回值
  类型：逻辑型
  意思：是否成功
备注：
*********************************************************************/
extern "C" BOOL NetCore_UDXSocket_DestroyEx(XNETHANDLE xhNet);
