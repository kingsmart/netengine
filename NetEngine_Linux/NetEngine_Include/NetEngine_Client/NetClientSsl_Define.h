#pragma once
/********************************************************************
//	Created:	2011/11/07  10:20
//	Filename:       ./NetSocketEngine/NetSocketEngine/NetClientEngine/NetClient_XSsl/NetClient_XSsl_Define.h
//	File Path:	./NetSocketEngine/NetSocketEngine/NetClientEngine/NetClient_XSsl/
//	File Base:	NetClient_XSsl
//	File Ext:	h
//      Project:        NetSocketEnginer(网络通信引擎 For Linux)
//	Author:		Dowflyon
//	Purpose:	客户端安全管理模块，导出定义
//	History:
*********************************************************************/
//////////////////////////////////////////////////////////////////////////
//               数据类型导出定义
//////////////////////////////////////////////////////////////////////////
//SSL证书文件证书编码类型。调用者无需关心编码实现部分，只需要传递即可
#define NETCLIENT_OPENSSL_CODEC_PEM_FILE 1                                //PEM编码格式：BASE64
#define NETCLIENT_OPENSSL_CODEC_ASN1_FILE 2                               //ASN1编码格式：DER
//////////////////////////////////////////////////////////////////////////
//               数据结构导出定义
//////////////////////////////////////////////////////////////////////////
//SSL安全传输协议库,用于客户端
typedef enum en_NetClient_OpenSsl_Version
{
    NETCLIENT_ENUM_OPENSSL_SSL_VERSION,                                   //同时支持SSL V2和V3版本的协议
    NETCLIENT_ENUM_OPENSSL_TLS_VERSION,                                   //TLS协议
    NETCLIENT_ENUM_OPENSSL_DTL_VERSION                                    //DTL协议
}ENUM_NETCLIENT_OPENSSL_TYPE,*LPENUM_NETCLIENT_OPENSSL_TYPE;
//服务器SSL信息
typedef struct tag_NetClient_OpenSsl_SrvInfo
{
    CHAR tszAlgorithm[64];                                                //服务端使用的加密算法
    CHAR tszSubject[MAX_PATH];                                            //服务端的证书拥有者信息
    CHAR tszIssuer[MAX_PATH];                                             //服务端的证书颁发者信息
}NETCLIENT_OPENSSL_SRVINFO,*LPNETCLIENT_OPENSSL_SRVINFO;
//////////////////////////////////////////////////////////////////////////
//                    函数导出定义
//////////////////////////////////////////////////////////////////////////
extern "C" DWORD NetClientSsl_GetLastError(int *pInt_ErrorCode = NULL);
/************************************************************************/
/*                 SSL安全通信函数导出定义                              */
/************************************************************************/
/********************************************************************
函数名称：NetClient_OPenSsl_Init
函数功能：初始化SSL连接属性
 参数.一：enSslProtocol
  In/Out：In
  类型：枚举型
  可空：Y
  意思：客户端要采用的密钥算法，默认为SSLV2和V3兼容版本
 参数.二：lpszCACertFile
  In/Out：In
  类型：整数型
  可空：Y
  意思：CA证书文件路径
 参数.三：lpszCertFile
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：客户端证书文件路径
 参数.四：lpszPrivateKey
  In/Out：In
  类型：常量字符指针
  可空：Y
  意思：客户端秘钥文件路径
返回值
  类型：逻辑型
  意思：是否初始化成功
备注：证书文件三个参数要么全部为NULL,要么全部不为NULL,证书文件全为空也可以
      与我们的SSL服务器交换数据
*********************************************************************/
extern "C" BOOL NetClient_OPenSsl_InitEx(XNETHANDLE *pxhNet, ENUM_NETCLIENT_OPENSSL_TYPE enSslProtocol = NETCLIENT_ENUM_OPENSSL_SSL_VERSION, LPCSTR lpszCACertFile = NULL, LPCSTR lpszCertFile = NULL, LPCSTR lpszPrivateKey = NULL);
/********************************************************************
函数名称：NetClient_OPenSsl_Connect
函数功能：连接到SSL服务
 参数.一：hSocket
  In/Out：In
  类型：套接字句柄
  可空：N
  意思：输入你已经创建好连接的套接字句柄
 参数.二：pSt_SslInfo
  In/Out：Out
  类型：数据结构指针
  可空：N
  意思：输出获取到的服务器SSL证书信息
返回值
  类型：逻辑型
  意思：是否成功
备注：网络事件你需要自己处理
*********************************************************************/
extern "C" BOOL NetClient_OPenSsl_ConnectEx(XNETHANDLE xhNet, SOCKET hSocket, NETCLIENT_OPENSSL_SRVINFO *pSt_SslInfo);
/************************************************************************
函数名称：NetClient_OPenSsl_Close
函数功能：关闭SSL客户端
返回值
  类型：逻辑型
  意思：是否成功关闭客户端
备注：
************************************************************************/
extern "C" BOOL NetClient_OPenSsl_CloseEx(XNETHANDLE xhNet);
/************************************************************************
函数名称：NetClient_OPenSsl_SendMsg
函数功能：SSL安全发送数据
  参数一：lpszMsgBuffer
   In/Out：In
   类型：字符指针
   可空：N
   意思：要发送的缓冲区数据
  参数二：pInt_Len
   In/Out：In/Out
   类型：整数型指针
   可空：N
   意思：输入发送的长度,输出发送成功的长度
返回值
  类型：逻辑型
  意思：是否成功发送
备注：你可以自己处理你的SOCKET句柄事件,当有事件可发(可接受)的时候,在调用此函数发送数据
      所以,你不能调用你的send函数来发送数据,只能通过此函数来发送,同样的,接受数据也是一样的
************************************************************************/
extern "C" BOOL NetClient_OPenSsl_RecvMsgEx(XNETHANDLE xhNet, CHAR *ptszMsgBuffer, int *pInt_MsgLen);
/********************************************************************
函数名称：NetClient_OPenSsl_RecvMsg
函数功能：读取SSL数据
 参数.一：ptszMsgBuffer
  In/Out：Out
  类型：常量字符指针
  可空：N
  意思：导出接受到的数据
 参数.二：pInt_MsgLen
  In/Out：In/Out
  类型：整数型指针
  可空：N
  意思：输入缓冲区大小,输出接受到的大小
返回值
  类型：逻辑型
  意思：是否成功
备注：读取出来的数据是解密后的明文数据
*********************************************************************/
extern "C" BOOL NetClient_OPenSsl_SendMsgEx(XNETHANDLE xhNet, LPCSTR lpszMsgBuffer, int nLen);
